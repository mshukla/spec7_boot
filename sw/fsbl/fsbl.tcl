
#set the path of workspace
setws build

platform create -name spec7 \
-hw ../../output/spec7_wrapper.xsa -no-boot-bsp
platform active spec7

#Create FSBL application
domain create -name "fsbl_domain" -os standalone -proc ps7_cortexa9_0
bsp setlib xilffs
bsp config stdin ps7_uart_1
bsp config stdout ps7_uart_1

platform generate

app create -name zynq_fsbl -template {Zynq FSBL} \
-platform spec7 -domain fsbl_domain -sysproj spec7_system

app config -name zynq_fsbl build-config
app build -name zynq_fsbl
