-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Sat Oct  3 15:01:18 2020
-- Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_synchronizer_simple_0/spec7_synchronizer_simple_0_stub.vhdl
-- Design      : spec7_synchronizer_simple_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z030fbg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity spec7_synchronizer_simple_0 is
  Port ( 
    data_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    new_clk : in STD_LOGIC;
    data_out : out STD_LOGIC_VECTOR ( 0 to 0 )
  );

end spec7_synchronizer_simple_0;

architecture stub of spec7_synchronizer_simple_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "data_in[0:0],new_clk,data_out[0:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "synchronizer_simple,Vivado 2019.2";
begin
end;
