-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Sat Oct  3 15:01:18 2020
-- Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
-- Command     : write_vhdl -force -mode funcsim
--               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_synchronizer_simple_0/spec7_synchronizer_simple_0_sim_netlist.vhdl
-- Design      : spec7_synchronizer_simple_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z030fbg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_synchronizer_simple_0_synchronizer_simple is
  port (
    data_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    new_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_synchronizer_simple_0_synchronizer_simple : entity is "synchronizer_simple";
end spec7_synchronizer_simple_0_synchronizer_simple;

architecture STRUCTURE of spec7_synchronizer_simple_0_synchronizer_simple is
  signal sync_1reg : STD_LOGIC;
begin
\sync_1reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => new_clk,
      CE => '1',
      D => data_in(0),
      Q => sync_1reg,
      R => '0'
    );
\sync_2reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => new_clk,
      CE => '1',
      D => sync_1reg,
      Q => data_out(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_synchronizer_simple_0 is
  port (
    data_in : in STD_LOGIC_VECTOR ( 0 to 0 );
    new_clk : in STD_LOGIC;
    data_out : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of spec7_synchronizer_simple_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of spec7_synchronizer_simple_0 : entity is "spec7_synchronizer_simple_0,synchronizer_simple,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of spec7_synchronizer_simple_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of spec7_synchronizer_simple_0 : entity is "synchronizer_simple,Vivado 2019.2";
end spec7_synchronizer_simple_0;

architecture STRUCTURE of spec7_synchronizer_simple_0 is
begin
inst: entity work.spec7_synchronizer_simple_0_synchronizer_simple
     port map (
      data_in(0) => data_in(0),
      data_out(0) => data_out(0),
      new_clk => new_clk
    );
end STRUCTURE;
