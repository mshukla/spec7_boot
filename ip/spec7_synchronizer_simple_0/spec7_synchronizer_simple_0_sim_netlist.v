// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Sat Oct  3 15:01:18 2020
// Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
// Command     : write_verilog -force -mode funcsim
//               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_synchronizer_simple_0/spec7_synchronizer_simple_0_sim_netlist.v
// Design      : spec7_synchronizer_simple_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z030fbg676-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "spec7_synchronizer_simple_0,synchronizer_simple,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "synchronizer_simple,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module spec7_synchronizer_simple_0
   (data_in,
    new_clk,
    data_out);
  input [0:0]data_in;
  input new_clk;
  output [0:0]data_out;

  wire [0:0]data_in;
  wire [0:0]data_out;
  wire new_clk;

  spec7_synchronizer_simple_0_synchronizer_simple inst
       (.data_in(data_in),
        .data_out(data_out),
        .new_clk(new_clk));
endmodule

(* ORIG_REF_NAME = "synchronizer_simple" *) 
module spec7_synchronizer_simple_0_synchronizer_simple
   (data_out,
    data_in,
    new_clk);
  output [0:0]data_out;
  input [0:0]data_in;
  input new_clk;

  wire [0:0]data_in;
  wire [0:0]data_out;
  wire new_clk;
  wire sync_1reg;

  FDRE \sync_1reg_reg[0] 
       (.C(new_clk),
        .CE(1'b1),
        .D(data_in),
        .Q(sync_1reg),
        .R(1'b0));
  FDRE \sync_2reg_reg[0] 
       (.C(new_clk),
        .CE(1'b1),
        .D(sync_1reg),
        .Q(data_out),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
