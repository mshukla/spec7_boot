// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Sat Oct  3 15:01:17 2020
// Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_synchronizer_simple_0/spec7_synchronizer_simple_0_stub.v
// Design      : spec7_synchronizer_simple_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z030fbg676-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "synchronizer_simple,Vivado 2019.2" *)
module spec7_synchronizer_simple_0(data_in, new_clk, data_out)
/* synthesis syn_black_box black_box_pad_pin="data_in[0:0],new_clk,data_out[0:0]" */;
  input [0:0]data_in;
  input new_clk;
  output [0:0]data_out;
endmodule
