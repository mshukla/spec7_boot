-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Sat Oct  3 15:01:19 2020
-- Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_pcie_app_7x_0/spec7_pcie_app_7x_0_stub.vhdl
-- Design      : spec7_pcie_app_7x_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z030fbg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity spec7_pcie_app_7x_0 is
  Port ( 
    user_clk : in STD_LOGIC;
    user_reset : in STD_LOGIC;
    user_lnk_up : in STD_LOGIC;
    s_axis_tx_tready : in STD_LOGIC;
    s_axis_tx_tdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_tx_tkeep : out STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_tx_tuser : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_tx_tlast : out STD_LOGIC;
    s_axis_tx_tvalid : out STD_LOGIC;
    m_axis_rx_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axis_rx_tkeep : in STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axis_rx_tlast : in STD_LOGIC;
    m_axis_rx_tvalid : in STD_LOGIC;
    m_axis_rx_tready : out STD_LOGIC;
    m_axis_rx_tuser : in STD_LOGIC_VECTOR ( 21 downto 0 );
    cfg_to_turnoff : in STD_LOGIC;
    cfg_bus_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_device_number : in STD_LOGIC_VECTOR ( 4 downto 0 );
    cfg_function_number : in STD_LOGIC_VECTOR ( 2 downto 0 );
    tx_cfg_gnt : out STD_LOGIC;
    cfg_pm_halt_aspm_l0s : out STD_LOGIC;
    cfg_pm_halt_aspm_l1 : out STD_LOGIC;
    cfg_pm_force_state_en : out STD_LOGIC;
    cfg_pm_force_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
    rx_np_ok : out STD_LOGIC;
    rx_np_req : out STD_LOGIC;
    cfg_turnoff_ok : out STD_LOGIC;
    cfg_trn_pending : out STD_LOGIC;
    cfg_pm_wake : out STD_LOGIC;
    cfg_dsn : out STD_LOGIC_VECTOR ( 63 downto 0 );
    fc_sel : out STD_LOGIC_VECTOR ( 2 downto 0 );
    cfg_err_cor : out STD_LOGIC;
    cfg_err_ur : out STD_LOGIC;
    cfg_err_ecrc : out STD_LOGIC;
    cfg_err_cpl_timeout : out STD_LOGIC;
    cfg_err_cpl_unexpect : out STD_LOGIC;
    cfg_err_cpl_abort : out STD_LOGIC;
    cfg_err_atomic_egress_blocked : out STD_LOGIC;
    cfg_err_internal_cor : out STD_LOGIC;
    cfg_err_malformed : out STD_LOGIC;
    cfg_err_mc_blocked : out STD_LOGIC;
    cfg_err_poisoned : out STD_LOGIC;
    cfg_err_norecovery : out STD_LOGIC;
    cfg_err_acs : out STD_LOGIC;
    cfg_err_internal_uncor : out STD_LOGIC;
    cfg_err_posted : out STD_LOGIC;
    cfg_err_locked : out STD_LOGIC;
    cfg_err_tlp_cpl_header : out STD_LOGIC_VECTOR ( 47 downto 0 );
    cfg_err_aer_headerlog : out STD_LOGIC_VECTOR ( 127 downto 0 );
    cfg_aer_interrupt_msgnum : out STD_LOGIC_VECTOR ( 4 downto 0 );
    pl_directed_link_change : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_directed_link_width : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_directed_link_speed : out STD_LOGIC;
    pl_directed_link_auton : out STD_LOGIC;
    pl_upstream_prefer_deemph : out STD_LOGIC;
    cfg_mgmt_di : out STD_LOGIC_VECTOR ( 31 downto 0 );
    cfg_mgmt_byte_en : out STD_LOGIC_VECTOR ( 3 downto 0 );
    cfg_mgmt_dwaddr : out STD_LOGIC_VECTOR ( 9 downto 0 );
    cfg_mgmt_wr_en : out STD_LOGIC;
    cfg_mgmt_rd_en : out STD_LOGIC;
    cfg_mgmt_wr_readonly : out STD_LOGIC;
    cfg_interrupt : out STD_LOGIC;
    cfg_interrupt_assert : out STD_LOGIC;
    cfg_interrupt_di : out STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_interrupt_stat : out STD_LOGIC;
    cfg_pciecap_interrupt_msgnum : out STD_LOGIC_VECTOR ( 4 downto 0 );
    wr_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_en : out STD_LOGIC
  );

end spec7_pcie_app_7x_0;

architecture stub of spec7_pcie_app_7x_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "user_clk,user_reset,user_lnk_up,s_axis_tx_tready,s_axis_tx_tdata[63:0],s_axis_tx_tkeep[7:0],s_axis_tx_tuser[3:0],s_axis_tx_tlast,s_axis_tx_tvalid,m_axis_rx_tdata[63:0],m_axis_rx_tkeep[7:0],m_axis_rx_tlast,m_axis_rx_tvalid,m_axis_rx_tready,m_axis_rx_tuser[21:0],cfg_to_turnoff,cfg_bus_number[7:0],cfg_device_number[4:0],cfg_function_number[2:0],tx_cfg_gnt,cfg_pm_halt_aspm_l0s,cfg_pm_halt_aspm_l1,cfg_pm_force_state_en,cfg_pm_force_state[1:0],rx_np_ok,rx_np_req,cfg_turnoff_ok,cfg_trn_pending,cfg_pm_wake,cfg_dsn[63:0],fc_sel[2:0],cfg_err_cor,cfg_err_ur,cfg_err_ecrc,cfg_err_cpl_timeout,cfg_err_cpl_unexpect,cfg_err_cpl_abort,cfg_err_atomic_egress_blocked,cfg_err_internal_cor,cfg_err_malformed,cfg_err_mc_blocked,cfg_err_poisoned,cfg_err_norecovery,cfg_err_acs,cfg_err_internal_uncor,cfg_err_posted,cfg_err_locked,cfg_err_tlp_cpl_header[47:0],cfg_err_aer_headerlog[127:0],cfg_aer_interrupt_msgnum[4:0],pl_directed_link_change[1:0],pl_directed_link_width[1:0],pl_directed_link_speed,pl_directed_link_auton,pl_upstream_prefer_deemph,cfg_mgmt_di[31:0],cfg_mgmt_byte_en[3:0],cfg_mgmt_dwaddr[9:0],cfg_mgmt_wr_en,cfg_mgmt_rd_en,cfg_mgmt_wr_readonly,cfg_interrupt,cfg_interrupt_assert,cfg_interrupt_di[7:0],cfg_interrupt_stat,cfg_pciecap_interrupt_msgnum[4:0],wr_data[31:0],wr_en";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "pcie_app_7x,Vivado 2019.2";
begin
end;
