// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Sat Oct  3 15:01:19 2020
// Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
// Command     : write_verilog -force -mode funcsim
//               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_pcie_app_7x_0/spec7_pcie_app_7x_0_sim_netlist.v
// Design      : spec7_pcie_app_7x_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z030fbg676-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "spec7_pcie_app_7x_0,pcie_app_7x,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "pcie_app_7x,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module spec7_pcie_app_7x_0
   (user_clk,
    user_reset,
    user_lnk_up,
    s_axis_tx_tready,
    s_axis_tx_tdata,
    s_axis_tx_tkeep,
    s_axis_tx_tuser,
    s_axis_tx_tlast,
    s_axis_tx_tvalid,
    m_axis_rx_tdata,
    m_axis_rx_tkeep,
    m_axis_rx_tlast,
    m_axis_rx_tvalid,
    m_axis_rx_tready,
    m_axis_rx_tuser,
    cfg_to_turnoff,
    cfg_bus_number,
    cfg_device_number,
    cfg_function_number,
    tx_cfg_gnt,
    cfg_pm_halt_aspm_l0s,
    cfg_pm_halt_aspm_l1,
    cfg_pm_force_state_en,
    cfg_pm_force_state,
    rx_np_ok,
    rx_np_req,
    cfg_turnoff_ok,
    cfg_trn_pending,
    cfg_pm_wake,
    cfg_dsn,
    fc_sel,
    cfg_err_cor,
    cfg_err_ur,
    cfg_err_ecrc,
    cfg_err_cpl_timeout,
    cfg_err_cpl_unexpect,
    cfg_err_cpl_abort,
    cfg_err_atomic_egress_blocked,
    cfg_err_internal_cor,
    cfg_err_malformed,
    cfg_err_mc_blocked,
    cfg_err_poisoned,
    cfg_err_norecovery,
    cfg_err_acs,
    cfg_err_internal_uncor,
    cfg_err_posted,
    cfg_err_locked,
    cfg_err_tlp_cpl_header,
    cfg_err_aer_headerlog,
    cfg_aer_interrupt_msgnum,
    pl_directed_link_change,
    pl_directed_link_width,
    pl_directed_link_speed,
    pl_directed_link_auton,
    pl_upstream_prefer_deemph,
    cfg_mgmt_di,
    cfg_mgmt_byte_en,
    cfg_mgmt_dwaddr,
    cfg_mgmt_wr_en,
    cfg_mgmt_rd_en,
    cfg_mgmt_wr_readonly,
    cfg_interrupt,
    cfg_interrupt_assert,
    cfg_interrupt_di,
    cfg_interrupt_stat,
    cfg_pciecap_interrupt_msgnum,
    wr_data,
    wr_en);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 user_signal_clock CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME user_signal_clock, ASSOCIATED_BUSIF m_axis_rx:s_axis_tx, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, INSERT_VIP 0" *) input user_clk;
  input user_reset;
  input user_lnk_up;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TREADY" *) input s_axis_tx_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TDATA" *) output [63:0]s_axis_tx_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TKEEP" *) output [7:0]s_axis_tx_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TUSER" *) output [3:0]s_axis_tx_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TLAST" *) output s_axis_tx_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TVALID" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axis_tx, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 4, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, LAYERED_METADATA undef, INSERT_VIP 0" *) output s_axis_tx_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TDATA" *) input [63:0]m_axis_rx_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TKEEP" *) input [7:0]m_axis_rx_tkeep;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TLAST" *) input m_axis_rx_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TVALID" *) input m_axis_rx_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TREADY" *) output m_axis_rx_tready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TUSER" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_axis_rx, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 22, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, LAYERED_METADATA undef, INSERT_VIP 0" *) input [21:0]m_axis_rx_tuser;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status turnoff" *) input cfg_to_turnoff;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status bus_number" *) input [7:0]cfg_bus_number;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status device_number" *) input [4:0]cfg_device_number;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status function_number" *) input [2:0]cfg_function_number;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control tx_cfg_gnt" *) output tx_cfg_gnt;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_halt_aspm_l0s" *) output cfg_pm_halt_aspm_l0s;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_halt_aspm_l1" *) output cfg_pm_halt_aspm_l1;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_force_state_en" *) output cfg_pm_force_state_en;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_force_state" *) output [1:0]cfg_pm_force_state;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control rx_np_ok" *) output rx_np_ok;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control rx_np_req" *) output rx_np_req;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control turnoff_ok" *) output cfg_turnoff_ok;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control trn_pending" *) output cfg_trn_pending;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_wake" *) output cfg_pm_wake;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control dsn" *) output [63:0]cfg_dsn;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_fc:1.0 pcie_cfg_fc SEL" *) output [2:0]fc_sel;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cor" *) output cfg_err_cor;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err ur" *) output cfg_err_ur;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err ecrc" *) output cfg_err_ecrc;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cpl_timeout" *) output cfg_err_cpl_timeout;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cpl_unexpect" *) output cfg_err_cpl_unexpect;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cpl_abort" *) output cfg_err_cpl_abort;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err atomic_egress_blocked" *) output cfg_err_atomic_egress_blocked;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err internal_cor" *) output cfg_err_internal_cor;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err malformed" *) output cfg_err_malformed;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err mc_blocked" *) output cfg_err_mc_blocked;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err poisoned" *) output cfg_err_poisoned;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err norecovery" *) output cfg_err_norecovery;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err acs" *) output cfg_err_acs;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err internal_uncor" *) output cfg_err_internal_uncor;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err posted" *) output cfg_err_posted;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err locked" *) output cfg_err_locked;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err tlp_cpl_header" *) output [47:0]cfg_err_tlp_cpl_header;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err err_aer_headerlog" *) output [127:0]cfg_err_aer_headerlog;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err aer_interrupt_msgnum" *) output [4:0]cfg_aer_interrupt_msgnum;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_change" *) output [1:0]pl_directed_link_change;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_width" *) output [1:0]pl_directed_link_width;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_speed" *) output pl_directed_link_speed;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_auton" *) output pl_directed_link_auton;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl upstream_prefer_deemph" *) output pl_upstream_prefer_deemph;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt WRITE_DATA" *) output [31:0]cfg_mgmt_di;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt BYTE_EN" *) output [3:0]cfg_mgmt_byte_en;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt ADDR" *) output [9:0]cfg_mgmt_dwaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt WRITE_EN" *) output cfg_mgmt_wr_en;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt READ_EN" *) output cfg_mgmt_rd_en;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt READONLY" *) output cfg_mgmt_wr_readonly;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt interrupt" *) output cfg_interrupt;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt assert" *) output cfg_interrupt_assert;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt write_data" *) output [7:0]cfg_interrupt_di;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt stat" *) output cfg_interrupt_stat;
  (* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt pciecap_interrupt_msgnum" *) output [4:0]cfg_pciecap_interrupt_msgnum;
  output [31:0]wr_data;
  output wr_en;

  wire \<const0> ;
  wire \<const1> ;
  wire [7:0]cfg_bus_number;
  wire [4:0]cfg_device_number;
  wire [2:0]cfg_function_number;
  wire cfg_to_turnoff;
  wire cfg_turnoff_ok;
  wire [63:0]m_axis_rx_tdata;
  wire m_axis_rx_tlast;
  wire m_axis_rx_tready;
  wire m_axis_rx_tvalid;
  wire [63:0]\^s_axis_tx_tdata ;
  wire [6:2]\^s_axis_tx_tkeep ;
  wire s_axis_tx_tlast;
  wire s_axis_tx_tready;
  wire s_axis_tx_tvalid;
  wire user_clk;
  wire user_lnk_up;
  wire user_reset;
  wire [31:0]wr_data;
  wire wr_en;

  assign cfg_aer_interrupt_msgnum[4] = \<const0> ;
  assign cfg_aer_interrupt_msgnum[3] = \<const0> ;
  assign cfg_aer_interrupt_msgnum[2] = \<const0> ;
  assign cfg_aer_interrupt_msgnum[1] = \<const0> ;
  assign cfg_aer_interrupt_msgnum[0] = \<const0> ;
  assign cfg_dsn[63] = \<const0> ;
  assign cfg_dsn[62] = \<const0> ;
  assign cfg_dsn[61] = \<const0> ;
  assign cfg_dsn[60] = \<const0> ;
  assign cfg_dsn[59] = \<const0> ;
  assign cfg_dsn[58] = \<const0> ;
  assign cfg_dsn[57] = \<const0> ;
  assign cfg_dsn[56] = \<const0> ;
  assign cfg_dsn[55] = \<const0> ;
  assign cfg_dsn[54] = \<const0> ;
  assign cfg_dsn[53] = \<const0> ;
  assign cfg_dsn[52] = \<const0> ;
  assign cfg_dsn[51] = \<const0> ;
  assign cfg_dsn[50] = \<const0> ;
  assign cfg_dsn[49] = \<const0> ;
  assign cfg_dsn[48] = \<const0> ;
  assign cfg_dsn[47] = \<const0> ;
  assign cfg_dsn[46] = \<const0> ;
  assign cfg_dsn[45] = \<const0> ;
  assign cfg_dsn[44] = \<const0> ;
  assign cfg_dsn[43] = \<const0> ;
  assign cfg_dsn[42] = \<const0> ;
  assign cfg_dsn[41] = \<const0> ;
  assign cfg_dsn[40] = \<const0> ;
  assign cfg_dsn[39] = \<const0> ;
  assign cfg_dsn[38] = \<const0> ;
  assign cfg_dsn[37] = \<const0> ;
  assign cfg_dsn[36] = \<const0> ;
  assign cfg_dsn[35] = \<const0> ;
  assign cfg_dsn[34] = \<const0> ;
  assign cfg_dsn[33] = \<const0> ;
  assign cfg_dsn[32] = \<const0> ;
  assign cfg_dsn[31] = \<const0> ;
  assign cfg_dsn[30] = \<const0> ;
  assign cfg_dsn[29] = \<const0> ;
  assign cfg_dsn[28] = \<const0> ;
  assign cfg_dsn[27] = \<const0> ;
  assign cfg_dsn[26] = \<const0> ;
  assign cfg_dsn[25] = \<const0> ;
  assign cfg_dsn[24] = \<const0> ;
  assign cfg_dsn[23] = \<const0> ;
  assign cfg_dsn[22] = \<const0> ;
  assign cfg_dsn[21] = \<const0> ;
  assign cfg_dsn[20] = \<const0> ;
  assign cfg_dsn[19] = \<const0> ;
  assign cfg_dsn[18] = \<const0> ;
  assign cfg_dsn[17] = \<const0> ;
  assign cfg_dsn[16] = \<const0> ;
  assign cfg_dsn[15] = \<const0> ;
  assign cfg_dsn[14] = \<const0> ;
  assign cfg_dsn[13] = \<const0> ;
  assign cfg_dsn[12] = \<const0> ;
  assign cfg_dsn[11] = \<const0> ;
  assign cfg_dsn[10] = \<const0> ;
  assign cfg_dsn[9] = \<const0> ;
  assign cfg_dsn[8] = \<const0> ;
  assign cfg_dsn[7] = \<const0> ;
  assign cfg_dsn[6] = \<const0> ;
  assign cfg_dsn[5] = \<const0> ;
  assign cfg_dsn[4] = \<const0> ;
  assign cfg_dsn[3] = \<const0> ;
  assign cfg_dsn[2] = \<const0> ;
  assign cfg_dsn[1] = \<const0> ;
  assign cfg_dsn[0] = \<const0> ;
  assign cfg_err_acs = \<const0> ;
  assign cfg_err_aer_headerlog[127] = \<const0> ;
  assign cfg_err_aer_headerlog[126] = \<const0> ;
  assign cfg_err_aer_headerlog[125] = \<const0> ;
  assign cfg_err_aer_headerlog[124] = \<const0> ;
  assign cfg_err_aer_headerlog[123] = \<const0> ;
  assign cfg_err_aer_headerlog[122] = \<const0> ;
  assign cfg_err_aer_headerlog[121] = \<const0> ;
  assign cfg_err_aer_headerlog[120] = \<const0> ;
  assign cfg_err_aer_headerlog[119] = \<const0> ;
  assign cfg_err_aer_headerlog[118] = \<const0> ;
  assign cfg_err_aer_headerlog[117] = \<const0> ;
  assign cfg_err_aer_headerlog[116] = \<const0> ;
  assign cfg_err_aer_headerlog[115] = \<const0> ;
  assign cfg_err_aer_headerlog[114] = \<const0> ;
  assign cfg_err_aer_headerlog[113] = \<const0> ;
  assign cfg_err_aer_headerlog[112] = \<const0> ;
  assign cfg_err_aer_headerlog[111] = \<const0> ;
  assign cfg_err_aer_headerlog[110] = \<const0> ;
  assign cfg_err_aer_headerlog[109] = \<const0> ;
  assign cfg_err_aer_headerlog[108] = \<const0> ;
  assign cfg_err_aer_headerlog[107] = \<const0> ;
  assign cfg_err_aer_headerlog[106] = \<const0> ;
  assign cfg_err_aer_headerlog[105] = \<const0> ;
  assign cfg_err_aer_headerlog[104] = \<const0> ;
  assign cfg_err_aer_headerlog[103] = \<const0> ;
  assign cfg_err_aer_headerlog[102] = \<const0> ;
  assign cfg_err_aer_headerlog[101] = \<const0> ;
  assign cfg_err_aer_headerlog[100] = \<const0> ;
  assign cfg_err_aer_headerlog[99] = \<const0> ;
  assign cfg_err_aer_headerlog[98] = \<const0> ;
  assign cfg_err_aer_headerlog[97] = \<const0> ;
  assign cfg_err_aer_headerlog[96] = \<const0> ;
  assign cfg_err_aer_headerlog[95] = \<const0> ;
  assign cfg_err_aer_headerlog[94] = \<const0> ;
  assign cfg_err_aer_headerlog[93] = \<const0> ;
  assign cfg_err_aer_headerlog[92] = \<const0> ;
  assign cfg_err_aer_headerlog[91] = \<const0> ;
  assign cfg_err_aer_headerlog[90] = \<const0> ;
  assign cfg_err_aer_headerlog[89] = \<const0> ;
  assign cfg_err_aer_headerlog[88] = \<const0> ;
  assign cfg_err_aer_headerlog[87] = \<const0> ;
  assign cfg_err_aer_headerlog[86] = \<const0> ;
  assign cfg_err_aer_headerlog[85] = \<const0> ;
  assign cfg_err_aer_headerlog[84] = \<const0> ;
  assign cfg_err_aer_headerlog[83] = \<const0> ;
  assign cfg_err_aer_headerlog[82] = \<const0> ;
  assign cfg_err_aer_headerlog[81] = \<const0> ;
  assign cfg_err_aer_headerlog[80] = \<const0> ;
  assign cfg_err_aer_headerlog[79] = \<const0> ;
  assign cfg_err_aer_headerlog[78] = \<const0> ;
  assign cfg_err_aer_headerlog[77] = \<const0> ;
  assign cfg_err_aer_headerlog[76] = \<const0> ;
  assign cfg_err_aer_headerlog[75] = \<const0> ;
  assign cfg_err_aer_headerlog[74] = \<const0> ;
  assign cfg_err_aer_headerlog[73] = \<const0> ;
  assign cfg_err_aer_headerlog[72] = \<const0> ;
  assign cfg_err_aer_headerlog[71] = \<const0> ;
  assign cfg_err_aer_headerlog[70] = \<const0> ;
  assign cfg_err_aer_headerlog[69] = \<const0> ;
  assign cfg_err_aer_headerlog[68] = \<const0> ;
  assign cfg_err_aer_headerlog[67] = \<const0> ;
  assign cfg_err_aer_headerlog[66] = \<const0> ;
  assign cfg_err_aer_headerlog[65] = \<const0> ;
  assign cfg_err_aer_headerlog[64] = \<const0> ;
  assign cfg_err_aer_headerlog[63] = \<const0> ;
  assign cfg_err_aer_headerlog[62] = \<const0> ;
  assign cfg_err_aer_headerlog[61] = \<const0> ;
  assign cfg_err_aer_headerlog[60] = \<const0> ;
  assign cfg_err_aer_headerlog[59] = \<const0> ;
  assign cfg_err_aer_headerlog[58] = \<const0> ;
  assign cfg_err_aer_headerlog[57] = \<const0> ;
  assign cfg_err_aer_headerlog[56] = \<const0> ;
  assign cfg_err_aer_headerlog[55] = \<const0> ;
  assign cfg_err_aer_headerlog[54] = \<const0> ;
  assign cfg_err_aer_headerlog[53] = \<const0> ;
  assign cfg_err_aer_headerlog[52] = \<const0> ;
  assign cfg_err_aer_headerlog[51] = \<const0> ;
  assign cfg_err_aer_headerlog[50] = \<const0> ;
  assign cfg_err_aer_headerlog[49] = \<const0> ;
  assign cfg_err_aer_headerlog[48] = \<const0> ;
  assign cfg_err_aer_headerlog[47] = \<const0> ;
  assign cfg_err_aer_headerlog[46] = \<const0> ;
  assign cfg_err_aer_headerlog[45] = \<const0> ;
  assign cfg_err_aer_headerlog[44] = \<const0> ;
  assign cfg_err_aer_headerlog[43] = \<const0> ;
  assign cfg_err_aer_headerlog[42] = \<const0> ;
  assign cfg_err_aer_headerlog[41] = \<const0> ;
  assign cfg_err_aer_headerlog[40] = \<const0> ;
  assign cfg_err_aer_headerlog[39] = \<const0> ;
  assign cfg_err_aer_headerlog[38] = \<const0> ;
  assign cfg_err_aer_headerlog[37] = \<const0> ;
  assign cfg_err_aer_headerlog[36] = \<const0> ;
  assign cfg_err_aer_headerlog[35] = \<const0> ;
  assign cfg_err_aer_headerlog[34] = \<const0> ;
  assign cfg_err_aer_headerlog[33] = \<const0> ;
  assign cfg_err_aer_headerlog[32] = \<const0> ;
  assign cfg_err_aer_headerlog[31] = \<const0> ;
  assign cfg_err_aer_headerlog[30] = \<const0> ;
  assign cfg_err_aer_headerlog[29] = \<const0> ;
  assign cfg_err_aer_headerlog[28] = \<const0> ;
  assign cfg_err_aer_headerlog[27] = \<const0> ;
  assign cfg_err_aer_headerlog[26] = \<const0> ;
  assign cfg_err_aer_headerlog[25] = \<const0> ;
  assign cfg_err_aer_headerlog[24] = \<const0> ;
  assign cfg_err_aer_headerlog[23] = \<const0> ;
  assign cfg_err_aer_headerlog[22] = \<const0> ;
  assign cfg_err_aer_headerlog[21] = \<const0> ;
  assign cfg_err_aer_headerlog[20] = \<const0> ;
  assign cfg_err_aer_headerlog[19] = \<const0> ;
  assign cfg_err_aer_headerlog[18] = \<const0> ;
  assign cfg_err_aer_headerlog[17] = \<const0> ;
  assign cfg_err_aer_headerlog[16] = \<const0> ;
  assign cfg_err_aer_headerlog[15] = \<const0> ;
  assign cfg_err_aer_headerlog[14] = \<const0> ;
  assign cfg_err_aer_headerlog[13] = \<const0> ;
  assign cfg_err_aer_headerlog[12] = \<const0> ;
  assign cfg_err_aer_headerlog[11] = \<const0> ;
  assign cfg_err_aer_headerlog[10] = \<const0> ;
  assign cfg_err_aer_headerlog[9] = \<const0> ;
  assign cfg_err_aer_headerlog[8] = \<const0> ;
  assign cfg_err_aer_headerlog[7] = \<const0> ;
  assign cfg_err_aer_headerlog[6] = \<const0> ;
  assign cfg_err_aer_headerlog[5] = \<const0> ;
  assign cfg_err_aer_headerlog[4] = \<const0> ;
  assign cfg_err_aer_headerlog[3] = \<const0> ;
  assign cfg_err_aer_headerlog[2] = \<const0> ;
  assign cfg_err_aer_headerlog[1] = \<const0> ;
  assign cfg_err_aer_headerlog[0] = \<const0> ;
  assign cfg_err_atomic_egress_blocked = \<const0> ;
  assign cfg_err_cor = \<const0> ;
  assign cfg_err_cpl_abort = \<const0> ;
  assign cfg_err_cpl_timeout = \<const0> ;
  assign cfg_err_cpl_unexpect = \<const0> ;
  assign cfg_err_ecrc = \<const0> ;
  assign cfg_err_internal_cor = \<const0> ;
  assign cfg_err_internal_uncor = \<const0> ;
  assign cfg_err_locked = \<const0> ;
  assign cfg_err_malformed = \<const0> ;
  assign cfg_err_mc_blocked = \<const0> ;
  assign cfg_err_norecovery = \<const0> ;
  assign cfg_err_poisoned = \<const0> ;
  assign cfg_err_posted = \<const0> ;
  assign cfg_err_tlp_cpl_header[47] = \<const0> ;
  assign cfg_err_tlp_cpl_header[46] = \<const0> ;
  assign cfg_err_tlp_cpl_header[45] = \<const0> ;
  assign cfg_err_tlp_cpl_header[44] = \<const0> ;
  assign cfg_err_tlp_cpl_header[43] = \<const0> ;
  assign cfg_err_tlp_cpl_header[42] = \<const0> ;
  assign cfg_err_tlp_cpl_header[41] = \<const0> ;
  assign cfg_err_tlp_cpl_header[40] = \<const0> ;
  assign cfg_err_tlp_cpl_header[39] = \<const0> ;
  assign cfg_err_tlp_cpl_header[38] = \<const0> ;
  assign cfg_err_tlp_cpl_header[37] = \<const0> ;
  assign cfg_err_tlp_cpl_header[36] = \<const0> ;
  assign cfg_err_tlp_cpl_header[35] = \<const0> ;
  assign cfg_err_tlp_cpl_header[34] = \<const0> ;
  assign cfg_err_tlp_cpl_header[33] = \<const0> ;
  assign cfg_err_tlp_cpl_header[32] = \<const0> ;
  assign cfg_err_tlp_cpl_header[31] = \<const0> ;
  assign cfg_err_tlp_cpl_header[30] = \<const0> ;
  assign cfg_err_tlp_cpl_header[29] = \<const0> ;
  assign cfg_err_tlp_cpl_header[28] = \<const0> ;
  assign cfg_err_tlp_cpl_header[27] = \<const0> ;
  assign cfg_err_tlp_cpl_header[26] = \<const0> ;
  assign cfg_err_tlp_cpl_header[25] = \<const0> ;
  assign cfg_err_tlp_cpl_header[24] = \<const0> ;
  assign cfg_err_tlp_cpl_header[23] = \<const0> ;
  assign cfg_err_tlp_cpl_header[22] = \<const0> ;
  assign cfg_err_tlp_cpl_header[21] = \<const0> ;
  assign cfg_err_tlp_cpl_header[20] = \<const0> ;
  assign cfg_err_tlp_cpl_header[19] = \<const0> ;
  assign cfg_err_tlp_cpl_header[18] = \<const0> ;
  assign cfg_err_tlp_cpl_header[17] = \<const0> ;
  assign cfg_err_tlp_cpl_header[16] = \<const0> ;
  assign cfg_err_tlp_cpl_header[15] = \<const0> ;
  assign cfg_err_tlp_cpl_header[14] = \<const0> ;
  assign cfg_err_tlp_cpl_header[13] = \<const0> ;
  assign cfg_err_tlp_cpl_header[12] = \<const0> ;
  assign cfg_err_tlp_cpl_header[11] = \<const0> ;
  assign cfg_err_tlp_cpl_header[10] = \<const0> ;
  assign cfg_err_tlp_cpl_header[9] = \<const0> ;
  assign cfg_err_tlp_cpl_header[8] = \<const0> ;
  assign cfg_err_tlp_cpl_header[7] = \<const0> ;
  assign cfg_err_tlp_cpl_header[6] = \<const0> ;
  assign cfg_err_tlp_cpl_header[5] = \<const0> ;
  assign cfg_err_tlp_cpl_header[4] = \<const0> ;
  assign cfg_err_tlp_cpl_header[3] = \<const0> ;
  assign cfg_err_tlp_cpl_header[2] = \<const0> ;
  assign cfg_err_tlp_cpl_header[1] = \<const0> ;
  assign cfg_err_tlp_cpl_header[0] = \<const0> ;
  assign cfg_err_ur = \<const0> ;
  assign cfg_interrupt = \<const0> ;
  assign cfg_interrupt_assert = \<const0> ;
  assign cfg_interrupt_di[7] = \<const0> ;
  assign cfg_interrupt_di[6] = \<const0> ;
  assign cfg_interrupt_di[5] = \<const0> ;
  assign cfg_interrupt_di[4] = \<const0> ;
  assign cfg_interrupt_di[3] = \<const0> ;
  assign cfg_interrupt_di[2] = \<const0> ;
  assign cfg_interrupt_di[1] = \<const0> ;
  assign cfg_interrupt_di[0] = \<const0> ;
  assign cfg_interrupt_stat = \<const0> ;
  assign cfg_mgmt_byte_en[3] = \<const0> ;
  assign cfg_mgmt_byte_en[2] = \<const0> ;
  assign cfg_mgmt_byte_en[1] = \<const0> ;
  assign cfg_mgmt_byte_en[0] = \<const0> ;
  assign cfg_mgmt_di[31] = \<const0> ;
  assign cfg_mgmt_di[30] = \<const0> ;
  assign cfg_mgmt_di[29] = \<const0> ;
  assign cfg_mgmt_di[28] = \<const0> ;
  assign cfg_mgmt_di[27] = \<const0> ;
  assign cfg_mgmt_di[26] = \<const0> ;
  assign cfg_mgmt_di[25] = \<const0> ;
  assign cfg_mgmt_di[24] = \<const0> ;
  assign cfg_mgmt_di[23] = \<const0> ;
  assign cfg_mgmt_di[22] = \<const0> ;
  assign cfg_mgmt_di[21] = \<const0> ;
  assign cfg_mgmt_di[20] = \<const0> ;
  assign cfg_mgmt_di[19] = \<const0> ;
  assign cfg_mgmt_di[18] = \<const0> ;
  assign cfg_mgmt_di[17] = \<const0> ;
  assign cfg_mgmt_di[16] = \<const0> ;
  assign cfg_mgmt_di[15] = \<const0> ;
  assign cfg_mgmt_di[14] = \<const0> ;
  assign cfg_mgmt_di[13] = \<const0> ;
  assign cfg_mgmt_di[12] = \<const0> ;
  assign cfg_mgmt_di[11] = \<const0> ;
  assign cfg_mgmt_di[10] = \<const0> ;
  assign cfg_mgmt_di[9] = \<const0> ;
  assign cfg_mgmt_di[8] = \<const0> ;
  assign cfg_mgmt_di[7] = \<const0> ;
  assign cfg_mgmt_di[6] = \<const0> ;
  assign cfg_mgmt_di[5] = \<const0> ;
  assign cfg_mgmt_di[4] = \<const0> ;
  assign cfg_mgmt_di[3] = \<const0> ;
  assign cfg_mgmt_di[2] = \<const0> ;
  assign cfg_mgmt_di[1] = \<const0> ;
  assign cfg_mgmt_di[0] = \<const0> ;
  assign cfg_mgmt_dwaddr[9] = \<const0> ;
  assign cfg_mgmt_dwaddr[8] = \<const0> ;
  assign cfg_mgmt_dwaddr[7] = \<const0> ;
  assign cfg_mgmt_dwaddr[6] = \<const0> ;
  assign cfg_mgmt_dwaddr[5] = \<const0> ;
  assign cfg_mgmt_dwaddr[4] = \<const0> ;
  assign cfg_mgmt_dwaddr[3] = \<const0> ;
  assign cfg_mgmt_dwaddr[2] = \<const0> ;
  assign cfg_mgmt_dwaddr[1] = \<const0> ;
  assign cfg_mgmt_dwaddr[0] = \<const0> ;
  assign cfg_mgmt_rd_en = \<const0> ;
  assign cfg_mgmt_wr_en = \<const0> ;
  assign cfg_mgmt_wr_readonly = \<const0> ;
  assign cfg_pciecap_interrupt_msgnum[4] = \<const0> ;
  assign cfg_pciecap_interrupt_msgnum[3] = \<const0> ;
  assign cfg_pciecap_interrupt_msgnum[2] = \<const0> ;
  assign cfg_pciecap_interrupt_msgnum[1] = \<const0> ;
  assign cfg_pciecap_interrupt_msgnum[0] = \<const0> ;
  assign cfg_pm_force_state[1] = \<const0> ;
  assign cfg_pm_force_state[0] = \<const0> ;
  assign cfg_pm_force_state_en = \<const0> ;
  assign cfg_pm_halt_aspm_l0s = \<const0> ;
  assign cfg_pm_halt_aspm_l1 = \<const0> ;
  assign cfg_pm_wake = \<const0> ;
  assign cfg_trn_pending = \<const0> ;
  assign fc_sel[2] = \<const0> ;
  assign fc_sel[1] = \<const0> ;
  assign fc_sel[0] = \<const0> ;
  assign pl_directed_link_auton = \<const0> ;
  assign pl_directed_link_change[1] = \<const0> ;
  assign pl_directed_link_change[0] = \<const0> ;
  assign pl_directed_link_speed = \<const0> ;
  assign pl_directed_link_width[1] = \<const0> ;
  assign pl_directed_link_width[0] = \<const0> ;
  assign pl_upstream_prefer_deemph = \<const1> ;
  assign rx_np_ok = \<const1> ;
  assign rx_np_req = \<const1> ;
  assign s_axis_tx_tdata[63:48] = \^s_axis_tx_tdata [63:48];
  assign s_axis_tx_tdata[47] = \<const0> ;
  assign s_axis_tx_tdata[46] = \<const0> ;
  assign s_axis_tx_tdata[45] = \<const0> ;
  assign s_axis_tx_tdata[44] = \<const0> ;
  assign s_axis_tx_tdata[43] = \<const0> ;
  assign s_axis_tx_tdata[42] = \<const0> ;
  assign s_axis_tx_tdata[41] = \<const0> ;
  assign s_axis_tx_tdata[40] = \<const0> ;
  assign s_axis_tx_tdata[39] = \<const0> ;
  assign s_axis_tx_tdata[38] = \<const0> ;
  assign s_axis_tx_tdata[37] = \<const0> ;
  assign s_axis_tx_tdata[36] = \<const0> ;
  assign s_axis_tx_tdata[35] = \<const0> ;
  assign s_axis_tx_tdata[34:0] = \^s_axis_tx_tdata [34:0];
  assign s_axis_tx_tkeep[7] = \^s_axis_tx_tkeep [6];
  assign s_axis_tx_tkeep[6] = \^s_axis_tx_tkeep [6];
  assign s_axis_tx_tkeep[5] = \^s_axis_tx_tkeep [6];
  assign s_axis_tx_tkeep[4] = \^s_axis_tx_tkeep [6];
  assign s_axis_tx_tkeep[3] = \^s_axis_tx_tkeep [2];
  assign s_axis_tx_tkeep[2] = \^s_axis_tx_tkeep [2];
  assign s_axis_tx_tkeep[1] = \^s_axis_tx_tkeep [2];
  assign s_axis_tx_tkeep[0] = \^s_axis_tx_tkeep [2];
  assign s_axis_tx_tuser[3] = \<const0> ;
  assign s_axis_tx_tuser[2] = \<const0> ;
  assign s_axis_tx_tuser[1] = \<const0> ;
  assign s_axis_tx_tuser[0] = \<const0> ;
  assign tx_cfg_gnt = \<const1> ;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  spec7_pcie_app_7x_0_pcie_app_7x inst
       (.cfg_bus_number(cfg_bus_number),
        .cfg_device_number(cfg_device_number),
        .cfg_function_number(cfg_function_number),
        .cfg_to_turnoff(cfg_to_turnoff),
        .cfg_turnoff_ok(cfg_turnoff_ok),
        .m_axis_rx_tdata(m_axis_rx_tdata),
        .m_axis_rx_tlast(m_axis_rx_tlast),
        .m_axis_rx_tready(m_axis_rx_tready),
        .m_axis_rx_tvalid(m_axis_rx_tvalid),
        .s_axis_tx_tdata({\^s_axis_tx_tdata [63:48],\^s_axis_tx_tdata [34:0]}),
        .s_axis_tx_tkeep({\^s_axis_tx_tkeep [6],\^s_axis_tx_tkeep [2]}),
        .s_axis_tx_tlast(s_axis_tx_tlast),
        .s_axis_tx_tready(s_axis_tx_tready),
        .s_axis_tx_tvalid(s_axis_tx_tvalid),
        .user_clk(user_clk),
        .user_lnk_up(user_lnk_up),
        .user_reset(user_reset),
        .wr_data(wr_data),
        .wr_en(wr_en));
endmodule

(* ORIG_REF_NAME = "PIO" *) 
module spec7_pcie_app_7x_0_PIO
   (wr_data,
    wr_en,
    s_axis_tx_tdata,
    s_axis_tx_tkeep,
    s_axis_tx_tvalid,
    cfg_turnoff_ok,
    m_axis_rx_tready,
    s_axis_tx_tlast,
    s_axis_tx_tready,
    cfg_to_turnoff,
    user_clk,
    m_axis_rx_tdata,
    cfg_bus_number,
    cfg_device_number,
    cfg_function_number,
    m_axis_rx_tvalid,
    m_axis_rx_tlast,
    user_lnk_up,
    user_reset);
  output [31:0]wr_data;
  output wr_en;
  output [50:0]s_axis_tx_tdata;
  output [1:0]s_axis_tx_tkeep;
  output s_axis_tx_tvalid;
  output cfg_turnoff_ok;
  output m_axis_rx_tready;
  output s_axis_tx_tlast;
  input s_axis_tx_tready;
  input cfg_to_turnoff;
  input user_clk;
  input [63:0]m_axis_rx_tdata;
  input [7:0]cfg_bus_number;
  input [4:0]cfg_device_number;
  input [2:0]cfg_function_number;
  input m_axis_rx_tvalid;
  input m_axis_rx_tlast;
  input user_lnk_up;
  input user_reset;

  wire PIO_EP_inst_n_0;
  wire PIO_EP_inst_n_4;
  wire [7:0]cfg_bus_number;
  wire [4:0]cfg_device_number;
  wire [2:0]cfg_function_number;
  wire cfg_to_turnoff;
  wire cfg_turnoff_ok;
  wire [63:0]m_axis_rx_tdata;
  wire m_axis_rx_tlast;
  wire m_axis_rx_tready;
  wire m_axis_rx_tvalid;
  wire pio_reset_n;
  wire pio_reset_n_i_1_n_0;
  wire [50:0]s_axis_tx_tdata;
  wire [1:0]s_axis_tx_tkeep;
  wire s_axis_tx_tlast;
  wire s_axis_tx_tready;
  wire s_axis_tx_tvalid;
  wire trn_pending;
  wire user_clk;
  wire user_lnk_up;
  wire user_reset;
  wire [31:0]wr_data;
  wire wr_en;

  spec7_pcie_app_7x_0_PIO_EP PIO_EP_inst
       (.SR(PIO_EP_inst_n_0),
        .cfg_bus_number(cfg_bus_number),
        .cfg_device_number(cfg_device_number),
        .cfg_function_number(cfg_function_number),
        .m_axis_rx_tdata(m_axis_rx_tdata),
        .m_axis_rx_tlast(m_axis_rx_tlast),
        .m_axis_rx_tready(m_axis_rx_tready),
        .m_axis_rx_tvalid(m_axis_rx_tvalid),
        .pio_reset_n(pio_reset_n),
        .s_axis_tx_tdata(s_axis_tx_tdata),
        .s_axis_tx_tkeep(s_axis_tx_tkeep),
        .s_axis_tx_tlast(s_axis_tx_tlast),
        .s_axis_tx_tready(s_axis_tx_tready),
        .s_axis_tx_tvalid(s_axis_tx_tvalid),
        .trn_pending(trn_pending),
        .trn_pending_reg(PIO_EP_inst_n_4),
        .user_clk(user_clk),
        .wr_data(wr_data),
        .wr_en(wr_en));
  spec7_pcie_app_7x_0_PIO_TO_CTRL PIO_TO_inst
       (.SR(PIO_EP_inst_n_0),
        .cfg_to_turnoff(cfg_to_turnoff),
        .cfg_turnoff_ok(cfg_turnoff_ok),
        .trn_pending(trn_pending),
        .trn_pending_reg_0(PIO_EP_inst_n_4),
        .user_clk(user_clk));
  LUT2 #(
    .INIT(4'h2)) 
    pio_reset_n_i_1
       (.I0(user_lnk_up),
        .I1(user_reset),
        .O(pio_reset_n_i_1_n_0));
  FDRE pio_reset_n_reg
       (.C(user_clk),
        .CE(1'b1),
        .D(pio_reset_n_i_1_n_0),
        .Q(pio_reset_n),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "PIO_EP" *) 
module spec7_pcie_app_7x_0_PIO_EP
   (SR,
    wr_en,
    m_axis_rx_tready,
    s_axis_tx_tlast,
    trn_pending_reg,
    wr_data,
    s_axis_tx_tdata,
    s_axis_tx_tkeep,
    s_axis_tx_tvalid,
    user_clk,
    m_axis_rx_tdata,
    s_axis_tx_tready,
    pio_reset_n,
    m_axis_rx_tvalid,
    trn_pending,
    cfg_bus_number,
    cfg_device_number,
    cfg_function_number,
    m_axis_rx_tlast);
  output [0:0]SR;
  output wr_en;
  output m_axis_rx_tready;
  output s_axis_tx_tlast;
  output trn_pending_reg;
  output [31:0]wr_data;
  output [50:0]s_axis_tx_tdata;
  output [1:0]s_axis_tx_tkeep;
  output s_axis_tx_tvalid;
  input user_clk;
  input [63:0]m_axis_rx_tdata;
  input s_axis_tx_tready;
  input pio_reset_n;
  input m_axis_rx_tvalid;
  input trn_pending;
  input [7:0]cfg_bus_number;
  input [4:0]cfg_device_number;
  input [2:0]cfg_function_number;
  input m_axis_rx_tlast;

  wire EP_RX_inst_n_70;
  wire EP_RX_inst_n_71;
  wire EP_RX_inst_n_72;
  wire EP_RX_inst_n_73;
  wire [0:0]SR;
  wire [7:0]cfg_bus_number;
  wire [4:0]cfg_device_number;
  wire [2:0]cfg_function_number;
  wire compl_done;
  wire [6:2]data0;
  wire \gen_cpl_64.state ;
  wire [63:0]m_axis_rx_tdata;
  wire m_axis_rx_tlast;
  wire m_axis_rx_tready;
  wire m_axis_rx_tvalid;
  wire [22:8]p_0_in;
  wire pio_reset_n;
  wire req_compl;
  wire req_compl_q;
  wire req_compl_wd;
  wire [7:0]req_len;
  wire [15:0]req_rid;
  wire [3:2]req_tag;
  wire [50:0]s_axis_tx_tdata;
  wire [1:0]s_axis_tx_tkeep;
  wire s_axis_tx_tlast;
  wire s_axis_tx_tready;
  wire s_axis_tx_tvalid;
  wire trn_pending;
  wire trn_pending_reg;
  wire user_clk;
  wire [31:0]wr_data;
  wire wr_en;

  spec7_pcie_app_7x_0_PIO_RX_ENGINE EP_RX_inst
       (.D({p_0_in[22:20],p_0_in[15:12],p_0_in[9:8]}),
        .Q(req_tag),
        .SR(SR),
        .compl_done(compl_done),
        .\gen_cpl_64.state (\gen_cpl_64.state ),
        .m_axis_rx_tdata(m_axis_rx_tdata),
        .m_axis_rx_tlast(m_axis_rx_tlast),
        .m_axis_rx_tready(m_axis_rx_tready),
        .m_axis_rx_tvalid(m_axis_rx_tvalid),
        .pio_reset_n(pio_reset_n),
        .\pio_rx_sm_64.req_addr_reg[6]_0 (data0),
        .\pio_rx_sm_64.req_be_reg[3]_0 ({EP_RX_inst_n_70,EP_RX_inst_n_71,EP_RX_inst_n_72,EP_RX_inst_n_73}),
        .\pio_rx_sm_64.req_len_reg[7]_0 (req_len),
        .\pio_rx_sm_64.req_rid_reg[15]_0 ({req_rid[15:7],req_rid[3:0]}),
        .req_compl(req_compl),
        .req_compl_q(req_compl_q),
        .req_compl_wd(req_compl_wd),
        .trn_pending(trn_pending),
        .trn_pending_reg(trn_pending_reg),
        .user_clk(user_clk),
        .wr_data(wr_data),
        .wr_en(wr_en));
  spec7_pcie_app_7x_0_PIO_TX_ENGINE EP_TX_inst
       (.D({p_0_in[22:20],p_0_in[15:12],p_0_in[9:8]}),
        .Q(req_tag),
        .SR(SR),
        .cfg_bus_number(cfg_bus_number),
        .cfg_device_number(cfg_device_number),
        .cfg_function_number(cfg_function_number),
        .compl_done(compl_done),
        .\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 ({req_rid[15:7],req_rid[3:0]}),
        .\gen_cpl_64.s_axis_tx_tdata_reg[6]_0 (data0),
        .\gen_cpl_64.s_axis_tx_tdata_reg[7]_0 (req_len),
        .\gen_cpl_64.state (\gen_cpl_64.state ),
        .pio_reset_n(pio_reset_n),
        .\rd_be_reg[3]_0 ({EP_RX_inst_n_70,EP_RX_inst_n_71,EP_RX_inst_n_72,EP_RX_inst_n_73}),
        .req_compl(req_compl),
        .req_compl_q(req_compl_q),
        .req_compl_wd(req_compl_wd),
        .s_axis_tx_tdata(s_axis_tx_tdata),
        .s_axis_tx_tkeep(s_axis_tx_tkeep),
        .s_axis_tx_tlast(s_axis_tx_tlast),
        .s_axis_tx_tready(s_axis_tx_tready),
        .s_axis_tx_tvalid(s_axis_tx_tvalid),
        .user_clk(user_clk));
endmodule

(* ORIG_REF_NAME = "PIO_RX_ENGINE" *) 
module spec7_pcie_app_7x_0_PIO_RX_ENGINE
   (req_compl,
    SR,
    wr_en,
    m_axis_rx_tready,
    req_compl_wd,
    trn_pending_reg,
    D,
    Q,
    \pio_rx_sm_64.req_len_reg[7]_0 ,
    \pio_rx_sm_64.req_rid_reg[15]_0 ,
    wr_data,
    \pio_rx_sm_64.req_be_reg[3]_0 ,
    \pio_rx_sm_64.req_addr_reg[6]_0 ,
    user_clk,
    m_axis_rx_tdata,
    m_axis_rx_tvalid,
    trn_pending,
    compl_done,
    pio_reset_n,
    m_axis_rx_tlast,
    req_compl_q,
    \gen_cpl_64.state );
  output req_compl;
  output [0:0]SR;
  output wr_en;
  output m_axis_rx_tready;
  output req_compl_wd;
  output trn_pending_reg;
  output [8:0]D;
  output [1:0]Q;
  output [7:0]\pio_rx_sm_64.req_len_reg[7]_0 ;
  output [12:0]\pio_rx_sm_64.req_rid_reg[15]_0 ;
  output [31:0]wr_data;
  output [3:0]\pio_rx_sm_64.req_be_reg[3]_0 ;
  output [4:0]\pio_rx_sm_64.req_addr_reg[6]_0 ;
  input user_clk;
  input [63:0]m_axis_rx_tdata;
  input m_axis_rx_tvalid;
  input trn_pending;
  input compl_done;
  input pio_reset_n;
  input m_axis_rx_tlast;
  input req_compl_q;
  input \gen_cpl_64.state ;

  wire [8:0]D;
  wire \FSM_sequential_pio_rx_sm_64.state[0]_i_1_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[0]_i_2_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[0]_i_3_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[0]_i_4_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[0]_i_5_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[1]_i_1_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[1]_i_2_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[1]_i_3_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[1]_i_4_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[2]_i_1_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[2]_i_2_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[2]_i_3_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[2]_i_4_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[2]_i_5_n_0 ;
  wire \FSM_sequential_pio_rx_sm_64.state[2]_i_6_n_0 ;
  wire [1:0]Q;
  wire [0:0]SR;
  wire compl_done;
  wire \gen_cpl_64.state ;
  wire [63:0]m_axis_rx_tdata;
  wire m_axis_rx_tlast;
  wire m_axis_rx_tready;
  wire m_axis_rx_tvalid;
  wire pio_reset_n;
  wire \pio_rx_sm_64.in_packet_q ;
  wire \pio_rx_sm_64.in_packet_q_i_1_n_0 ;
  wire \pio_rx_sm_64.m_axis_rx_tready_i_1_n_0 ;
  wire \pio_rx_sm_64.m_axis_rx_tready_i_2_n_0 ;
  wire \pio_rx_sm_64.m_axis_rx_tready_i_3_n_0 ;
  wire \pio_rx_sm_64.m_axis_rx_tready_i_4_n_0 ;
  wire \pio_rx_sm_64.m_axis_rx_tready_i_5_n_0 ;
  wire \pio_rx_sm_64.m_axis_rx_tready_i_6_n_0 ;
  wire \pio_rx_sm_64.m_axis_rx_tready_i_7_n_0 ;
  wire \pio_rx_sm_64.m_axis_rx_tready_i_8_n_0 ;
  wire \pio_rx_sm_64.req_addr[6]_i_1_n_0 ;
  wire [4:0]\pio_rx_sm_64.req_addr_reg[6]_0 ;
  wire \pio_rx_sm_64.req_be[3]_i_1_n_0 ;
  wire [3:0]\pio_rx_sm_64.req_be_reg[3]_0 ;
  wire \pio_rx_sm_64.req_compl_i_1_n_0 ;
  wire \pio_rx_sm_64.req_compl_wd_i_1_n_0 ;
  wire [7:0]\pio_rx_sm_64.req_len_reg[7]_0 ;
  wire [12:0]\pio_rx_sm_64.req_rid_reg[15]_0 ;
  wire \pio_rx_sm_64.tlp_type[7]_i_1_n_0 ;
  wire \pio_rx_sm_64.tlp_type[7]_i_2_n_0 ;
  wire \pio_rx_sm_64.wr_data[31]_i_1_n_0 ;
  wire [6:2]req_addr1_in;
  wire [1:0]req_attr;
  wire req_compl;
  wire req_compl_q;
  wire req_compl_wd;
  wire req_ep;
  wire [9:8]req_len;
  wire [6:4]req_rid;
  wire [7:0]req_tag;
  wire [2:0]req_tc;
  wire req_td;
  wire [2:0]state;
  wire [7:0]tlp_type;
  wire trn_pending;
  wire trn_pending_reg;
  wire user_clk;
  wire [31:0]wr_data;
  wire [31:0]wr_data1_in;
  wire wr_en;

  LUT6 #(
    .INIT(64'hFFFFFFFFEFEEEEEE)) 
    \FSM_sequential_pio_rx_sm_64.state[0]_i_1 
       (.I0(\FSM_sequential_pio_rx_sm_64.state[0]_i_2_n_0 ),
        .I1(\FSM_sequential_pio_rx_sm_64.state[0]_i_3_n_0 ),
        .I2(\pio_rx_sm_64.m_axis_rx_tready_i_4_n_0 ),
        .I3(\FSM_sequential_pio_rx_sm_64.state[0]_i_4_n_0 ),
        .I4(\FSM_sequential_pio_rx_sm_64.state[1]_i_3_n_0 ),
        .I5(\FSM_sequential_pio_rx_sm_64.state[0]_i_5_n_0 ),
        .O(\FSM_sequential_pio_rx_sm_64.state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hE0F0F0F0)) 
    \FSM_sequential_pio_rx_sm_64.state[0]_i_2 
       (.I0(\pio_rx_sm_64.m_axis_rx_tready_i_7_n_0 ),
        .I1(\pio_rx_sm_64.m_axis_rx_tready_i_8_n_0 ),
        .I2(state[0]),
        .I3(state[1]),
        .I4(state[2]),
        .O(\FSM_sequential_pio_rx_sm_64.state[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_sequential_pio_rx_sm_64.state[0]_i_3 
       (.I0(m_axis_rx_tvalid),
        .I1(state[2]),
        .I2(state[1]),
        .O(\FSM_sequential_pio_rx_sm_64.state[0]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_pio_rx_sm_64.state[0]_i_4 
       (.I0(state[1]),
        .I1(m_axis_rx_tdata[30]),
        .O(\FSM_sequential_pio_rx_sm_64.state[0]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \FSM_sequential_pio_rx_sm_64.state[0]_i_5 
       (.I0(m_axis_rx_tvalid),
        .I1(state[1]),
        .I2(state[0]),
        .O(\FSM_sequential_pio_rx_sm_64.state[0]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBF8CBF8CBF8C)) 
    \FSM_sequential_pio_rx_sm_64.state[1]_i_1 
       (.I0(\FSM_sequential_pio_rx_sm_64.state[2]_i_2_n_0 ),
        .I1(state[1]),
        .I2(state[2]),
        .I3(\FSM_sequential_pio_rx_sm_64.state[1]_i_2_n_0 ),
        .I4(\FSM_sequential_pio_rx_sm_64.state[1]_i_3_n_0 ),
        .I5(\FSM_sequential_pio_rx_sm_64.state[1]_i_4_n_0 ),
        .O(\FSM_sequential_pio_rx_sm_64.state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_sequential_pio_rx_sm_64.state[1]_i_2 
       (.I0(state[0]),
        .I1(m_axis_rx_tvalid),
        .O(\FSM_sequential_pio_rx_sm_64.state[1]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'h0800)) 
    \FSM_sequential_pio_rx_sm_64.state[1]_i_3 
       (.I0(\FSM_sequential_pio_rx_sm_64.state[2]_i_6_n_0 ),
        .I1(\FSM_sequential_pio_rx_sm_64.state[2]_i_5_n_0 ),
        .I2(m_axis_rx_tdata[1]),
        .I3(m_axis_rx_tdata[0]),
        .O(\FSM_sequential_pio_rx_sm_64.state[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'h00100110)) 
    \FSM_sequential_pio_rx_sm_64.state[1]_i_4 
       (.I0(state[2]),
        .I1(\pio_rx_sm_64.tlp_type[7]_i_2_n_0 ),
        .I2(m_axis_rx_tdata[30]),
        .I3(m_axis_rx_tdata[29]),
        .I4(m_axis_rx_tdata[25]),
        .O(\FSM_sequential_pio_rx_sm_64.state[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFBBBBFFFFFC00)) 
    \FSM_sequential_pio_rx_sm_64.state[2]_i_1 
       (.I0(\FSM_sequential_pio_rx_sm_64.state[2]_i_2_n_0 ),
        .I1(state[1]),
        .I2(state[0]),
        .I3(m_axis_rx_tvalid),
        .I4(\FSM_sequential_pio_rx_sm_64.state[2]_i_3_n_0 ),
        .I5(state[2]),
        .O(\FSM_sequential_pio_rx_sm_64.state[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFEEEFFFFFFFF)) 
    \FSM_sequential_pio_rx_sm_64.state[2]_i_2 
       (.I0(tlp_type[7]),
        .I1(tlp_type[4]),
        .I2(tlp_type[5]),
        .I3(tlp_type[1]),
        .I4(\pio_rx_sm_64.m_axis_rx_tready_i_7_n_0 ),
        .I5(state[0]),
        .O(\FSM_sequential_pio_rx_sm_64.state[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h0040000000000000)) 
    \FSM_sequential_pio_rx_sm_64.state[2]_i_3 
       (.I0(\pio_rx_sm_64.tlp_type[7]_i_2_n_0 ),
        .I1(\FSM_sequential_pio_rx_sm_64.state[2]_i_4_n_0 ),
        .I2(m_axis_rx_tdata[0]),
        .I3(m_axis_rx_tdata[1]),
        .I4(\FSM_sequential_pio_rx_sm_64.state[2]_i_5_n_0 ),
        .I5(\FSM_sequential_pio_rx_sm_64.state[2]_i_6_n_0 ),
        .O(\FSM_sequential_pio_rx_sm_64.state[2]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0440)) 
    \FSM_sequential_pio_rx_sm_64.state[2]_i_4 
       (.I0(state[1]),
        .I1(m_axis_rx_tdata[30]),
        .I2(m_axis_rx_tdata[29]),
        .I3(m_axis_rx_tdata[25]),
        .O(\FSM_sequential_pio_rx_sm_64.state[2]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_sequential_pio_rx_sm_64.state[2]_i_5 
       (.I0(m_axis_rx_tdata[5]),
        .I1(m_axis_rx_tdata[4]),
        .I2(m_axis_rx_tdata[3]),
        .I3(m_axis_rx_tdata[2]),
        .O(\FSM_sequential_pio_rx_sm_64.state[2]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \FSM_sequential_pio_rx_sm_64.state[2]_i_6 
       (.I0(m_axis_rx_tdata[9]),
        .I1(m_axis_rx_tdata[8]),
        .I2(m_axis_rx_tdata[7]),
        .I3(m_axis_rx_tdata[6]),
        .O(\FSM_sequential_pio_rx_sm_64.state[2]_i_6_n_0 ));
  (* FSM_ENCODED_STATES = "PIO_RX_MEM_RD32_DW1DW2:001,PIO_RX_MEM_WR32_DW1DW2:010,PIO_RX_MEM_RD64_DW1DW2:011,PIO_RX_MEM_WR64_DW1DW2:100,PIO_RX_MEM_WR64_DW3:101,PIO_RX_IO_WR_DW1DW2:110,PIO_RX_WAIT_STATE:111,PIO_RX_RST_STATE:000" *) 
  FDRE \FSM_sequential_pio_rx_sm_64.state_reg[0] 
       (.C(user_clk),
        .CE(1'b1),
        .D(\FSM_sequential_pio_rx_sm_64.state[0]_i_1_n_0 ),
        .Q(state[0]),
        .R(SR));
  (* FSM_ENCODED_STATES = "PIO_RX_MEM_RD32_DW1DW2:001,PIO_RX_MEM_WR32_DW1DW2:010,PIO_RX_MEM_RD64_DW1DW2:011,PIO_RX_MEM_WR64_DW1DW2:100,PIO_RX_MEM_WR64_DW3:101,PIO_RX_IO_WR_DW1DW2:110,PIO_RX_WAIT_STATE:111,PIO_RX_RST_STATE:000" *) 
  FDRE \FSM_sequential_pio_rx_sm_64.state_reg[1] 
       (.C(user_clk),
        .CE(1'b1),
        .D(\FSM_sequential_pio_rx_sm_64.state[1]_i_1_n_0 ),
        .Q(state[1]),
        .R(SR));
  (* FSM_ENCODED_STATES = "PIO_RX_MEM_RD32_DW1DW2:001,PIO_RX_MEM_WR32_DW1DW2:010,PIO_RX_MEM_RD64_DW1DW2:011,PIO_RX_MEM_WR64_DW1DW2:100,PIO_RX_MEM_WR64_DW3:101,PIO_RX_IO_WR_DW1DW2:110,PIO_RX_WAIT_STATE:111,PIO_RX_RST_STATE:000" *) 
  FDRE \FSM_sequential_pio_rx_sm_64.state_reg[2] 
       (.C(user_clk),
        .CE(1'b1),
        .D(\FSM_sequential_pio_rx_sm_64.state[2]_i_1_n_0 ),
        .Q(state[2]),
        .R(SR));
  LUT5 #(
    .INIT(32'hAA00C000)) 
    \gen_cpl_64.s_axis_tx_tdata[12]_i_1 
       (.I0(req_tag[4]),
        .I1(req_attr[0]),
        .I2(req_compl_q),
        .I3(pio_reset_n),
        .I4(\gen_cpl_64.state ),
        .O(D[2]));
  LUT5 #(
    .INIT(32'hAA00C000)) 
    \gen_cpl_64.s_axis_tx_tdata[13]_i_1 
       (.I0(req_tag[5]),
        .I1(req_attr[1]),
        .I2(req_compl_q),
        .I3(pio_reset_n),
        .I4(\gen_cpl_64.state ),
        .O(D[3]));
  LUT5 #(
    .INIT(32'hAA00C000)) 
    \gen_cpl_64.s_axis_tx_tdata[14]_i_1 
       (.I0(req_tag[6]),
        .I1(req_ep),
        .I2(req_compl_q),
        .I3(pio_reset_n),
        .I4(\gen_cpl_64.state ),
        .O(D[4]));
  LUT5 #(
    .INIT(32'hAA00C000)) 
    \gen_cpl_64.s_axis_tx_tdata[15]_i_1 
       (.I0(req_tag[7]),
        .I1(req_td),
        .I2(req_compl_q),
        .I3(pio_reset_n),
        .I4(\gen_cpl_64.state ),
        .O(D[5]));
  LUT5 #(
    .INIT(32'hAA00C000)) 
    \gen_cpl_64.s_axis_tx_tdata[20]_i_1 
       (.I0(req_rid[4]),
        .I1(req_tc[0]),
        .I2(req_compl_q),
        .I3(pio_reset_n),
        .I4(\gen_cpl_64.state ),
        .O(D[6]));
  LUT5 #(
    .INIT(32'hAA00C000)) 
    \gen_cpl_64.s_axis_tx_tdata[21]_i_1 
       (.I0(req_rid[5]),
        .I1(req_tc[1]),
        .I2(req_compl_q),
        .I3(pio_reset_n),
        .I4(\gen_cpl_64.state ),
        .O(D[7]));
  LUT5 #(
    .INIT(32'hAA00C000)) 
    \gen_cpl_64.s_axis_tx_tdata[22]_i_1 
       (.I0(req_rid[6]),
        .I1(req_tc[2]),
        .I2(req_compl_q),
        .I3(pio_reset_n),
        .I4(\gen_cpl_64.state ),
        .O(D[8]));
  LUT5 #(
    .INIT(32'hAA00C000)) 
    \gen_cpl_64.s_axis_tx_tdata[8]_i_1 
       (.I0(req_tag[0]),
        .I1(req_len[8]),
        .I2(req_compl_q),
        .I3(pio_reset_n),
        .I4(\gen_cpl_64.state ),
        .O(D[0]));
  LUT5 #(
    .INIT(32'hAA00C000)) 
    \gen_cpl_64.s_axis_tx_tdata[9]_i_1 
       (.I0(req_tag[1]),
        .I1(req_len[9]),
        .I2(req_compl_q),
        .I3(pio_reset_n),
        .I4(\gen_cpl_64.state ),
        .O(D[1]));
  LUT1 #(
    .INIT(2'h1)) 
    \gen_cpl_64.s_axis_tx_tvalid_i_1 
       (.I0(pio_reset_n),
        .O(SR));
  LUT5 #(
    .INIT(32'h0C888888)) 
    \pio_rx_sm_64.in_packet_q_i_1 
       (.I0(\pio_rx_sm_64.in_packet_q ),
        .I1(pio_reset_n),
        .I2(m_axis_rx_tlast),
        .I3(m_axis_rx_tvalid),
        .I4(m_axis_rx_tready),
        .O(\pio_rx_sm_64.in_packet_q_i_1_n_0 ));
  FDRE \pio_rx_sm_64.in_packet_q_reg 
       (.C(user_clk),
        .CE(1'b1),
        .D(\pio_rx_sm_64.in_packet_q_i_1_n_0 ),
        .Q(\pio_rx_sm_64.in_packet_q ),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFEAFFFFFFEA00)) 
    \pio_rx_sm_64.m_axis_rx_tready_i_1 
       (.I0(\pio_rx_sm_64.m_axis_rx_tready_i_2_n_0 ),
        .I1(\pio_rx_sm_64.m_axis_rx_tready_i_3_n_0 ),
        .I2(\pio_rx_sm_64.m_axis_rx_tready_i_4_n_0 ),
        .I3(\pio_rx_sm_64.m_axis_rx_tready_i_5_n_0 ),
        .I4(\pio_rx_sm_64.m_axis_rx_tready_i_6_n_0 ),
        .I5(m_axis_rx_tready),
        .O(\pio_rx_sm_64.m_axis_rx_tready_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    \pio_rx_sm_64.m_axis_rx_tready_i_2 
       (.I0(m_axis_rx_tdata[30]),
        .I1(m_axis_rx_tdata[29]),
        .I2(state[2]),
        .I3(state[0]),
        .I4(state[1]),
        .O(\pio_rx_sm_64.m_axis_rx_tready_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \pio_rx_sm_64.m_axis_rx_tready_i_3 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(state[2]),
        .O(\pio_rx_sm_64.m_axis_rx_tready_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'hEA)) 
    \pio_rx_sm_64.m_axis_rx_tready_i_4 
       (.I0(\pio_rx_sm_64.tlp_type[7]_i_2_n_0 ),
        .I1(m_axis_rx_tdata[29]),
        .I2(m_axis_rx_tdata[25]),
        .O(\pio_rx_sm_64.m_axis_rx_tready_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F01)) 
    \pio_rx_sm_64.m_axis_rx_tready_i_5 
       (.I0(state[1]),
        .I1(state[2]),
        .I2(state[0]),
        .I3(m_axis_rx_tvalid),
        .O(\pio_rx_sm_64.m_axis_rx_tready_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'h02000000)) 
    \pio_rx_sm_64.m_axis_rx_tready_i_6 
       (.I0(state[0]),
        .I1(\pio_rx_sm_64.m_axis_rx_tready_i_7_n_0 ),
        .I2(\pio_rx_sm_64.m_axis_rx_tready_i_8_n_0 ),
        .I3(state[2]),
        .I4(state[1]),
        .O(\pio_rx_sm_64.m_axis_rx_tready_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFF1)) 
    \pio_rx_sm_64.m_axis_rx_tready_i_7 
       (.I0(tlp_type[6]),
        .I1(compl_done),
        .I2(tlp_type[0]),
        .I3(tlp_type[3]),
        .I4(tlp_type[2]),
        .O(\pio_rx_sm_64.m_axis_rx_tready_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFEEE)) 
    \pio_rx_sm_64.m_axis_rx_tready_i_8 
       (.I0(tlp_type[7]),
        .I1(tlp_type[4]),
        .I2(tlp_type[5]),
        .I3(tlp_type[1]),
        .O(\pio_rx_sm_64.m_axis_rx_tready_i_8_n_0 ));
  FDRE \pio_rx_sm_64.m_axis_rx_tready_reg 
       (.C(user_clk),
        .CE(1'b1),
        .D(\pio_rx_sm_64.m_axis_rx_tready_i_1_n_0 ),
        .Q(m_axis_rx_tready),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \pio_rx_sm_64.req_addr[2]_i_1 
       (.I0(m_axis_rx_tdata[34]),
        .I1(state[1]),
        .I2(m_axis_rx_tdata[2]),
        .O(req_addr1_in[2]));
  LUT3 #(
    .INIT(8'hB8)) 
    \pio_rx_sm_64.req_addr[3]_i_1 
       (.I0(m_axis_rx_tdata[35]),
        .I1(state[1]),
        .I2(m_axis_rx_tdata[3]),
        .O(req_addr1_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \pio_rx_sm_64.req_addr[4]_i_1 
       (.I0(m_axis_rx_tdata[36]),
        .I1(state[1]),
        .I2(m_axis_rx_tdata[4]),
        .O(req_addr1_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \pio_rx_sm_64.req_addr[5]_i_1 
       (.I0(m_axis_rx_tdata[37]),
        .I1(state[1]),
        .I2(m_axis_rx_tdata[5]),
        .O(req_addr1_in[5]));
  LUT3 #(
    .INIT(8'h08)) 
    \pio_rx_sm_64.req_addr[6]_i_1 
       (.I0(m_axis_rx_tvalid),
        .I1(state[0]),
        .I2(state[2]),
        .O(\pio_rx_sm_64.req_addr[6]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \pio_rx_sm_64.req_addr[6]_i_2 
       (.I0(m_axis_rx_tdata[38]),
        .I1(state[1]),
        .I2(m_axis_rx_tdata[6]),
        .O(req_addr1_in[6]));
  FDRE \pio_rx_sm_64.req_addr_reg[2] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_addr[6]_i_1_n_0 ),
        .D(req_addr1_in[2]),
        .Q(\pio_rx_sm_64.req_addr_reg[6]_0 [0]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_addr_reg[3] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_addr[6]_i_1_n_0 ),
        .D(req_addr1_in[3]),
        .Q(\pio_rx_sm_64.req_addr_reg[6]_0 [1]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_addr_reg[4] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_addr[6]_i_1_n_0 ),
        .D(req_addr1_in[4]),
        .Q(\pio_rx_sm_64.req_addr_reg[6]_0 [2]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_addr_reg[5] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_addr[6]_i_1_n_0 ),
        .D(req_addr1_in[5]),
        .Q(\pio_rx_sm_64.req_addr_reg[6]_0 [3]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_addr_reg[6] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_addr[6]_i_1_n_0 ),
        .D(req_addr1_in[6]),
        .Q(\pio_rx_sm_64.req_addr_reg[6]_0 [4]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_attr_reg[0] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[12]),
        .Q(req_attr[0]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_attr_reg[1] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[13]),
        .Q(req_attr[1]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0004440400000000)) 
    \pio_rx_sm_64.req_be[3]_i_1 
       (.I0(\pio_rx_sm_64.tlp_type[7]_i_2_n_0 ),
        .I1(\pio_rx_sm_64.m_axis_rx_tready_i_3_n_0 ),
        .I2(m_axis_rx_tdata[30]),
        .I3(m_axis_rx_tdata[25]),
        .I4(m_axis_rx_tdata[29]),
        .I5(\FSM_sequential_pio_rx_sm_64.state[1]_i_3_n_0 ),
        .O(\pio_rx_sm_64.req_be[3]_i_1_n_0 ));
  FDRE \pio_rx_sm_64.req_be_reg[0] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[32]),
        .Q(\pio_rx_sm_64.req_be_reg[3]_0 [0]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_be_reg[1] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[33]),
        .Q(\pio_rx_sm_64.req_be_reg[3]_0 [1]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_be_reg[2] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[34]),
        .Q(\pio_rx_sm_64.req_be_reg[3]_0 [2]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_be_reg[3] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[35]),
        .Q(\pio_rx_sm_64.req_be_reg[3]_0 [3]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h3800)) 
    \pio_rx_sm_64.req_compl_i_1 
       (.I0(state[1]),
        .I1(state[2]),
        .I2(state[0]),
        .I3(m_axis_rx_tvalid),
        .O(\pio_rx_sm_64.req_compl_i_1_n_0 ));
  FDRE \pio_rx_sm_64.req_compl_reg 
       (.C(user_clk),
        .CE(1'b1),
        .D(\pio_rx_sm_64.req_compl_i_1_n_0 ),
        .Q(req_compl),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hF7FF2023)) 
    \pio_rx_sm_64.req_compl_wd_i_1 
       (.I0(m_axis_rx_tvalid),
        .I1(state[2]),
        .I2(state[0]),
        .I3(state[1]),
        .I4(req_compl_wd),
        .O(\pio_rx_sm_64.req_compl_wd_i_1_n_0 ));
  FDSE \pio_rx_sm_64.req_compl_wd_reg 
       (.C(user_clk),
        .CE(1'b1),
        .D(\pio_rx_sm_64.req_compl_wd_i_1_n_0 ),
        .Q(req_compl_wd),
        .S(SR));
  FDRE \pio_rx_sm_64.req_ep_reg 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[14]),
        .Q(req_ep),
        .R(SR));
  FDRE \pio_rx_sm_64.req_len_reg[0] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[0]),
        .Q(\pio_rx_sm_64.req_len_reg[7]_0 [0]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_len_reg[1] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[1]),
        .Q(\pio_rx_sm_64.req_len_reg[7]_0 [1]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_len_reg[2] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[2]),
        .Q(\pio_rx_sm_64.req_len_reg[7]_0 [2]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_len_reg[3] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[3]),
        .Q(\pio_rx_sm_64.req_len_reg[7]_0 [3]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_len_reg[4] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[4]),
        .Q(\pio_rx_sm_64.req_len_reg[7]_0 [4]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_len_reg[5] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[5]),
        .Q(\pio_rx_sm_64.req_len_reg[7]_0 [5]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_len_reg[6] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[6]),
        .Q(\pio_rx_sm_64.req_len_reg[7]_0 [6]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_len_reg[7] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[7]),
        .Q(\pio_rx_sm_64.req_len_reg[7]_0 [7]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_len_reg[8] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[8]),
        .Q(req_len[8]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_len_reg[9] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[9]),
        .Q(req_len[9]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[0] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[48]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [0]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[10] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[58]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [7]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[11] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[59]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [8]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[12] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[60]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [9]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[13] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[61]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [10]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[14] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[62]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [11]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[15] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[63]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [12]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[1] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[49]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [1]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[2] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[50]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [2]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[3] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[51]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [3]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[4] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[52]),
        .Q(req_rid[4]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[5] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[53]),
        .Q(req_rid[5]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[6] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[54]),
        .Q(req_rid[6]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[7] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[55]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [4]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[8] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[56]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [5]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_rid_reg[9] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[57]),
        .Q(\pio_rx_sm_64.req_rid_reg[15]_0 [6]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_tag_reg[0] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[40]),
        .Q(req_tag[0]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_tag_reg[1] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[41]),
        .Q(req_tag[1]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_tag_reg[2] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[42]),
        .Q(Q[0]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_tag_reg[3] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[43]),
        .Q(Q[1]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_tag_reg[4] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[44]),
        .Q(req_tag[4]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_tag_reg[5] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[45]),
        .Q(req_tag[5]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_tag_reg[6] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[46]),
        .Q(req_tag[6]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_tag_reg[7] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[47]),
        .Q(req_tag[7]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_tc_reg[0] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[20]),
        .Q(req_tc[0]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_tc_reg[1] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[21]),
        .Q(req_tc[1]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_tc_reg[2] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[22]),
        .Q(req_tc[2]),
        .R(SR));
  FDRE \pio_rx_sm_64.req_td_reg 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.req_be[3]_i_1_n_0 ),
        .D(m_axis_rx_tdata[15]),
        .Q(req_td),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000000000010101)) 
    \pio_rx_sm_64.tlp_type[7]_i_1 
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[25]),
        .I4(m_axis_rx_tdata[29]),
        .I5(\pio_rx_sm_64.tlp_type[7]_i_2_n_0 ),
        .O(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFB)) 
    \pio_rx_sm_64.tlp_type[7]_i_2 
       (.I0(\pio_rx_sm_64.in_packet_q ),
        .I1(m_axis_rx_tvalid),
        .I2(m_axis_rx_tdata[27]),
        .I3(m_axis_rx_tdata[28]),
        .I4(m_axis_rx_tdata[26]),
        .I5(m_axis_rx_tdata[24]),
        .O(\pio_rx_sm_64.tlp_type[7]_i_2_n_0 ));
  FDRE \pio_rx_sm_64.tlp_type_reg[0] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[24]),
        .Q(tlp_type[0]),
        .R(SR));
  FDRE \pio_rx_sm_64.tlp_type_reg[1] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[25]),
        .Q(tlp_type[1]),
        .R(SR));
  FDRE \pio_rx_sm_64.tlp_type_reg[2] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[26]),
        .Q(tlp_type[2]),
        .R(SR));
  FDRE \pio_rx_sm_64.tlp_type_reg[3] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[27]),
        .Q(tlp_type[3]),
        .R(SR));
  FDRE \pio_rx_sm_64.tlp_type_reg[4] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[28]),
        .Q(tlp_type[4]),
        .R(SR));
  FDRE \pio_rx_sm_64.tlp_type_reg[5] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[29]),
        .Q(tlp_type[5]),
        .R(SR));
  FDRE \pio_rx_sm_64.tlp_type_reg[6] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[30]),
        .Q(tlp_type[6]),
        .R(SR));
  FDRE \pio_rx_sm_64.tlp_type_reg[7] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.tlp_type[7]_i_1_n_0 ),
        .D(m_axis_rx_tdata[31]),
        .Q(tlp_type[7]),
        .R(SR));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[0]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[0]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[32]),
        .O(wr_data1_in[0]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[10]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[10]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[42]),
        .O(wr_data1_in[10]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[11]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[11]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[43]),
        .O(wr_data1_in[11]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[12]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[12]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[44]),
        .O(wr_data1_in[12]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[13]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[13]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[45]),
        .O(wr_data1_in[13]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[14]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[14]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[46]),
        .O(wr_data1_in[14]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[15]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[15]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[47]),
        .O(wr_data1_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[16]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[16]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[48]),
        .O(wr_data1_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[17]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[17]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[49]),
        .O(wr_data1_in[17]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[18]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[18]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[50]),
        .O(wr_data1_in[18]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[19]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[19]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[51]),
        .O(wr_data1_in[19]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[1]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[1]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[33]),
        .O(wr_data1_in[1]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[20]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[20]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[52]),
        .O(wr_data1_in[20]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[21]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[21]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[53]),
        .O(wr_data1_in[21]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[22]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[22]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[54]),
        .O(wr_data1_in[22]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[23]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[23]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[55]),
        .O(wr_data1_in[23]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[24]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[24]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[56]),
        .O(wr_data1_in[24]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[25]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[25]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[57]),
        .O(wr_data1_in[25]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[26]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[26]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[58]),
        .O(wr_data1_in[26]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[27]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[27]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[59]),
        .O(wr_data1_in[27]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[28]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[28]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[60]),
        .O(wr_data1_in[28]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[29]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[29]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[61]),
        .O(wr_data1_in[29]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[2]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[2]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[34]),
        .O(wr_data1_in[2]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[30]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[30]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[62]),
        .O(wr_data1_in[30]));
  LUT4 #(
    .INIT(16'h4480)) 
    \pio_rx_sm_64.wr_data[31]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tvalid),
        .I2(state[2]),
        .I3(state[1]),
        .O(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[31]_i_2 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[31]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[63]),
        .O(wr_data1_in[31]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[3]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[3]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[35]),
        .O(wr_data1_in[3]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[4]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[4]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[36]),
        .O(wr_data1_in[4]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[5]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[5]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[37]),
        .O(wr_data1_in[5]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[6]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[6]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[38]),
        .O(wr_data1_in[6]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[7]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[7]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[39]),
        .O(wr_data1_in[7]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[8]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[8]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[40]),
        .O(wr_data1_in[8]));
  LUT4 #(
    .INIT(16'hF888)) 
    \pio_rx_sm_64.wr_data[9]_i_1 
       (.I0(state[0]),
        .I1(m_axis_rx_tdata[9]),
        .I2(state[1]),
        .I3(m_axis_rx_tdata[41]),
        .O(wr_data1_in[9]));
  FDRE \pio_rx_sm_64.wr_data_reg[0] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[0]),
        .Q(wr_data[0]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[10] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[10]),
        .Q(wr_data[10]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[11] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[11]),
        .Q(wr_data[11]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[12] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[12]),
        .Q(wr_data[12]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[13] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[13]),
        .Q(wr_data[13]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[14] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[14]),
        .Q(wr_data[14]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[15] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[15]),
        .Q(wr_data[15]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[16] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[16]),
        .Q(wr_data[16]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[17] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[17]),
        .Q(wr_data[17]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[18] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[18]),
        .Q(wr_data[18]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[19] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[19]),
        .Q(wr_data[19]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[1] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[1]),
        .Q(wr_data[1]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[20] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[20]),
        .Q(wr_data[20]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[21] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[21]),
        .Q(wr_data[21]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[22] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[22]),
        .Q(wr_data[22]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[23] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[23]),
        .Q(wr_data[23]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[24] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[24]),
        .Q(wr_data[24]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[25] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[25]),
        .Q(wr_data[25]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[26] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[26]),
        .Q(wr_data[26]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[27] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[27]),
        .Q(wr_data[27]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[28] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[28]),
        .Q(wr_data[28]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[29] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[29]),
        .Q(wr_data[29]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[2] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[2]),
        .Q(wr_data[2]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[30] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[30]),
        .Q(wr_data[30]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[31] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[31]),
        .Q(wr_data[31]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[3] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[3]),
        .Q(wr_data[3]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[4] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[4]),
        .Q(wr_data[4]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[5] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[5]),
        .Q(wr_data[5]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[6] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[6]),
        .Q(wr_data[6]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[7] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[7]),
        .Q(wr_data[7]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[8] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[8]),
        .Q(wr_data[8]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_data_reg[9] 
       (.C(user_clk),
        .CE(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .D(wr_data1_in[9]),
        .Q(wr_data[9]),
        .R(SR));
  FDRE \pio_rx_sm_64.wr_en_reg 
       (.C(user_clk),
        .CE(1'b1),
        .D(\pio_rx_sm_64.wr_data[31]_i_1_n_0 ),
        .Q(wr_en),
        .R(SR));
  LUT3 #(
    .INIT(8'h4E)) 
    trn_pending_i_1
       (.I0(trn_pending),
        .I1(req_compl),
        .I2(compl_done),
        .O(trn_pending_reg));
endmodule

(* ORIG_REF_NAME = "PIO_TO_CTRL" *) 
module spec7_pcie_app_7x_0_PIO_TO_CTRL
   (cfg_turnoff_ok,
    trn_pending,
    SR,
    user_clk,
    trn_pending_reg_0,
    cfg_to_turnoff);
  output cfg_turnoff_ok;
  output trn_pending;
  input [0:0]SR;
  input user_clk;
  input trn_pending_reg_0;
  input cfg_to_turnoff;

  wire [0:0]SR;
  wire cfg_to_turnoff;
  wire cfg_turnoff_ok;
  wire cfg_turnoff_ok_i_1_n_0;
  wire trn_pending;
  wire trn_pending_reg_0;
  wire user_clk;

  LUT2 #(
    .INIT(4'h2)) 
    cfg_turnoff_ok_i_1
       (.I0(cfg_to_turnoff),
        .I1(trn_pending),
        .O(cfg_turnoff_ok_i_1_n_0));
  FDRE cfg_turnoff_ok_reg
       (.C(user_clk),
        .CE(1'b1),
        .D(cfg_turnoff_ok_i_1_n_0),
        .Q(cfg_turnoff_ok),
        .R(SR));
  FDRE trn_pending_reg
       (.C(user_clk),
        .CE(1'b1),
        .D(trn_pending_reg_0),
        .Q(trn_pending),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "PIO_TX_ENGINE" *) 
module spec7_pcie_app_7x_0_PIO_TX_ENGINE
   (req_compl_q,
    \gen_cpl_64.state ,
    s_axis_tx_tlast,
    compl_done,
    s_axis_tx_tdata,
    s_axis_tx_tkeep,
    s_axis_tx_tvalid,
    SR,
    req_compl,
    user_clk,
    req_compl_wd,
    s_axis_tx_tready,
    pio_reset_n,
    D,
    \gen_cpl_64.s_axis_tx_tdata_reg[7]_0 ,
    \gen_cpl_64.s_axis_tx_tdata_reg[6]_0 ,
    \gen_cpl_64.s_axis_tx_tdata_reg[31]_0 ,
    \rd_be_reg[3]_0 ,
    Q,
    cfg_bus_number,
    cfg_device_number,
    cfg_function_number);
  output req_compl_q;
  output \gen_cpl_64.state ;
  output s_axis_tx_tlast;
  output compl_done;
  output [50:0]s_axis_tx_tdata;
  output [1:0]s_axis_tx_tkeep;
  output s_axis_tx_tvalid;
  input [0:0]SR;
  input req_compl;
  input user_clk;
  input req_compl_wd;
  input s_axis_tx_tready;
  input pio_reset_n;
  input [8:0]D;
  input [7:0]\gen_cpl_64.s_axis_tx_tdata_reg[7]_0 ;
  input [4:0]\gen_cpl_64.s_axis_tx_tdata_reg[6]_0 ;
  input [12:0]\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 ;
  input [3:0]\rd_be_reg[3]_0 ;
  input [1:0]Q;
  input [7:0]cfg_bus_number;
  input [4:0]cfg_device_number;
  input [2:0]cfg_function_number;

  wire [8:0]D;
  wire [1:0]Q;
  wire [0:0]SR;
  wire [7:0]cfg_bus_number;
  wire [4:0]cfg_device_number;
  wire [2:0]cfg_function_number;
  wire compl_done;
  wire \gen_cpl_64.compl_done_i_1_n_0 ;
  wire \gen_cpl_64.s_axis_tx_tdata[0]_i_2_n_0 ;
  wire \gen_cpl_64.s_axis_tx_tdata[1]_i_2_n_0 ;
  wire \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ;
  wire \gen_cpl_64.s_axis_tx_tdata[33]_i_2_n_0 ;
  wire \gen_cpl_64.s_axis_tx_tdata[34]_i_1_n_0 ;
  wire \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ;
  wire \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ;
  wire [12:0]\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 ;
  wire [4:0]\gen_cpl_64.s_axis_tx_tdata_reg[6]_0 ;
  wire [7:0]\gen_cpl_64.s_axis_tx_tdata_reg[7]_0 ;
  wire \gen_cpl_64.s_axis_tx_tkeep[7]_i_1_n_0 ;
  wire \gen_cpl_64.s_axis_tx_tlast_i_1_n_0 ;
  wire \gen_cpl_64.s_axis_tx_tvalid_i_2_n_0 ;
  wire \gen_cpl_64.state ;
  wire \gen_cpl_64.state_i_1_n_0 ;
  wire [33:0]p_0_in;
  wire pio_reset_n;
  wire [3:0]\rd_be_reg[3]_0 ;
  wire req_compl;
  wire req_compl_q;
  wire req_compl_wd;
  wire [50:0]s_axis_tx_tdata;
  wire [1:0]s_axis_tx_tkeep;
  wire s_axis_tx_tlast;
  wire s_axis_tx_tready;
  wire s_axis_tx_tvalid;
  wire [4:0]sel0;
  wire user_clk;

  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hC8C8C000)) 
    \gen_cpl_64.compl_done_i_1 
       (.I0(req_compl_q),
        .I1(pio_reset_n),
        .I2(\gen_cpl_64.state ),
        .I3(s_axis_tx_tready),
        .I4(compl_done),
        .O(\gen_cpl_64.compl_done_i_1_n_0 ));
  FDRE \gen_cpl_64.compl_done_reg 
       (.C(user_clk),
        .CE(1'b1),
        .D(\gen_cpl_64.compl_done_i_1_n_0 ),
        .Q(compl_done),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFFF400F400F400)) 
    \gen_cpl_64.s_axis_tx_tdata[0]_i_1 
       (.I0(sel0[2]),
        .I1(sel0[3]),
        .I2(sel0[1]),
        .I3(\gen_cpl_64.s_axis_tx_tdata[0]_i_2_n_0 ),
        .I4(\gen_cpl_64.s_axis_tx_tdata_reg[7]_0 [0]),
        .I5(\gen_cpl_64.s_axis_tx_tdata[33]_i_2_n_0 ),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT4 #(
    .INIT(16'h0080)) 
    \gen_cpl_64.s_axis_tx_tdata[0]_i_2 
       (.I0(pio_reset_n),
        .I1(\gen_cpl_64.state ),
        .I2(sel0[4]),
        .I3(sel0[0]),
        .O(\gen_cpl_64.s_axis_tx_tdata[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88880000F0000000)) 
    \gen_cpl_64.s_axis_tx_tdata[1]_i_1 
       (.I0(sel0[4]),
        .I1(\gen_cpl_64.s_axis_tx_tdata[1]_i_2_n_0 ),
        .I2(\gen_cpl_64.s_axis_tx_tdata_reg[7]_0 [1]),
        .I3(req_compl_q),
        .I4(pio_reset_n),
        .I5(\gen_cpl_64.state ),
        .O(p_0_in[1]));
  LUT4 #(
    .INIT(16'h1110)) 
    \gen_cpl_64.s_axis_tx_tdata[1]_i_2 
       (.I0(sel0[1]),
        .I1(sel0[0]),
        .I2(sel0[3]),
        .I3(sel0[2]),
        .O(\gen_cpl_64.s_axis_tx_tdata[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'hB080)) 
    \gen_cpl_64.s_axis_tx_tdata[25]_i_1 
       (.I0(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [6]),
        .I1(\gen_cpl_64.state ),
        .I2(pio_reset_n),
        .I3(req_compl_q),
        .O(p_0_in[25]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT4 #(
    .INIT(16'hB080)) 
    \gen_cpl_64.s_axis_tx_tdata[27]_i_1 
       (.I0(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [8]),
        .I1(\gen_cpl_64.state ),
        .I2(pio_reset_n),
        .I3(req_compl_q),
        .O(p_0_in[27]));
  LUT6 #(
    .INIT(64'h88880000F0000000)) 
    \gen_cpl_64.s_axis_tx_tdata[2]_i_1 
       (.I0(\gen_cpl_64.s_axis_tx_tdata_reg[6]_0 [0]),
        .I1(sel0[4]),
        .I2(\gen_cpl_64.s_axis_tx_tdata_reg[7]_0 [2]),
        .I3(req_compl_q),
        .I4(pio_reset_n),
        .I5(\gen_cpl_64.state ),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT5 #(
    .INIT(32'hAA00C000)) 
    \gen_cpl_64.s_axis_tx_tdata[30]_i_1 
       (.I0(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [11]),
        .I1(sel0[4]),
        .I2(req_compl_q),
        .I3(pio_reset_n),
        .I4(\gen_cpl_64.state ),
        .O(p_0_in[30]));
  LUT2 #(
    .INIT(4'h7)) 
    \gen_cpl_64.s_axis_tx_tdata[31]_i_1 
       (.I0(pio_reset_n),
        .I1(\gen_cpl_64.state ),
        .O(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h51009F00)) 
    \gen_cpl_64.s_axis_tx_tdata[32]_i_1 
       (.I0(sel0[3]),
        .I1(sel0[1]),
        .I2(sel0[2]),
        .I3(\gen_cpl_64.s_axis_tx_tdata[33]_i_2_n_0 ),
        .I4(sel0[0]),
        .O(p_0_in[32]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT5 #(
    .INIT(32'h0EE80000)) 
    \gen_cpl_64.s_axis_tx_tdata[33]_i_1 
       (.I0(sel0[1]),
        .I1(sel0[2]),
        .I2(sel0[3]),
        .I3(sel0[0]),
        .I4(\gen_cpl_64.s_axis_tx_tdata[33]_i_2_n_0 ),
        .O(p_0_in[33]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h08)) 
    \gen_cpl_64.s_axis_tx_tdata[33]_i_2 
       (.I0(req_compl_q),
        .I1(pio_reset_n),
        .I2(\gen_cpl_64.state ),
        .O(\gen_cpl_64.s_axis_tx_tdata[33]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \gen_cpl_64.s_axis_tx_tdata[34]_i_1 
       (.I0(sel0[0]),
        .I1(sel0[3]),
        .O(\gen_cpl_64.s_axis_tx_tdata[34]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h88880000F0000000)) 
    \gen_cpl_64.s_axis_tx_tdata[3]_i_1 
       (.I0(\gen_cpl_64.s_axis_tx_tdata_reg[6]_0 [1]),
        .I1(sel0[4]),
        .I2(\gen_cpl_64.s_axis_tx_tdata_reg[7]_0 [3]),
        .I3(req_compl_q),
        .I4(pio_reset_n),
        .I5(\gen_cpl_64.state ),
        .O(p_0_in[3]));
  LUT6 #(
    .INIT(64'h88880000F0000000)) 
    \gen_cpl_64.s_axis_tx_tdata[4]_i_1 
       (.I0(\gen_cpl_64.s_axis_tx_tdata_reg[6]_0 [2]),
        .I1(sel0[4]),
        .I2(\gen_cpl_64.s_axis_tx_tdata_reg[7]_0 [4]),
        .I3(req_compl_q),
        .I4(pio_reset_n),
        .I5(\gen_cpl_64.state ),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h88880000F0000000)) 
    \gen_cpl_64.s_axis_tx_tdata[5]_i_1 
       (.I0(\gen_cpl_64.s_axis_tx_tdata_reg[6]_0 [3]),
        .I1(sel0[4]),
        .I2(\gen_cpl_64.s_axis_tx_tdata_reg[7]_0 [5]),
        .I3(req_compl_q),
        .I4(pio_reset_n),
        .I5(\gen_cpl_64.state ),
        .O(p_0_in[5]));
  LUT4 #(
    .INIT(16'h8FBF)) 
    \gen_cpl_64.s_axis_tx_tdata[63]_i_1 
       (.I0(s_axis_tx_tready),
        .I1(\gen_cpl_64.state ),
        .I2(pio_reset_n),
        .I3(req_compl_q),
        .O(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hF7)) 
    \gen_cpl_64.s_axis_tx_tdata[63]_i_2 
       (.I0(\gen_cpl_64.state ),
        .I1(pio_reset_n),
        .I2(s_axis_tx_tready),
        .O(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h88880000F0000000)) 
    \gen_cpl_64.s_axis_tx_tdata[6]_i_1 
       (.I0(\gen_cpl_64.s_axis_tx_tdata_reg[6]_0 [4]),
        .I1(sel0[4]),
        .I2(\gen_cpl_64.s_axis_tx_tdata_reg[7]_0 [6]),
        .I3(req_compl_q),
        .I4(pio_reset_n),
        .I5(\gen_cpl_64.state ),
        .O(p_0_in[6]));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[0] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[0]),
        .Q(s_axis_tx_tdata[0]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[10] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(Q[0]),
        .Q(s_axis_tx_tdata[10]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[11] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(Q[1]),
        .Q(s_axis_tx_tdata[11]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[12] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(D[2]),
        .Q(s_axis_tx_tdata[12]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[13] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(D[3]),
        .Q(s_axis_tx_tdata[13]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[14] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(D[4]),
        .Q(s_axis_tx_tdata[14]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[15] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(D[5]),
        .Q(s_axis_tx_tdata[15]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[16] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [0]),
        .Q(s_axis_tx_tdata[16]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[17] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [1]),
        .Q(s_axis_tx_tdata[17]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[18] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [2]),
        .Q(s_axis_tx_tdata[18]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[19] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [3]),
        .Q(s_axis_tx_tdata[19]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[1] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[1]),
        .Q(s_axis_tx_tdata[1]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[20] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(D[6]),
        .Q(s_axis_tx_tdata[20]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[21] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(D[7]),
        .Q(s_axis_tx_tdata[21]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[22] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(D[8]),
        .Q(s_axis_tx_tdata[22]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[23] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [4]),
        .Q(s_axis_tx_tdata[23]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[24] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [5]),
        .Q(s_axis_tx_tdata[24]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[25] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[25]),
        .Q(s_axis_tx_tdata[25]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[26] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [7]),
        .Q(s_axis_tx_tdata[26]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[27] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[27]),
        .Q(s_axis_tx_tdata[27]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[28] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [9]),
        .Q(s_axis_tx_tdata[28]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[29] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [10]),
        .Q(s_axis_tx_tdata[29]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[2] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[2]),
        .Q(s_axis_tx_tdata[2]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[30] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[30]),
        .Q(s_axis_tx_tdata[30]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[31] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata_reg[31]_0 [12]),
        .Q(s_axis_tx_tdata[31]),
        .R(\gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[32] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[32]),
        .Q(s_axis_tx_tdata[32]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[33] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[33]),
        .Q(s_axis_tx_tdata[33]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[34] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata[34]_i_1_n_0 ),
        .Q(s_axis_tx_tdata[34]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[3] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[3]),
        .Q(s_axis_tx_tdata[3]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[48] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_function_number[0]),
        .Q(s_axis_tx_tdata[35]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[49] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_function_number[1]),
        .Q(s_axis_tx_tdata[36]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[4] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[4]),
        .Q(s_axis_tx_tdata[4]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[50] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_function_number[2]),
        .Q(s_axis_tx_tdata[37]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[51] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_device_number[0]),
        .Q(s_axis_tx_tdata[38]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[52] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_device_number[1]),
        .Q(s_axis_tx_tdata[39]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[53] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_device_number[2]),
        .Q(s_axis_tx_tdata[40]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[54] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_device_number[3]),
        .Q(s_axis_tx_tdata[41]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[55] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_device_number[4]),
        .Q(s_axis_tx_tdata[42]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[56] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_bus_number[0]),
        .Q(s_axis_tx_tdata[43]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[57] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_bus_number[1]),
        .Q(s_axis_tx_tdata[44]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[58] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_bus_number[2]),
        .Q(s_axis_tx_tdata[45]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[59] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_bus_number[3]),
        .Q(s_axis_tx_tdata[46]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[5] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[5]),
        .Q(s_axis_tx_tdata[5]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[60] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_bus_number[4]),
        .Q(s_axis_tx_tdata[47]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[61] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_bus_number[5]),
        .Q(s_axis_tx_tdata[48]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[62] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_bus_number[6]),
        .Q(s_axis_tx_tdata[49]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[63] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(cfg_bus_number[7]),
        .Q(s_axis_tx_tdata[50]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[6] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(p_0_in[6]),
        .Q(s_axis_tx_tdata[6]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[7] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tdata_reg[7]_0 [7]),
        .Q(s_axis_tx_tdata[7]),
        .R(\gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[8] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(D[0]),
        .Q(s_axis_tx_tdata[8]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tdata_reg[9] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(D[1]),
        .Q(s_axis_tx_tdata[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB0)) 
    \gen_cpl_64.s_axis_tx_tkeep[7]_i_1 
       (.I0(sel0[4]),
        .I1(\gen_cpl_64.state ),
        .I2(pio_reset_n),
        .O(\gen_cpl_64.s_axis_tx_tkeep[7]_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tkeep_reg[3] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(pio_reset_n),
        .Q(s_axis_tx_tkeep[0]),
        .R(1'b0));
  FDRE \gen_cpl_64.s_axis_tx_tkeep_reg[7] 
       (.C(user_clk),
        .CE(\gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0 ),
        .D(\gen_cpl_64.s_axis_tx_tkeep[7]_i_1_n_0 ),
        .Q(s_axis_tx_tkeep[1]),
        .R(1'b0));
  LUT4 #(
    .INIT(16'hE000)) 
    \gen_cpl_64.s_axis_tx_tlast_i_1 
       (.I0(s_axis_tx_tlast),
        .I1(s_axis_tx_tready),
        .I2(\gen_cpl_64.state ),
        .I3(pio_reset_n),
        .O(\gen_cpl_64.s_axis_tx_tlast_i_1_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tlast_reg 
       (.C(user_clk),
        .CE(1'b1),
        .D(\gen_cpl_64.s_axis_tx_tlast_i_1_n_0 ),
        .Q(s_axis_tx_tlast),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT4 #(
    .INIT(16'hFACA)) 
    \gen_cpl_64.s_axis_tx_tvalid_i_2 
       (.I0(req_compl_q),
        .I1(s_axis_tx_tready),
        .I2(\gen_cpl_64.state ),
        .I3(s_axis_tx_tvalid),
        .O(\gen_cpl_64.s_axis_tx_tvalid_i_2_n_0 ));
  FDRE \gen_cpl_64.s_axis_tx_tvalid_reg 
       (.C(user_clk),
        .CE(1'b1),
        .D(\gen_cpl_64.s_axis_tx_tvalid_i_2_n_0 ),
        .Q(s_axis_tx_tvalid),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h4A)) 
    \gen_cpl_64.state_i_1 
       (.I0(\gen_cpl_64.state ),
        .I1(req_compl_q),
        .I2(s_axis_tx_tready),
        .O(\gen_cpl_64.state_i_1_n_0 ));
  FDRE \gen_cpl_64.state_reg 
       (.C(user_clk),
        .CE(1'b1),
        .D(\gen_cpl_64.state_i_1_n_0 ),
        .Q(\gen_cpl_64.state ),
        .R(SR));
  FDRE \rd_be_reg[0] 
       (.C(user_clk),
        .CE(1'b1),
        .D(\rd_be_reg[3]_0 [0]),
        .Q(sel0[0]),
        .R(SR));
  FDRE \rd_be_reg[1] 
       (.C(user_clk),
        .CE(1'b1),
        .D(\rd_be_reg[3]_0 [1]),
        .Q(sel0[1]),
        .R(SR));
  FDRE \rd_be_reg[2] 
       (.C(user_clk),
        .CE(1'b1),
        .D(\rd_be_reg[3]_0 [2]),
        .Q(sel0[2]),
        .R(SR));
  FDRE \rd_be_reg[3] 
       (.C(user_clk),
        .CE(1'b1),
        .D(\rd_be_reg[3]_0 [3]),
        .Q(sel0[3]),
        .R(SR));
  FDRE req_compl_q_reg
       (.C(user_clk),
        .CE(1'b1),
        .D(req_compl),
        .Q(req_compl_q),
        .R(SR));
  FDSE req_compl_wd_q_reg
       (.C(user_clk),
        .CE(1'b1),
        .D(req_compl_wd),
        .Q(sel0[4]),
        .S(SR));
endmodule

(* ORIG_REF_NAME = "pcie_app_7x" *) 
module spec7_pcie_app_7x_0_pcie_app_7x
   (wr_data,
    wr_en,
    s_axis_tx_tdata,
    s_axis_tx_tkeep,
    s_axis_tx_tvalid,
    cfg_turnoff_ok,
    m_axis_rx_tready,
    s_axis_tx_tlast,
    s_axis_tx_tready,
    cfg_to_turnoff,
    user_clk,
    m_axis_rx_tdata,
    cfg_bus_number,
    cfg_device_number,
    cfg_function_number,
    m_axis_rx_tvalid,
    m_axis_rx_tlast,
    user_lnk_up,
    user_reset);
  output [31:0]wr_data;
  output wr_en;
  output [50:0]s_axis_tx_tdata;
  output [1:0]s_axis_tx_tkeep;
  output s_axis_tx_tvalid;
  output cfg_turnoff_ok;
  output m_axis_rx_tready;
  output s_axis_tx_tlast;
  input s_axis_tx_tready;
  input cfg_to_turnoff;
  input user_clk;
  input [63:0]m_axis_rx_tdata;
  input [7:0]cfg_bus_number;
  input [4:0]cfg_device_number;
  input [2:0]cfg_function_number;
  input m_axis_rx_tvalid;
  input m_axis_rx_tlast;
  input user_lnk_up;
  input user_reset;

  wire [7:0]cfg_bus_number;
  wire [4:0]cfg_device_number;
  wire [2:0]cfg_function_number;
  wire cfg_to_turnoff;
  wire cfg_turnoff_ok;
  wire [63:0]m_axis_rx_tdata;
  wire m_axis_rx_tlast;
  wire m_axis_rx_tready;
  wire m_axis_rx_tvalid;
  wire [50:0]s_axis_tx_tdata;
  wire [1:0]s_axis_tx_tkeep;
  wire s_axis_tx_tlast;
  wire s_axis_tx_tready;
  wire s_axis_tx_tvalid;
  wire user_clk;
  wire user_lnk_up;
  wire user_reset;
  wire [31:0]wr_data;
  wire wr_en;

  spec7_pcie_app_7x_0_PIO PIO
       (.cfg_bus_number(cfg_bus_number),
        .cfg_device_number(cfg_device_number),
        .cfg_function_number(cfg_function_number),
        .cfg_to_turnoff(cfg_to_turnoff),
        .cfg_turnoff_ok(cfg_turnoff_ok),
        .m_axis_rx_tdata(m_axis_rx_tdata),
        .m_axis_rx_tlast(m_axis_rx_tlast),
        .m_axis_rx_tready(m_axis_rx_tready),
        .m_axis_rx_tvalid(m_axis_rx_tvalid),
        .s_axis_tx_tdata(s_axis_tx_tdata),
        .s_axis_tx_tkeep(s_axis_tx_tkeep),
        .s_axis_tx_tlast(s_axis_tx_tlast),
        .s_axis_tx_tready(s_axis_tx_tready),
        .s_axis_tx_tvalid(s_axis_tx_tvalid),
        .user_clk(user_clk),
        .user_lnk_up(user_lnk_up),
        .user_reset(user_reset),
        .wr_data(wr_data),
        .wr_en(wr_en));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
