-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Sat Oct  3 15:01:19 2020
-- Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
-- Command     : write_vhdl -force -mode funcsim
--               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_pcie_app_7x_0/spec7_pcie_app_7x_0_sim_netlist.vhdl
-- Design      : spec7_pcie_app_7x_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z030fbg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_pcie_app_7x_0_PIO_RX_ENGINE is
  port (
    req_compl : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    wr_en : out STD_LOGIC;
    m_axis_rx_tready : out STD_LOGIC;
    req_compl_wd : out STD_LOGIC;
    trn_pending_reg : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 8 downto 0 );
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \pio_rx_sm_64.req_len_reg[7]_0\ : out STD_LOGIC_VECTOR ( 7 downto 0 );
    \pio_rx_sm_64.req_rid_reg[15]_0\ : out STD_LOGIC_VECTOR ( 12 downto 0 );
    wr_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \pio_rx_sm_64.req_be_reg[3]_0\ : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \pio_rx_sm_64.req_addr_reg[6]_0\ : out STD_LOGIC_VECTOR ( 4 downto 0 );
    user_clk : in STD_LOGIC;
    m_axis_rx_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axis_rx_tvalid : in STD_LOGIC;
    trn_pending : in STD_LOGIC;
    compl_done : in STD_LOGIC;
    pio_reset_n : in STD_LOGIC;
    m_axis_rx_tlast : in STD_LOGIC;
    req_compl_q : in STD_LOGIC;
    \gen_cpl_64.state\ : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_pcie_app_7x_0_PIO_RX_ENGINE : entity is "PIO_RX_ENGINE";
end spec7_pcie_app_7x_0_PIO_RX_ENGINE;

architecture STRUCTURE of spec7_pcie_app_7x_0_PIO_RX_ENGINE is
  signal \FSM_sequential_pio_rx_sm_64.state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[0]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[0]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[0]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[0]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[2]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[2]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[2]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_sequential_pio_rx_sm_64.state[2]_i_6_n_0\ : STD_LOGIC;
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^m_axis_rx_tready\ : STD_LOGIC;
  signal \pio_rx_sm_64.in_packet_q\ : STD_LOGIC;
  signal \pio_rx_sm_64.in_packet_q_i_1_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.m_axis_rx_tready_i_1_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.m_axis_rx_tready_i_2_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.m_axis_rx_tready_i_3_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.m_axis_rx_tready_i_4_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.m_axis_rx_tready_i_5_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.m_axis_rx_tready_i_6_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.m_axis_rx_tready_i_7_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.m_axis_rx_tready_i_8_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.req_addr[6]_i_1_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.req_be[3]_i_1_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.req_compl_i_1_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.req_compl_wd_i_1_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.tlp_type[7]_i_1_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.tlp_type[7]_i_2_n_0\ : STD_LOGIC;
  signal \pio_rx_sm_64.wr_data[31]_i_1_n_0\ : STD_LOGIC;
  signal req_addr1_in : STD_LOGIC_VECTOR ( 6 downto 2 );
  signal req_attr : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \^req_compl\ : STD_LOGIC;
  signal \^req_compl_wd\ : STD_LOGIC;
  signal req_ep : STD_LOGIC;
  signal req_len : STD_LOGIC_VECTOR ( 9 downto 8 );
  signal req_rid : STD_LOGIC_VECTOR ( 6 downto 4 );
  signal req_tag : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal req_tc : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal req_td : STD_LOGIC;
  signal state : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal tlp_type : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal wr_data1_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_pio_rx_sm_64.state[0]_i_2\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \FSM_sequential_pio_rx_sm_64.state[0]_i_3\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \FSM_sequential_pio_rx_sm_64.state[0]_i_4\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \FSM_sequential_pio_rx_sm_64.state[0]_i_5\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \FSM_sequential_pio_rx_sm_64.state[1]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \FSM_sequential_pio_rx_sm_64.state[1]_i_4\ : label is "soft_lutpair1";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_pio_rx_sm_64.state_reg[0]\ : label is "PIO_RX_MEM_RD32_DW1DW2:001,PIO_RX_MEM_WR32_DW1DW2:010,PIO_RX_MEM_RD64_DW1DW2:011,PIO_RX_MEM_WR64_DW1DW2:100,PIO_RX_MEM_WR64_DW3:101,PIO_RX_IO_WR_DW1DW2:110,PIO_RX_WAIT_STATE:111,PIO_RX_RST_STATE:000";
  attribute FSM_ENCODED_STATES of \FSM_sequential_pio_rx_sm_64.state_reg[1]\ : label is "PIO_RX_MEM_RD32_DW1DW2:001,PIO_RX_MEM_WR32_DW1DW2:010,PIO_RX_MEM_RD64_DW1DW2:011,PIO_RX_MEM_WR64_DW1DW2:100,PIO_RX_MEM_WR64_DW3:101,PIO_RX_IO_WR_DW1DW2:110,PIO_RX_WAIT_STATE:111,PIO_RX_RST_STATE:000";
  attribute FSM_ENCODED_STATES of \FSM_sequential_pio_rx_sm_64.state_reg[2]\ : label is "PIO_RX_MEM_RD32_DW1DW2:001,PIO_RX_MEM_WR32_DW1DW2:010,PIO_RX_MEM_RD64_DW1DW2:011,PIO_RX_MEM_WR64_DW1DW2:100,PIO_RX_MEM_WR64_DW3:101,PIO_RX_IO_WR_DW1DW2:110,PIO_RX_WAIT_STATE:111,PIO_RX_RST_STATE:000";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.m_axis_rx_tready_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.m_axis_rx_tready_i_3\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.m_axis_rx_tready_i_4\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.m_axis_rx_tready_i_5\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.m_axis_rx_tready_i_6\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.req_addr[2]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.req_addr[4]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.req_addr[5]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.req_addr[6]_i_2\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.req_compl_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.req_compl_wd_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.wr_data[16]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.wr_data[17]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \pio_rx_sm_64.wr_data[31]_i_2\ : label is "soft_lutpair4";
begin
  SR(0) <= \^sr\(0);
  m_axis_rx_tready <= \^m_axis_rx_tready\;
  req_compl <= \^req_compl\;
  req_compl_wd <= \^req_compl_wd\;
\FSM_sequential_pio_rx_sm_64.state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEFEEEEEE"
    )
        port map (
      I0 => \FSM_sequential_pio_rx_sm_64.state[0]_i_2_n_0\,
      I1 => \FSM_sequential_pio_rx_sm_64.state[0]_i_3_n_0\,
      I2 => \pio_rx_sm_64.m_axis_rx_tready_i_4_n_0\,
      I3 => \FSM_sequential_pio_rx_sm_64.state[0]_i_4_n_0\,
      I4 => \FSM_sequential_pio_rx_sm_64.state[1]_i_3_n_0\,
      I5 => \FSM_sequential_pio_rx_sm_64.state[0]_i_5_n_0\,
      O => \FSM_sequential_pio_rx_sm_64.state[0]_i_1_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"E0F0F0F0"
    )
        port map (
      I0 => \pio_rx_sm_64.m_axis_rx_tready_i_7_n_0\,
      I1 => \pio_rx_sm_64.m_axis_rx_tready_i_8_n_0\,
      I2 => state(0),
      I3 => state(1),
      I4 => state(2),
      O => \FSM_sequential_pio_rx_sm_64.state[0]_i_2_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[0]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => m_axis_rx_tvalid,
      I1 => state(2),
      I2 => state(1),
      O => \FSM_sequential_pio_rx_sm_64.state[0]_i_3_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[0]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => state(1),
      I1 => m_axis_rx_tdata(30),
      O => \FSM_sequential_pio_rx_sm_64.state[0]_i_4_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[0]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => m_axis_rx_tvalid,
      I1 => state(1),
      I2 => state(0),
      O => \FSM_sequential_pio_rx_sm_64.state[0]_i_5_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBF8CBF8CBF8C"
    )
        port map (
      I0 => \FSM_sequential_pio_rx_sm_64.state[2]_i_2_n_0\,
      I1 => state(1),
      I2 => state(2),
      I3 => \FSM_sequential_pio_rx_sm_64.state[1]_i_2_n_0\,
      I4 => \FSM_sequential_pio_rx_sm_64.state[1]_i_3_n_0\,
      I5 => \FSM_sequential_pio_rx_sm_64.state[1]_i_4_n_0\,
      O => \FSM_sequential_pio_rx_sm_64.state[1]_i_1_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[1]_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tvalid,
      O => \FSM_sequential_pio_rx_sm_64.state[1]_i_2_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[1]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0800"
    )
        port map (
      I0 => \FSM_sequential_pio_rx_sm_64.state[2]_i_6_n_0\,
      I1 => \FSM_sequential_pio_rx_sm_64.state[2]_i_5_n_0\,
      I2 => m_axis_rx_tdata(1),
      I3 => m_axis_rx_tdata(0),
      O => \FSM_sequential_pio_rx_sm_64.state[1]_i_3_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[1]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00100110"
    )
        port map (
      I0 => state(2),
      I1 => \pio_rx_sm_64.tlp_type[7]_i_2_n_0\,
      I2 => m_axis_rx_tdata(30),
      I3 => m_axis_rx_tdata(29),
      I4 => m_axis_rx_tdata(25),
      O => \FSM_sequential_pio_rx_sm_64.state[1]_i_4_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFBBBBFFFFFC00"
    )
        port map (
      I0 => \FSM_sequential_pio_rx_sm_64.state[2]_i_2_n_0\,
      I1 => state(1),
      I2 => state(0),
      I3 => m_axis_rx_tvalid,
      I4 => \FSM_sequential_pio_rx_sm_64.state[2]_i_3_n_0\,
      I5 => state(2),
      O => \FSM_sequential_pio_rx_sm_64.state[2]_i_1_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFEEEFFFFFFFF"
    )
        port map (
      I0 => tlp_type(7),
      I1 => tlp_type(4),
      I2 => tlp_type(5),
      I3 => tlp_type(1),
      I4 => \pio_rx_sm_64.m_axis_rx_tready_i_7_n_0\,
      I5 => state(0),
      O => \FSM_sequential_pio_rx_sm_64.state[2]_i_2_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0040000000000000"
    )
        port map (
      I0 => \pio_rx_sm_64.tlp_type[7]_i_2_n_0\,
      I1 => \FSM_sequential_pio_rx_sm_64.state[2]_i_4_n_0\,
      I2 => m_axis_rx_tdata(0),
      I3 => m_axis_rx_tdata(1),
      I4 => \FSM_sequential_pio_rx_sm_64.state[2]_i_5_n_0\,
      I5 => \FSM_sequential_pio_rx_sm_64.state[2]_i_6_n_0\,
      O => \FSM_sequential_pio_rx_sm_64.state[2]_i_3_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[2]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0440"
    )
        port map (
      I0 => state(1),
      I1 => m_axis_rx_tdata(30),
      I2 => m_axis_rx_tdata(29),
      I3 => m_axis_rx_tdata(25),
      O => \FSM_sequential_pio_rx_sm_64.state[2]_i_4_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[2]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => m_axis_rx_tdata(5),
      I1 => m_axis_rx_tdata(4),
      I2 => m_axis_rx_tdata(3),
      I3 => m_axis_rx_tdata(2),
      O => \FSM_sequential_pio_rx_sm_64.state[2]_i_5_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state[2]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => m_axis_rx_tdata(9),
      I1 => m_axis_rx_tdata(8),
      I2 => m_axis_rx_tdata(7),
      I3 => m_axis_rx_tdata(6),
      O => \FSM_sequential_pio_rx_sm_64.state[2]_i_6_n_0\
    );
\FSM_sequential_pio_rx_sm_64.state_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \FSM_sequential_pio_rx_sm_64.state[0]_i_1_n_0\,
      Q => state(0),
      R => \^sr\(0)
    );
\FSM_sequential_pio_rx_sm_64.state_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \FSM_sequential_pio_rx_sm_64.state[1]_i_1_n_0\,
      Q => state(1),
      R => \^sr\(0)
    );
\FSM_sequential_pio_rx_sm_64.state_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \FSM_sequential_pio_rx_sm_64.state[2]_i_1_n_0\,
      Q => state(2),
      R => \^sr\(0)
    );
\gen_cpl_64.s_axis_tx_tdata[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00C000"
    )
        port map (
      I0 => req_tag(4),
      I1 => req_attr(0),
      I2 => req_compl_q,
      I3 => pio_reset_n,
      I4 => \gen_cpl_64.state\,
      O => D(2)
    );
\gen_cpl_64.s_axis_tx_tdata[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00C000"
    )
        port map (
      I0 => req_tag(5),
      I1 => req_attr(1),
      I2 => req_compl_q,
      I3 => pio_reset_n,
      I4 => \gen_cpl_64.state\,
      O => D(3)
    );
\gen_cpl_64.s_axis_tx_tdata[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00C000"
    )
        port map (
      I0 => req_tag(6),
      I1 => req_ep,
      I2 => req_compl_q,
      I3 => pio_reset_n,
      I4 => \gen_cpl_64.state\,
      O => D(4)
    );
\gen_cpl_64.s_axis_tx_tdata[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00C000"
    )
        port map (
      I0 => req_tag(7),
      I1 => req_td,
      I2 => req_compl_q,
      I3 => pio_reset_n,
      I4 => \gen_cpl_64.state\,
      O => D(5)
    );
\gen_cpl_64.s_axis_tx_tdata[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00C000"
    )
        port map (
      I0 => req_rid(4),
      I1 => req_tc(0),
      I2 => req_compl_q,
      I3 => pio_reset_n,
      I4 => \gen_cpl_64.state\,
      O => D(6)
    );
\gen_cpl_64.s_axis_tx_tdata[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00C000"
    )
        port map (
      I0 => req_rid(5),
      I1 => req_tc(1),
      I2 => req_compl_q,
      I3 => pio_reset_n,
      I4 => \gen_cpl_64.state\,
      O => D(7)
    );
\gen_cpl_64.s_axis_tx_tdata[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00C000"
    )
        port map (
      I0 => req_rid(6),
      I1 => req_tc(2),
      I2 => req_compl_q,
      I3 => pio_reset_n,
      I4 => \gen_cpl_64.state\,
      O => D(8)
    );
\gen_cpl_64.s_axis_tx_tdata[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00C000"
    )
        port map (
      I0 => req_tag(0),
      I1 => req_len(8),
      I2 => req_compl_q,
      I3 => pio_reset_n,
      I4 => \gen_cpl_64.state\,
      O => D(0)
    );
\gen_cpl_64.s_axis_tx_tdata[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00C000"
    )
        port map (
      I0 => req_tag(1),
      I1 => req_len(9),
      I2 => req_compl_q,
      I3 => pio_reset_n,
      I4 => \gen_cpl_64.state\,
      O => D(1)
    );
\gen_cpl_64.s_axis_tx_tvalid_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => pio_reset_n,
      O => \^sr\(0)
    );
\pio_rx_sm_64.in_packet_q_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0C888888"
    )
        port map (
      I0 => \pio_rx_sm_64.in_packet_q\,
      I1 => pio_reset_n,
      I2 => m_axis_rx_tlast,
      I3 => m_axis_rx_tvalid,
      I4 => \^m_axis_rx_tready\,
      O => \pio_rx_sm_64.in_packet_q_i_1_n_0\
    );
\pio_rx_sm_64.in_packet_q_reg\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \pio_rx_sm_64.in_packet_q_i_1_n_0\,
      Q => \pio_rx_sm_64.in_packet_q\,
      R => '0'
    );
\pio_rx_sm_64.m_axis_rx_tready_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFEAFFFFFFEA00"
    )
        port map (
      I0 => \pio_rx_sm_64.m_axis_rx_tready_i_2_n_0\,
      I1 => \pio_rx_sm_64.m_axis_rx_tready_i_3_n_0\,
      I2 => \pio_rx_sm_64.m_axis_rx_tready_i_4_n_0\,
      I3 => \pio_rx_sm_64.m_axis_rx_tready_i_5_n_0\,
      I4 => \pio_rx_sm_64.m_axis_rx_tready_i_6_n_0\,
      I5 => \^m_axis_rx_tready\,
      O => \pio_rx_sm_64.m_axis_rx_tready_i_1_n_0\
    );
\pio_rx_sm_64.m_axis_rx_tready_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => m_axis_rx_tdata(30),
      I1 => m_axis_rx_tdata(29),
      I2 => state(2),
      I3 => state(0),
      I4 => state(1),
      O => \pio_rx_sm_64.m_axis_rx_tready_i_2_n_0\
    );
\pio_rx_sm_64.m_axis_rx_tready_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => state(1),
      I1 => state(0),
      I2 => state(2),
      O => \pio_rx_sm_64.m_axis_rx_tready_i_3_n_0\
    );
\pio_rx_sm_64.m_axis_rx_tready_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"EA"
    )
        port map (
      I0 => \pio_rx_sm_64.tlp_type[7]_i_2_n_0\,
      I1 => m_axis_rx_tdata(29),
      I2 => m_axis_rx_tdata(25),
      O => \pio_rx_sm_64.m_axis_rx_tready_i_4_n_0\
    );
\pio_rx_sm_64.m_axis_rx_tready_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F01"
    )
        port map (
      I0 => state(1),
      I1 => state(2),
      I2 => state(0),
      I3 => m_axis_rx_tvalid,
      O => \pio_rx_sm_64.m_axis_rx_tready_i_5_n_0\
    );
\pio_rx_sm_64.m_axis_rx_tready_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"02000000"
    )
        port map (
      I0 => state(0),
      I1 => \pio_rx_sm_64.m_axis_rx_tready_i_7_n_0\,
      I2 => \pio_rx_sm_64.m_axis_rx_tready_i_8_n_0\,
      I3 => state(2),
      I4 => state(1),
      O => \pio_rx_sm_64.m_axis_rx_tready_i_6_n_0\
    );
\pio_rx_sm_64.m_axis_rx_tready_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFF1"
    )
        port map (
      I0 => tlp_type(6),
      I1 => compl_done,
      I2 => tlp_type(0),
      I3 => tlp_type(3),
      I4 => tlp_type(2),
      O => \pio_rx_sm_64.m_axis_rx_tready_i_7_n_0\
    );
\pio_rx_sm_64.m_axis_rx_tready_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FEEE"
    )
        port map (
      I0 => tlp_type(7),
      I1 => tlp_type(4),
      I2 => tlp_type(5),
      I3 => tlp_type(1),
      O => \pio_rx_sm_64.m_axis_rx_tready_i_8_n_0\
    );
\pio_rx_sm_64.m_axis_rx_tready_reg\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \pio_rx_sm_64.m_axis_rx_tready_i_1_n_0\,
      Q => \^m_axis_rx_tready\,
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_addr[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => m_axis_rx_tdata(34),
      I1 => state(1),
      I2 => m_axis_rx_tdata(2),
      O => req_addr1_in(2)
    );
\pio_rx_sm_64.req_addr[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => m_axis_rx_tdata(35),
      I1 => state(1),
      I2 => m_axis_rx_tdata(3),
      O => req_addr1_in(3)
    );
\pio_rx_sm_64.req_addr[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => m_axis_rx_tdata(36),
      I1 => state(1),
      I2 => m_axis_rx_tdata(4),
      O => req_addr1_in(4)
    );
\pio_rx_sm_64.req_addr[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => m_axis_rx_tdata(37),
      I1 => state(1),
      I2 => m_axis_rx_tdata(5),
      O => req_addr1_in(5)
    );
\pio_rx_sm_64.req_addr[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => m_axis_rx_tvalid,
      I1 => state(0),
      I2 => state(2),
      O => \pio_rx_sm_64.req_addr[6]_i_1_n_0\
    );
\pio_rx_sm_64.req_addr[6]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => m_axis_rx_tdata(38),
      I1 => state(1),
      I2 => m_axis_rx_tdata(6),
      O => req_addr1_in(6)
    );
\pio_rx_sm_64.req_addr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_addr[6]_i_1_n_0\,
      D => req_addr1_in(2),
      Q => \pio_rx_sm_64.req_addr_reg[6]_0\(0),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_addr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_addr[6]_i_1_n_0\,
      D => req_addr1_in(3),
      Q => \pio_rx_sm_64.req_addr_reg[6]_0\(1),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_addr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_addr[6]_i_1_n_0\,
      D => req_addr1_in(4),
      Q => \pio_rx_sm_64.req_addr_reg[6]_0\(2),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_addr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_addr[6]_i_1_n_0\,
      D => req_addr1_in(5),
      Q => \pio_rx_sm_64.req_addr_reg[6]_0\(3),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_addr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_addr[6]_i_1_n_0\,
      D => req_addr1_in(6),
      Q => \pio_rx_sm_64.req_addr_reg[6]_0\(4),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_attr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(12),
      Q => req_attr(0),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_attr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(13),
      Q => req_attr(1),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_be[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0004440400000000"
    )
        port map (
      I0 => \pio_rx_sm_64.tlp_type[7]_i_2_n_0\,
      I1 => \pio_rx_sm_64.m_axis_rx_tready_i_3_n_0\,
      I2 => m_axis_rx_tdata(30),
      I3 => m_axis_rx_tdata(25),
      I4 => m_axis_rx_tdata(29),
      I5 => \FSM_sequential_pio_rx_sm_64.state[1]_i_3_n_0\,
      O => \pio_rx_sm_64.req_be[3]_i_1_n_0\
    );
\pio_rx_sm_64.req_be_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(32),
      Q => \pio_rx_sm_64.req_be_reg[3]_0\(0),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_be_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(33),
      Q => \pio_rx_sm_64.req_be_reg[3]_0\(1),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_be_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(34),
      Q => \pio_rx_sm_64.req_be_reg[3]_0\(2),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_be_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(35),
      Q => \pio_rx_sm_64.req_be_reg[3]_0\(3),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_compl_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3800"
    )
        port map (
      I0 => state(1),
      I1 => state(2),
      I2 => state(0),
      I3 => m_axis_rx_tvalid,
      O => \pio_rx_sm_64.req_compl_i_1_n_0\
    );
\pio_rx_sm_64.req_compl_reg\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \pio_rx_sm_64.req_compl_i_1_n_0\,
      Q => \^req_compl\,
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_compl_wd_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F7FF2023"
    )
        port map (
      I0 => m_axis_rx_tvalid,
      I1 => state(2),
      I2 => state(0),
      I3 => state(1),
      I4 => \^req_compl_wd\,
      O => \pio_rx_sm_64.req_compl_wd_i_1_n_0\
    );
\pio_rx_sm_64.req_compl_wd_reg\: unisim.vcomponents.FDSE
     port map (
      C => user_clk,
      CE => '1',
      D => \pio_rx_sm_64.req_compl_wd_i_1_n_0\,
      Q => \^req_compl_wd\,
      S => \^sr\(0)
    );
\pio_rx_sm_64.req_ep_reg\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(14),
      Q => req_ep,
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_len_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(0),
      Q => \pio_rx_sm_64.req_len_reg[7]_0\(0),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_len_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(1),
      Q => \pio_rx_sm_64.req_len_reg[7]_0\(1),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_len_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(2),
      Q => \pio_rx_sm_64.req_len_reg[7]_0\(2),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_len_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(3),
      Q => \pio_rx_sm_64.req_len_reg[7]_0\(3),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_len_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(4),
      Q => \pio_rx_sm_64.req_len_reg[7]_0\(4),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_len_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(5),
      Q => \pio_rx_sm_64.req_len_reg[7]_0\(5),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_len_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(6),
      Q => \pio_rx_sm_64.req_len_reg[7]_0\(6),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_len_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(7),
      Q => \pio_rx_sm_64.req_len_reg[7]_0\(7),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_len_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(8),
      Q => req_len(8),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_len_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(9),
      Q => req_len(9),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(48),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(0),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(58),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(7),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(59),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(8),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(60),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(9),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(61),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(10),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(62),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(11),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(63),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(12),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(49),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(1),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(50),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(2),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(51),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(3),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(52),
      Q => req_rid(4),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(53),
      Q => req_rid(5),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(54),
      Q => req_rid(6),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(55),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(4),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(56),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(5),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_rid_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(57),
      Q => \pio_rx_sm_64.req_rid_reg[15]_0\(6),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_tag_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(40),
      Q => req_tag(0),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_tag_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(41),
      Q => req_tag(1),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_tag_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(42),
      Q => Q(0),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_tag_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(43),
      Q => Q(1),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_tag_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(44),
      Q => req_tag(4),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_tag_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(45),
      Q => req_tag(5),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_tag_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(46),
      Q => req_tag(6),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_tag_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(47),
      Q => req_tag(7),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_tc_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(20),
      Q => req_tc(0),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_tc_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(21),
      Q => req_tc(1),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_tc_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(22),
      Q => req_tc(2),
      R => \^sr\(0)
    );
\pio_rx_sm_64.req_td_reg\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.req_be[3]_i_1_n_0\,
      D => m_axis_rx_tdata(15),
      Q => req_td,
      R => \^sr\(0)
    );
\pio_rx_sm_64.tlp_type[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000010101"
    )
        port map (
      I0 => state(2),
      I1 => state(0),
      I2 => state(1),
      I3 => m_axis_rx_tdata(25),
      I4 => m_axis_rx_tdata(29),
      I5 => \pio_rx_sm_64.tlp_type[7]_i_2_n_0\,
      O => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\
    );
\pio_rx_sm_64.tlp_type[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFB"
    )
        port map (
      I0 => \pio_rx_sm_64.in_packet_q\,
      I1 => m_axis_rx_tvalid,
      I2 => m_axis_rx_tdata(27),
      I3 => m_axis_rx_tdata(28),
      I4 => m_axis_rx_tdata(26),
      I5 => m_axis_rx_tdata(24),
      O => \pio_rx_sm_64.tlp_type[7]_i_2_n_0\
    );
\pio_rx_sm_64.tlp_type_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(24),
      Q => tlp_type(0),
      R => \^sr\(0)
    );
\pio_rx_sm_64.tlp_type_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(25),
      Q => tlp_type(1),
      R => \^sr\(0)
    );
\pio_rx_sm_64.tlp_type_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(26),
      Q => tlp_type(2),
      R => \^sr\(0)
    );
\pio_rx_sm_64.tlp_type_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(27),
      Q => tlp_type(3),
      R => \^sr\(0)
    );
\pio_rx_sm_64.tlp_type_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(28),
      Q => tlp_type(4),
      R => \^sr\(0)
    );
\pio_rx_sm_64.tlp_type_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(29),
      Q => tlp_type(5),
      R => \^sr\(0)
    );
\pio_rx_sm_64.tlp_type_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(30),
      Q => tlp_type(6),
      R => \^sr\(0)
    );
\pio_rx_sm_64.tlp_type_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.tlp_type[7]_i_1_n_0\,
      D => m_axis_rx_tdata(31),
      Q => tlp_type(7),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(0),
      I2 => state(1),
      I3 => m_axis_rx_tdata(32),
      O => wr_data1_in(0)
    );
\pio_rx_sm_64.wr_data[10]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(10),
      I2 => state(1),
      I3 => m_axis_rx_tdata(42),
      O => wr_data1_in(10)
    );
\pio_rx_sm_64.wr_data[11]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(11),
      I2 => state(1),
      I3 => m_axis_rx_tdata(43),
      O => wr_data1_in(11)
    );
\pio_rx_sm_64.wr_data[12]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(12),
      I2 => state(1),
      I3 => m_axis_rx_tdata(44),
      O => wr_data1_in(12)
    );
\pio_rx_sm_64.wr_data[13]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(13),
      I2 => state(1),
      I3 => m_axis_rx_tdata(45),
      O => wr_data1_in(13)
    );
\pio_rx_sm_64.wr_data[14]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(14),
      I2 => state(1),
      I3 => m_axis_rx_tdata(46),
      O => wr_data1_in(14)
    );
\pio_rx_sm_64.wr_data[15]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(15),
      I2 => state(1),
      I3 => m_axis_rx_tdata(47),
      O => wr_data1_in(15)
    );
\pio_rx_sm_64.wr_data[16]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(16),
      I2 => state(1),
      I3 => m_axis_rx_tdata(48),
      O => wr_data1_in(16)
    );
\pio_rx_sm_64.wr_data[17]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(17),
      I2 => state(1),
      I3 => m_axis_rx_tdata(49),
      O => wr_data1_in(17)
    );
\pio_rx_sm_64.wr_data[18]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(18),
      I2 => state(1),
      I3 => m_axis_rx_tdata(50),
      O => wr_data1_in(18)
    );
\pio_rx_sm_64.wr_data[19]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(19),
      I2 => state(1),
      I3 => m_axis_rx_tdata(51),
      O => wr_data1_in(19)
    );
\pio_rx_sm_64.wr_data[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(1),
      I2 => state(1),
      I3 => m_axis_rx_tdata(33),
      O => wr_data1_in(1)
    );
\pio_rx_sm_64.wr_data[20]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(20),
      I2 => state(1),
      I3 => m_axis_rx_tdata(52),
      O => wr_data1_in(20)
    );
\pio_rx_sm_64.wr_data[21]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(21),
      I2 => state(1),
      I3 => m_axis_rx_tdata(53),
      O => wr_data1_in(21)
    );
\pio_rx_sm_64.wr_data[22]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(22),
      I2 => state(1),
      I3 => m_axis_rx_tdata(54),
      O => wr_data1_in(22)
    );
\pio_rx_sm_64.wr_data[23]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(23),
      I2 => state(1),
      I3 => m_axis_rx_tdata(55),
      O => wr_data1_in(23)
    );
\pio_rx_sm_64.wr_data[24]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(24),
      I2 => state(1),
      I3 => m_axis_rx_tdata(56),
      O => wr_data1_in(24)
    );
\pio_rx_sm_64.wr_data[25]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(25),
      I2 => state(1),
      I3 => m_axis_rx_tdata(57),
      O => wr_data1_in(25)
    );
\pio_rx_sm_64.wr_data[26]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(26),
      I2 => state(1),
      I3 => m_axis_rx_tdata(58),
      O => wr_data1_in(26)
    );
\pio_rx_sm_64.wr_data[27]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(27),
      I2 => state(1),
      I3 => m_axis_rx_tdata(59),
      O => wr_data1_in(27)
    );
\pio_rx_sm_64.wr_data[28]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(28),
      I2 => state(1),
      I3 => m_axis_rx_tdata(60),
      O => wr_data1_in(28)
    );
\pio_rx_sm_64.wr_data[29]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(29),
      I2 => state(1),
      I3 => m_axis_rx_tdata(61),
      O => wr_data1_in(29)
    );
\pio_rx_sm_64.wr_data[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(2),
      I2 => state(1),
      I3 => m_axis_rx_tdata(34),
      O => wr_data1_in(2)
    );
\pio_rx_sm_64.wr_data[30]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(30),
      I2 => state(1),
      I3 => m_axis_rx_tdata(62),
      O => wr_data1_in(30)
    );
\pio_rx_sm_64.wr_data[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4480"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tvalid,
      I2 => state(2),
      I3 => state(1),
      O => \pio_rx_sm_64.wr_data[31]_i_1_n_0\
    );
\pio_rx_sm_64.wr_data[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(31),
      I2 => state(1),
      I3 => m_axis_rx_tdata(63),
      O => wr_data1_in(31)
    );
\pio_rx_sm_64.wr_data[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(3),
      I2 => state(1),
      I3 => m_axis_rx_tdata(35),
      O => wr_data1_in(3)
    );
\pio_rx_sm_64.wr_data[4]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(4),
      I2 => state(1),
      I3 => m_axis_rx_tdata(36),
      O => wr_data1_in(4)
    );
\pio_rx_sm_64.wr_data[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(5),
      I2 => state(1),
      I3 => m_axis_rx_tdata(37),
      O => wr_data1_in(5)
    );
\pio_rx_sm_64.wr_data[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(6),
      I2 => state(1),
      I3 => m_axis_rx_tdata(38),
      O => wr_data1_in(6)
    );
\pio_rx_sm_64.wr_data[7]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(7),
      I2 => state(1),
      I3 => m_axis_rx_tdata(39),
      O => wr_data1_in(7)
    );
\pio_rx_sm_64.wr_data[8]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(8),
      I2 => state(1),
      I3 => m_axis_rx_tdata(40),
      O => wr_data1_in(8)
    );
\pio_rx_sm_64.wr_data[9]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => state(0),
      I1 => m_axis_rx_tdata(9),
      I2 => state(1),
      I3 => m_axis_rx_tdata(41),
      O => wr_data1_in(9)
    );
\pio_rx_sm_64.wr_data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(0),
      Q => wr_data(0),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(10),
      Q => wr_data(10),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(11),
      Q => wr_data(11),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(12),
      Q => wr_data(12),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(13),
      Q => wr_data(13),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(14),
      Q => wr_data(14),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(15),
      Q => wr_data(15),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(16),
      Q => wr_data(16),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(17),
      Q => wr_data(17),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(18),
      Q => wr_data(18),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(19),
      Q => wr_data(19),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(1),
      Q => wr_data(1),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(20),
      Q => wr_data(20),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(21),
      Q => wr_data(21),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(22),
      Q => wr_data(22),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(23),
      Q => wr_data(23),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(24),
      Q => wr_data(24),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(25),
      Q => wr_data(25),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(26),
      Q => wr_data(26),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(27),
      Q => wr_data(27),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(28),
      Q => wr_data(28),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(29),
      Q => wr_data(29),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(2),
      Q => wr_data(2),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(30),
      Q => wr_data(30),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(31),
      Q => wr_data(31),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(3),
      Q => wr_data(3),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(4),
      Q => wr_data(4),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(5),
      Q => wr_data(5),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(6),
      Q => wr_data(6),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(7),
      Q => wr_data(7),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(8),
      Q => wr_data(8),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      D => wr_data1_in(9),
      Q => wr_data(9),
      R => \^sr\(0)
    );
\pio_rx_sm_64.wr_en_reg\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \pio_rx_sm_64.wr_data[31]_i_1_n_0\,
      Q => wr_en,
      R => \^sr\(0)
    );
trn_pending_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4E"
    )
        port map (
      I0 => trn_pending,
      I1 => \^req_compl\,
      I2 => compl_done,
      O => trn_pending_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_pcie_app_7x_0_PIO_TO_CTRL is
  port (
    cfg_turnoff_ok : out STD_LOGIC;
    trn_pending : out STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    user_clk : in STD_LOGIC;
    trn_pending_reg_0 : in STD_LOGIC;
    cfg_to_turnoff : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_pcie_app_7x_0_PIO_TO_CTRL : entity is "PIO_TO_CTRL";
end spec7_pcie_app_7x_0_PIO_TO_CTRL;

architecture STRUCTURE of spec7_pcie_app_7x_0_PIO_TO_CTRL is
  signal cfg_turnoff_ok_i_1_n_0 : STD_LOGIC;
  signal \^trn_pending\ : STD_LOGIC;
begin
  trn_pending <= \^trn_pending\;
cfg_turnoff_ok_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => cfg_to_turnoff,
      I1 => \^trn_pending\,
      O => cfg_turnoff_ok_i_1_n_0
    );
cfg_turnoff_ok_reg: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => cfg_turnoff_ok_i_1_n_0,
      Q => cfg_turnoff_ok,
      R => SR(0)
    );
trn_pending_reg: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => trn_pending_reg_0,
      Q => \^trn_pending\,
      R => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_pcie_app_7x_0_PIO_TX_ENGINE is
  port (
    req_compl_q : out STD_LOGIC;
    \gen_cpl_64.state\ : out STD_LOGIC;
    s_axis_tx_tlast : out STD_LOGIC;
    compl_done : out STD_LOGIC;
    s_axis_tx_tdata : out STD_LOGIC_VECTOR ( 50 downto 0 );
    s_axis_tx_tkeep : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axis_tx_tvalid : out STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    req_compl : in STD_LOGIC;
    user_clk : in STD_LOGIC;
    req_compl_wd : in STD_LOGIC;
    s_axis_tx_tready : in STD_LOGIC;
    pio_reset_n : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 8 downto 0 );
    \gen_cpl_64.s_axis_tx_tdata_reg[7]_0\ : in STD_LOGIC_VECTOR ( 7 downto 0 );
    \gen_cpl_64.s_axis_tx_tdata_reg[6]_0\ : in STD_LOGIC_VECTOR ( 4 downto 0 );
    \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\ : in STD_LOGIC_VECTOR ( 12 downto 0 );
    \rd_be_reg[3]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    cfg_bus_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_device_number : in STD_LOGIC_VECTOR ( 4 downto 0 );
    cfg_function_number : in STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_pcie_app_7x_0_PIO_TX_ENGINE : entity is "PIO_TX_ENGINE";
end spec7_pcie_app_7x_0_PIO_TX_ENGINE;

architecture STRUCTURE of spec7_pcie_app_7x_0_PIO_TX_ENGINE is
  signal \^compl_done\ : STD_LOGIC;
  signal \gen_cpl_64.compl_done_i_1_n_0\ : STD_LOGIC;
  signal \gen_cpl_64.s_axis_tx_tdata[0]_i_2_n_0\ : STD_LOGIC;
  signal \gen_cpl_64.s_axis_tx_tdata[1]_i_2_n_0\ : STD_LOGIC;
  signal \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\ : STD_LOGIC;
  signal \gen_cpl_64.s_axis_tx_tdata[33]_i_2_n_0\ : STD_LOGIC;
  signal \gen_cpl_64.s_axis_tx_tdata[34]_i_1_n_0\ : STD_LOGIC;
  signal \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\ : STD_LOGIC;
  signal \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\ : STD_LOGIC;
  signal \gen_cpl_64.s_axis_tx_tkeep[7]_i_1_n_0\ : STD_LOGIC;
  signal \gen_cpl_64.s_axis_tx_tlast_i_1_n_0\ : STD_LOGIC;
  signal \gen_cpl_64.s_axis_tx_tvalid_i_2_n_0\ : STD_LOGIC;
  signal \^gen_cpl_64.state\ : STD_LOGIC;
  signal \gen_cpl_64.state_i_1_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 33 downto 0 );
  signal \^req_compl_q\ : STD_LOGIC;
  signal \^s_axis_tx_tlast\ : STD_LOGIC;
  signal \^s_axis_tx_tvalid\ : STD_LOGIC;
  signal sel0 : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \gen_cpl_64.compl_done_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \gen_cpl_64.s_axis_tx_tdata[0]_i_2\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \gen_cpl_64.s_axis_tx_tdata[25]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \gen_cpl_64.s_axis_tx_tdata[27]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \gen_cpl_64.s_axis_tx_tdata[30]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \gen_cpl_64.s_axis_tx_tdata[32]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \gen_cpl_64.s_axis_tx_tdata[33]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \gen_cpl_64.s_axis_tx_tdata[33]_i_2\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \gen_cpl_64.s_axis_tx_tdata[34]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \gen_cpl_64.s_axis_tx_tkeep[7]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \gen_cpl_64.s_axis_tx_tvalid_i_2\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \gen_cpl_64.state_i_1\ : label is "soft_lutpair15";
begin
  compl_done <= \^compl_done\;
  \gen_cpl_64.state\ <= \^gen_cpl_64.state\;
  req_compl_q <= \^req_compl_q\;
  s_axis_tx_tlast <= \^s_axis_tx_tlast\;
  s_axis_tx_tvalid <= \^s_axis_tx_tvalid\;
\gen_cpl_64.compl_done_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"C8C8C000"
    )
        port map (
      I0 => \^req_compl_q\,
      I1 => pio_reset_n,
      I2 => \^gen_cpl_64.state\,
      I3 => s_axis_tx_tready,
      I4 => \^compl_done\,
      O => \gen_cpl_64.compl_done_i_1_n_0\
    );
\gen_cpl_64.compl_done_reg\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \gen_cpl_64.compl_done_i_1_n_0\,
      Q => \^compl_done\,
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFF400F400F400"
    )
        port map (
      I0 => sel0(2),
      I1 => sel0(3),
      I2 => sel0(1),
      I3 => \gen_cpl_64.s_axis_tx_tdata[0]_i_2_n_0\,
      I4 => \gen_cpl_64.s_axis_tx_tdata_reg[7]_0\(0),
      I5 => \gen_cpl_64.s_axis_tx_tdata[33]_i_2_n_0\,
      O => p_0_in(0)
    );
\gen_cpl_64.s_axis_tx_tdata[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0080"
    )
        port map (
      I0 => pio_reset_n,
      I1 => \^gen_cpl_64.state\,
      I2 => sel0(4),
      I3 => sel0(0),
      O => \gen_cpl_64.s_axis_tx_tdata[0]_i_2_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88880000F0000000"
    )
        port map (
      I0 => sel0(4),
      I1 => \gen_cpl_64.s_axis_tx_tdata[1]_i_2_n_0\,
      I2 => \gen_cpl_64.s_axis_tx_tdata_reg[7]_0\(1),
      I3 => \^req_compl_q\,
      I4 => pio_reset_n,
      I5 => \^gen_cpl_64.state\,
      O => p_0_in(1)
    );
\gen_cpl_64.s_axis_tx_tdata[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1110"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(0),
      I2 => sel0(3),
      I3 => sel0(2),
      O => \gen_cpl_64.s_axis_tx_tdata[1]_i_2_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata[25]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B080"
    )
        port map (
      I0 => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(6),
      I1 => \^gen_cpl_64.state\,
      I2 => pio_reset_n,
      I3 => \^req_compl_q\,
      O => p_0_in(25)
    );
\gen_cpl_64.s_axis_tx_tdata[27]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"B080"
    )
        port map (
      I0 => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(8),
      I1 => \^gen_cpl_64.state\,
      I2 => pio_reset_n,
      I3 => \^req_compl_q\,
      O => p_0_in(27)
    );
\gen_cpl_64.s_axis_tx_tdata[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88880000F0000000"
    )
        port map (
      I0 => \gen_cpl_64.s_axis_tx_tdata_reg[6]_0\(0),
      I1 => sel0(4),
      I2 => \gen_cpl_64.s_axis_tx_tdata_reg[7]_0\(2),
      I3 => \^req_compl_q\,
      I4 => pio_reset_n,
      I5 => \^gen_cpl_64.state\,
      O => p_0_in(2)
    );
\gen_cpl_64.s_axis_tx_tdata[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA00C000"
    )
        port map (
      I0 => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(11),
      I1 => sel0(4),
      I2 => \^req_compl_q\,
      I3 => pio_reset_n,
      I4 => \^gen_cpl_64.state\,
      O => p_0_in(30)
    );
\gen_cpl_64.s_axis_tx_tdata[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => pio_reset_n,
      I1 => \^gen_cpl_64.state\,
      O => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata[32]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"51009F00"
    )
        port map (
      I0 => sel0(3),
      I1 => sel0(1),
      I2 => sel0(2),
      I3 => \gen_cpl_64.s_axis_tx_tdata[33]_i_2_n_0\,
      I4 => sel0(0),
      O => p_0_in(32)
    );
\gen_cpl_64.s_axis_tx_tdata[33]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"0EE80000"
    )
        port map (
      I0 => sel0(1),
      I1 => sel0(2),
      I2 => sel0(3),
      I3 => sel0(0),
      I4 => \gen_cpl_64.s_axis_tx_tdata[33]_i_2_n_0\,
      O => p_0_in(33)
    );
\gen_cpl_64.s_axis_tx_tdata[33]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"08"
    )
        port map (
      I0 => \^req_compl_q\,
      I1 => pio_reset_n,
      I2 => \^gen_cpl_64.state\,
      O => \gen_cpl_64.s_axis_tx_tdata[33]_i_2_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata[34]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => sel0(0),
      I1 => sel0(3),
      O => \gen_cpl_64.s_axis_tx_tdata[34]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88880000F0000000"
    )
        port map (
      I0 => \gen_cpl_64.s_axis_tx_tdata_reg[6]_0\(1),
      I1 => sel0(4),
      I2 => \gen_cpl_64.s_axis_tx_tdata_reg[7]_0\(3),
      I3 => \^req_compl_q\,
      I4 => pio_reset_n,
      I5 => \^gen_cpl_64.state\,
      O => p_0_in(3)
    );
\gen_cpl_64.s_axis_tx_tdata[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88880000F0000000"
    )
        port map (
      I0 => \gen_cpl_64.s_axis_tx_tdata_reg[6]_0\(2),
      I1 => sel0(4),
      I2 => \gen_cpl_64.s_axis_tx_tdata_reg[7]_0\(4),
      I3 => \^req_compl_q\,
      I4 => pio_reset_n,
      I5 => \^gen_cpl_64.state\,
      O => p_0_in(4)
    );
\gen_cpl_64.s_axis_tx_tdata[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88880000F0000000"
    )
        port map (
      I0 => \gen_cpl_64.s_axis_tx_tdata_reg[6]_0\(3),
      I1 => sel0(4),
      I2 => \gen_cpl_64.s_axis_tx_tdata_reg[7]_0\(5),
      I3 => \^req_compl_q\,
      I4 => pio_reset_n,
      I5 => \^gen_cpl_64.state\,
      O => p_0_in(5)
    );
\gen_cpl_64.s_axis_tx_tdata[63]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8FBF"
    )
        port map (
      I0 => s_axis_tx_tready,
      I1 => \^gen_cpl_64.state\,
      I2 => pio_reset_n,
      I3 => \^req_compl_q\,
      O => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata[63]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"F7"
    )
        port map (
      I0 => \^gen_cpl_64.state\,
      I1 => pio_reset_n,
      I2 => s_axis_tx_tready,
      O => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88880000F0000000"
    )
        port map (
      I0 => \gen_cpl_64.s_axis_tx_tdata_reg[6]_0\(4),
      I1 => sel0(4),
      I2 => \gen_cpl_64.s_axis_tx_tdata_reg[7]_0\(6),
      I3 => \^req_compl_q\,
      I4 => pio_reset_n,
      I5 => \^gen_cpl_64.state\,
      O => p_0_in(6)
    );
\gen_cpl_64.s_axis_tx_tdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(0),
      Q => s_axis_tx_tdata(0),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => Q(0),
      Q => s_axis_tx_tdata(10),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => Q(1),
      Q => s_axis_tx_tdata(11),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => D(2),
      Q => s_axis_tx_tdata(12),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => D(3),
      Q => s_axis_tx_tdata(13),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => D(4),
      Q => s_axis_tx_tdata(14),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => D(5),
      Q => s_axis_tx_tdata(15),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(0),
      Q => s_axis_tx_tdata(16),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(1),
      Q => s_axis_tx_tdata(17),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(2),
      Q => s_axis_tx_tdata(18),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(3),
      Q => s_axis_tx_tdata(19),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(1),
      Q => s_axis_tx_tdata(1),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => D(6),
      Q => s_axis_tx_tdata(20),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => D(7),
      Q => s_axis_tx_tdata(21),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => D(8),
      Q => s_axis_tx_tdata(22),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(4),
      Q => s_axis_tx_tdata(23),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(5),
      Q => s_axis_tx_tdata(24),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(25),
      Q => s_axis_tx_tdata(25),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(7),
      Q => s_axis_tx_tdata(26),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(27),
      Q => s_axis_tx_tdata(27),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(9),
      Q => s_axis_tx_tdata(28),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(10),
      Q => s_axis_tx_tdata(29),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(2),
      Q => s_axis_tx_tdata(2),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(30),
      Q => s_axis_tx_tdata(30),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(12),
      Q => s_axis_tx_tdata(31),
      R => \gen_cpl_64.s_axis_tx_tdata[31]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[32]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(32),
      Q => s_axis_tx_tdata(32),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[33]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(33),
      Q => s_axis_tx_tdata(33),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[34]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata[34]_i_1_n_0\,
      Q => s_axis_tx_tdata(34),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(3),
      Q => s_axis_tx_tdata(3),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[48]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_function_number(0),
      Q => s_axis_tx_tdata(35),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[49]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_function_number(1),
      Q => s_axis_tx_tdata(36),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(4),
      Q => s_axis_tx_tdata(4),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[50]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_function_number(2),
      Q => s_axis_tx_tdata(37),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[51]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_device_number(0),
      Q => s_axis_tx_tdata(38),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[52]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_device_number(1),
      Q => s_axis_tx_tdata(39),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[53]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_device_number(2),
      Q => s_axis_tx_tdata(40),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[54]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_device_number(3),
      Q => s_axis_tx_tdata(41),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[55]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_device_number(4),
      Q => s_axis_tx_tdata(42),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[56]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_bus_number(0),
      Q => s_axis_tx_tdata(43),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[57]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_bus_number(1),
      Q => s_axis_tx_tdata(44),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[58]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_bus_number(2),
      Q => s_axis_tx_tdata(45),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[59]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_bus_number(3),
      Q => s_axis_tx_tdata(46),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(5),
      Q => s_axis_tx_tdata(5),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[60]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_bus_number(4),
      Q => s_axis_tx_tdata(47),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[61]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_bus_number(5),
      Q => s_axis_tx_tdata(48),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[62]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_bus_number(6),
      Q => s_axis_tx_tdata(49),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[63]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => cfg_bus_number(7),
      Q => s_axis_tx_tdata(50),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => p_0_in(6),
      Q => s_axis_tx_tdata(6),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tdata_reg[7]_0\(7),
      Q => s_axis_tx_tdata(7),
      R => \gen_cpl_64.s_axis_tx_tdata[63]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => D(0),
      Q => s_axis_tx_tdata(8),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => D(1),
      Q => s_axis_tx_tdata(9),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tkeep[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B0"
    )
        port map (
      I0 => sel0(4),
      I1 => \^gen_cpl_64.state\,
      I2 => pio_reset_n,
      O => \gen_cpl_64.s_axis_tx_tkeep[7]_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tkeep_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => pio_reset_n,
      Q => s_axis_tx_tkeep(0),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tkeep_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => \gen_cpl_64.s_axis_tx_tdata[63]_i_2_n_0\,
      D => \gen_cpl_64.s_axis_tx_tkeep[7]_i_1_n_0\,
      Q => s_axis_tx_tkeep(1),
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tlast_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E000"
    )
        port map (
      I0 => \^s_axis_tx_tlast\,
      I1 => s_axis_tx_tready,
      I2 => \^gen_cpl_64.state\,
      I3 => pio_reset_n,
      O => \gen_cpl_64.s_axis_tx_tlast_i_1_n_0\
    );
\gen_cpl_64.s_axis_tx_tlast_reg\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \gen_cpl_64.s_axis_tx_tlast_i_1_n_0\,
      Q => \^s_axis_tx_tlast\,
      R => '0'
    );
\gen_cpl_64.s_axis_tx_tvalid_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FACA"
    )
        port map (
      I0 => \^req_compl_q\,
      I1 => s_axis_tx_tready,
      I2 => \^gen_cpl_64.state\,
      I3 => \^s_axis_tx_tvalid\,
      O => \gen_cpl_64.s_axis_tx_tvalid_i_2_n_0\
    );
\gen_cpl_64.s_axis_tx_tvalid_reg\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \gen_cpl_64.s_axis_tx_tvalid_i_2_n_0\,
      Q => \^s_axis_tx_tvalid\,
      R => SR(0)
    );
\gen_cpl_64.state_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4A"
    )
        port map (
      I0 => \^gen_cpl_64.state\,
      I1 => \^req_compl_q\,
      I2 => s_axis_tx_tready,
      O => \gen_cpl_64.state_i_1_n_0\
    );
\gen_cpl_64.state_reg\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \gen_cpl_64.state_i_1_n_0\,
      Q => \^gen_cpl_64.state\,
      R => SR(0)
    );
\rd_be_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \rd_be_reg[3]_0\(0),
      Q => sel0(0),
      R => SR(0)
    );
\rd_be_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \rd_be_reg[3]_0\(1),
      Q => sel0(1),
      R => SR(0)
    );
\rd_be_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \rd_be_reg[3]_0\(2),
      Q => sel0(2),
      R => SR(0)
    );
\rd_be_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => \rd_be_reg[3]_0\(3),
      Q => sel0(3),
      R => SR(0)
    );
req_compl_q_reg: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => req_compl,
      Q => \^req_compl_q\,
      R => SR(0)
    );
req_compl_wd_q_reg: unisim.vcomponents.FDSE
     port map (
      C => user_clk,
      CE => '1',
      D => req_compl_wd,
      Q => sel0(4),
      S => SR(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_pcie_app_7x_0_PIO_EP is
  port (
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    wr_en : out STD_LOGIC;
    m_axis_rx_tready : out STD_LOGIC;
    s_axis_tx_tlast : out STD_LOGIC;
    trn_pending_reg : out STD_LOGIC;
    wr_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axis_tx_tdata : out STD_LOGIC_VECTOR ( 50 downto 0 );
    s_axis_tx_tkeep : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axis_tx_tvalid : out STD_LOGIC;
    user_clk : in STD_LOGIC;
    m_axis_rx_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_tx_tready : in STD_LOGIC;
    pio_reset_n : in STD_LOGIC;
    m_axis_rx_tvalid : in STD_LOGIC;
    trn_pending : in STD_LOGIC;
    cfg_bus_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_device_number : in STD_LOGIC_VECTOR ( 4 downto 0 );
    cfg_function_number : in STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axis_rx_tlast : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_pcie_app_7x_0_PIO_EP : entity is "PIO_EP";
end spec7_pcie_app_7x_0_PIO_EP;

architecture STRUCTURE of spec7_pcie_app_7x_0_PIO_EP is
  signal EP_RX_inst_n_70 : STD_LOGIC;
  signal EP_RX_inst_n_71 : STD_LOGIC;
  signal EP_RX_inst_n_72 : STD_LOGIC;
  signal EP_RX_inst_n_73 : STD_LOGIC;
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal compl_done : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 6 downto 2 );
  signal \gen_cpl_64.state\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 22 downto 8 );
  signal req_compl : STD_LOGIC;
  signal req_compl_q : STD_LOGIC;
  signal req_compl_wd : STD_LOGIC;
  signal req_len : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal req_rid : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal req_tag : STD_LOGIC_VECTOR ( 3 downto 2 );
begin
  SR(0) <= \^sr\(0);
EP_RX_inst: entity work.spec7_pcie_app_7x_0_PIO_RX_ENGINE
     port map (
      D(8 downto 6) => p_0_in(22 downto 20),
      D(5 downto 2) => p_0_in(15 downto 12),
      D(1 downto 0) => p_0_in(9 downto 8),
      Q(1 downto 0) => req_tag(3 downto 2),
      SR(0) => \^sr\(0),
      compl_done => compl_done,
      \gen_cpl_64.state\ => \gen_cpl_64.state\,
      m_axis_rx_tdata(63 downto 0) => m_axis_rx_tdata(63 downto 0),
      m_axis_rx_tlast => m_axis_rx_tlast,
      m_axis_rx_tready => m_axis_rx_tready,
      m_axis_rx_tvalid => m_axis_rx_tvalid,
      pio_reset_n => pio_reset_n,
      \pio_rx_sm_64.req_addr_reg[6]_0\(4 downto 0) => data0(6 downto 2),
      \pio_rx_sm_64.req_be_reg[3]_0\(3) => EP_RX_inst_n_70,
      \pio_rx_sm_64.req_be_reg[3]_0\(2) => EP_RX_inst_n_71,
      \pio_rx_sm_64.req_be_reg[3]_0\(1) => EP_RX_inst_n_72,
      \pio_rx_sm_64.req_be_reg[3]_0\(0) => EP_RX_inst_n_73,
      \pio_rx_sm_64.req_len_reg[7]_0\(7 downto 0) => req_len(7 downto 0),
      \pio_rx_sm_64.req_rid_reg[15]_0\(12 downto 4) => req_rid(15 downto 7),
      \pio_rx_sm_64.req_rid_reg[15]_0\(3 downto 0) => req_rid(3 downto 0),
      req_compl => req_compl,
      req_compl_q => req_compl_q,
      req_compl_wd => req_compl_wd,
      trn_pending => trn_pending,
      trn_pending_reg => trn_pending_reg,
      user_clk => user_clk,
      wr_data(31 downto 0) => wr_data(31 downto 0),
      wr_en => wr_en
    );
EP_TX_inst: entity work.spec7_pcie_app_7x_0_PIO_TX_ENGINE
     port map (
      D(8 downto 6) => p_0_in(22 downto 20),
      D(5 downto 2) => p_0_in(15 downto 12),
      D(1 downto 0) => p_0_in(9 downto 8),
      Q(1 downto 0) => req_tag(3 downto 2),
      SR(0) => \^sr\(0),
      cfg_bus_number(7 downto 0) => cfg_bus_number(7 downto 0),
      cfg_device_number(4 downto 0) => cfg_device_number(4 downto 0),
      cfg_function_number(2 downto 0) => cfg_function_number(2 downto 0),
      compl_done => compl_done,
      \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(12 downto 4) => req_rid(15 downto 7),
      \gen_cpl_64.s_axis_tx_tdata_reg[31]_0\(3 downto 0) => req_rid(3 downto 0),
      \gen_cpl_64.s_axis_tx_tdata_reg[6]_0\(4 downto 0) => data0(6 downto 2),
      \gen_cpl_64.s_axis_tx_tdata_reg[7]_0\(7 downto 0) => req_len(7 downto 0),
      \gen_cpl_64.state\ => \gen_cpl_64.state\,
      pio_reset_n => pio_reset_n,
      \rd_be_reg[3]_0\(3) => EP_RX_inst_n_70,
      \rd_be_reg[3]_0\(2) => EP_RX_inst_n_71,
      \rd_be_reg[3]_0\(1) => EP_RX_inst_n_72,
      \rd_be_reg[3]_0\(0) => EP_RX_inst_n_73,
      req_compl => req_compl,
      req_compl_q => req_compl_q,
      req_compl_wd => req_compl_wd,
      s_axis_tx_tdata(50 downto 0) => s_axis_tx_tdata(50 downto 0),
      s_axis_tx_tkeep(1 downto 0) => s_axis_tx_tkeep(1 downto 0),
      s_axis_tx_tlast => s_axis_tx_tlast,
      s_axis_tx_tready => s_axis_tx_tready,
      s_axis_tx_tvalid => s_axis_tx_tvalid,
      user_clk => user_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_pcie_app_7x_0_PIO is
  port (
    wr_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_en : out STD_LOGIC;
    s_axis_tx_tdata : out STD_LOGIC_VECTOR ( 50 downto 0 );
    s_axis_tx_tkeep : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axis_tx_tvalid : out STD_LOGIC;
    cfg_turnoff_ok : out STD_LOGIC;
    m_axis_rx_tready : out STD_LOGIC;
    s_axis_tx_tlast : out STD_LOGIC;
    s_axis_tx_tready : in STD_LOGIC;
    cfg_to_turnoff : in STD_LOGIC;
    user_clk : in STD_LOGIC;
    m_axis_rx_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    cfg_bus_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_device_number : in STD_LOGIC_VECTOR ( 4 downto 0 );
    cfg_function_number : in STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axis_rx_tvalid : in STD_LOGIC;
    m_axis_rx_tlast : in STD_LOGIC;
    user_lnk_up : in STD_LOGIC;
    user_reset : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_pcie_app_7x_0_PIO : entity is "PIO";
end spec7_pcie_app_7x_0_PIO;

architecture STRUCTURE of spec7_pcie_app_7x_0_PIO is
  signal PIO_EP_inst_n_0 : STD_LOGIC;
  signal PIO_EP_inst_n_4 : STD_LOGIC;
  signal pio_reset_n : STD_LOGIC;
  signal pio_reset_n_i_1_n_0 : STD_LOGIC;
  signal trn_pending : STD_LOGIC;
begin
PIO_EP_inst: entity work.spec7_pcie_app_7x_0_PIO_EP
     port map (
      SR(0) => PIO_EP_inst_n_0,
      cfg_bus_number(7 downto 0) => cfg_bus_number(7 downto 0),
      cfg_device_number(4 downto 0) => cfg_device_number(4 downto 0),
      cfg_function_number(2 downto 0) => cfg_function_number(2 downto 0),
      m_axis_rx_tdata(63 downto 0) => m_axis_rx_tdata(63 downto 0),
      m_axis_rx_tlast => m_axis_rx_tlast,
      m_axis_rx_tready => m_axis_rx_tready,
      m_axis_rx_tvalid => m_axis_rx_tvalid,
      pio_reset_n => pio_reset_n,
      s_axis_tx_tdata(50 downto 0) => s_axis_tx_tdata(50 downto 0),
      s_axis_tx_tkeep(1 downto 0) => s_axis_tx_tkeep(1 downto 0),
      s_axis_tx_tlast => s_axis_tx_tlast,
      s_axis_tx_tready => s_axis_tx_tready,
      s_axis_tx_tvalid => s_axis_tx_tvalid,
      trn_pending => trn_pending,
      trn_pending_reg => PIO_EP_inst_n_4,
      user_clk => user_clk,
      wr_data(31 downto 0) => wr_data(31 downto 0),
      wr_en => wr_en
    );
PIO_TO_inst: entity work.spec7_pcie_app_7x_0_PIO_TO_CTRL
     port map (
      SR(0) => PIO_EP_inst_n_0,
      cfg_to_turnoff => cfg_to_turnoff,
      cfg_turnoff_ok => cfg_turnoff_ok,
      trn_pending => trn_pending,
      trn_pending_reg_0 => PIO_EP_inst_n_4,
      user_clk => user_clk
    );
pio_reset_n_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => user_lnk_up,
      I1 => user_reset,
      O => pio_reset_n_i_1_n_0
    );
pio_reset_n_reg: unisim.vcomponents.FDRE
     port map (
      C => user_clk,
      CE => '1',
      D => pio_reset_n_i_1_n_0,
      Q => pio_reset_n,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_pcie_app_7x_0_pcie_app_7x is
  port (
    wr_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_en : out STD_LOGIC;
    s_axis_tx_tdata : out STD_LOGIC_VECTOR ( 50 downto 0 );
    s_axis_tx_tkeep : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axis_tx_tvalid : out STD_LOGIC;
    cfg_turnoff_ok : out STD_LOGIC;
    m_axis_rx_tready : out STD_LOGIC;
    s_axis_tx_tlast : out STD_LOGIC;
    s_axis_tx_tready : in STD_LOGIC;
    cfg_to_turnoff : in STD_LOGIC;
    user_clk : in STD_LOGIC;
    m_axis_rx_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    cfg_bus_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_device_number : in STD_LOGIC_VECTOR ( 4 downto 0 );
    cfg_function_number : in STD_LOGIC_VECTOR ( 2 downto 0 );
    m_axis_rx_tvalid : in STD_LOGIC;
    m_axis_rx_tlast : in STD_LOGIC;
    user_lnk_up : in STD_LOGIC;
    user_reset : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_pcie_app_7x_0_pcie_app_7x : entity is "pcie_app_7x";
end spec7_pcie_app_7x_0_pcie_app_7x;

architecture STRUCTURE of spec7_pcie_app_7x_0_pcie_app_7x is
begin
PIO: entity work.spec7_pcie_app_7x_0_PIO
     port map (
      cfg_bus_number(7 downto 0) => cfg_bus_number(7 downto 0),
      cfg_device_number(4 downto 0) => cfg_device_number(4 downto 0),
      cfg_function_number(2 downto 0) => cfg_function_number(2 downto 0),
      cfg_to_turnoff => cfg_to_turnoff,
      cfg_turnoff_ok => cfg_turnoff_ok,
      m_axis_rx_tdata(63 downto 0) => m_axis_rx_tdata(63 downto 0),
      m_axis_rx_tlast => m_axis_rx_tlast,
      m_axis_rx_tready => m_axis_rx_tready,
      m_axis_rx_tvalid => m_axis_rx_tvalid,
      s_axis_tx_tdata(50 downto 0) => s_axis_tx_tdata(50 downto 0),
      s_axis_tx_tkeep(1 downto 0) => s_axis_tx_tkeep(1 downto 0),
      s_axis_tx_tlast => s_axis_tx_tlast,
      s_axis_tx_tready => s_axis_tx_tready,
      s_axis_tx_tvalid => s_axis_tx_tvalid,
      user_clk => user_clk,
      user_lnk_up => user_lnk_up,
      user_reset => user_reset,
      wr_data(31 downto 0) => wr_data(31 downto 0),
      wr_en => wr_en
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_pcie_app_7x_0 is
  port (
    user_clk : in STD_LOGIC;
    user_reset : in STD_LOGIC;
    user_lnk_up : in STD_LOGIC;
    s_axis_tx_tready : in STD_LOGIC;
    s_axis_tx_tdata : out STD_LOGIC_VECTOR ( 63 downto 0 );
    s_axis_tx_tkeep : out STD_LOGIC_VECTOR ( 7 downto 0 );
    s_axis_tx_tuser : out STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axis_tx_tlast : out STD_LOGIC;
    s_axis_tx_tvalid : out STD_LOGIC;
    m_axis_rx_tdata : in STD_LOGIC_VECTOR ( 63 downto 0 );
    m_axis_rx_tkeep : in STD_LOGIC_VECTOR ( 7 downto 0 );
    m_axis_rx_tlast : in STD_LOGIC;
    m_axis_rx_tvalid : in STD_LOGIC;
    m_axis_rx_tready : out STD_LOGIC;
    m_axis_rx_tuser : in STD_LOGIC_VECTOR ( 21 downto 0 );
    cfg_to_turnoff : in STD_LOGIC;
    cfg_bus_number : in STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_device_number : in STD_LOGIC_VECTOR ( 4 downto 0 );
    cfg_function_number : in STD_LOGIC_VECTOR ( 2 downto 0 );
    tx_cfg_gnt : out STD_LOGIC;
    cfg_pm_halt_aspm_l0s : out STD_LOGIC;
    cfg_pm_halt_aspm_l1 : out STD_LOGIC;
    cfg_pm_force_state_en : out STD_LOGIC;
    cfg_pm_force_state : out STD_LOGIC_VECTOR ( 1 downto 0 );
    rx_np_ok : out STD_LOGIC;
    rx_np_req : out STD_LOGIC;
    cfg_turnoff_ok : out STD_LOGIC;
    cfg_trn_pending : out STD_LOGIC;
    cfg_pm_wake : out STD_LOGIC;
    cfg_dsn : out STD_LOGIC_VECTOR ( 63 downto 0 );
    fc_sel : out STD_LOGIC_VECTOR ( 2 downto 0 );
    cfg_err_cor : out STD_LOGIC;
    cfg_err_ur : out STD_LOGIC;
    cfg_err_ecrc : out STD_LOGIC;
    cfg_err_cpl_timeout : out STD_LOGIC;
    cfg_err_cpl_unexpect : out STD_LOGIC;
    cfg_err_cpl_abort : out STD_LOGIC;
    cfg_err_atomic_egress_blocked : out STD_LOGIC;
    cfg_err_internal_cor : out STD_LOGIC;
    cfg_err_malformed : out STD_LOGIC;
    cfg_err_mc_blocked : out STD_LOGIC;
    cfg_err_poisoned : out STD_LOGIC;
    cfg_err_norecovery : out STD_LOGIC;
    cfg_err_acs : out STD_LOGIC;
    cfg_err_internal_uncor : out STD_LOGIC;
    cfg_err_posted : out STD_LOGIC;
    cfg_err_locked : out STD_LOGIC;
    cfg_err_tlp_cpl_header : out STD_LOGIC_VECTOR ( 47 downto 0 );
    cfg_err_aer_headerlog : out STD_LOGIC_VECTOR ( 127 downto 0 );
    cfg_aer_interrupt_msgnum : out STD_LOGIC_VECTOR ( 4 downto 0 );
    pl_directed_link_change : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_directed_link_width : out STD_LOGIC_VECTOR ( 1 downto 0 );
    pl_directed_link_speed : out STD_LOGIC;
    pl_directed_link_auton : out STD_LOGIC;
    pl_upstream_prefer_deemph : out STD_LOGIC;
    cfg_mgmt_di : out STD_LOGIC_VECTOR ( 31 downto 0 );
    cfg_mgmt_byte_en : out STD_LOGIC_VECTOR ( 3 downto 0 );
    cfg_mgmt_dwaddr : out STD_LOGIC_VECTOR ( 9 downto 0 );
    cfg_mgmt_wr_en : out STD_LOGIC;
    cfg_mgmt_rd_en : out STD_LOGIC;
    cfg_mgmt_wr_readonly : out STD_LOGIC;
    cfg_interrupt : out STD_LOGIC;
    cfg_interrupt_assert : out STD_LOGIC;
    cfg_interrupt_di : out STD_LOGIC_VECTOR ( 7 downto 0 );
    cfg_interrupt_stat : out STD_LOGIC;
    cfg_pciecap_interrupt_msgnum : out STD_LOGIC_VECTOR ( 4 downto 0 );
    wr_data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    wr_en : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of spec7_pcie_app_7x_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of spec7_pcie_app_7x_0 : entity is "spec7_pcie_app_7x_0,pcie_app_7x,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of spec7_pcie_app_7x_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of spec7_pcie_app_7x_0 : entity is "pcie_app_7x,Vivado 2019.2";
end spec7_pcie_app_7x_0;

architecture STRUCTURE of spec7_pcie_app_7x_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^s_axis_tx_tdata\ : STD_LOGIC_VECTOR ( 63 downto 0 );
  signal \^s_axis_tx_tkeep\ : STD_LOGIC_VECTOR ( 6 downto 2 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of cfg_err_acs : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err acs";
  attribute X_INTERFACE_INFO of cfg_err_atomic_egress_blocked : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err atomic_egress_blocked";
  attribute X_INTERFACE_INFO of cfg_err_cor : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cor";
  attribute X_INTERFACE_INFO of cfg_err_cpl_abort : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cpl_abort";
  attribute X_INTERFACE_INFO of cfg_err_cpl_timeout : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cpl_timeout";
  attribute X_INTERFACE_INFO of cfg_err_cpl_unexpect : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cpl_unexpect";
  attribute X_INTERFACE_INFO of cfg_err_ecrc : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err ecrc";
  attribute X_INTERFACE_INFO of cfg_err_internal_cor : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err internal_cor";
  attribute X_INTERFACE_INFO of cfg_err_internal_uncor : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err internal_uncor";
  attribute X_INTERFACE_INFO of cfg_err_locked : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err locked";
  attribute X_INTERFACE_INFO of cfg_err_malformed : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err malformed";
  attribute X_INTERFACE_INFO of cfg_err_mc_blocked : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err mc_blocked";
  attribute X_INTERFACE_INFO of cfg_err_norecovery : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err norecovery";
  attribute X_INTERFACE_INFO of cfg_err_poisoned : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err poisoned";
  attribute X_INTERFACE_INFO of cfg_err_posted : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err posted";
  attribute X_INTERFACE_INFO of cfg_err_ur : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err ur";
  attribute X_INTERFACE_INFO of cfg_interrupt : signal is "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt interrupt";
  attribute X_INTERFACE_INFO of cfg_interrupt_assert : signal is "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt assert";
  attribute X_INTERFACE_INFO of cfg_interrupt_stat : signal is "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt stat";
  attribute X_INTERFACE_INFO of cfg_mgmt_rd_en : signal is "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt READ_EN";
  attribute X_INTERFACE_INFO of cfg_mgmt_wr_en : signal is "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt WRITE_EN";
  attribute X_INTERFACE_INFO of cfg_mgmt_wr_readonly : signal is "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt READONLY";
  attribute X_INTERFACE_INFO of cfg_pm_force_state_en : signal is "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_force_state_en";
  attribute X_INTERFACE_INFO of cfg_pm_halt_aspm_l0s : signal is "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_halt_aspm_l0s";
  attribute X_INTERFACE_INFO of cfg_pm_halt_aspm_l1 : signal is "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_halt_aspm_l1";
  attribute X_INTERFACE_INFO of cfg_pm_wake : signal is "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_wake";
  attribute X_INTERFACE_INFO of cfg_to_turnoff : signal is "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status turnoff";
  attribute X_INTERFACE_INFO of cfg_trn_pending : signal is "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control trn_pending";
  attribute X_INTERFACE_INFO of cfg_turnoff_ok : signal is "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control turnoff_ok";
  attribute X_INTERFACE_INFO of m_axis_rx_tlast : signal is "xilinx.com:interface:axis:1.0 m_axis_rx TLAST";
  attribute X_INTERFACE_INFO of m_axis_rx_tready : signal is "xilinx.com:interface:axis:1.0 m_axis_rx TREADY";
  attribute X_INTERFACE_INFO of m_axis_rx_tvalid : signal is "xilinx.com:interface:axis:1.0 m_axis_rx TVALID";
  attribute X_INTERFACE_INFO of pl_directed_link_auton : signal is "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_auton";
  attribute X_INTERFACE_INFO of pl_directed_link_speed : signal is "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_speed";
  attribute X_INTERFACE_INFO of pl_upstream_prefer_deemph : signal is "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl upstream_prefer_deemph";
  attribute X_INTERFACE_INFO of rx_np_ok : signal is "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control rx_np_ok";
  attribute X_INTERFACE_INFO of rx_np_req : signal is "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control rx_np_req";
  attribute X_INTERFACE_INFO of s_axis_tx_tlast : signal is "xilinx.com:interface:axis:1.0 s_axis_tx TLAST";
  attribute X_INTERFACE_INFO of s_axis_tx_tready : signal is "xilinx.com:interface:axis:1.0 s_axis_tx TREADY";
  attribute X_INTERFACE_INFO of s_axis_tx_tvalid : signal is "xilinx.com:interface:axis:1.0 s_axis_tx TVALID";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s_axis_tx_tvalid : signal is "XIL_INTERFACENAME s_axis_tx, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 4, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of tx_cfg_gnt : signal is "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control tx_cfg_gnt";
  attribute X_INTERFACE_INFO of user_clk : signal is "xilinx.com:signal:clock:1.0 user_signal_clock CLK";
  attribute X_INTERFACE_PARAMETER of user_clk : signal is "XIL_INTERFACENAME user_signal_clock, ASSOCIATED_BUSIF m_axis_rx:s_axis_tx, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of cfg_aer_interrupt_msgnum : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err aer_interrupt_msgnum";
  attribute X_INTERFACE_INFO of cfg_bus_number : signal is "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status bus_number";
  attribute X_INTERFACE_INFO of cfg_device_number : signal is "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status device_number";
  attribute X_INTERFACE_INFO of cfg_dsn : signal is "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control dsn";
  attribute X_INTERFACE_INFO of cfg_err_aer_headerlog : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err err_aer_headerlog";
  attribute X_INTERFACE_INFO of cfg_err_tlp_cpl_header : signal is "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err tlp_cpl_header";
  attribute X_INTERFACE_INFO of cfg_function_number : signal is "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status function_number";
  attribute X_INTERFACE_INFO of cfg_interrupt_di : signal is "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt write_data";
  attribute X_INTERFACE_INFO of cfg_mgmt_byte_en : signal is "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt BYTE_EN";
  attribute X_INTERFACE_INFO of cfg_mgmt_di : signal is "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt WRITE_DATA";
  attribute X_INTERFACE_INFO of cfg_mgmt_dwaddr : signal is "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt ADDR";
  attribute X_INTERFACE_INFO of cfg_pciecap_interrupt_msgnum : signal is "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt pciecap_interrupt_msgnum";
  attribute X_INTERFACE_INFO of cfg_pm_force_state : signal is "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_force_state";
  attribute X_INTERFACE_INFO of fc_sel : signal is "xilinx.com:interface:pcie_cfg_fc:1.0 pcie_cfg_fc SEL";
  attribute X_INTERFACE_INFO of m_axis_rx_tdata : signal is "xilinx.com:interface:axis:1.0 m_axis_rx TDATA";
  attribute X_INTERFACE_INFO of m_axis_rx_tkeep : signal is "xilinx.com:interface:axis:1.0 m_axis_rx TKEEP";
  attribute X_INTERFACE_INFO of m_axis_rx_tuser : signal is "xilinx.com:interface:axis:1.0 m_axis_rx TUSER";
  attribute X_INTERFACE_PARAMETER of m_axis_rx_tuser : signal is "XIL_INTERFACENAME m_axis_rx, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 22, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of pl_directed_link_change : signal is "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_change";
  attribute X_INTERFACE_INFO of pl_directed_link_width : signal is "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_width";
  attribute X_INTERFACE_INFO of s_axis_tx_tdata : signal is "xilinx.com:interface:axis:1.0 s_axis_tx TDATA";
  attribute X_INTERFACE_INFO of s_axis_tx_tkeep : signal is "xilinx.com:interface:axis:1.0 s_axis_tx TKEEP";
  attribute X_INTERFACE_INFO of s_axis_tx_tuser : signal is "xilinx.com:interface:axis:1.0 s_axis_tx TUSER";
begin
  cfg_aer_interrupt_msgnum(4) <= \<const0>\;
  cfg_aer_interrupt_msgnum(3) <= \<const0>\;
  cfg_aer_interrupt_msgnum(2) <= \<const0>\;
  cfg_aer_interrupt_msgnum(1) <= \<const0>\;
  cfg_aer_interrupt_msgnum(0) <= \<const0>\;
  cfg_dsn(63) <= \<const0>\;
  cfg_dsn(62) <= \<const0>\;
  cfg_dsn(61) <= \<const0>\;
  cfg_dsn(60) <= \<const0>\;
  cfg_dsn(59) <= \<const0>\;
  cfg_dsn(58) <= \<const0>\;
  cfg_dsn(57) <= \<const0>\;
  cfg_dsn(56) <= \<const0>\;
  cfg_dsn(55) <= \<const0>\;
  cfg_dsn(54) <= \<const0>\;
  cfg_dsn(53) <= \<const0>\;
  cfg_dsn(52) <= \<const0>\;
  cfg_dsn(51) <= \<const0>\;
  cfg_dsn(50) <= \<const0>\;
  cfg_dsn(49) <= \<const0>\;
  cfg_dsn(48) <= \<const0>\;
  cfg_dsn(47) <= \<const0>\;
  cfg_dsn(46) <= \<const0>\;
  cfg_dsn(45) <= \<const0>\;
  cfg_dsn(44) <= \<const0>\;
  cfg_dsn(43) <= \<const0>\;
  cfg_dsn(42) <= \<const0>\;
  cfg_dsn(41) <= \<const0>\;
  cfg_dsn(40) <= \<const0>\;
  cfg_dsn(39) <= \<const0>\;
  cfg_dsn(38) <= \<const0>\;
  cfg_dsn(37) <= \<const0>\;
  cfg_dsn(36) <= \<const0>\;
  cfg_dsn(35) <= \<const0>\;
  cfg_dsn(34) <= \<const0>\;
  cfg_dsn(33) <= \<const0>\;
  cfg_dsn(32) <= \<const0>\;
  cfg_dsn(31) <= \<const0>\;
  cfg_dsn(30) <= \<const0>\;
  cfg_dsn(29) <= \<const0>\;
  cfg_dsn(28) <= \<const0>\;
  cfg_dsn(27) <= \<const0>\;
  cfg_dsn(26) <= \<const0>\;
  cfg_dsn(25) <= \<const0>\;
  cfg_dsn(24) <= \<const0>\;
  cfg_dsn(23) <= \<const0>\;
  cfg_dsn(22) <= \<const0>\;
  cfg_dsn(21) <= \<const0>\;
  cfg_dsn(20) <= \<const0>\;
  cfg_dsn(19) <= \<const0>\;
  cfg_dsn(18) <= \<const0>\;
  cfg_dsn(17) <= \<const0>\;
  cfg_dsn(16) <= \<const0>\;
  cfg_dsn(15) <= \<const0>\;
  cfg_dsn(14) <= \<const0>\;
  cfg_dsn(13) <= \<const0>\;
  cfg_dsn(12) <= \<const0>\;
  cfg_dsn(11) <= \<const0>\;
  cfg_dsn(10) <= \<const0>\;
  cfg_dsn(9) <= \<const0>\;
  cfg_dsn(8) <= \<const0>\;
  cfg_dsn(7) <= \<const0>\;
  cfg_dsn(6) <= \<const0>\;
  cfg_dsn(5) <= \<const0>\;
  cfg_dsn(4) <= \<const0>\;
  cfg_dsn(3) <= \<const0>\;
  cfg_dsn(2) <= \<const0>\;
  cfg_dsn(1) <= \<const0>\;
  cfg_dsn(0) <= \<const0>\;
  cfg_err_acs <= \<const0>\;
  cfg_err_aer_headerlog(127) <= \<const0>\;
  cfg_err_aer_headerlog(126) <= \<const0>\;
  cfg_err_aer_headerlog(125) <= \<const0>\;
  cfg_err_aer_headerlog(124) <= \<const0>\;
  cfg_err_aer_headerlog(123) <= \<const0>\;
  cfg_err_aer_headerlog(122) <= \<const0>\;
  cfg_err_aer_headerlog(121) <= \<const0>\;
  cfg_err_aer_headerlog(120) <= \<const0>\;
  cfg_err_aer_headerlog(119) <= \<const0>\;
  cfg_err_aer_headerlog(118) <= \<const0>\;
  cfg_err_aer_headerlog(117) <= \<const0>\;
  cfg_err_aer_headerlog(116) <= \<const0>\;
  cfg_err_aer_headerlog(115) <= \<const0>\;
  cfg_err_aer_headerlog(114) <= \<const0>\;
  cfg_err_aer_headerlog(113) <= \<const0>\;
  cfg_err_aer_headerlog(112) <= \<const0>\;
  cfg_err_aer_headerlog(111) <= \<const0>\;
  cfg_err_aer_headerlog(110) <= \<const0>\;
  cfg_err_aer_headerlog(109) <= \<const0>\;
  cfg_err_aer_headerlog(108) <= \<const0>\;
  cfg_err_aer_headerlog(107) <= \<const0>\;
  cfg_err_aer_headerlog(106) <= \<const0>\;
  cfg_err_aer_headerlog(105) <= \<const0>\;
  cfg_err_aer_headerlog(104) <= \<const0>\;
  cfg_err_aer_headerlog(103) <= \<const0>\;
  cfg_err_aer_headerlog(102) <= \<const0>\;
  cfg_err_aer_headerlog(101) <= \<const0>\;
  cfg_err_aer_headerlog(100) <= \<const0>\;
  cfg_err_aer_headerlog(99) <= \<const0>\;
  cfg_err_aer_headerlog(98) <= \<const0>\;
  cfg_err_aer_headerlog(97) <= \<const0>\;
  cfg_err_aer_headerlog(96) <= \<const0>\;
  cfg_err_aer_headerlog(95) <= \<const0>\;
  cfg_err_aer_headerlog(94) <= \<const0>\;
  cfg_err_aer_headerlog(93) <= \<const0>\;
  cfg_err_aer_headerlog(92) <= \<const0>\;
  cfg_err_aer_headerlog(91) <= \<const0>\;
  cfg_err_aer_headerlog(90) <= \<const0>\;
  cfg_err_aer_headerlog(89) <= \<const0>\;
  cfg_err_aer_headerlog(88) <= \<const0>\;
  cfg_err_aer_headerlog(87) <= \<const0>\;
  cfg_err_aer_headerlog(86) <= \<const0>\;
  cfg_err_aer_headerlog(85) <= \<const0>\;
  cfg_err_aer_headerlog(84) <= \<const0>\;
  cfg_err_aer_headerlog(83) <= \<const0>\;
  cfg_err_aer_headerlog(82) <= \<const0>\;
  cfg_err_aer_headerlog(81) <= \<const0>\;
  cfg_err_aer_headerlog(80) <= \<const0>\;
  cfg_err_aer_headerlog(79) <= \<const0>\;
  cfg_err_aer_headerlog(78) <= \<const0>\;
  cfg_err_aer_headerlog(77) <= \<const0>\;
  cfg_err_aer_headerlog(76) <= \<const0>\;
  cfg_err_aer_headerlog(75) <= \<const0>\;
  cfg_err_aer_headerlog(74) <= \<const0>\;
  cfg_err_aer_headerlog(73) <= \<const0>\;
  cfg_err_aer_headerlog(72) <= \<const0>\;
  cfg_err_aer_headerlog(71) <= \<const0>\;
  cfg_err_aer_headerlog(70) <= \<const0>\;
  cfg_err_aer_headerlog(69) <= \<const0>\;
  cfg_err_aer_headerlog(68) <= \<const0>\;
  cfg_err_aer_headerlog(67) <= \<const0>\;
  cfg_err_aer_headerlog(66) <= \<const0>\;
  cfg_err_aer_headerlog(65) <= \<const0>\;
  cfg_err_aer_headerlog(64) <= \<const0>\;
  cfg_err_aer_headerlog(63) <= \<const0>\;
  cfg_err_aer_headerlog(62) <= \<const0>\;
  cfg_err_aer_headerlog(61) <= \<const0>\;
  cfg_err_aer_headerlog(60) <= \<const0>\;
  cfg_err_aer_headerlog(59) <= \<const0>\;
  cfg_err_aer_headerlog(58) <= \<const0>\;
  cfg_err_aer_headerlog(57) <= \<const0>\;
  cfg_err_aer_headerlog(56) <= \<const0>\;
  cfg_err_aer_headerlog(55) <= \<const0>\;
  cfg_err_aer_headerlog(54) <= \<const0>\;
  cfg_err_aer_headerlog(53) <= \<const0>\;
  cfg_err_aer_headerlog(52) <= \<const0>\;
  cfg_err_aer_headerlog(51) <= \<const0>\;
  cfg_err_aer_headerlog(50) <= \<const0>\;
  cfg_err_aer_headerlog(49) <= \<const0>\;
  cfg_err_aer_headerlog(48) <= \<const0>\;
  cfg_err_aer_headerlog(47) <= \<const0>\;
  cfg_err_aer_headerlog(46) <= \<const0>\;
  cfg_err_aer_headerlog(45) <= \<const0>\;
  cfg_err_aer_headerlog(44) <= \<const0>\;
  cfg_err_aer_headerlog(43) <= \<const0>\;
  cfg_err_aer_headerlog(42) <= \<const0>\;
  cfg_err_aer_headerlog(41) <= \<const0>\;
  cfg_err_aer_headerlog(40) <= \<const0>\;
  cfg_err_aer_headerlog(39) <= \<const0>\;
  cfg_err_aer_headerlog(38) <= \<const0>\;
  cfg_err_aer_headerlog(37) <= \<const0>\;
  cfg_err_aer_headerlog(36) <= \<const0>\;
  cfg_err_aer_headerlog(35) <= \<const0>\;
  cfg_err_aer_headerlog(34) <= \<const0>\;
  cfg_err_aer_headerlog(33) <= \<const0>\;
  cfg_err_aer_headerlog(32) <= \<const0>\;
  cfg_err_aer_headerlog(31) <= \<const0>\;
  cfg_err_aer_headerlog(30) <= \<const0>\;
  cfg_err_aer_headerlog(29) <= \<const0>\;
  cfg_err_aer_headerlog(28) <= \<const0>\;
  cfg_err_aer_headerlog(27) <= \<const0>\;
  cfg_err_aer_headerlog(26) <= \<const0>\;
  cfg_err_aer_headerlog(25) <= \<const0>\;
  cfg_err_aer_headerlog(24) <= \<const0>\;
  cfg_err_aer_headerlog(23) <= \<const0>\;
  cfg_err_aer_headerlog(22) <= \<const0>\;
  cfg_err_aer_headerlog(21) <= \<const0>\;
  cfg_err_aer_headerlog(20) <= \<const0>\;
  cfg_err_aer_headerlog(19) <= \<const0>\;
  cfg_err_aer_headerlog(18) <= \<const0>\;
  cfg_err_aer_headerlog(17) <= \<const0>\;
  cfg_err_aer_headerlog(16) <= \<const0>\;
  cfg_err_aer_headerlog(15) <= \<const0>\;
  cfg_err_aer_headerlog(14) <= \<const0>\;
  cfg_err_aer_headerlog(13) <= \<const0>\;
  cfg_err_aer_headerlog(12) <= \<const0>\;
  cfg_err_aer_headerlog(11) <= \<const0>\;
  cfg_err_aer_headerlog(10) <= \<const0>\;
  cfg_err_aer_headerlog(9) <= \<const0>\;
  cfg_err_aer_headerlog(8) <= \<const0>\;
  cfg_err_aer_headerlog(7) <= \<const0>\;
  cfg_err_aer_headerlog(6) <= \<const0>\;
  cfg_err_aer_headerlog(5) <= \<const0>\;
  cfg_err_aer_headerlog(4) <= \<const0>\;
  cfg_err_aer_headerlog(3) <= \<const0>\;
  cfg_err_aer_headerlog(2) <= \<const0>\;
  cfg_err_aer_headerlog(1) <= \<const0>\;
  cfg_err_aer_headerlog(0) <= \<const0>\;
  cfg_err_atomic_egress_blocked <= \<const0>\;
  cfg_err_cor <= \<const0>\;
  cfg_err_cpl_abort <= \<const0>\;
  cfg_err_cpl_timeout <= \<const0>\;
  cfg_err_cpl_unexpect <= \<const0>\;
  cfg_err_ecrc <= \<const0>\;
  cfg_err_internal_cor <= \<const0>\;
  cfg_err_internal_uncor <= \<const0>\;
  cfg_err_locked <= \<const0>\;
  cfg_err_malformed <= \<const0>\;
  cfg_err_mc_blocked <= \<const0>\;
  cfg_err_norecovery <= \<const0>\;
  cfg_err_poisoned <= \<const0>\;
  cfg_err_posted <= \<const0>\;
  cfg_err_tlp_cpl_header(47) <= \<const0>\;
  cfg_err_tlp_cpl_header(46) <= \<const0>\;
  cfg_err_tlp_cpl_header(45) <= \<const0>\;
  cfg_err_tlp_cpl_header(44) <= \<const0>\;
  cfg_err_tlp_cpl_header(43) <= \<const0>\;
  cfg_err_tlp_cpl_header(42) <= \<const0>\;
  cfg_err_tlp_cpl_header(41) <= \<const0>\;
  cfg_err_tlp_cpl_header(40) <= \<const0>\;
  cfg_err_tlp_cpl_header(39) <= \<const0>\;
  cfg_err_tlp_cpl_header(38) <= \<const0>\;
  cfg_err_tlp_cpl_header(37) <= \<const0>\;
  cfg_err_tlp_cpl_header(36) <= \<const0>\;
  cfg_err_tlp_cpl_header(35) <= \<const0>\;
  cfg_err_tlp_cpl_header(34) <= \<const0>\;
  cfg_err_tlp_cpl_header(33) <= \<const0>\;
  cfg_err_tlp_cpl_header(32) <= \<const0>\;
  cfg_err_tlp_cpl_header(31) <= \<const0>\;
  cfg_err_tlp_cpl_header(30) <= \<const0>\;
  cfg_err_tlp_cpl_header(29) <= \<const0>\;
  cfg_err_tlp_cpl_header(28) <= \<const0>\;
  cfg_err_tlp_cpl_header(27) <= \<const0>\;
  cfg_err_tlp_cpl_header(26) <= \<const0>\;
  cfg_err_tlp_cpl_header(25) <= \<const0>\;
  cfg_err_tlp_cpl_header(24) <= \<const0>\;
  cfg_err_tlp_cpl_header(23) <= \<const0>\;
  cfg_err_tlp_cpl_header(22) <= \<const0>\;
  cfg_err_tlp_cpl_header(21) <= \<const0>\;
  cfg_err_tlp_cpl_header(20) <= \<const0>\;
  cfg_err_tlp_cpl_header(19) <= \<const0>\;
  cfg_err_tlp_cpl_header(18) <= \<const0>\;
  cfg_err_tlp_cpl_header(17) <= \<const0>\;
  cfg_err_tlp_cpl_header(16) <= \<const0>\;
  cfg_err_tlp_cpl_header(15) <= \<const0>\;
  cfg_err_tlp_cpl_header(14) <= \<const0>\;
  cfg_err_tlp_cpl_header(13) <= \<const0>\;
  cfg_err_tlp_cpl_header(12) <= \<const0>\;
  cfg_err_tlp_cpl_header(11) <= \<const0>\;
  cfg_err_tlp_cpl_header(10) <= \<const0>\;
  cfg_err_tlp_cpl_header(9) <= \<const0>\;
  cfg_err_tlp_cpl_header(8) <= \<const0>\;
  cfg_err_tlp_cpl_header(7) <= \<const0>\;
  cfg_err_tlp_cpl_header(6) <= \<const0>\;
  cfg_err_tlp_cpl_header(5) <= \<const0>\;
  cfg_err_tlp_cpl_header(4) <= \<const0>\;
  cfg_err_tlp_cpl_header(3) <= \<const0>\;
  cfg_err_tlp_cpl_header(2) <= \<const0>\;
  cfg_err_tlp_cpl_header(1) <= \<const0>\;
  cfg_err_tlp_cpl_header(0) <= \<const0>\;
  cfg_err_ur <= \<const0>\;
  cfg_interrupt <= \<const0>\;
  cfg_interrupt_assert <= \<const0>\;
  cfg_interrupt_di(7) <= \<const0>\;
  cfg_interrupt_di(6) <= \<const0>\;
  cfg_interrupt_di(5) <= \<const0>\;
  cfg_interrupt_di(4) <= \<const0>\;
  cfg_interrupt_di(3) <= \<const0>\;
  cfg_interrupt_di(2) <= \<const0>\;
  cfg_interrupt_di(1) <= \<const0>\;
  cfg_interrupt_di(0) <= \<const0>\;
  cfg_interrupt_stat <= \<const0>\;
  cfg_mgmt_byte_en(3) <= \<const0>\;
  cfg_mgmt_byte_en(2) <= \<const0>\;
  cfg_mgmt_byte_en(1) <= \<const0>\;
  cfg_mgmt_byte_en(0) <= \<const0>\;
  cfg_mgmt_di(31) <= \<const0>\;
  cfg_mgmt_di(30) <= \<const0>\;
  cfg_mgmt_di(29) <= \<const0>\;
  cfg_mgmt_di(28) <= \<const0>\;
  cfg_mgmt_di(27) <= \<const0>\;
  cfg_mgmt_di(26) <= \<const0>\;
  cfg_mgmt_di(25) <= \<const0>\;
  cfg_mgmt_di(24) <= \<const0>\;
  cfg_mgmt_di(23) <= \<const0>\;
  cfg_mgmt_di(22) <= \<const0>\;
  cfg_mgmt_di(21) <= \<const0>\;
  cfg_mgmt_di(20) <= \<const0>\;
  cfg_mgmt_di(19) <= \<const0>\;
  cfg_mgmt_di(18) <= \<const0>\;
  cfg_mgmt_di(17) <= \<const0>\;
  cfg_mgmt_di(16) <= \<const0>\;
  cfg_mgmt_di(15) <= \<const0>\;
  cfg_mgmt_di(14) <= \<const0>\;
  cfg_mgmt_di(13) <= \<const0>\;
  cfg_mgmt_di(12) <= \<const0>\;
  cfg_mgmt_di(11) <= \<const0>\;
  cfg_mgmt_di(10) <= \<const0>\;
  cfg_mgmt_di(9) <= \<const0>\;
  cfg_mgmt_di(8) <= \<const0>\;
  cfg_mgmt_di(7) <= \<const0>\;
  cfg_mgmt_di(6) <= \<const0>\;
  cfg_mgmt_di(5) <= \<const0>\;
  cfg_mgmt_di(4) <= \<const0>\;
  cfg_mgmt_di(3) <= \<const0>\;
  cfg_mgmt_di(2) <= \<const0>\;
  cfg_mgmt_di(1) <= \<const0>\;
  cfg_mgmt_di(0) <= \<const0>\;
  cfg_mgmt_dwaddr(9) <= \<const0>\;
  cfg_mgmt_dwaddr(8) <= \<const0>\;
  cfg_mgmt_dwaddr(7) <= \<const0>\;
  cfg_mgmt_dwaddr(6) <= \<const0>\;
  cfg_mgmt_dwaddr(5) <= \<const0>\;
  cfg_mgmt_dwaddr(4) <= \<const0>\;
  cfg_mgmt_dwaddr(3) <= \<const0>\;
  cfg_mgmt_dwaddr(2) <= \<const0>\;
  cfg_mgmt_dwaddr(1) <= \<const0>\;
  cfg_mgmt_dwaddr(0) <= \<const0>\;
  cfg_mgmt_rd_en <= \<const0>\;
  cfg_mgmt_wr_en <= \<const0>\;
  cfg_mgmt_wr_readonly <= \<const0>\;
  cfg_pciecap_interrupt_msgnum(4) <= \<const0>\;
  cfg_pciecap_interrupt_msgnum(3) <= \<const0>\;
  cfg_pciecap_interrupt_msgnum(2) <= \<const0>\;
  cfg_pciecap_interrupt_msgnum(1) <= \<const0>\;
  cfg_pciecap_interrupt_msgnum(0) <= \<const0>\;
  cfg_pm_force_state(1) <= \<const0>\;
  cfg_pm_force_state(0) <= \<const0>\;
  cfg_pm_force_state_en <= \<const0>\;
  cfg_pm_halt_aspm_l0s <= \<const0>\;
  cfg_pm_halt_aspm_l1 <= \<const0>\;
  cfg_pm_wake <= \<const0>\;
  cfg_trn_pending <= \<const0>\;
  fc_sel(2) <= \<const0>\;
  fc_sel(1) <= \<const0>\;
  fc_sel(0) <= \<const0>\;
  pl_directed_link_auton <= \<const0>\;
  pl_directed_link_change(1) <= \<const0>\;
  pl_directed_link_change(0) <= \<const0>\;
  pl_directed_link_speed <= \<const0>\;
  pl_directed_link_width(1) <= \<const0>\;
  pl_directed_link_width(0) <= \<const0>\;
  pl_upstream_prefer_deemph <= \<const1>\;
  rx_np_ok <= \<const1>\;
  rx_np_req <= \<const1>\;
  s_axis_tx_tdata(63 downto 48) <= \^s_axis_tx_tdata\(63 downto 48);
  s_axis_tx_tdata(47) <= \<const0>\;
  s_axis_tx_tdata(46) <= \<const0>\;
  s_axis_tx_tdata(45) <= \<const0>\;
  s_axis_tx_tdata(44) <= \<const0>\;
  s_axis_tx_tdata(43) <= \<const0>\;
  s_axis_tx_tdata(42) <= \<const0>\;
  s_axis_tx_tdata(41) <= \<const0>\;
  s_axis_tx_tdata(40) <= \<const0>\;
  s_axis_tx_tdata(39) <= \<const0>\;
  s_axis_tx_tdata(38) <= \<const0>\;
  s_axis_tx_tdata(37) <= \<const0>\;
  s_axis_tx_tdata(36) <= \<const0>\;
  s_axis_tx_tdata(35) <= \<const0>\;
  s_axis_tx_tdata(34 downto 0) <= \^s_axis_tx_tdata\(34 downto 0);
  s_axis_tx_tkeep(7) <= \^s_axis_tx_tkeep\(6);
  s_axis_tx_tkeep(6) <= \^s_axis_tx_tkeep\(6);
  s_axis_tx_tkeep(5) <= \^s_axis_tx_tkeep\(6);
  s_axis_tx_tkeep(4) <= \^s_axis_tx_tkeep\(6);
  s_axis_tx_tkeep(3) <= \^s_axis_tx_tkeep\(2);
  s_axis_tx_tkeep(2) <= \^s_axis_tx_tkeep\(2);
  s_axis_tx_tkeep(1) <= \^s_axis_tx_tkeep\(2);
  s_axis_tx_tkeep(0) <= \^s_axis_tx_tkeep\(2);
  s_axis_tx_tuser(3) <= \<const0>\;
  s_axis_tx_tuser(2) <= \<const0>\;
  s_axis_tx_tuser(1) <= \<const0>\;
  s_axis_tx_tuser(0) <= \<const0>\;
  tx_cfg_gnt <= \<const1>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.spec7_pcie_app_7x_0_pcie_app_7x
     port map (
      cfg_bus_number(7 downto 0) => cfg_bus_number(7 downto 0),
      cfg_device_number(4 downto 0) => cfg_device_number(4 downto 0),
      cfg_function_number(2 downto 0) => cfg_function_number(2 downto 0),
      cfg_to_turnoff => cfg_to_turnoff,
      cfg_turnoff_ok => cfg_turnoff_ok,
      m_axis_rx_tdata(63 downto 0) => m_axis_rx_tdata(63 downto 0),
      m_axis_rx_tlast => m_axis_rx_tlast,
      m_axis_rx_tready => m_axis_rx_tready,
      m_axis_rx_tvalid => m_axis_rx_tvalid,
      s_axis_tx_tdata(50 downto 35) => \^s_axis_tx_tdata\(63 downto 48),
      s_axis_tx_tdata(34 downto 0) => \^s_axis_tx_tdata\(34 downto 0),
      s_axis_tx_tkeep(1) => \^s_axis_tx_tkeep\(6),
      s_axis_tx_tkeep(0) => \^s_axis_tx_tkeep\(2),
      s_axis_tx_tlast => s_axis_tx_tlast,
      s_axis_tx_tready => s_axis_tx_tready,
      s_axis_tx_tvalid => s_axis_tx_tvalid,
      user_clk => user_clk,
      user_lnk_up => user_lnk_up,
      user_reset => user_reset,
      wr_data(31 downto 0) => wr_data(31 downto 0),
      wr_en => wr_en
    );
end STRUCTURE;
