// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Sat Oct  3 15:01:19 2020
// Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_pcie_app_7x_0/spec7_pcie_app_7x_0_stub.v
// Design      : spec7_pcie_app_7x_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z030fbg676-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "pcie_app_7x,Vivado 2019.2" *)
module spec7_pcie_app_7x_0(user_clk, user_reset, user_lnk_up, 
  s_axis_tx_tready, s_axis_tx_tdata, s_axis_tx_tkeep, s_axis_tx_tuser, s_axis_tx_tlast, 
  s_axis_tx_tvalid, m_axis_rx_tdata, m_axis_rx_tkeep, m_axis_rx_tlast, m_axis_rx_tvalid, 
  m_axis_rx_tready, m_axis_rx_tuser, cfg_to_turnoff, cfg_bus_number, cfg_device_number, 
  cfg_function_number, tx_cfg_gnt, cfg_pm_halt_aspm_l0s, cfg_pm_halt_aspm_l1, 
  cfg_pm_force_state_en, cfg_pm_force_state, rx_np_ok, rx_np_req, cfg_turnoff_ok, 
  cfg_trn_pending, cfg_pm_wake, cfg_dsn, fc_sel, cfg_err_cor, cfg_err_ur, cfg_err_ecrc, 
  cfg_err_cpl_timeout, cfg_err_cpl_unexpect, cfg_err_cpl_abort, 
  cfg_err_atomic_egress_blocked, cfg_err_internal_cor, cfg_err_malformed, 
  cfg_err_mc_blocked, cfg_err_poisoned, cfg_err_norecovery, cfg_err_acs, 
  cfg_err_internal_uncor, cfg_err_posted, cfg_err_locked, cfg_err_tlp_cpl_header, 
  cfg_err_aer_headerlog, cfg_aer_interrupt_msgnum, pl_directed_link_change, 
  pl_directed_link_width, pl_directed_link_speed, pl_directed_link_auton, 
  pl_upstream_prefer_deemph, cfg_mgmt_di, cfg_mgmt_byte_en, cfg_mgmt_dwaddr, 
  cfg_mgmt_wr_en, cfg_mgmt_rd_en, cfg_mgmt_wr_readonly, cfg_interrupt, 
  cfg_interrupt_assert, cfg_interrupt_di, cfg_interrupt_stat, 
  cfg_pciecap_interrupt_msgnum, wr_data, wr_en)
/* synthesis syn_black_box black_box_pad_pin="user_clk,user_reset,user_lnk_up,s_axis_tx_tready,s_axis_tx_tdata[63:0],s_axis_tx_tkeep[7:0],s_axis_tx_tuser[3:0],s_axis_tx_tlast,s_axis_tx_tvalid,m_axis_rx_tdata[63:0],m_axis_rx_tkeep[7:0],m_axis_rx_tlast,m_axis_rx_tvalid,m_axis_rx_tready,m_axis_rx_tuser[21:0],cfg_to_turnoff,cfg_bus_number[7:0],cfg_device_number[4:0],cfg_function_number[2:0],tx_cfg_gnt,cfg_pm_halt_aspm_l0s,cfg_pm_halt_aspm_l1,cfg_pm_force_state_en,cfg_pm_force_state[1:0],rx_np_ok,rx_np_req,cfg_turnoff_ok,cfg_trn_pending,cfg_pm_wake,cfg_dsn[63:0],fc_sel[2:0],cfg_err_cor,cfg_err_ur,cfg_err_ecrc,cfg_err_cpl_timeout,cfg_err_cpl_unexpect,cfg_err_cpl_abort,cfg_err_atomic_egress_blocked,cfg_err_internal_cor,cfg_err_malformed,cfg_err_mc_blocked,cfg_err_poisoned,cfg_err_norecovery,cfg_err_acs,cfg_err_internal_uncor,cfg_err_posted,cfg_err_locked,cfg_err_tlp_cpl_header[47:0],cfg_err_aer_headerlog[127:0],cfg_aer_interrupt_msgnum[4:0],pl_directed_link_change[1:0],pl_directed_link_width[1:0],pl_directed_link_speed,pl_directed_link_auton,pl_upstream_prefer_deemph,cfg_mgmt_di[31:0],cfg_mgmt_byte_en[3:0],cfg_mgmt_dwaddr[9:0],cfg_mgmt_wr_en,cfg_mgmt_rd_en,cfg_mgmt_wr_readonly,cfg_interrupt,cfg_interrupt_assert,cfg_interrupt_di[7:0],cfg_interrupt_stat,cfg_pciecap_interrupt_msgnum[4:0],wr_data[31:0],wr_en" */;
  input user_clk;
  input user_reset;
  input user_lnk_up;
  input s_axis_tx_tready;
  output [63:0]s_axis_tx_tdata;
  output [7:0]s_axis_tx_tkeep;
  output [3:0]s_axis_tx_tuser;
  output s_axis_tx_tlast;
  output s_axis_tx_tvalid;
  input [63:0]m_axis_rx_tdata;
  input [7:0]m_axis_rx_tkeep;
  input m_axis_rx_tlast;
  input m_axis_rx_tvalid;
  output m_axis_rx_tready;
  input [21:0]m_axis_rx_tuser;
  input cfg_to_turnoff;
  input [7:0]cfg_bus_number;
  input [4:0]cfg_device_number;
  input [2:0]cfg_function_number;
  output tx_cfg_gnt;
  output cfg_pm_halt_aspm_l0s;
  output cfg_pm_halt_aspm_l1;
  output cfg_pm_force_state_en;
  output [1:0]cfg_pm_force_state;
  output rx_np_ok;
  output rx_np_req;
  output cfg_turnoff_ok;
  output cfg_trn_pending;
  output cfg_pm_wake;
  output [63:0]cfg_dsn;
  output [2:0]fc_sel;
  output cfg_err_cor;
  output cfg_err_ur;
  output cfg_err_ecrc;
  output cfg_err_cpl_timeout;
  output cfg_err_cpl_unexpect;
  output cfg_err_cpl_abort;
  output cfg_err_atomic_egress_blocked;
  output cfg_err_internal_cor;
  output cfg_err_malformed;
  output cfg_err_mc_blocked;
  output cfg_err_poisoned;
  output cfg_err_norecovery;
  output cfg_err_acs;
  output cfg_err_internal_uncor;
  output cfg_err_posted;
  output cfg_err_locked;
  output [47:0]cfg_err_tlp_cpl_header;
  output [127:0]cfg_err_aer_headerlog;
  output [4:0]cfg_aer_interrupt_msgnum;
  output [1:0]pl_directed_link_change;
  output [1:0]pl_directed_link_width;
  output pl_directed_link_speed;
  output pl_directed_link_auton;
  output pl_upstream_prefer_deemph;
  output [31:0]cfg_mgmt_di;
  output [3:0]cfg_mgmt_byte_en;
  output [9:0]cfg_mgmt_dwaddr;
  output cfg_mgmt_wr_en;
  output cfg_mgmt_rd_en;
  output cfg_mgmt_wr_readonly;
  output cfg_interrupt;
  output cfg_interrupt_assert;
  output [7:0]cfg_interrupt_di;
  output cfg_interrupt_stat;
  output [4:0]cfg_pciecap_interrupt_msgnum;
  output [31:0]wr_data;
  output wr_en;
endmodule
