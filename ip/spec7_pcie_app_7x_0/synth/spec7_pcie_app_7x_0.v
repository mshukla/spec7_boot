// (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:user:pcie_app_7x:1.0
// IP Revision: 2

(* X_CORE_INFO = "pcie_app_7x,Vivado 2019.2" *)
(* CHECK_LICENSE_TYPE = "spec7_pcie_app_7x_0,pcie_app_7x,{}" *)
(* CORE_GENERATION_INFO = "spec7_pcie_app_7x_0,pcie_app_7x,{x_ipProduct=Vivado 2019.2,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=pcie_app_7x,x_ipVersion=1.0,x_ipCoreRevision=2,x_ipLanguage=VHDL,x_ipSimLanguage=MIXED,C_DATA_WIDTH=64,KEEP_WIDTH=8,TCQ=1}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module spec7_pcie_app_7x_0 (
  user_clk,
  user_reset,
  user_lnk_up,
  s_axis_tx_tready,
  s_axis_tx_tdata,
  s_axis_tx_tkeep,
  s_axis_tx_tuser,
  s_axis_tx_tlast,
  s_axis_tx_tvalid,
  m_axis_rx_tdata,
  m_axis_rx_tkeep,
  m_axis_rx_tlast,
  m_axis_rx_tvalid,
  m_axis_rx_tready,
  m_axis_rx_tuser,
  cfg_to_turnoff,
  cfg_bus_number,
  cfg_device_number,
  cfg_function_number,
  tx_cfg_gnt,
  cfg_pm_halt_aspm_l0s,
  cfg_pm_halt_aspm_l1,
  cfg_pm_force_state_en,
  cfg_pm_force_state,
  rx_np_ok,
  rx_np_req,
  cfg_turnoff_ok,
  cfg_trn_pending,
  cfg_pm_wake,
  cfg_dsn,
  fc_sel,
  cfg_err_cor,
  cfg_err_ur,
  cfg_err_ecrc,
  cfg_err_cpl_timeout,
  cfg_err_cpl_unexpect,
  cfg_err_cpl_abort,
  cfg_err_atomic_egress_blocked,
  cfg_err_internal_cor,
  cfg_err_malformed,
  cfg_err_mc_blocked,
  cfg_err_poisoned,
  cfg_err_norecovery,
  cfg_err_acs,
  cfg_err_internal_uncor,
  cfg_err_posted,
  cfg_err_locked,
  cfg_err_tlp_cpl_header,
  cfg_err_aer_headerlog,
  cfg_aer_interrupt_msgnum,
  pl_directed_link_change,
  pl_directed_link_width,
  pl_directed_link_speed,
  pl_directed_link_auton,
  pl_upstream_prefer_deemph,
  cfg_mgmt_di,
  cfg_mgmt_byte_en,
  cfg_mgmt_dwaddr,
  cfg_mgmt_wr_en,
  cfg_mgmt_rd_en,
  cfg_mgmt_wr_readonly,
  cfg_interrupt,
  cfg_interrupt_assert,
  cfg_interrupt_di,
  cfg_interrupt_stat,
  cfg_pciecap_interrupt_msgnum,
  wr_data,
  wr_en
);

(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME user_signal_clock, ASSOCIATED_BUSIF m_axis_rx:s_axis_tx, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 user_signal_clock CLK" *)
input wire user_clk;
input wire user_reset;
input wire user_lnk_up;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TREADY" *)
input wire s_axis_tx_tready;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TDATA" *)
output wire [63 : 0] s_axis_tx_tdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TKEEP" *)
output wire [7 : 0] s_axis_tx_tkeep;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TUSER" *)
output wire [3 : 0] s_axis_tx_tuser;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TLAST" *)
output wire s_axis_tx_tlast;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axis_tx, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 4, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 s_axis_tx TVALID" *)
output wire s_axis_tx_tvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TDATA" *)
input wire [63 : 0] m_axis_rx_tdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TKEEP" *)
input wire [7 : 0] m_axis_rx_tkeep;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TLAST" *)
input wire m_axis_rx_tlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TVALID" *)
input wire m_axis_rx_tvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TREADY" *)
output wire m_axis_rx_tready;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_axis_rx, TDATA_NUM_BYTES 8, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 22, HAS_TREADY 1, HAS_TSTRB 0, HAS_TKEEP 1, HAS_TLAST 1, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, LAYERED_METADATA undef, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 m_axis_rx TUSER" *)
input wire [21 : 0] m_axis_rx_tuser;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status turnoff" *)
input wire cfg_to_turnoff;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status bus_number" *)
input wire [7 : 0] cfg_bus_number;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status device_number" *)
input wire [4 : 0] cfg_device_number;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_status:1.0 pcie2_cfg_status function_number" *)
input wire [2 : 0] cfg_function_number;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control tx_cfg_gnt" *)
output wire tx_cfg_gnt;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_halt_aspm_l0s" *)
output wire cfg_pm_halt_aspm_l0s;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_halt_aspm_l1" *)
output wire cfg_pm_halt_aspm_l1;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_force_state_en" *)
output wire cfg_pm_force_state_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_force_state" *)
output wire [1 : 0] cfg_pm_force_state;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control rx_np_ok" *)
output wire rx_np_ok;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control rx_np_req" *)
output wire rx_np_req;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control turnoff_ok" *)
output wire cfg_turnoff_ok;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control trn_pending" *)
output wire cfg_trn_pending;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control pm_wake" *)
output wire cfg_pm_wake;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_control:1.0 pcie2_cfg_control dsn" *)
output wire [63 : 0] cfg_dsn;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_fc:1.0 pcie_cfg_fc SEL" *)
output wire [2 : 0] fc_sel;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cor" *)
output wire cfg_err_cor;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err ur" *)
output wire cfg_err_ur;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err ecrc" *)
output wire cfg_err_ecrc;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cpl_timeout" *)
output wire cfg_err_cpl_timeout;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cpl_unexpect" *)
output wire cfg_err_cpl_unexpect;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err cpl_abort" *)
output wire cfg_err_cpl_abort;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err atomic_egress_blocked" *)
output wire cfg_err_atomic_egress_blocked;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err internal_cor" *)
output wire cfg_err_internal_cor;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err malformed" *)
output wire cfg_err_malformed;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err mc_blocked" *)
output wire cfg_err_mc_blocked;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err poisoned" *)
output wire cfg_err_poisoned;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err norecovery" *)
output wire cfg_err_norecovery;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err acs" *)
output wire cfg_err_acs;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err internal_uncor" *)
output wire cfg_err_internal_uncor;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err posted" *)
output wire cfg_err_posted;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err locked" *)
output wire cfg_err_locked;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err tlp_cpl_header" *)
output wire [47 : 0] cfg_err_tlp_cpl_header;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err err_aer_headerlog" *)
output wire [127 : 0] cfg_err_aer_headerlog;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_err:1.0 pcie2_cfg_err aer_interrupt_msgnum" *)
output wire [4 : 0] cfg_aer_interrupt_msgnum;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_change" *)
output wire [1 : 0] pl_directed_link_change;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_width" *)
output wire [1 : 0] pl_directed_link_width;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_speed" *)
output wire pl_directed_link_speed;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl directed_link_auton" *)
output wire pl_directed_link_auton;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_pl:1.0 pcie2_pl upstream_prefer_deemph" *)
output wire pl_upstream_prefer_deemph;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt WRITE_DATA" *)
output wire [31 : 0] cfg_mgmt_di;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt BYTE_EN" *)
output wire [3 : 0] cfg_mgmt_byte_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt ADDR" *)
output wire [9 : 0] cfg_mgmt_dwaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt WRITE_EN" *)
output wire cfg_mgmt_wr_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt READ_EN" *)
output wire cfg_mgmt_rd_en;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie_cfg_mgmt:1.0 pcie_cfg_mgmt READONLY" *)
output wire cfg_mgmt_wr_readonly;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt interrupt" *)
output wire cfg_interrupt;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt assert" *)
output wire cfg_interrupt_assert;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt write_data" *)
output wire [7 : 0] cfg_interrupt_di;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt stat" *)
output wire cfg_interrupt_stat;
(* X_INTERFACE_INFO = "xilinx.com:interface:pcie2_cfg_interrupt:1.0 cfg_interrupt pciecap_interrupt_msgnum" *)
output wire [4 : 0] cfg_pciecap_interrupt_msgnum;
output wire [31 : 0] wr_data;
output wire wr_en;

  pcie_app_7x #(
    .C_DATA_WIDTH(64),
    .KEEP_WIDTH(8),
    .TCQ(1)
  ) inst (
    .user_clk(user_clk),
    .user_reset(user_reset),
    .user_lnk_up(user_lnk_up),
    .s_axis_tx_tready(s_axis_tx_tready),
    .s_axis_tx_tdata(s_axis_tx_tdata),
    .s_axis_tx_tkeep(s_axis_tx_tkeep),
    .s_axis_tx_tuser(s_axis_tx_tuser),
    .s_axis_tx_tlast(s_axis_tx_tlast),
    .s_axis_tx_tvalid(s_axis_tx_tvalid),
    .m_axis_rx_tdata(m_axis_rx_tdata),
    .m_axis_rx_tkeep(m_axis_rx_tkeep),
    .m_axis_rx_tlast(m_axis_rx_tlast),
    .m_axis_rx_tvalid(m_axis_rx_tvalid),
    .m_axis_rx_tready(m_axis_rx_tready),
    .m_axis_rx_tuser(m_axis_rx_tuser),
    .cfg_to_turnoff(cfg_to_turnoff),
    .cfg_bus_number(cfg_bus_number),
    .cfg_device_number(cfg_device_number),
    .cfg_function_number(cfg_function_number),
    .tx_cfg_gnt(tx_cfg_gnt),
    .cfg_pm_halt_aspm_l0s(cfg_pm_halt_aspm_l0s),
    .cfg_pm_halt_aspm_l1(cfg_pm_halt_aspm_l1),
    .cfg_pm_force_state_en(cfg_pm_force_state_en),
    .cfg_pm_force_state(cfg_pm_force_state),
    .rx_np_ok(rx_np_ok),
    .rx_np_req(rx_np_req),
    .cfg_turnoff_ok(cfg_turnoff_ok),
    .cfg_trn_pending(cfg_trn_pending),
    .cfg_pm_wake(cfg_pm_wake),
    .cfg_dsn(cfg_dsn),
    .fc_sel(fc_sel),
    .cfg_err_cor(cfg_err_cor),
    .cfg_err_ur(cfg_err_ur),
    .cfg_err_ecrc(cfg_err_ecrc),
    .cfg_err_cpl_timeout(cfg_err_cpl_timeout),
    .cfg_err_cpl_unexpect(cfg_err_cpl_unexpect),
    .cfg_err_cpl_abort(cfg_err_cpl_abort),
    .cfg_err_atomic_egress_blocked(cfg_err_atomic_egress_blocked),
    .cfg_err_internal_cor(cfg_err_internal_cor),
    .cfg_err_malformed(cfg_err_malformed),
    .cfg_err_mc_blocked(cfg_err_mc_blocked),
    .cfg_err_poisoned(cfg_err_poisoned),
    .cfg_err_norecovery(cfg_err_norecovery),
    .cfg_err_acs(cfg_err_acs),
    .cfg_err_internal_uncor(cfg_err_internal_uncor),
    .cfg_err_posted(cfg_err_posted),
    .cfg_err_locked(cfg_err_locked),
    .cfg_err_tlp_cpl_header(cfg_err_tlp_cpl_header),
    .cfg_err_aer_headerlog(cfg_err_aer_headerlog),
    .cfg_aer_interrupt_msgnum(cfg_aer_interrupt_msgnum),
    .pl_directed_link_change(pl_directed_link_change),
    .pl_directed_link_width(pl_directed_link_width),
    .pl_directed_link_speed(pl_directed_link_speed),
    .pl_directed_link_auton(pl_directed_link_auton),
    .pl_upstream_prefer_deemph(pl_upstream_prefer_deemph),
    .cfg_mgmt_di(cfg_mgmt_di),
    .cfg_mgmt_byte_en(cfg_mgmt_byte_en),
    .cfg_mgmt_dwaddr(cfg_mgmt_dwaddr),
    .cfg_mgmt_wr_en(cfg_mgmt_wr_en),
    .cfg_mgmt_rd_en(cfg_mgmt_rd_en),
    .cfg_mgmt_wr_readonly(cfg_mgmt_wr_readonly),
    .cfg_interrupt(cfg_interrupt),
    .cfg_interrupt_assert(cfg_interrupt_assert),
    .cfg_interrupt_di(cfg_interrupt_di),
    .cfg_interrupt_stat(cfg_interrupt_stat),
    .cfg_pciecap_interrupt_msgnum(cfg_pciecap_interrupt_msgnum),
    .wr_data(wr_data),
    .wr_en(wr_en)
  );
endmodule
