module pio (
    input clk,
    input reset_n,
    output reg pio_valid,
    output reg [31:0] pio_data
  );

  reg [7:0] cntr;

  always @(posedge clk)
    if(!reset_n)
      cntr <= 'd0;
    else
      cntr <= cntr + 'd1;

  always @(posedge clk)
    if(!reset_n) begin
      pio_valid <= 'd0;
      pio_data  <= 32'd0;
    end
    else if (cntr == 'd1) begin
      pio_valid <= 1'b1;
      pio_data  <= pio_data + cntr;
    end
    else
    pio_valid <= 'd0;

endmodule
