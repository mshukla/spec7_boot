-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Sat Oct  3 15:01:20 2020
-- Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
-- Command     : write_vhdl -force -mode funcsim
--               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_axi_master_0/spec7_axi_master_0_sim_netlist.vhdl
-- Design      : spec7_axi_master_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z030fbg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_axi_master_0_axi_master is
  port (
    m_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_awvalid : out STD_LOGIC;
    m_wlast : out STD_LOGIC;
    trnfr_cmpl : out STD_LOGIC_VECTOR ( 0 to 0 );
    pio_valid_in : in STD_LOGIC;
    m_resetn : in STD_LOGIC;
    m_wready : in STD_LOGIC;
    m_clk : in STD_LOGIC;
    pio_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_bvalid : in STD_LOGIC;
    ps_image_read : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_awready : in STD_LOGIC;
    BASE_ADDR_MM : in STD_LOGIC_VECTOR ( 31 downto 0 );
    NUM_BYTES : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_axi_master_0_axi_master : entity is "axi_master";
end spec7_axi_master_0_axi_master;

architecture STRUCTURE of spec7_axi_master_0_axi_master is
  signal \FSM_onehot_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_4_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_5_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_6_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[4]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[6]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[6]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[6]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[2]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[3]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[4]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[5]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[6]\ : STD_LOGIC;
  signal data_to_send : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal data_to_send0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \data_to_send0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__0_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__0_n_1\ : STD_LOGIC;
  signal \data_to_send0_carry__0_n_2\ : STD_LOGIC;
  signal \data_to_send0_carry__0_n_3\ : STD_LOGIC;
  signal \data_to_send0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__1_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__1_n_1\ : STD_LOGIC;
  signal \data_to_send0_carry__1_n_2\ : STD_LOGIC;
  signal \data_to_send0_carry__1_n_3\ : STD_LOGIC;
  signal \data_to_send0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__2_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__2_n_1\ : STD_LOGIC;
  signal \data_to_send0_carry__2_n_2\ : STD_LOGIC;
  signal \data_to_send0_carry__2_n_3\ : STD_LOGIC;
  signal \data_to_send0_carry__3_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__3_i_2_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__3_i_3_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__3_i_4_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__3_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__3_n_1\ : STD_LOGIC;
  signal \data_to_send0_carry__3_n_2\ : STD_LOGIC;
  signal \data_to_send0_carry__3_n_3\ : STD_LOGIC;
  signal \data_to_send0_carry__4_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__4_i_2_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__4_i_3_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__4_i_4_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__4_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__4_n_1\ : STD_LOGIC;
  signal \data_to_send0_carry__4_n_2\ : STD_LOGIC;
  signal \data_to_send0_carry__4_n_3\ : STD_LOGIC;
  signal \data_to_send0_carry__5_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__5_i_2_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__5_i_3_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__5_i_4_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__5_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__5_n_1\ : STD_LOGIC;
  signal \data_to_send0_carry__5_n_2\ : STD_LOGIC;
  signal \data_to_send0_carry__5_n_3\ : STD_LOGIC;
  signal \data_to_send0_carry__6_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__6_i_2_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__6_i_3_n_0\ : STD_LOGIC;
  signal \data_to_send0_carry__6_n_2\ : STD_LOGIC;
  signal \data_to_send0_carry__6_n_3\ : STD_LOGIC;
  signal data_to_send0_carry_i_1_n_0 : STD_LOGIC;
  signal data_to_send0_carry_i_2_n_0 : STD_LOGIC;
  signal data_to_send0_carry_i_3_n_0 : STD_LOGIC;
  signal data_to_send0_carry_n_0 : STD_LOGIC;
  signal data_to_send0_carry_n_1 : STD_LOGIC;
  signal data_to_send0_carry_n_2 : STD_LOGIC;
  signal data_to_send0_carry_n_3 : STD_LOGIC;
  signal \data_to_send[0]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[10]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[11]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[12]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[13]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[14]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[15]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[16]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[17]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[18]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[19]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[1]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[20]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[21]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[22]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[23]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[24]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[25]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[26]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[27]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[28]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[29]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[2]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[30]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[31]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[31]_i_2_n_0\ : STD_LOGIC;
  signal \data_to_send[31]_i_3_n_0\ : STD_LOGIC;
  signal \data_to_send[31]_i_4_n_0\ : STD_LOGIC;
  signal \data_to_send[3]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[4]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[5]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[6]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[7]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[8]_i_1_n_0\ : STD_LOGIC;
  signal \data_to_send[9]_i_1_n_0\ : STD_LOGIC;
  signal in10 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \^m_awaddr\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \m_awaddr0_carry__0_n_0\ : STD_LOGIC;
  signal \m_awaddr0_carry__0_n_1\ : STD_LOGIC;
  signal \m_awaddr0_carry__0_n_2\ : STD_LOGIC;
  signal \m_awaddr0_carry__0_n_3\ : STD_LOGIC;
  signal \m_awaddr0_carry__1_n_0\ : STD_LOGIC;
  signal \m_awaddr0_carry__1_n_1\ : STD_LOGIC;
  signal \m_awaddr0_carry__1_n_2\ : STD_LOGIC;
  signal \m_awaddr0_carry__1_n_3\ : STD_LOGIC;
  signal \m_awaddr0_carry__2_n_0\ : STD_LOGIC;
  signal \m_awaddr0_carry__2_n_1\ : STD_LOGIC;
  signal \m_awaddr0_carry__2_n_2\ : STD_LOGIC;
  signal \m_awaddr0_carry__2_n_3\ : STD_LOGIC;
  signal \m_awaddr0_carry__3_n_0\ : STD_LOGIC;
  signal \m_awaddr0_carry__3_n_1\ : STD_LOGIC;
  signal \m_awaddr0_carry__3_n_2\ : STD_LOGIC;
  signal \m_awaddr0_carry__3_n_3\ : STD_LOGIC;
  signal \m_awaddr0_carry__4_n_0\ : STD_LOGIC;
  signal \m_awaddr0_carry__4_n_1\ : STD_LOGIC;
  signal \m_awaddr0_carry__4_n_2\ : STD_LOGIC;
  signal \m_awaddr0_carry__4_n_3\ : STD_LOGIC;
  signal \m_awaddr0_carry__5_n_0\ : STD_LOGIC;
  signal \m_awaddr0_carry__5_n_1\ : STD_LOGIC;
  signal \m_awaddr0_carry__5_n_2\ : STD_LOGIC;
  signal \m_awaddr0_carry__5_n_3\ : STD_LOGIC;
  signal \m_awaddr0_carry__6_n_2\ : STD_LOGIC;
  signal \m_awaddr0_carry__6_n_3\ : STD_LOGIC;
  signal m_awaddr0_carry_i_1_n_0 : STD_LOGIC;
  signal m_awaddr0_carry_n_0 : STD_LOGIC;
  signal m_awaddr0_carry_n_1 : STD_LOGIC;
  signal m_awaddr0_carry_n_2 : STD_LOGIC;
  signal m_awaddr0_carry_n_3 : STD_LOGIC;
  signal \m_awaddr[31]_i_1_n_0\ : STD_LOGIC;
  signal \^m_awvalid\ : STD_LOGIC;
  signal m_awvalid_i_1_n_0 : STD_LOGIC;
  signal m_awvalid_i_2_n_0 : STD_LOGIC;
  signal \m_wdata[31]_i_1_n_0\ : STD_LOGIC;
  signal \^m_wlast\ : STD_LOGIC;
  signal m_wvalid_i_1_n_0 : STD_LOGIC;
  signal p_1_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal pio_data_in_r1 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \pio_data_in_r1[31]_i_1_n_0\ : STD_LOGIC;
  signal \^trnfr_cmpl\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal trnfr_cmpl_i_10_n_0 : STD_LOGIC;
  signal trnfr_cmpl_i_11_n_0 : STD_LOGIC;
  signal trnfr_cmpl_i_1_n_0 : STD_LOGIC;
  signal trnfr_cmpl_i_2_n_0 : STD_LOGIC;
  signal trnfr_cmpl_i_3_n_0 : STD_LOGIC;
  signal trnfr_cmpl_i_4_n_0 : STD_LOGIC;
  signal trnfr_cmpl_i_5_n_0 : STD_LOGIC;
  signal trnfr_cmpl_i_6_n_0 : STD_LOGIC;
  signal trnfr_cmpl_i_7_n_0 : STD_LOGIC;
  signal trnfr_cmpl_i_8_n_0 : STD_LOGIC;
  signal trnfr_cmpl_i_9_n_0 : STD_LOGIC;
  signal \NLW_data_to_send0_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_data_to_send0_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_m_awaddr0_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_m_awaddr0_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_state[1]_i_2\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \FSM_onehot_state[1]_i_3\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \FSM_onehot_state[1]_i_6\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \FSM_onehot_state[2]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \FSM_onehot_state[3]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \FSM_onehot_state[4]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \FSM_onehot_state[5]_i_1\ : label is "soft_lutpair2";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[0]\ : label is "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[1]\ : label is "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[2]\ : label is "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[3]\ : label is "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[4]\ : label is "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[5]\ : label is "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[6]\ : label is "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001";
  attribute SOFT_HLUTNM of trnfr_cmpl_i_5 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of trnfr_cmpl_i_6 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of trnfr_cmpl_i_8 : label is "soft_lutpair3";
begin
  m_awaddr(31 downto 0) <= \^m_awaddr\(31 downto 0);
  m_awvalid <= \^m_awvalid\;
  m_wlast <= \^m_wlast\;
  trnfr_cmpl(0) <= \^trnfr_cmpl\(0);
\FSM_onehot_state[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8F8F80000000000"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[4]\,
      I1 => m_wready,
      I2 => \FSM_onehot_state_reg_n_0_[5]\,
      I3 => m_bresp(1),
      I4 => m_bresp(0),
      I5 => m_bvalid,
      O => \FSM_onehot_state[0]_i_1_n_0\
    );
\FSM_onehot_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFE0000"
    )
        port map (
      I0 => trnfr_cmpl_i_4_n_0,
      I1 => \FSM_onehot_state[1]_i_2_n_0\,
      I2 => \FSM_onehot_state[1]_i_3_n_0\,
      I3 => \FSM_onehot_state[1]_i_4_n_0\,
      I4 => trnfr_cmpl_i_2_n_0,
      I5 => \FSM_onehot_state[1]_i_5_n_0\,
      O => \FSM_onehot_state[1]_i_1_n_0\
    );
\FSM_onehot_state[1]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => data_to_send(6),
      I1 => data_to_send(3),
      I2 => data_to_send(24),
      I3 => data_to_send(4),
      I4 => trnfr_cmpl_i_9_n_0,
      O => \FSM_onehot_state[1]_i_2_n_0\
    );
\FSM_onehot_state[1]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => data_to_send(30),
      I1 => data_to_send(8),
      I2 => data_to_send(11),
      I3 => data_to_send(9),
      I4 => trnfr_cmpl_i_7_n_0,
      O => \FSM_onehot_state[1]_i_3_n_0\
    );
\FSM_onehot_state[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => data_to_send(16),
      I1 => data_to_send(17),
      I2 => data_to_send(10),
      I3 => data_to_send(7),
      I4 => data_to_send(25),
      I5 => data_to_send(15),
      O => \FSM_onehot_state[1]_i_4_n_0\
    );
\FSM_onehot_state[1]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000080"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[4]\,
      I1 => m_wready,
      I2 => m_bvalid,
      I3 => m_bresp(0),
      I4 => m_bresp(1),
      I5 => \FSM_onehot_state[1]_i_6_n_0\,
      O => \FSM_onehot_state[1]_i_5_n_0\
    );
\FSM_onehot_state[1]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F8FFF8F8"
    )
        port map (
      I0 => ps_image_read(0),
      I1 => \FSM_onehot_state_reg_n_0_[6]\,
      I2 => \FSM_onehot_state_reg_n_0_[0]\,
      I3 => pio_valid_in,
      I4 => \FSM_onehot_state_reg_n_0_[1]\,
      O => \FSM_onehot_state[1]_i_6_n_0\
    );
\FSM_onehot_state[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"5540"
    )
        port map (
      I0 => m_awready,
      I1 => pio_valid_in,
      I2 => \FSM_onehot_state_reg_n_0_[1]\,
      I3 => \FSM_onehot_state_reg_n_0_[2]\,
      O => \FSM_onehot_state[2]_i_1_n_0\
    );
\FSM_onehot_state[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA80"
    )
        port map (
      I0 => m_awready,
      I1 => pio_valid_in,
      I2 => \FSM_onehot_state_reg_n_0_[1]\,
      I3 => \FSM_onehot_state_reg_n_0_[2]\,
      O => \FSM_onehot_state[3]_i_1_n_0\
    );
\FSM_onehot_state[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"54"
    )
        port map (
      I0 => m_wready,
      I1 => \FSM_onehot_state_reg_n_0_[4]\,
      I2 => \FSM_onehot_state_reg_n_0_[3]\,
      O => \FSM_onehot_state[4]_i_1_n_0\
    );
\FSM_onehot_state[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"CCFE00F0"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[4]\,
      I1 => \FSM_onehot_state_reg_n_0_[3]\,
      I2 => \FSM_onehot_state_reg_n_0_[5]\,
      I3 => m_bvalid,
      I4 => m_wready,
      O => \FSM_onehot_state[5]_i_1_n_0\
    );
\FSM_onehot_state[6]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => m_resetn,
      O => \FSM_onehot_state[6]_i_1_n_0\
    );
\FSM_onehot_state[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFEFFFFFFFF"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[6]\,
      I1 => \FSM_onehot_state_reg_n_0_[0]\,
      I2 => \FSM_onehot_state_reg_n_0_[1]\,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      I4 => \FSM_onehot_state_reg_n_0_[4]\,
      I5 => m_awvalid_i_2_n_0,
      O => \FSM_onehot_state[6]_i_2_n_0\
    );
\FSM_onehot_state[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"10FF1010"
    )
        port map (
      I0 => trnfr_cmpl_i_4_n_0,
      I1 => trnfr_cmpl_i_3_n_0,
      I2 => trnfr_cmpl_i_2_n_0,
      I3 => ps_image_read(0),
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      O => \FSM_onehot_state[6]_i_3_n_0\
    );
\FSM_onehot_state_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => m_clk,
      CE => \FSM_onehot_state[6]_i_2_n_0\,
      D => \FSM_onehot_state[0]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[0]\,
      S => \FSM_onehot_state[6]_i_1_n_0\
    );
\FSM_onehot_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m_clk,
      CE => \FSM_onehot_state[6]_i_2_n_0\,
      D => \FSM_onehot_state[1]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[1]\,
      R => \FSM_onehot_state[6]_i_1_n_0\
    );
\FSM_onehot_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m_clk,
      CE => \FSM_onehot_state[6]_i_2_n_0\,
      D => \FSM_onehot_state[2]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[2]\,
      R => \FSM_onehot_state[6]_i_1_n_0\
    );
\FSM_onehot_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m_clk,
      CE => \FSM_onehot_state[6]_i_2_n_0\,
      D => \FSM_onehot_state[3]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[3]\,
      R => \FSM_onehot_state[6]_i_1_n_0\
    );
\FSM_onehot_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m_clk,
      CE => \FSM_onehot_state[6]_i_2_n_0\,
      D => \FSM_onehot_state[4]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[4]\,
      R => \FSM_onehot_state[6]_i_1_n_0\
    );
\FSM_onehot_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m_clk,
      CE => \FSM_onehot_state[6]_i_2_n_0\,
      D => \FSM_onehot_state[5]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[5]\,
      R => \FSM_onehot_state[6]_i_1_n_0\
    );
\FSM_onehot_state_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m_clk,
      CE => \FSM_onehot_state[6]_i_2_n_0\,
      D => \FSM_onehot_state[6]_i_3_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[6]\,
      R => \FSM_onehot_state[6]_i_1_n_0\
    );
data_to_send0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => data_to_send0_carry_n_0,
      CO(2) => data_to_send0_carry_n_1,
      CO(1) => data_to_send0_carry_n_2,
      CO(0) => data_to_send0_carry_n_3,
      CYINIT => '0',
      DI(3 downto 1) => data_to_send(4 downto 2),
      DI(0) => '0',
      O(3 downto 0) => data_to_send0(4 downto 1),
      S(3) => data_to_send0_carry_i_1_n_0,
      S(2) => data_to_send0_carry_i_2_n_0,
      S(1) => data_to_send0_carry_i_3_n_0,
      S(0) => data_to_send(1)
    );
\data_to_send0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => data_to_send0_carry_n_0,
      CO(3) => \data_to_send0_carry__0_n_0\,
      CO(2) => \data_to_send0_carry__0_n_1\,
      CO(1) => \data_to_send0_carry__0_n_2\,
      CO(0) => \data_to_send0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_to_send(8 downto 5),
      O(3 downto 0) => data_to_send0(8 downto 5),
      S(3) => \data_to_send0_carry__0_i_1_n_0\,
      S(2) => \data_to_send0_carry__0_i_2_n_0\,
      S(1) => \data_to_send0_carry__0_i_3_n_0\,
      S(0) => \data_to_send0_carry__0_i_4_n_0\
    );
\data_to_send0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(8),
      O => \data_to_send0_carry__0_i_1_n_0\
    );
\data_to_send0_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(7),
      O => \data_to_send0_carry__0_i_2_n_0\
    );
\data_to_send0_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(6),
      O => \data_to_send0_carry__0_i_3_n_0\
    );
\data_to_send0_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(5),
      O => \data_to_send0_carry__0_i_4_n_0\
    );
\data_to_send0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_to_send0_carry__0_n_0\,
      CO(3) => \data_to_send0_carry__1_n_0\,
      CO(2) => \data_to_send0_carry__1_n_1\,
      CO(1) => \data_to_send0_carry__1_n_2\,
      CO(0) => \data_to_send0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_to_send(12 downto 9),
      O(3 downto 0) => data_to_send0(12 downto 9),
      S(3) => \data_to_send0_carry__1_i_1_n_0\,
      S(2) => \data_to_send0_carry__1_i_2_n_0\,
      S(1) => \data_to_send0_carry__1_i_3_n_0\,
      S(0) => \data_to_send0_carry__1_i_4_n_0\
    );
\data_to_send0_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(12),
      O => \data_to_send0_carry__1_i_1_n_0\
    );
\data_to_send0_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(11),
      O => \data_to_send0_carry__1_i_2_n_0\
    );
\data_to_send0_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(10),
      O => \data_to_send0_carry__1_i_3_n_0\
    );
\data_to_send0_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(9),
      O => \data_to_send0_carry__1_i_4_n_0\
    );
\data_to_send0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_to_send0_carry__1_n_0\,
      CO(3) => \data_to_send0_carry__2_n_0\,
      CO(2) => \data_to_send0_carry__2_n_1\,
      CO(1) => \data_to_send0_carry__2_n_2\,
      CO(0) => \data_to_send0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_to_send(16 downto 13),
      O(3 downto 0) => data_to_send0(16 downto 13),
      S(3) => \data_to_send0_carry__2_i_1_n_0\,
      S(2) => \data_to_send0_carry__2_i_2_n_0\,
      S(1) => \data_to_send0_carry__2_i_3_n_0\,
      S(0) => \data_to_send0_carry__2_i_4_n_0\
    );
\data_to_send0_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(16),
      O => \data_to_send0_carry__2_i_1_n_0\
    );
\data_to_send0_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(15),
      O => \data_to_send0_carry__2_i_2_n_0\
    );
\data_to_send0_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(14),
      O => \data_to_send0_carry__2_i_3_n_0\
    );
\data_to_send0_carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(13),
      O => \data_to_send0_carry__2_i_4_n_0\
    );
\data_to_send0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_to_send0_carry__2_n_0\,
      CO(3) => \data_to_send0_carry__3_n_0\,
      CO(2) => \data_to_send0_carry__3_n_1\,
      CO(1) => \data_to_send0_carry__3_n_2\,
      CO(0) => \data_to_send0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_to_send(20 downto 17),
      O(3 downto 0) => data_to_send0(20 downto 17),
      S(3) => \data_to_send0_carry__3_i_1_n_0\,
      S(2) => \data_to_send0_carry__3_i_2_n_0\,
      S(1) => \data_to_send0_carry__3_i_3_n_0\,
      S(0) => \data_to_send0_carry__3_i_4_n_0\
    );
\data_to_send0_carry__3_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(20),
      O => \data_to_send0_carry__3_i_1_n_0\
    );
\data_to_send0_carry__3_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(19),
      O => \data_to_send0_carry__3_i_2_n_0\
    );
\data_to_send0_carry__3_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(18),
      O => \data_to_send0_carry__3_i_3_n_0\
    );
\data_to_send0_carry__3_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(17),
      O => \data_to_send0_carry__3_i_4_n_0\
    );
\data_to_send0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_to_send0_carry__3_n_0\,
      CO(3) => \data_to_send0_carry__4_n_0\,
      CO(2) => \data_to_send0_carry__4_n_1\,
      CO(1) => \data_to_send0_carry__4_n_2\,
      CO(0) => \data_to_send0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_to_send(24 downto 21),
      O(3 downto 0) => data_to_send0(24 downto 21),
      S(3) => \data_to_send0_carry__4_i_1_n_0\,
      S(2) => \data_to_send0_carry__4_i_2_n_0\,
      S(1) => \data_to_send0_carry__4_i_3_n_0\,
      S(0) => \data_to_send0_carry__4_i_4_n_0\
    );
\data_to_send0_carry__4_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(24),
      O => \data_to_send0_carry__4_i_1_n_0\
    );
\data_to_send0_carry__4_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(23),
      O => \data_to_send0_carry__4_i_2_n_0\
    );
\data_to_send0_carry__4_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(22),
      O => \data_to_send0_carry__4_i_3_n_0\
    );
\data_to_send0_carry__4_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(21),
      O => \data_to_send0_carry__4_i_4_n_0\
    );
\data_to_send0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_to_send0_carry__4_n_0\,
      CO(3) => \data_to_send0_carry__5_n_0\,
      CO(2) => \data_to_send0_carry__5_n_1\,
      CO(1) => \data_to_send0_carry__5_n_2\,
      CO(0) => \data_to_send0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => data_to_send(28 downto 25),
      O(3 downto 0) => data_to_send0(28 downto 25),
      S(3) => \data_to_send0_carry__5_i_1_n_0\,
      S(2) => \data_to_send0_carry__5_i_2_n_0\,
      S(1) => \data_to_send0_carry__5_i_3_n_0\,
      S(0) => \data_to_send0_carry__5_i_4_n_0\
    );
\data_to_send0_carry__5_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(28),
      O => \data_to_send0_carry__5_i_1_n_0\
    );
\data_to_send0_carry__5_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(27),
      O => \data_to_send0_carry__5_i_2_n_0\
    );
\data_to_send0_carry__5_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(26),
      O => \data_to_send0_carry__5_i_3_n_0\
    );
\data_to_send0_carry__5_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(25),
      O => \data_to_send0_carry__5_i_4_n_0\
    );
\data_to_send0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \data_to_send0_carry__5_n_0\,
      CO(3 downto 2) => \NLW_data_to_send0_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \data_to_send0_carry__6_n_2\,
      CO(0) => \data_to_send0_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1 downto 0) => data_to_send(30 downto 29),
      O(3) => \NLW_data_to_send0_carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => data_to_send0(31 downto 29),
      S(3) => '0',
      S(2) => \data_to_send0_carry__6_i_1_n_0\,
      S(1) => \data_to_send0_carry__6_i_2_n_0\,
      S(0) => \data_to_send0_carry__6_i_3_n_0\
    );
\data_to_send0_carry__6_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(31),
      O => \data_to_send0_carry__6_i_1_n_0\
    );
\data_to_send0_carry__6_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(30),
      O => \data_to_send0_carry__6_i_2_n_0\
    );
\data_to_send0_carry__6_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(29),
      O => \data_to_send0_carry__6_i_3_n_0\
    );
data_to_send0_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(4),
      O => data_to_send0_carry_i_1_n_0
    );
data_to_send0_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(3),
      O => data_to_send0_carry_i_2_n_0
    );
data_to_send0_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => data_to_send(2),
      O => data_to_send0_carry_i_3_n_0
    );
\data_to_send[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(0),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send(0),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[0]_i_1_n_0\
    );
\data_to_send[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(10),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(10),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[10]_i_1_n_0\
    );
\data_to_send[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(11),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(11),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[11]_i_1_n_0\
    );
\data_to_send[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(12),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(12),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[12]_i_1_n_0\
    );
\data_to_send[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(13),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(13),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[13]_i_1_n_0\
    );
\data_to_send[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(14),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(14),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[14]_i_1_n_0\
    );
\data_to_send[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(15),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(15),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[15]_i_1_n_0\
    );
\data_to_send[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(16),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(16),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[16]_i_1_n_0\
    );
\data_to_send[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(17),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(17),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[17]_i_1_n_0\
    );
\data_to_send[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(18),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(18),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[18]_i_1_n_0\
    );
\data_to_send[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(19),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(19),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[19]_i_1_n_0\
    );
\data_to_send[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(1),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(1),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[1]_i_1_n_0\
    );
\data_to_send[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(20),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(20),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[20]_i_1_n_0\
    );
\data_to_send[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(21),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(21),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[21]_i_1_n_0\
    );
\data_to_send[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(22),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(22),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[22]_i_1_n_0\
    );
\data_to_send[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(23),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(23),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[23]_i_1_n_0\
    );
\data_to_send[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(24),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(24),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[24]_i_1_n_0\
    );
\data_to_send[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(25),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(25),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[25]_i_1_n_0\
    );
\data_to_send[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(26),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(26),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[26]_i_1_n_0\
    );
\data_to_send[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(27),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(27),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[27]_i_1_n_0\
    );
\data_to_send[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(28),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(28),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[28]_i_1_n_0\
    );
\data_to_send[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(29),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(29),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[29]_i_1_n_0\
    );
\data_to_send[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(2),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(2),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[2]_i_1_n_0\
    );
\data_to_send[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(30),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(30),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[30]_i_1_n_0\
    );
\data_to_send[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF0200FFFFFFFF"
    )
        port map (
      I0 => m_bvalid,
      I1 => m_bresp(0),
      I2 => m_bresp(1),
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      I4 => \FSM_onehot_state_reg_n_0_[0]\,
      I5 => m_resetn,
      O => \data_to_send[31]_i_1_n_0\
    );
\data_to_send[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(31),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(31),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[31]_i_2_n_0\
    );
\data_to_send[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => m_resetn,
      I1 => \FSM_onehot_state_reg_n_0_[0]\,
      O => \data_to_send[31]_i_3_n_0\
    );
\data_to_send[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"7"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[5]\,
      I1 => m_resetn,
      O => \data_to_send[31]_i_4_n_0\
    );
\data_to_send[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(3),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(3),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[3]_i_1_n_0\
    );
\data_to_send[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(4),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(4),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[4]_i_1_n_0\
    );
\data_to_send[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(5),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(5),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[5]_i_1_n_0\
    );
\data_to_send[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(6),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(6),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[6]_i_1_n_0\
    );
\data_to_send[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(7),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(7),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[7]_i_1_n_0\
    );
\data_to_send[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(8),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(8),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[8]_i_1_n_0\
    );
\data_to_send[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4F444F444F444444"
    )
        port map (
      I0 => \data_to_send[31]_i_3_n_0\,
      I1 => NUM_BYTES(9),
      I2 => \data_to_send[31]_i_4_n_0\,
      I3 => data_to_send0(9),
      I4 => trnfr_cmpl_i_4_n_0,
      I5 => trnfr_cmpl_i_3_n_0,
      O => \data_to_send[9]_i_1_n_0\
    );
\data_to_send_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[0]_i_1_n_0\,
      Q => data_to_send(0),
      R => '0'
    );
\data_to_send_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[10]_i_1_n_0\,
      Q => data_to_send(10),
      R => '0'
    );
\data_to_send_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[11]_i_1_n_0\,
      Q => data_to_send(11),
      R => '0'
    );
\data_to_send_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[12]_i_1_n_0\,
      Q => data_to_send(12),
      R => '0'
    );
\data_to_send_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[13]_i_1_n_0\,
      Q => data_to_send(13),
      R => '0'
    );
\data_to_send_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[14]_i_1_n_0\,
      Q => data_to_send(14),
      R => '0'
    );
\data_to_send_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[15]_i_1_n_0\,
      Q => data_to_send(15),
      R => '0'
    );
\data_to_send_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[16]_i_1_n_0\,
      Q => data_to_send(16),
      R => '0'
    );
\data_to_send_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[17]_i_1_n_0\,
      Q => data_to_send(17),
      R => '0'
    );
\data_to_send_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[18]_i_1_n_0\,
      Q => data_to_send(18),
      R => '0'
    );
\data_to_send_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[19]_i_1_n_0\,
      Q => data_to_send(19),
      R => '0'
    );
\data_to_send_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[1]_i_1_n_0\,
      Q => data_to_send(1),
      R => '0'
    );
\data_to_send_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[20]_i_1_n_0\,
      Q => data_to_send(20),
      R => '0'
    );
\data_to_send_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[21]_i_1_n_0\,
      Q => data_to_send(21),
      R => '0'
    );
\data_to_send_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[22]_i_1_n_0\,
      Q => data_to_send(22),
      R => '0'
    );
\data_to_send_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[23]_i_1_n_0\,
      Q => data_to_send(23),
      R => '0'
    );
\data_to_send_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[24]_i_1_n_0\,
      Q => data_to_send(24),
      R => '0'
    );
\data_to_send_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[25]_i_1_n_0\,
      Q => data_to_send(25),
      R => '0'
    );
\data_to_send_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[26]_i_1_n_0\,
      Q => data_to_send(26),
      R => '0'
    );
\data_to_send_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[27]_i_1_n_0\,
      Q => data_to_send(27),
      R => '0'
    );
\data_to_send_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[28]_i_1_n_0\,
      Q => data_to_send(28),
      R => '0'
    );
\data_to_send_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[29]_i_1_n_0\,
      Q => data_to_send(29),
      R => '0'
    );
\data_to_send_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[2]_i_1_n_0\,
      Q => data_to_send(2),
      R => '0'
    );
\data_to_send_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[30]_i_1_n_0\,
      Q => data_to_send(30),
      R => '0'
    );
\data_to_send_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[31]_i_2_n_0\,
      Q => data_to_send(31),
      R => '0'
    );
\data_to_send_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[3]_i_1_n_0\,
      Q => data_to_send(3),
      R => '0'
    );
\data_to_send_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[4]_i_1_n_0\,
      Q => data_to_send(4),
      R => '0'
    );
\data_to_send_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[5]_i_1_n_0\,
      Q => data_to_send(5),
      R => '0'
    );
\data_to_send_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[6]_i_1_n_0\,
      Q => data_to_send(6),
      R => '0'
    );
\data_to_send_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[7]_i_1_n_0\,
      Q => data_to_send(7),
      R => '0'
    );
\data_to_send_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[8]_i_1_n_0\,
      Q => data_to_send(8),
      R => '0'
    );
\data_to_send_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \data_to_send[31]_i_1_n_0\,
      D => \data_to_send[9]_i_1_n_0\,
      Q => data_to_send(9),
      R => '0'
    );
m_awaddr0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => m_awaddr0_carry_n_0,
      CO(2) => m_awaddr0_carry_n_1,
      CO(1) => m_awaddr0_carry_n_2,
      CO(0) => m_awaddr0_carry_n_3,
      CYINIT => '0',
      DI(3 downto 2) => B"00",
      DI(1) => \^m_awaddr\(2),
      DI(0) => '0',
      O(3 downto 0) => in10(4 downto 1),
      S(3 downto 2) => \^m_awaddr\(4 downto 3),
      S(1) => m_awaddr0_carry_i_1_n_0,
      S(0) => \^m_awaddr\(1)
    );
\m_awaddr0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => m_awaddr0_carry_n_0,
      CO(3) => \m_awaddr0_carry__0_n_0\,
      CO(2) => \m_awaddr0_carry__0_n_1\,
      CO(1) => \m_awaddr0_carry__0_n_2\,
      CO(0) => \m_awaddr0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => in10(8 downto 5),
      S(3 downto 0) => \^m_awaddr\(8 downto 5)
    );
\m_awaddr0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_awaddr0_carry__0_n_0\,
      CO(3) => \m_awaddr0_carry__1_n_0\,
      CO(2) => \m_awaddr0_carry__1_n_1\,
      CO(1) => \m_awaddr0_carry__1_n_2\,
      CO(0) => \m_awaddr0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => in10(12 downto 9),
      S(3 downto 0) => \^m_awaddr\(12 downto 9)
    );
\m_awaddr0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_awaddr0_carry__1_n_0\,
      CO(3) => \m_awaddr0_carry__2_n_0\,
      CO(2) => \m_awaddr0_carry__2_n_1\,
      CO(1) => \m_awaddr0_carry__2_n_2\,
      CO(0) => \m_awaddr0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => in10(16 downto 13),
      S(3 downto 0) => \^m_awaddr\(16 downto 13)
    );
\m_awaddr0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_awaddr0_carry__2_n_0\,
      CO(3) => \m_awaddr0_carry__3_n_0\,
      CO(2) => \m_awaddr0_carry__3_n_1\,
      CO(1) => \m_awaddr0_carry__3_n_2\,
      CO(0) => \m_awaddr0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => in10(20 downto 17),
      S(3 downto 0) => \^m_awaddr\(20 downto 17)
    );
\m_awaddr0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_awaddr0_carry__3_n_0\,
      CO(3) => \m_awaddr0_carry__4_n_0\,
      CO(2) => \m_awaddr0_carry__4_n_1\,
      CO(1) => \m_awaddr0_carry__4_n_2\,
      CO(0) => \m_awaddr0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => in10(24 downto 21),
      S(3 downto 0) => \^m_awaddr\(24 downto 21)
    );
\m_awaddr0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_awaddr0_carry__4_n_0\,
      CO(3) => \m_awaddr0_carry__5_n_0\,
      CO(2) => \m_awaddr0_carry__5_n_1\,
      CO(1) => \m_awaddr0_carry__5_n_2\,
      CO(0) => \m_awaddr0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => in10(28 downto 25),
      S(3 downto 0) => \^m_awaddr\(28 downto 25)
    );
\m_awaddr0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \m_awaddr0_carry__5_n_0\,
      CO(3 downto 2) => \NLW_m_awaddr0_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \m_awaddr0_carry__6_n_2\,
      CO(0) => \m_awaddr0_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_m_awaddr0_carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => in10(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => \^m_awaddr\(31 downto 29)
    );
m_awaddr0_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^m_awaddr\(2),
      O => m_awaddr0_carry_i_1_n_0
    );
\m_awaddr[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => \^m_awaddr\(0),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(0),
      O => p_1_in(0)
    );
\m_awaddr[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(10),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(10),
      O => p_1_in(10)
    );
\m_awaddr[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(11),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(11),
      O => p_1_in(11)
    );
\m_awaddr[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(12),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(12),
      O => p_1_in(12)
    );
\m_awaddr[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(13),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(13),
      O => p_1_in(13)
    );
\m_awaddr[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(14),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(14),
      O => p_1_in(14)
    );
\m_awaddr[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(15),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(15),
      O => p_1_in(15)
    );
\m_awaddr[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(16),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(16),
      O => p_1_in(16)
    );
\m_awaddr[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(17),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(17),
      O => p_1_in(17)
    );
\m_awaddr[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(18),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(18),
      O => p_1_in(18)
    );
\m_awaddr[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(19),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(19),
      O => p_1_in(19)
    );
\m_awaddr[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(1),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(1),
      O => p_1_in(1)
    );
\m_awaddr[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(20),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(20),
      O => p_1_in(20)
    );
\m_awaddr[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(21),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(21),
      O => p_1_in(21)
    );
\m_awaddr[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(22),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(22),
      O => p_1_in(22)
    );
\m_awaddr[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(23),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(23),
      O => p_1_in(23)
    );
\m_awaddr[24]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(24),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(24),
      O => p_1_in(24)
    );
\m_awaddr[25]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(25),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(25),
      O => p_1_in(25)
    );
\m_awaddr[26]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(26),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(26),
      O => p_1_in(26)
    );
\m_awaddr[27]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(27),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(27),
      O => p_1_in(27)
    );
\m_awaddr[28]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(28),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(28),
      O => p_1_in(28)
    );
\m_awaddr[29]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(29),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(29),
      O => p_1_in(29)
    );
\m_awaddr[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(2),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(2),
      O => p_1_in(2)
    );
\m_awaddr[30]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(30),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(30),
      O => p_1_in(30)
    );
\m_awaddr[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4FFF4FFF4F"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => m_awready,
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => ps_image_read(0),
      O => \m_awaddr[31]_i_1_n_0\
    );
\m_awaddr[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(31),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(31),
      O => p_1_in(31)
    );
\m_awaddr[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(3),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(3),
      O => p_1_in(3)
    );
\m_awaddr[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(4),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(4),
      O => p_1_in(4)
    );
\m_awaddr[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(5),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(5),
      O => p_1_in(5)
    );
\m_awaddr[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(6),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(6),
      O => p_1_in(6)
    );
\m_awaddr[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(7),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(7),
      O => p_1_in(7)
    );
\m_awaddr[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(8),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(8),
      O => p_1_in(8)
    );
\m_awaddr[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF4F40404040"
    )
        port map (
      I0 => m_awvalid_i_2_n_0,
      I1 => in10(9),
      I2 => m_resetn,
      I3 => \FSM_onehot_state_reg_n_0_[0]\,
      I4 => \FSM_onehot_state_reg_n_0_[6]\,
      I5 => BASE_ADDR_MM(9),
      O => p_1_in(9)
    );
\m_awaddr_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(0),
      Q => \^m_awaddr\(0),
      R => '0'
    );
\m_awaddr_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(10),
      Q => \^m_awaddr\(10),
      R => '0'
    );
\m_awaddr_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(11),
      Q => \^m_awaddr\(11),
      R => '0'
    );
\m_awaddr_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(12),
      Q => \^m_awaddr\(12),
      R => '0'
    );
\m_awaddr_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(13),
      Q => \^m_awaddr\(13),
      R => '0'
    );
\m_awaddr_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(14),
      Q => \^m_awaddr\(14),
      R => '0'
    );
\m_awaddr_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(15),
      Q => \^m_awaddr\(15),
      R => '0'
    );
\m_awaddr_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(16),
      Q => \^m_awaddr\(16),
      R => '0'
    );
\m_awaddr_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(17),
      Q => \^m_awaddr\(17),
      R => '0'
    );
\m_awaddr_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(18),
      Q => \^m_awaddr\(18),
      R => '0'
    );
\m_awaddr_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(19),
      Q => \^m_awaddr\(19),
      R => '0'
    );
\m_awaddr_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(1),
      Q => \^m_awaddr\(1),
      R => '0'
    );
\m_awaddr_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(20),
      Q => \^m_awaddr\(20),
      R => '0'
    );
\m_awaddr_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(21),
      Q => \^m_awaddr\(21),
      R => '0'
    );
\m_awaddr_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(22),
      Q => \^m_awaddr\(22),
      R => '0'
    );
\m_awaddr_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(23),
      Q => \^m_awaddr\(23),
      R => '0'
    );
\m_awaddr_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(24),
      Q => \^m_awaddr\(24),
      R => '0'
    );
\m_awaddr_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(25),
      Q => \^m_awaddr\(25),
      R => '0'
    );
\m_awaddr_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(26),
      Q => \^m_awaddr\(26),
      R => '0'
    );
\m_awaddr_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(27),
      Q => \^m_awaddr\(27),
      R => '0'
    );
\m_awaddr_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(28),
      Q => \^m_awaddr\(28),
      R => '0'
    );
\m_awaddr_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(29),
      Q => \^m_awaddr\(29),
      R => '0'
    );
\m_awaddr_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(2),
      Q => \^m_awaddr\(2),
      R => '0'
    );
\m_awaddr_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(30),
      Q => \^m_awaddr\(30),
      R => '0'
    );
\m_awaddr_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(31),
      Q => \^m_awaddr\(31),
      R => '0'
    );
\m_awaddr_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(3),
      Q => \^m_awaddr\(3),
      R => '0'
    );
\m_awaddr_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(4),
      Q => \^m_awaddr\(4),
      R => '0'
    );
\m_awaddr_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(5),
      Q => \^m_awaddr\(5),
      R => '0'
    );
\m_awaddr_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(6),
      Q => \^m_awaddr\(6),
      R => '0'
    );
\m_awaddr_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(7),
      Q => \^m_awaddr\(7),
      R => '0'
    );
\m_awaddr_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(8),
      Q => \^m_awaddr\(8),
      R => '0'
    );
\m_awaddr_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_awaddr[31]_i_1_n_0\,
      D => p_1_in(9),
      Q => \^m_awaddr\(9),
      R => '0'
    );
m_awvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFA2A2A200000000"
    )
        port map (
      I0 => \^m_awvalid\,
      I1 => m_awready,
      I2 => m_awvalid_i_2_n_0,
      I3 => pio_valid_in,
      I4 => \FSM_onehot_state_reg_n_0_[1]\,
      I5 => m_resetn,
      O => m_awvalid_i_1_n_0
    );
m_awvalid_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[3]\,
      I1 => \FSM_onehot_state_reg_n_0_[2]\,
      O => m_awvalid_i_2_n_0
    );
m_awvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => '1',
      D => m_awvalid_i_1_n_0,
      Q => \^m_awvalid\,
      R => '0'
    );
\m_wdata[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => m_resetn,
      I1 => \FSM_onehot_state_reg_n_0_[3]\,
      O => \m_wdata[31]_i_1_n_0\
    );
\m_wdata_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(0),
      Q => m_wdata(0),
      R => '0'
    );
\m_wdata_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(10),
      Q => m_wdata(10),
      R => '0'
    );
\m_wdata_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(11),
      Q => m_wdata(11),
      R => '0'
    );
\m_wdata_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(12),
      Q => m_wdata(12),
      R => '0'
    );
\m_wdata_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(13),
      Q => m_wdata(13),
      R => '0'
    );
\m_wdata_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(14),
      Q => m_wdata(14),
      R => '0'
    );
\m_wdata_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(15),
      Q => m_wdata(15),
      R => '0'
    );
\m_wdata_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(16),
      Q => m_wdata(16),
      R => '0'
    );
\m_wdata_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(17),
      Q => m_wdata(17),
      R => '0'
    );
\m_wdata_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(18),
      Q => m_wdata(18),
      R => '0'
    );
\m_wdata_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(19),
      Q => m_wdata(19),
      R => '0'
    );
\m_wdata_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(1),
      Q => m_wdata(1),
      R => '0'
    );
\m_wdata_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(20),
      Q => m_wdata(20),
      R => '0'
    );
\m_wdata_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(21),
      Q => m_wdata(21),
      R => '0'
    );
\m_wdata_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(22),
      Q => m_wdata(22),
      R => '0'
    );
\m_wdata_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(23),
      Q => m_wdata(23),
      R => '0'
    );
\m_wdata_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(24),
      Q => m_wdata(24),
      R => '0'
    );
\m_wdata_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(25),
      Q => m_wdata(25),
      R => '0'
    );
\m_wdata_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(26),
      Q => m_wdata(26),
      R => '0'
    );
\m_wdata_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(27),
      Q => m_wdata(27),
      R => '0'
    );
\m_wdata_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(28),
      Q => m_wdata(28),
      R => '0'
    );
\m_wdata_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(29),
      Q => m_wdata(29),
      R => '0'
    );
\m_wdata_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(2),
      Q => m_wdata(2),
      R => '0'
    );
\m_wdata_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(30),
      Q => m_wdata(30),
      R => '0'
    );
\m_wdata_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(31),
      Q => m_wdata(31),
      R => '0'
    );
\m_wdata_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(3),
      Q => m_wdata(3),
      R => '0'
    );
\m_wdata_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(4),
      Q => m_wdata(4),
      R => '0'
    );
\m_wdata_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(5),
      Q => m_wdata(5),
      R => '0'
    );
\m_wdata_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(6),
      Q => m_wdata(6),
      R => '0'
    );
\m_wdata_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(7),
      Q => m_wdata(7),
      R => '0'
    );
\m_wdata_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(8),
      Q => m_wdata(8),
      R => '0'
    );
\m_wdata_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \m_wdata[31]_i_1_n_0\,
      D => pio_data_in_r1(9),
      Q => m_wdata(9),
      R => '0'
    );
m_wvalid_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFF222A00000000"
    )
        port map (
      I0 => \^m_wlast\,
      I1 => m_wready,
      I2 => \FSM_onehot_state_reg_n_0_[4]\,
      I3 => \FSM_onehot_state_reg_n_0_[5]\,
      I4 => \FSM_onehot_state_reg_n_0_[3]\,
      I5 => m_resetn,
      O => m_wvalid_i_1_n_0
    );
m_wvalid_reg: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => '1',
      D => m_wvalid_i_1_n_0,
      Q => \^m_wlast\,
      R => '0'
    );
\pio_data_in_r1[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => pio_valid_in,
      I2 => m_resetn,
      O => \pio_data_in_r1[31]_i_1_n_0\
    );
\pio_data_in_r1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(0),
      Q => pio_data_in_r1(0),
      R => '0'
    );
\pio_data_in_r1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(10),
      Q => pio_data_in_r1(10),
      R => '0'
    );
\pio_data_in_r1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(11),
      Q => pio_data_in_r1(11),
      R => '0'
    );
\pio_data_in_r1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(12),
      Q => pio_data_in_r1(12),
      R => '0'
    );
\pio_data_in_r1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(13),
      Q => pio_data_in_r1(13),
      R => '0'
    );
\pio_data_in_r1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(14),
      Q => pio_data_in_r1(14),
      R => '0'
    );
\pio_data_in_r1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(15),
      Q => pio_data_in_r1(15),
      R => '0'
    );
\pio_data_in_r1_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(16),
      Q => pio_data_in_r1(16),
      R => '0'
    );
\pio_data_in_r1_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(17),
      Q => pio_data_in_r1(17),
      R => '0'
    );
\pio_data_in_r1_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(18),
      Q => pio_data_in_r1(18),
      R => '0'
    );
\pio_data_in_r1_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(19),
      Q => pio_data_in_r1(19),
      R => '0'
    );
\pio_data_in_r1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(1),
      Q => pio_data_in_r1(1),
      R => '0'
    );
\pio_data_in_r1_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(20),
      Q => pio_data_in_r1(20),
      R => '0'
    );
\pio_data_in_r1_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(21),
      Q => pio_data_in_r1(21),
      R => '0'
    );
\pio_data_in_r1_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(22),
      Q => pio_data_in_r1(22),
      R => '0'
    );
\pio_data_in_r1_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(23),
      Q => pio_data_in_r1(23),
      R => '0'
    );
\pio_data_in_r1_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(24),
      Q => pio_data_in_r1(24),
      R => '0'
    );
\pio_data_in_r1_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(25),
      Q => pio_data_in_r1(25),
      R => '0'
    );
\pio_data_in_r1_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(26),
      Q => pio_data_in_r1(26),
      R => '0'
    );
\pio_data_in_r1_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(27),
      Q => pio_data_in_r1(27),
      R => '0'
    );
\pio_data_in_r1_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(28),
      Q => pio_data_in_r1(28),
      R => '0'
    );
\pio_data_in_r1_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(29),
      Q => pio_data_in_r1(29),
      R => '0'
    );
\pio_data_in_r1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(2),
      Q => pio_data_in_r1(2),
      R => '0'
    );
\pio_data_in_r1_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(30),
      Q => pio_data_in_r1(30),
      R => '0'
    );
\pio_data_in_r1_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(31),
      Q => pio_data_in_r1(31),
      R => '0'
    );
\pio_data_in_r1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(3),
      Q => pio_data_in_r1(3),
      R => '0'
    );
\pio_data_in_r1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(4),
      Q => pio_data_in_r1(4),
      R => '0'
    );
\pio_data_in_r1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(5),
      Q => pio_data_in_r1(5),
      R => '0'
    );
\pio_data_in_r1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(6),
      Q => pio_data_in_r1(6),
      R => '0'
    );
\pio_data_in_r1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(7),
      Q => pio_data_in_r1(7),
      R => '0'
    );
\pio_data_in_r1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(8),
      Q => pio_data_in_r1(8),
      R => '0'
    );
\pio_data_in_r1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => \pio_data_in_r1[31]_i_1_n_0\,
      D => pio_data_in(9),
      Q => pio_data_in_r1(9),
      R => '0'
    );
trnfr_cmpl_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCCCAAAE00000000"
    )
        port map (
      I0 => \^trnfr_cmpl\(0),
      I1 => trnfr_cmpl_i_2_n_0,
      I2 => trnfr_cmpl_i_3_n_0,
      I3 => trnfr_cmpl_i_4_n_0,
      I4 => trnfr_cmpl_i_5_n_0,
      I5 => m_resetn,
      O => trnfr_cmpl_i_1_n_0
    );
trnfr_cmpl_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => data_to_send(27),
      I1 => data_to_send(28),
      I2 => data_to_send(29),
      I3 => data_to_send(31),
      O => trnfr_cmpl_i_10_n_0
    );
trnfr_cmpl_i_11: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => data_to_send(13),
      I1 => data_to_send(5),
      I2 => data_to_send(23),
      O => trnfr_cmpl_i_11_n_0
    );
trnfr_cmpl_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0200"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[5]\,
      I1 => m_bresp(1),
      I2 => m_bresp(0),
      I3 => m_bvalid,
      O => trnfr_cmpl_i_2_n_0
    );
trnfr_cmpl_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \FSM_onehot_state[1]_i_4_n_0\,
      I1 => trnfr_cmpl_i_6_n_0,
      I2 => trnfr_cmpl_i_7_n_0,
      I3 => trnfr_cmpl_i_8_n_0,
      I4 => trnfr_cmpl_i_9_n_0,
      O => trnfr_cmpl_i_3_n_0
    );
trnfr_cmpl_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFE0"
    )
        port map (
      I0 => data_to_send(0),
      I1 => data_to_send(1),
      I2 => data_to_send(2),
      I3 => trnfr_cmpl_i_10_n_0,
      I4 => trnfr_cmpl_i_11_n_0,
      O => trnfr_cmpl_i_4_n_0
    );
trnfr_cmpl_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => ps_image_read(0),
      I1 => \FSM_onehot_state_reg_n_0_[6]\,
      O => trnfr_cmpl_i_5_n_0
    );
trnfr_cmpl_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => data_to_send(9),
      I1 => data_to_send(11),
      I2 => data_to_send(8),
      I3 => data_to_send(30),
      O => trnfr_cmpl_i_6_n_0
    );
trnfr_cmpl_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => data_to_send(21),
      I1 => data_to_send(26),
      I2 => data_to_send(12),
      I3 => data_to_send(22),
      O => trnfr_cmpl_i_7_n_0
    );
trnfr_cmpl_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => data_to_send(4),
      I1 => data_to_send(24),
      I2 => data_to_send(3),
      I3 => data_to_send(6),
      O => trnfr_cmpl_i_8_n_0
    );
trnfr_cmpl_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => data_to_send(19),
      I1 => data_to_send(20),
      I2 => data_to_send(14),
      I3 => data_to_send(18),
      O => trnfr_cmpl_i_9_n_0
    );
trnfr_cmpl_reg: unisim.vcomponents.FDRE
     port map (
      C => m_clk,
      CE => '1',
      D => trnfr_cmpl_i_1_n_0,
      Q => \^trnfr_cmpl\(0),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_axi_master_0 is
  port (
    ps_image_read : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_clk : in STD_LOGIC;
    m_resetn : in STD_LOGIC;
    pio_valid_in : in STD_LOGIC;
    pio_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    BASE_ADDR_MM : in STD_LOGIC_VECTOR ( 31 downto 0 );
    NUM_BYTES : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_awvalid : out STD_LOGIC;
    m_awready : in STD_LOGIC;
    m_awid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_wvalid : out STD_LOGIC;
    m_wready : in STD_LOGIC;
    m_wid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_wlast : out STD_LOGIC;
    m_bvalid : in STD_LOGIC;
    m_bready : out STD_LOGIC;
    m_bid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    m_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_arvalid : out STD_LOGIC;
    m_arready : in STD_LOGIC;
    m_arid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_rvalid : in STD_LOGIC;
    m_rready : out STD_LOGIC;
    m_rid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    m_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_rlast : in STD_LOGIC;
    trnfr_cmpl : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of spec7_axi_master_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of spec7_axi_master_0 : entity is "spec7_axi_master_0,axi_master,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of spec7_axi_master_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of spec7_axi_master_0 : entity is "axi_master,Vivado 2019.2";
end spec7_axi_master_0;

architecture STRUCTURE of spec7_axi_master_0 is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  signal \^m_wlast\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m_arready : signal is "xilinx.com:interface:aximm:1.0 m ARREADY";
  attribute X_INTERFACE_INFO of m_arvalid : signal is "xilinx.com:interface:aximm:1.0 m ARVALID";
  attribute X_INTERFACE_INFO of m_awready : signal is "xilinx.com:interface:aximm:1.0 m AWREADY";
  attribute X_INTERFACE_INFO of m_awvalid : signal is "xilinx.com:interface:aximm:1.0 m AWVALID";
  attribute X_INTERFACE_INFO of m_bready : signal is "xilinx.com:interface:aximm:1.0 m BREADY";
  attribute X_INTERFACE_INFO of m_bvalid : signal is "xilinx.com:interface:aximm:1.0 m BVALID";
  attribute X_INTERFACE_INFO of m_clk : signal is "xilinx.com:signal:clock:1.0 m_signal_clock CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m_clk : signal is "XIL_INTERFACENAME m_signal_clock, ASSOCIATED_BUSIF m, ASSOCIATED_RESET m_resetn, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m_resetn : signal is "xilinx.com:signal:reset:1.0 m_signal_reset RST";
  attribute X_INTERFACE_PARAMETER of m_resetn : signal is "XIL_INTERFACENAME m_signal_reset, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m_rlast : signal is "xilinx.com:interface:aximm:1.0 m RLAST";
  attribute X_INTERFACE_PARAMETER of m_rlast : signal is "XIL_INTERFACENAME m, DATA_WIDTH 32, PROTOCOL AXI3, FREQ_HZ 62500000, ID_WIDTH 4, ADDR_WIDTH 32, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 16, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m_rready : signal is "xilinx.com:interface:aximm:1.0 m RREADY";
  attribute X_INTERFACE_INFO of m_rvalid : signal is "xilinx.com:interface:aximm:1.0 m RVALID";
  attribute X_INTERFACE_INFO of m_wlast : signal is "xilinx.com:interface:aximm:1.0 m WLAST";
  attribute X_INTERFACE_INFO of m_wready : signal is "xilinx.com:interface:aximm:1.0 m WREADY";
  attribute X_INTERFACE_INFO of m_wvalid : signal is "xilinx.com:interface:aximm:1.0 m WVALID";
  attribute X_INTERFACE_INFO of m_araddr : signal is "xilinx.com:interface:aximm:1.0 m ARADDR";
  attribute X_INTERFACE_INFO of m_arburst : signal is "xilinx.com:interface:aximm:1.0 m ARBURST";
  attribute X_INTERFACE_INFO of m_arcache : signal is "xilinx.com:interface:aximm:1.0 m ARCACHE";
  attribute X_INTERFACE_INFO of m_arid : signal is "xilinx.com:interface:aximm:1.0 m ARID";
  attribute X_INTERFACE_INFO of m_arlen : signal is "xilinx.com:interface:aximm:1.0 m ARLEN";
  attribute X_INTERFACE_INFO of m_arprot : signal is "xilinx.com:interface:aximm:1.0 m ARPROT";
  attribute X_INTERFACE_INFO of m_arsize : signal is "xilinx.com:interface:aximm:1.0 m ARSIZE";
  attribute X_INTERFACE_INFO of m_awaddr : signal is "xilinx.com:interface:aximm:1.0 m AWADDR";
  attribute X_INTERFACE_INFO of m_awburst : signal is "xilinx.com:interface:aximm:1.0 m AWBURST";
  attribute X_INTERFACE_INFO of m_awcache : signal is "xilinx.com:interface:aximm:1.0 m AWCACHE";
  attribute X_INTERFACE_INFO of m_awid : signal is "xilinx.com:interface:aximm:1.0 m AWID";
  attribute X_INTERFACE_INFO of m_awlen : signal is "xilinx.com:interface:aximm:1.0 m AWLEN";
  attribute X_INTERFACE_INFO of m_awprot : signal is "xilinx.com:interface:aximm:1.0 m AWPROT";
  attribute X_INTERFACE_INFO of m_awsize : signal is "xilinx.com:interface:aximm:1.0 m AWSIZE";
  attribute X_INTERFACE_INFO of m_bid : signal is "xilinx.com:interface:aximm:1.0 m BID";
  attribute X_INTERFACE_INFO of m_bresp : signal is "xilinx.com:interface:aximm:1.0 m BRESP";
  attribute X_INTERFACE_INFO of m_rdata : signal is "xilinx.com:interface:aximm:1.0 m RDATA";
  attribute X_INTERFACE_INFO of m_rid : signal is "xilinx.com:interface:aximm:1.0 m RID";
  attribute X_INTERFACE_INFO of m_rresp : signal is "xilinx.com:interface:aximm:1.0 m RRESP";
  attribute X_INTERFACE_INFO of m_wdata : signal is "xilinx.com:interface:aximm:1.0 m WDATA";
  attribute X_INTERFACE_INFO of m_wid : signal is "xilinx.com:interface:aximm:1.0 m WID";
  attribute X_INTERFACE_INFO of m_wstrb : signal is "xilinx.com:interface:aximm:1.0 m WSTRB";
begin
  m_araddr(31) <= \<const0>\;
  m_araddr(30) <= \<const0>\;
  m_araddr(29) <= \<const0>\;
  m_araddr(28) <= \<const0>\;
  m_araddr(27) <= \<const0>\;
  m_araddr(26) <= \<const0>\;
  m_araddr(25) <= \<const0>\;
  m_araddr(24) <= \<const0>\;
  m_araddr(23) <= \<const0>\;
  m_araddr(22) <= \<const0>\;
  m_araddr(21) <= \<const0>\;
  m_araddr(20) <= \<const0>\;
  m_araddr(19) <= \<const0>\;
  m_araddr(18) <= \<const0>\;
  m_araddr(17) <= \<const0>\;
  m_araddr(16) <= \<const0>\;
  m_araddr(15) <= \<const0>\;
  m_araddr(14) <= \<const0>\;
  m_araddr(13) <= \<const0>\;
  m_araddr(12) <= \<const0>\;
  m_araddr(11) <= \<const0>\;
  m_araddr(10) <= \<const0>\;
  m_araddr(9) <= \<const0>\;
  m_araddr(8) <= \<const0>\;
  m_araddr(7) <= \<const0>\;
  m_araddr(6) <= \<const0>\;
  m_araddr(5) <= \<const0>\;
  m_araddr(4) <= \<const0>\;
  m_araddr(3) <= \<const0>\;
  m_araddr(2) <= \<const0>\;
  m_araddr(1) <= \<const0>\;
  m_araddr(0) <= \<const0>\;
  m_arburst(1) <= \<const0>\;
  m_arburst(0) <= \<const0>\;
  m_arcache(3) <= \<const0>\;
  m_arcache(2) <= \<const0>\;
  m_arcache(1) <= \<const0>\;
  m_arcache(0) <= \<const0>\;
  m_arid(3) <= \<const0>\;
  m_arid(2) <= \<const0>\;
  m_arid(1) <= \<const0>\;
  m_arid(0) <= \<const0>\;
  m_arlen(3) <= \<const0>\;
  m_arlen(2) <= \<const0>\;
  m_arlen(1) <= \<const0>\;
  m_arlen(0) <= \<const0>\;
  m_arprot(2) <= \<const0>\;
  m_arprot(1) <= \<const0>\;
  m_arprot(0) <= \<const0>\;
  m_arsize(2) <= \<const0>\;
  m_arsize(1) <= \<const0>\;
  m_arsize(0) <= \<const0>\;
  m_arvalid <= \<const0>\;
  m_awburst(1) <= \<const0>\;
  m_awburst(0) <= \<const1>\;
  m_awcache(3) <= \<const0>\;
  m_awcache(2) <= \<const0>\;
  m_awcache(1) <= \<const0>\;
  m_awcache(0) <= \<const0>\;
  m_awid(3) <= \<const0>\;
  m_awid(2) <= \<const0>\;
  m_awid(1) <= \<const0>\;
  m_awid(0) <= \<const0>\;
  m_awlen(3) <= \<const0>\;
  m_awlen(2) <= \<const0>\;
  m_awlen(1) <= \<const0>\;
  m_awlen(0) <= \<const0>\;
  m_awprot(2) <= \<const0>\;
  m_awprot(1) <= \<const0>\;
  m_awprot(0) <= \<const0>\;
  m_awsize(2) <= \<const0>\;
  m_awsize(1) <= \<const1>\;
  m_awsize(0) <= \<const0>\;
  m_bready <= \<const1>\;
  m_rready <= \<const1>\;
  m_wid(3) <= \<const0>\;
  m_wid(2) <= \<const0>\;
  m_wid(1) <= \<const0>\;
  m_wid(0) <= \<const0>\;
  m_wlast <= \^m_wlast\;
  m_wstrb(3) <= \<const1>\;
  m_wstrb(2) <= \<const1>\;
  m_wstrb(1) <= \<const1>\;
  m_wstrb(0) <= \<const1>\;
  m_wvalid <= \^m_wlast\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.spec7_axi_master_0_axi_master
     port map (
      BASE_ADDR_MM(31 downto 0) => BASE_ADDR_MM(31 downto 0),
      NUM_BYTES(31 downto 0) => NUM_BYTES(31 downto 0),
      m_awaddr(31 downto 0) => m_awaddr(31 downto 0),
      m_awready => m_awready,
      m_awvalid => m_awvalid,
      m_bresp(1 downto 0) => m_bresp(1 downto 0),
      m_bvalid => m_bvalid,
      m_clk => m_clk,
      m_resetn => m_resetn,
      m_wdata(31 downto 0) => m_wdata(31 downto 0),
      m_wlast => \^m_wlast\,
      m_wready => m_wready,
      pio_data_in(31 downto 0) => pio_data_in(31 downto 0),
      pio_valid_in => pio_valid_in,
      ps_image_read(0) => ps_image_read(0),
      trnfr_cmpl(0) => trnfr_cmpl(0)
    );
end STRUCTURE;
