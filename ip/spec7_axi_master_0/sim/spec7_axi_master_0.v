// (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:user:axi_master:1.0
// IP Revision: 8

`timescale 1ns/1ps

(* DowngradeIPIdentifiedWarnings = "yes" *)
module spec7_axi_master_0 (
  ps_image_read,
  m_clk,
  m_resetn,
  pio_valid_in,
  pio_data_in,
  BASE_ADDR_MM,
  NUM_BYTES,
  m_awvalid,
  m_awready,
  m_awid,
  m_awaddr,
  m_awlen,
  m_awsize,
  m_awburst,
  m_awprot,
  m_awcache,
  m_wvalid,
  m_wready,
  m_wid,
  m_wdata,
  m_wstrb,
  m_wlast,
  m_bvalid,
  m_bready,
  m_bid,
  m_bresp,
  m_arvalid,
  m_arready,
  m_arid,
  m_araddr,
  m_arlen,
  m_arsize,
  m_arburst,
  m_arprot,
  m_arcache,
  m_rvalid,
  m_rready,
  m_rid,
  m_rdata,
  m_rresp,
  m_rlast,
  trnfr_cmpl
);

input wire [0 : 0] ps_image_read;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_signal_clock, ASSOCIATED_BUSIF m, ASSOCIATED_RESET m_resetn, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m_signal_clock CLK" *)
input wire m_clk;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_signal_reset, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m_signal_reset RST" *)
input wire m_resetn;
input wire pio_valid_in;
input wire [31 : 0] pio_data_in;
input wire [31 : 0] BASE_ADDR_MM;
input wire [31 : 0] NUM_BYTES;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWVALID" *)
output wire m_awvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWREADY" *)
input wire m_awready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWID" *)
output wire [3 : 0] m_awid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWADDR" *)
output wire [31 : 0] m_awaddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWLEN" *)
output wire [3 : 0] m_awlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWSIZE" *)
output wire [2 : 0] m_awsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWBURST" *)
output wire [1 : 0] m_awburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWPROT" *)
output wire [2 : 0] m_awprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWCACHE" *)
output wire [3 : 0] m_awcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WVALID" *)
output wire m_wvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WREADY" *)
input wire m_wready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WID" *)
output wire [3 : 0] m_wid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WDATA" *)
output wire [31 : 0] m_wdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WSTRB" *)
output wire [3 : 0] m_wstrb;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WLAST" *)
output wire m_wlast;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m BVALID" *)
input wire m_bvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m BREADY" *)
output wire m_bready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m BID" *)
input wire [3 : 0] m_bid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m BRESP" *)
input wire [1 : 0] m_bresp;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARVALID" *)
output wire m_arvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARREADY" *)
input wire m_arready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARID" *)
output wire [3 : 0] m_arid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARADDR" *)
output wire [31 : 0] m_araddr;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARLEN" *)
output wire [3 : 0] m_arlen;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARSIZE" *)
output wire [2 : 0] m_arsize;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARBURST" *)
output wire [1 : 0] m_arburst;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARPROT" *)
output wire [2 : 0] m_arprot;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARCACHE" *)
output wire [3 : 0] m_arcache;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RVALID" *)
input wire m_rvalid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RREADY" *)
output wire m_rready;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RID" *)
input wire [3 : 0] m_rid;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RDATA" *)
input wire [31 : 0] m_rdata;
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RRESP" *)
input wire [1 : 0] m_rresp;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m, DATA_WIDTH 32, PROTOCOL AXI3, FREQ_HZ 62500000, ID_WIDTH 4, ADDR_WIDTH 32, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 16, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, NUM_READ_THREADS 1, NUM_WRITE_THREAD\
S 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RLAST" *)
input wire m_rlast;
output wire [0 : 0] trnfr_cmpl;

  axi_master #(
    .M_ID_WIDTH(4),
    .M_ADDR_WIDTH(32),
    .M_LEN_WIDTH(4),
    .M_DATA_WIDTH(32),
    .NUM_BYTES_WIDTH(32)
  ) inst (
    .ps_image_read(ps_image_read),
    .m_clk(m_clk),
    .m_resetn(m_resetn),
    .pio_valid_in(pio_valid_in),
    .pio_data_in(pio_data_in),
    .BASE_ADDR_MM(BASE_ADDR_MM),
    .NUM_BYTES(NUM_BYTES),
    .m_awvalid(m_awvalid),
    .m_awready(m_awready),
    .m_awid(m_awid),
    .m_awaddr(m_awaddr),
    .m_awlen(m_awlen),
    .m_awsize(m_awsize),
    .m_awburst(m_awburst),
    .m_awprot(m_awprot),
    .m_awcache(m_awcache),
    .m_wvalid(m_wvalid),
    .m_wready(m_wready),
    .m_wid(m_wid),
    .m_wdata(m_wdata),
    .m_wstrb(m_wstrb),
    .m_wlast(m_wlast),
    .m_bvalid(m_bvalid),
    .m_bready(m_bready),
    .m_bid(m_bid),
    .m_bresp(m_bresp),
    .m_arvalid(m_arvalid),
    .m_arready(m_arready),
    .m_arid(m_arid),
    .m_araddr(m_araddr),
    .m_arlen(m_arlen),
    .m_arsize(m_arsize),
    .m_arburst(m_arburst),
    .m_arprot(m_arprot),
    .m_arcache(m_arcache),
    .m_rvalid(m_rvalid),
    .m_rready(m_rready),
    .m_rid(m_rid),
    .m_rdata(m_rdata),
    .m_rresp(m_rresp),
    .m_rlast(m_rlast),
    .trnfr_cmpl(trnfr_cmpl)
  );
endmodule
