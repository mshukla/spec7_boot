-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Sat Oct  3 15:01:20 2020
-- Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
-- Command     : write_vhdl -force -mode synth_stub
--               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_axi_master_0/spec7_axi_master_0_stub.vhdl
-- Design      : spec7_axi_master_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z030fbg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity spec7_axi_master_0 is
  Port ( 
    ps_image_read : in STD_LOGIC_VECTOR ( 0 to 0 );
    m_clk : in STD_LOGIC;
    m_resetn : in STD_LOGIC;
    pio_valid_in : in STD_LOGIC;
    pio_data_in : in STD_LOGIC_VECTOR ( 31 downto 0 );
    BASE_ADDR_MM : in STD_LOGIC_VECTOR ( 31 downto 0 );
    NUM_BYTES : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_awvalid : out STD_LOGIC;
    m_awready : in STD_LOGIC;
    m_awid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_awlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_awsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_awburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_awcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_wvalid : out STD_LOGIC;
    m_wready : in STD_LOGIC;
    m_wid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_wlast : out STD_LOGIC;
    m_bvalid : in STD_LOGIC;
    m_bready : out STD_LOGIC;
    m_bid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    m_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_arvalid : out STD_LOGIC;
    m_arready : in STD_LOGIC;
    m_arid : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m_arlen : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_arsize : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_arburst : out STD_LOGIC_VECTOR ( 1 downto 0 );
    m_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
    m_arcache : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m_rvalid : in STD_LOGIC;
    m_rready : out STD_LOGIC;
    m_rid : in STD_LOGIC_VECTOR ( 3 downto 0 );
    m_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
    m_rlast : in STD_LOGIC;
    trnfr_cmpl : out STD_LOGIC_VECTOR ( 0 to 0 )
  );

end spec7_axi_master_0;

architecture stub of spec7_axi_master_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "ps_image_read[0:0],m_clk,m_resetn,pio_valid_in,pio_data_in[31:0],BASE_ADDR_MM[31:0],NUM_BYTES[31:0],m_awvalid,m_awready,m_awid[3:0],m_awaddr[31:0],m_awlen[3:0],m_awsize[2:0],m_awburst[1:0],m_awprot[2:0],m_awcache[3:0],m_wvalid,m_wready,m_wid[3:0],m_wdata[31:0],m_wstrb[3:0],m_wlast,m_bvalid,m_bready,m_bid[3:0],m_bresp[1:0],m_arvalid,m_arready,m_arid[3:0],m_araddr[31:0],m_arlen[3:0],m_arsize[2:0],m_arburst[1:0],m_arprot[2:0],m_arcache[3:0],m_rvalid,m_rready,m_rid[3:0],m_rdata[31:0],m_rresp[1:0],m_rlast,trnfr_cmpl[0:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "axi_master,Vivado 2019.2";
begin
end;
