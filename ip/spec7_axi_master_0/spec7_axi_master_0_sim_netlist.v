// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Sat Oct  3 15:01:20 2020
// Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
// Command     : write_verilog -force -mode funcsim
//               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_axi_master_0/spec7_axi_master_0_sim_netlist.v
// Design      : spec7_axi_master_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z030fbg676-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "spec7_axi_master_0,axi_master,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "axi_master,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module spec7_axi_master_0
   (ps_image_read,
    m_clk,
    m_resetn,
    pio_valid_in,
    pio_data_in,
    BASE_ADDR_MM,
    NUM_BYTES,
    m_awvalid,
    m_awready,
    m_awid,
    m_awaddr,
    m_awlen,
    m_awsize,
    m_awburst,
    m_awprot,
    m_awcache,
    m_wvalid,
    m_wready,
    m_wid,
    m_wdata,
    m_wstrb,
    m_wlast,
    m_bvalid,
    m_bready,
    m_bid,
    m_bresp,
    m_arvalid,
    m_arready,
    m_arid,
    m_araddr,
    m_arlen,
    m_arsize,
    m_arburst,
    m_arprot,
    m_arcache,
    m_rvalid,
    m_rready,
    m_rid,
    m_rdata,
    m_rresp,
    m_rlast,
    trnfr_cmpl);
  input [0:0]ps_image_read;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m_signal_clock CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_signal_clock, ASSOCIATED_BUSIF m, ASSOCIATED_RESET m_resetn, FREQ_HZ 62500000, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, INSERT_VIP 0" *) input m_clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m_signal_reset RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m_signal_reset, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m_resetn;
  input pio_valid_in;
  input [31:0]pio_data_in;
  input [31:0]BASE_ADDR_MM;
  input [31:0]NUM_BYTES;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWVALID" *) output m_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWREADY" *) input m_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWID" *) output [3:0]m_awid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWADDR" *) output [31:0]m_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWLEN" *) output [3:0]m_awlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWSIZE" *) output [2:0]m_awsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWBURST" *) output [1:0]m_awburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWPROT" *) output [2:0]m_awprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m AWCACHE" *) output [3:0]m_awcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WVALID" *) output m_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WREADY" *) input m_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WID" *) output [3:0]m_wid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WDATA" *) output [31:0]m_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WSTRB" *) output [3:0]m_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m WLAST" *) output m_wlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m BVALID" *) input m_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m BREADY" *) output m_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m BID" *) input [3:0]m_bid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m BRESP" *) input [1:0]m_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARVALID" *) output m_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARREADY" *) input m_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARID" *) output [3:0]m_arid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARADDR" *) output [31:0]m_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARLEN" *) output [3:0]m_arlen;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARSIZE" *) output [2:0]m_arsize;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARBURST" *) output [1:0]m_arburst;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARPROT" *) output [2:0]m_arprot;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m ARCACHE" *) output [3:0]m_arcache;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RVALID" *) input m_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RREADY" *) output m_rready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RID" *) input [3:0]m_rid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RDATA" *) input [31:0]m_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RRESP" *) input [1:0]m_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 m RLAST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m, DATA_WIDTH 32, PROTOCOL AXI3, FREQ_HZ 62500000, ID_WIDTH 4, ADDR_WIDTH 32, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 1, HAS_LOCK 0, HAS_PROT 1, HAS_CACHE 1, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 1, NUM_READ_OUTSTANDING 2, NUM_WRITE_OUTSTANDING 2, MAX_BURST_LENGTH 16, PHASE 0.000, CLK_DOMAIN spec7_pcie_7x_0_user_clk_out, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input m_rlast;
  output [0:0]trnfr_cmpl;

  wire \<const0> ;
  wire \<const1> ;
  wire [31:0]BASE_ADDR_MM;
  wire [31:0]NUM_BYTES;
  wire [31:0]m_awaddr;
  wire m_awready;
  wire m_awvalid;
  wire [1:0]m_bresp;
  wire m_bvalid;
  wire m_clk;
  wire m_resetn;
  wire [31:0]m_wdata;
  wire m_wlast;
  wire m_wready;
  wire [31:0]pio_data_in;
  wire pio_valid_in;
  wire [0:0]ps_image_read;
  wire [0:0]trnfr_cmpl;

  assign m_araddr[31] = \<const0> ;
  assign m_araddr[30] = \<const0> ;
  assign m_araddr[29] = \<const0> ;
  assign m_araddr[28] = \<const0> ;
  assign m_araddr[27] = \<const0> ;
  assign m_araddr[26] = \<const0> ;
  assign m_araddr[25] = \<const0> ;
  assign m_araddr[24] = \<const0> ;
  assign m_araddr[23] = \<const0> ;
  assign m_araddr[22] = \<const0> ;
  assign m_araddr[21] = \<const0> ;
  assign m_araddr[20] = \<const0> ;
  assign m_araddr[19] = \<const0> ;
  assign m_araddr[18] = \<const0> ;
  assign m_araddr[17] = \<const0> ;
  assign m_araddr[16] = \<const0> ;
  assign m_araddr[15] = \<const0> ;
  assign m_araddr[14] = \<const0> ;
  assign m_araddr[13] = \<const0> ;
  assign m_araddr[12] = \<const0> ;
  assign m_araddr[11] = \<const0> ;
  assign m_araddr[10] = \<const0> ;
  assign m_araddr[9] = \<const0> ;
  assign m_araddr[8] = \<const0> ;
  assign m_araddr[7] = \<const0> ;
  assign m_araddr[6] = \<const0> ;
  assign m_araddr[5] = \<const0> ;
  assign m_araddr[4] = \<const0> ;
  assign m_araddr[3] = \<const0> ;
  assign m_araddr[2] = \<const0> ;
  assign m_araddr[1] = \<const0> ;
  assign m_araddr[0] = \<const0> ;
  assign m_arburst[1] = \<const0> ;
  assign m_arburst[0] = \<const0> ;
  assign m_arcache[3] = \<const0> ;
  assign m_arcache[2] = \<const0> ;
  assign m_arcache[1] = \<const0> ;
  assign m_arcache[0] = \<const0> ;
  assign m_arid[3] = \<const0> ;
  assign m_arid[2] = \<const0> ;
  assign m_arid[1] = \<const0> ;
  assign m_arid[0] = \<const0> ;
  assign m_arlen[3] = \<const0> ;
  assign m_arlen[2] = \<const0> ;
  assign m_arlen[1] = \<const0> ;
  assign m_arlen[0] = \<const0> ;
  assign m_arprot[2] = \<const0> ;
  assign m_arprot[1] = \<const0> ;
  assign m_arprot[0] = \<const0> ;
  assign m_arsize[2] = \<const0> ;
  assign m_arsize[1] = \<const0> ;
  assign m_arsize[0] = \<const0> ;
  assign m_arvalid = \<const0> ;
  assign m_awburst[1] = \<const0> ;
  assign m_awburst[0] = \<const1> ;
  assign m_awcache[3] = \<const0> ;
  assign m_awcache[2] = \<const0> ;
  assign m_awcache[1] = \<const0> ;
  assign m_awcache[0] = \<const0> ;
  assign m_awid[3] = \<const0> ;
  assign m_awid[2] = \<const0> ;
  assign m_awid[1] = \<const0> ;
  assign m_awid[0] = \<const0> ;
  assign m_awlen[3] = \<const0> ;
  assign m_awlen[2] = \<const0> ;
  assign m_awlen[1] = \<const0> ;
  assign m_awlen[0] = \<const0> ;
  assign m_awprot[2] = \<const0> ;
  assign m_awprot[1] = \<const0> ;
  assign m_awprot[0] = \<const0> ;
  assign m_awsize[2] = \<const0> ;
  assign m_awsize[1] = \<const1> ;
  assign m_awsize[0] = \<const0> ;
  assign m_bready = \<const1> ;
  assign m_rready = \<const1> ;
  assign m_wid[3] = \<const0> ;
  assign m_wid[2] = \<const0> ;
  assign m_wid[1] = \<const0> ;
  assign m_wid[0] = \<const0> ;
  assign m_wstrb[3] = \<const1> ;
  assign m_wstrb[2] = \<const1> ;
  assign m_wstrb[1] = \<const1> ;
  assign m_wstrb[0] = \<const1> ;
  assign m_wvalid = m_wlast;
  GND GND
       (.G(\<const0> ));
  VCC VCC
       (.P(\<const1> ));
  spec7_axi_master_0_axi_master inst
       (.BASE_ADDR_MM(BASE_ADDR_MM),
        .NUM_BYTES(NUM_BYTES),
        .m_awaddr(m_awaddr),
        .m_awready(m_awready),
        .m_awvalid(m_awvalid),
        .m_bresp(m_bresp),
        .m_bvalid(m_bvalid),
        .m_clk(m_clk),
        .m_resetn(m_resetn),
        .m_wdata(m_wdata),
        .m_wlast(m_wlast),
        .m_wready(m_wready),
        .pio_data_in(pio_data_in),
        .pio_valid_in(pio_valid_in),
        .ps_image_read(ps_image_read),
        .trnfr_cmpl(trnfr_cmpl));
endmodule

(* ORIG_REF_NAME = "axi_master" *) 
module spec7_axi_master_0_axi_master
   (m_awaddr,
    m_wdata,
    m_awvalid,
    m_wlast,
    trnfr_cmpl,
    pio_valid_in,
    m_resetn,
    m_wready,
    m_clk,
    pio_data_in,
    m_bresp,
    m_bvalid,
    ps_image_read,
    m_awready,
    BASE_ADDR_MM,
    NUM_BYTES);
  output [31:0]m_awaddr;
  output [31:0]m_wdata;
  output m_awvalid;
  output m_wlast;
  output [0:0]trnfr_cmpl;
  input pio_valid_in;
  input m_resetn;
  input m_wready;
  input m_clk;
  input [31:0]pio_data_in;
  input [1:0]m_bresp;
  input m_bvalid;
  input [0:0]ps_image_read;
  input m_awready;
  input [31:0]BASE_ADDR_MM;
  input [31:0]NUM_BYTES;

  wire [31:0]BASE_ADDR_MM;
  wire \FSM_onehot_state[0]_i_1_n_0 ;
  wire \FSM_onehot_state[1]_i_1_n_0 ;
  wire \FSM_onehot_state[1]_i_2_n_0 ;
  wire \FSM_onehot_state[1]_i_3_n_0 ;
  wire \FSM_onehot_state[1]_i_4_n_0 ;
  wire \FSM_onehot_state[1]_i_5_n_0 ;
  wire \FSM_onehot_state[1]_i_6_n_0 ;
  wire \FSM_onehot_state[2]_i_1_n_0 ;
  wire \FSM_onehot_state[3]_i_1_n_0 ;
  wire \FSM_onehot_state[4]_i_1_n_0 ;
  wire \FSM_onehot_state[5]_i_1_n_0 ;
  wire \FSM_onehot_state[6]_i_1_n_0 ;
  wire \FSM_onehot_state[6]_i_2_n_0 ;
  wire \FSM_onehot_state[6]_i_3_n_0 ;
  wire \FSM_onehot_state_reg_n_0_[0] ;
  wire \FSM_onehot_state_reg_n_0_[1] ;
  wire \FSM_onehot_state_reg_n_0_[2] ;
  wire \FSM_onehot_state_reg_n_0_[3] ;
  wire \FSM_onehot_state_reg_n_0_[4] ;
  wire \FSM_onehot_state_reg_n_0_[5] ;
  wire \FSM_onehot_state_reg_n_0_[6] ;
  wire [31:0]NUM_BYTES;
  wire [31:0]data_to_send;
  wire [31:1]data_to_send0;
  wire data_to_send0_carry__0_i_1_n_0;
  wire data_to_send0_carry__0_i_2_n_0;
  wire data_to_send0_carry__0_i_3_n_0;
  wire data_to_send0_carry__0_i_4_n_0;
  wire data_to_send0_carry__0_n_0;
  wire data_to_send0_carry__0_n_1;
  wire data_to_send0_carry__0_n_2;
  wire data_to_send0_carry__0_n_3;
  wire data_to_send0_carry__1_i_1_n_0;
  wire data_to_send0_carry__1_i_2_n_0;
  wire data_to_send0_carry__1_i_3_n_0;
  wire data_to_send0_carry__1_i_4_n_0;
  wire data_to_send0_carry__1_n_0;
  wire data_to_send0_carry__1_n_1;
  wire data_to_send0_carry__1_n_2;
  wire data_to_send0_carry__1_n_3;
  wire data_to_send0_carry__2_i_1_n_0;
  wire data_to_send0_carry__2_i_2_n_0;
  wire data_to_send0_carry__2_i_3_n_0;
  wire data_to_send0_carry__2_i_4_n_0;
  wire data_to_send0_carry__2_n_0;
  wire data_to_send0_carry__2_n_1;
  wire data_to_send0_carry__2_n_2;
  wire data_to_send0_carry__2_n_3;
  wire data_to_send0_carry__3_i_1_n_0;
  wire data_to_send0_carry__3_i_2_n_0;
  wire data_to_send0_carry__3_i_3_n_0;
  wire data_to_send0_carry__3_i_4_n_0;
  wire data_to_send0_carry__3_n_0;
  wire data_to_send0_carry__3_n_1;
  wire data_to_send0_carry__3_n_2;
  wire data_to_send0_carry__3_n_3;
  wire data_to_send0_carry__4_i_1_n_0;
  wire data_to_send0_carry__4_i_2_n_0;
  wire data_to_send0_carry__4_i_3_n_0;
  wire data_to_send0_carry__4_i_4_n_0;
  wire data_to_send0_carry__4_n_0;
  wire data_to_send0_carry__4_n_1;
  wire data_to_send0_carry__4_n_2;
  wire data_to_send0_carry__4_n_3;
  wire data_to_send0_carry__5_i_1_n_0;
  wire data_to_send0_carry__5_i_2_n_0;
  wire data_to_send0_carry__5_i_3_n_0;
  wire data_to_send0_carry__5_i_4_n_0;
  wire data_to_send0_carry__5_n_0;
  wire data_to_send0_carry__5_n_1;
  wire data_to_send0_carry__5_n_2;
  wire data_to_send0_carry__5_n_3;
  wire data_to_send0_carry__6_i_1_n_0;
  wire data_to_send0_carry__6_i_2_n_0;
  wire data_to_send0_carry__6_i_3_n_0;
  wire data_to_send0_carry__6_n_2;
  wire data_to_send0_carry__6_n_3;
  wire data_to_send0_carry_i_1_n_0;
  wire data_to_send0_carry_i_2_n_0;
  wire data_to_send0_carry_i_3_n_0;
  wire data_to_send0_carry_n_0;
  wire data_to_send0_carry_n_1;
  wire data_to_send0_carry_n_2;
  wire data_to_send0_carry_n_3;
  wire \data_to_send[0]_i_1_n_0 ;
  wire \data_to_send[10]_i_1_n_0 ;
  wire \data_to_send[11]_i_1_n_0 ;
  wire \data_to_send[12]_i_1_n_0 ;
  wire \data_to_send[13]_i_1_n_0 ;
  wire \data_to_send[14]_i_1_n_0 ;
  wire \data_to_send[15]_i_1_n_0 ;
  wire \data_to_send[16]_i_1_n_0 ;
  wire \data_to_send[17]_i_1_n_0 ;
  wire \data_to_send[18]_i_1_n_0 ;
  wire \data_to_send[19]_i_1_n_0 ;
  wire \data_to_send[1]_i_1_n_0 ;
  wire \data_to_send[20]_i_1_n_0 ;
  wire \data_to_send[21]_i_1_n_0 ;
  wire \data_to_send[22]_i_1_n_0 ;
  wire \data_to_send[23]_i_1_n_0 ;
  wire \data_to_send[24]_i_1_n_0 ;
  wire \data_to_send[25]_i_1_n_0 ;
  wire \data_to_send[26]_i_1_n_0 ;
  wire \data_to_send[27]_i_1_n_0 ;
  wire \data_to_send[28]_i_1_n_0 ;
  wire \data_to_send[29]_i_1_n_0 ;
  wire \data_to_send[2]_i_1_n_0 ;
  wire \data_to_send[30]_i_1_n_0 ;
  wire \data_to_send[31]_i_1_n_0 ;
  wire \data_to_send[31]_i_2_n_0 ;
  wire \data_to_send[31]_i_3_n_0 ;
  wire \data_to_send[31]_i_4_n_0 ;
  wire \data_to_send[3]_i_1_n_0 ;
  wire \data_to_send[4]_i_1_n_0 ;
  wire \data_to_send[5]_i_1_n_0 ;
  wire \data_to_send[6]_i_1_n_0 ;
  wire \data_to_send[7]_i_1_n_0 ;
  wire \data_to_send[8]_i_1_n_0 ;
  wire \data_to_send[9]_i_1_n_0 ;
  wire [31:1]in10;
  wire [31:0]m_awaddr;
  wire m_awaddr0_carry__0_n_0;
  wire m_awaddr0_carry__0_n_1;
  wire m_awaddr0_carry__0_n_2;
  wire m_awaddr0_carry__0_n_3;
  wire m_awaddr0_carry__1_n_0;
  wire m_awaddr0_carry__1_n_1;
  wire m_awaddr0_carry__1_n_2;
  wire m_awaddr0_carry__1_n_3;
  wire m_awaddr0_carry__2_n_0;
  wire m_awaddr0_carry__2_n_1;
  wire m_awaddr0_carry__2_n_2;
  wire m_awaddr0_carry__2_n_3;
  wire m_awaddr0_carry__3_n_0;
  wire m_awaddr0_carry__3_n_1;
  wire m_awaddr0_carry__3_n_2;
  wire m_awaddr0_carry__3_n_3;
  wire m_awaddr0_carry__4_n_0;
  wire m_awaddr0_carry__4_n_1;
  wire m_awaddr0_carry__4_n_2;
  wire m_awaddr0_carry__4_n_3;
  wire m_awaddr0_carry__5_n_0;
  wire m_awaddr0_carry__5_n_1;
  wire m_awaddr0_carry__5_n_2;
  wire m_awaddr0_carry__5_n_3;
  wire m_awaddr0_carry__6_n_2;
  wire m_awaddr0_carry__6_n_3;
  wire m_awaddr0_carry_i_1_n_0;
  wire m_awaddr0_carry_n_0;
  wire m_awaddr0_carry_n_1;
  wire m_awaddr0_carry_n_2;
  wire m_awaddr0_carry_n_3;
  wire \m_awaddr[31]_i_1_n_0 ;
  wire m_awready;
  wire m_awvalid;
  wire m_awvalid_i_1_n_0;
  wire m_awvalid_i_2_n_0;
  wire [1:0]m_bresp;
  wire m_bvalid;
  wire m_clk;
  wire m_resetn;
  wire [31:0]m_wdata;
  wire \m_wdata[31]_i_1_n_0 ;
  wire m_wlast;
  wire m_wready;
  wire m_wvalid_i_1_n_0;
  wire [31:0]p_1_in;
  wire [31:0]pio_data_in;
  wire [31:0]pio_data_in_r1;
  wire \pio_data_in_r1[31]_i_1_n_0 ;
  wire pio_valid_in;
  wire [0:0]ps_image_read;
  wire [0:0]trnfr_cmpl;
  wire trnfr_cmpl_i_10_n_0;
  wire trnfr_cmpl_i_11_n_0;
  wire trnfr_cmpl_i_1_n_0;
  wire trnfr_cmpl_i_2_n_0;
  wire trnfr_cmpl_i_3_n_0;
  wire trnfr_cmpl_i_4_n_0;
  wire trnfr_cmpl_i_5_n_0;
  wire trnfr_cmpl_i_6_n_0;
  wire trnfr_cmpl_i_7_n_0;
  wire trnfr_cmpl_i_8_n_0;
  wire trnfr_cmpl_i_9_n_0;
  wire [3:2]NLW_data_to_send0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_data_to_send0_carry__6_O_UNCONNECTED;
  wire [3:2]NLW_m_awaddr0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_m_awaddr0_carry__6_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'hF8F8F80000000000)) 
    \FSM_onehot_state[0]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[4] ),
        .I1(m_wready),
        .I2(\FSM_onehot_state_reg_n_0_[5] ),
        .I3(m_bresp[1]),
        .I4(m_bresp[0]),
        .I5(m_bvalid),
        .O(\FSM_onehot_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFE0000)) 
    \FSM_onehot_state[1]_i_1 
       (.I0(trnfr_cmpl_i_4_n_0),
        .I1(\FSM_onehot_state[1]_i_2_n_0 ),
        .I2(\FSM_onehot_state[1]_i_3_n_0 ),
        .I3(\FSM_onehot_state[1]_i_4_n_0 ),
        .I4(trnfr_cmpl_i_2_n_0),
        .I5(\FSM_onehot_state[1]_i_5_n_0 ),
        .O(\FSM_onehot_state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_onehot_state[1]_i_2 
       (.I0(data_to_send[6]),
        .I1(data_to_send[3]),
        .I2(data_to_send[24]),
        .I3(data_to_send[4]),
        .I4(trnfr_cmpl_i_9_n_0),
        .O(\FSM_onehot_state[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_onehot_state[1]_i_3 
       (.I0(data_to_send[30]),
        .I1(data_to_send[8]),
        .I2(data_to_send[11]),
        .I3(data_to_send[9]),
        .I4(trnfr_cmpl_i_7_n_0),
        .O(\FSM_onehot_state[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \FSM_onehot_state[1]_i_4 
       (.I0(data_to_send[16]),
        .I1(data_to_send[17]),
        .I2(data_to_send[10]),
        .I3(data_to_send[7]),
        .I4(data_to_send[25]),
        .I5(data_to_send[15]),
        .O(\FSM_onehot_state[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000080)) 
    \FSM_onehot_state[1]_i_5 
       (.I0(\FSM_onehot_state_reg_n_0_[4] ),
        .I1(m_wready),
        .I2(m_bvalid),
        .I3(m_bresp[0]),
        .I4(m_bresp[1]),
        .I5(\FSM_onehot_state[1]_i_6_n_0 ),
        .O(\FSM_onehot_state[1]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hF8FFF8F8)) 
    \FSM_onehot_state[1]_i_6 
       (.I0(ps_image_read),
        .I1(\FSM_onehot_state_reg_n_0_[6] ),
        .I2(\FSM_onehot_state_reg_n_0_[0] ),
        .I3(pio_valid_in),
        .I4(\FSM_onehot_state_reg_n_0_[1] ),
        .O(\FSM_onehot_state[1]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'h5540)) 
    \FSM_onehot_state[2]_i_1 
       (.I0(m_awready),
        .I1(pio_valid_in),
        .I2(\FSM_onehot_state_reg_n_0_[1] ),
        .I3(\FSM_onehot_state_reg_n_0_[2] ),
        .O(\FSM_onehot_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT4 #(
    .INIT(16'hAA80)) 
    \FSM_onehot_state[3]_i_1 
       (.I0(m_awready),
        .I1(pio_valid_in),
        .I2(\FSM_onehot_state_reg_n_0_[1] ),
        .I3(\FSM_onehot_state_reg_n_0_[2] ),
        .O(\FSM_onehot_state[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h54)) 
    \FSM_onehot_state[4]_i_1 
       (.I0(m_wready),
        .I1(\FSM_onehot_state_reg_n_0_[4] ),
        .I2(\FSM_onehot_state_reg_n_0_[3] ),
        .O(\FSM_onehot_state[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hCCFE00F0)) 
    \FSM_onehot_state[5]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[4] ),
        .I1(\FSM_onehot_state_reg_n_0_[3] ),
        .I2(\FSM_onehot_state_reg_n_0_[5] ),
        .I3(m_bvalid),
        .I4(m_wready),
        .O(\FSM_onehot_state[5]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \FSM_onehot_state[6]_i_1 
       (.I0(m_resetn),
        .O(\FSM_onehot_state[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFEFFFFFFFF)) 
    \FSM_onehot_state[6]_i_2 
       (.I0(\FSM_onehot_state_reg_n_0_[6] ),
        .I1(\FSM_onehot_state_reg_n_0_[0] ),
        .I2(\FSM_onehot_state_reg_n_0_[1] ),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .I4(\FSM_onehot_state_reg_n_0_[4] ),
        .I5(m_awvalid_i_2_n_0),
        .O(\FSM_onehot_state[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h10FF1010)) 
    \FSM_onehot_state[6]_i_3 
       (.I0(trnfr_cmpl_i_4_n_0),
        .I1(trnfr_cmpl_i_3_n_0),
        .I2(trnfr_cmpl_i_2_n_0),
        .I3(ps_image_read),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .O(\FSM_onehot_state[6]_i_3_n_0 ));
  (* FSM_ENCODED_STATES = "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg[0] 
       (.C(m_clk),
        .CE(\FSM_onehot_state[6]_i_2_n_0 ),
        .D(\FSM_onehot_state[0]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[0] ),
        .S(\FSM_onehot_state[6]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[1] 
       (.C(m_clk),
        .CE(\FSM_onehot_state[6]_i_2_n_0 ),
        .D(\FSM_onehot_state[1]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[1] ),
        .R(\FSM_onehot_state[6]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[2] 
       (.C(m_clk),
        .CE(\FSM_onehot_state[6]_i_2_n_0 ),
        .D(\FSM_onehot_state[2]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[2] ),
        .R(\FSM_onehot_state[6]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[3] 
       (.C(m_clk),
        .CE(\FSM_onehot_state[6]_i_2_n_0 ),
        .D(\FSM_onehot_state[3]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[3] ),
        .R(\FSM_onehot_state[6]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[4] 
       (.C(m_clk),
        .CE(\FSM_onehot_state[6]_i_2_n_0 ),
        .D(\FSM_onehot_state[4]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[4] ),
        .R(\FSM_onehot_state[6]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[5] 
       (.C(m_clk),
        .CE(\FSM_onehot_state[6]_i_2_n_0 ),
        .D(\FSM_onehot_state[5]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[5] ),
        .R(\FSM_onehot_state[6]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "WAIT_FOR_AWREADY:0000100,SEND_WDATA:0001000,WAIT_FOR_WREADY:0010000,WAIT_FOR_IMG_RD:1000000,WAIT_FOR_PIO_VALID:0000010,WAIT_FOR_BRESP:0100000,IDLE:0000001" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[6] 
       (.C(m_clk),
        .CE(\FSM_onehot_state[6]_i_2_n_0 ),
        .D(\FSM_onehot_state[6]_i_3_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[6] ),
        .R(\FSM_onehot_state[6]_i_1_n_0 ));
  CARRY4 data_to_send0_carry
       (.CI(1'b0),
        .CO({data_to_send0_carry_n_0,data_to_send0_carry_n_1,data_to_send0_carry_n_2,data_to_send0_carry_n_3}),
        .CYINIT(1'b0),
        .DI({data_to_send[4:2],1'b0}),
        .O(data_to_send0[4:1]),
        .S({data_to_send0_carry_i_1_n_0,data_to_send0_carry_i_2_n_0,data_to_send0_carry_i_3_n_0,data_to_send[1]}));
  CARRY4 data_to_send0_carry__0
       (.CI(data_to_send0_carry_n_0),
        .CO({data_to_send0_carry__0_n_0,data_to_send0_carry__0_n_1,data_to_send0_carry__0_n_2,data_to_send0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(data_to_send[8:5]),
        .O(data_to_send0[8:5]),
        .S({data_to_send0_carry__0_i_1_n_0,data_to_send0_carry__0_i_2_n_0,data_to_send0_carry__0_i_3_n_0,data_to_send0_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__0_i_1
       (.I0(data_to_send[8]),
        .O(data_to_send0_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__0_i_2
       (.I0(data_to_send[7]),
        .O(data_to_send0_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__0_i_3
       (.I0(data_to_send[6]),
        .O(data_to_send0_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__0_i_4
       (.I0(data_to_send[5]),
        .O(data_to_send0_carry__0_i_4_n_0));
  CARRY4 data_to_send0_carry__1
       (.CI(data_to_send0_carry__0_n_0),
        .CO({data_to_send0_carry__1_n_0,data_to_send0_carry__1_n_1,data_to_send0_carry__1_n_2,data_to_send0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(data_to_send[12:9]),
        .O(data_to_send0[12:9]),
        .S({data_to_send0_carry__1_i_1_n_0,data_to_send0_carry__1_i_2_n_0,data_to_send0_carry__1_i_3_n_0,data_to_send0_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__1_i_1
       (.I0(data_to_send[12]),
        .O(data_to_send0_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__1_i_2
       (.I0(data_to_send[11]),
        .O(data_to_send0_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__1_i_3
       (.I0(data_to_send[10]),
        .O(data_to_send0_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__1_i_4
       (.I0(data_to_send[9]),
        .O(data_to_send0_carry__1_i_4_n_0));
  CARRY4 data_to_send0_carry__2
       (.CI(data_to_send0_carry__1_n_0),
        .CO({data_to_send0_carry__2_n_0,data_to_send0_carry__2_n_1,data_to_send0_carry__2_n_2,data_to_send0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI(data_to_send[16:13]),
        .O(data_to_send0[16:13]),
        .S({data_to_send0_carry__2_i_1_n_0,data_to_send0_carry__2_i_2_n_0,data_to_send0_carry__2_i_3_n_0,data_to_send0_carry__2_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__2_i_1
       (.I0(data_to_send[16]),
        .O(data_to_send0_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__2_i_2
       (.I0(data_to_send[15]),
        .O(data_to_send0_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__2_i_3
       (.I0(data_to_send[14]),
        .O(data_to_send0_carry__2_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__2_i_4
       (.I0(data_to_send[13]),
        .O(data_to_send0_carry__2_i_4_n_0));
  CARRY4 data_to_send0_carry__3
       (.CI(data_to_send0_carry__2_n_0),
        .CO({data_to_send0_carry__3_n_0,data_to_send0_carry__3_n_1,data_to_send0_carry__3_n_2,data_to_send0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI(data_to_send[20:17]),
        .O(data_to_send0[20:17]),
        .S({data_to_send0_carry__3_i_1_n_0,data_to_send0_carry__3_i_2_n_0,data_to_send0_carry__3_i_3_n_0,data_to_send0_carry__3_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__3_i_1
       (.I0(data_to_send[20]),
        .O(data_to_send0_carry__3_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__3_i_2
       (.I0(data_to_send[19]),
        .O(data_to_send0_carry__3_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__3_i_3
       (.I0(data_to_send[18]),
        .O(data_to_send0_carry__3_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__3_i_4
       (.I0(data_to_send[17]),
        .O(data_to_send0_carry__3_i_4_n_0));
  CARRY4 data_to_send0_carry__4
       (.CI(data_to_send0_carry__3_n_0),
        .CO({data_to_send0_carry__4_n_0,data_to_send0_carry__4_n_1,data_to_send0_carry__4_n_2,data_to_send0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI(data_to_send[24:21]),
        .O(data_to_send0[24:21]),
        .S({data_to_send0_carry__4_i_1_n_0,data_to_send0_carry__4_i_2_n_0,data_to_send0_carry__4_i_3_n_0,data_to_send0_carry__4_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__4_i_1
       (.I0(data_to_send[24]),
        .O(data_to_send0_carry__4_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__4_i_2
       (.I0(data_to_send[23]),
        .O(data_to_send0_carry__4_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__4_i_3
       (.I0(data_to_send[22]),
        .O(data_to_send0_carry__4_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__4_i_4
       (.I0(data_to_send[21]),
        .O(data_to_send0_carry__4_i_4_n_0));
  CARRY4 data_to_send0_carry__5
       (.CI(data_to_send0_carry__4_n_0),
        .CO({data_to_send0_carry__5_n_0,data_to_send0_carry__5_n_1,data_to_send0_carry__5_n_2,data_to_send0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI(data_to_send[28:25]),
        .O(data_to_send0[28:25]),
        .S({data_to_send0_carry__5_i_1_n_0,data_to_send0_carry__5_i_2_n_0,data_to_send0_carry__5_i_3_n_0,data_to_send0_carry__5_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__5_i_1
       (.I0(data_to_send[28]),
        .O(data_to_send0_carry__5_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__5_i_2
       (.I0(data_to_send[27]),
        .O(data_to_send0_carry__5_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__5_i_3
       (.I0(data_to_send[26]),
        .O(data_to_send0_carry__5_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__5_i_4
       (.I0(data_to_send[25]),
        .O(data_to_send0_carry__5_i_4_n_0));
  CARRY4 data_to_send0_carry__6
       (.CI(data_to_send0_carry__5_n_0),
        .CO({NLW_data_to_send0_carry__6_CO_UNCONNECTED[3:2],data_to_send0_carry__6_n_2,data_to_send0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,data_to_send[30:29]}),
        .O({NLW_data_to_send0_carry__6_O_UNCONNECTED[3],data_to_send0[31:29]}),
        .S({1'b0,data_to_send0_carry__6_i_1_n_0,data_to_send0_carry__6_i_2_n_0,data_to_send0_carry__6_i_3_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__6_i_1
       (.I0(data_to_send[31]),
        .O(data_to_send0_carry__6_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__6_i_2
       (.I0(data_to_send[30]),
        .O(data_to_send0_carry__6_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry__6_i_3
       (.I0(data_to_send[29]),
        .O(data_to_send0_carry__6_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry_i_1
       (.I0(data_to_send[4]),
        .O(data_to_send0_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry_i_2
       (.I0(data_to_send[3]),
        .O(data_to_send0_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    data_to_send0_carry_i_3
       (.I0(data_to_send[2]),
        .O(data_to_send0_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[0]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[0]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send[0]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[10]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[10]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[10]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[10]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[11]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[11]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[11]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[11]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[12]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[12]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[12]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[12]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[13]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[13]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[13]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[13]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[14]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[14]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[14]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[14]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[15]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[15]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[15]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[15]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[16]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[16]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[16]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[16]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[17]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[17]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[17]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[17]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[18]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[18]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[18]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[18]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[19]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[19]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[19]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[19]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[1]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[1]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[1]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[20]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[20]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[20]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[20]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[21]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[21]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[21]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[21]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[22]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[22]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[22]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[22]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[23]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[23]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[23]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[23]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[24]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[24]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[24]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[25]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[25]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[25]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[26]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[26]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[26]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[27]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[27]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[27]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[27]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[28]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[28]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[28]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[29]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[29]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[29]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[2]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[2]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[2]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[30]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[30]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[30]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[30]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFF0200FFFFFFFF)) 
    \data_to_send[31]_i_1 
       (.I0(m_bvalid),
        .I1(m_bresp[0]),
        .I2(m_bresp[1]),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .I4(\FSM_onehot_state_reg_n_0_[0] ),
        .I5(m_resetn),
        .O(\data_to_send[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[31]_i_2 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[31]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[31]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[31]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \data_to_send[31]_i_3 
       (.I0(m_resetn),
        .I1(\FSM_onehot_state_reg_n_0_[0] ),
        .O(\data_to_send[31]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h7)) 
    \data_to_send[31]_i_4 
       (.I0(\FSM_onehot_state_reg_n_0_[5] ),
        .I1(m_resetn),
        .O(\data_to_send[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[3]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[3]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[3]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[4]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[4]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[4]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[4]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[5]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[5]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[5]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[6]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[6]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[6]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[6]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[7]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[7]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[7]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[8]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[8]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[8]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[8]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h4F444F444F444444)) 
    \data_to_send[9]_i_1 
       (.I0(\data_to_send[31]_i_3_n_0 ),
        .I1(NUM_BYTES[9]),
        .I2(\data_to_send[31]_i_4_n_0 ),
        .I3(data_to_send0[9]),
        .I4(trnfr_cmpl_i_4_n_0),
        .I5(trnfr_cmpl_i_3_n_0),
        .O(\data_to_send[9]_i_1_n_0 ));
  FDRE \data_to_send_reg[0] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[0]_i_1_n_0 ),
        .Q(data_to_send[0]),
        .R(1'b0));
  FDRE \data_to_send_reg[10] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[10]_i_1_n_0 ),
        .Q(data_to_send[10]),
        .R(1'b0));
  FDRE \data_to_send_reg[11] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[11]_i_1_n_0 ),
        .Q(data_to_send[11]),
        .R(1'b0));
  FDRE \data_to_send_reg[12] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[12]_i_1_n_0 ),
        .Q(data_to_send[12]),
        .R(1'b0));
  FDRE \data_to_send_reg[13] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[13]_i_1_n_0 ),
        .Q(data_to_send[13]),
        .R(1'b0));
  FDRE \data_to_send_reg[14] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[14]_i_1_n_0 ),
        .Q(data_to_send[14]),
        .R(1'b0));
  FDRE \data_to_send_reg[15] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[15]_i_1_n_0 ),
        .Q(data_to_send[15]),
        .R(1'b0));
  FDRE \data_to_send_reg[16] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[16]_i_1_n_0 ),
        .Q(data_to_send[16]),
        .R(1'b0));
  FDRE \data_to_send_reg[17] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[17]_i_1_n_0 ),
        .Q(data_to_send[17]),
        .R(1'b0));
  FDRE \data_to_send_reg[18] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[18]_i_1_n_0 ),
        .Q(data_to_send[18]),
        .R(1'b0));
  FDRE \data_to_send_reg[19] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[19]_i_1_n_0 ),
        .Q(data_to_send[19]),
        .R(1'b0));
  FDRE \data_to_send_reg[1] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[1]_i_1_n_0 ),
        .Q(data_to_send[1]),
        .R(1'b0));
  FDRE \data_to_send_reg[20] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[20]_i_1_n_0 ),
        .Q(data_to_send[20]),
        .R(1'b0));
  FDRE \data_to_send_reg[21] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[21]_i_1_n_0 ),
        .Q(data_to_send[21]),
        .R(1'b0));
  FDRE \data_to_send_reg[22] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[22]_i_1_n_0 ),
        .Q(data_to_send[22]),
        .R(1'b0));
  FDRE \data_to_send_reg[23] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[23]_i_1_n_0 ),
        .Q(data_to_send[23]),
        .R(1'b0));
  FDRE \data_to_send_reg[24] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[24]_i_1_n_0 ),
        .Q(data_to_send[24]),
        .R(1'b0));
  FDRE \data_to_send_reg[25] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[25]_i_1_n_0 ),
        .Q(data_to_send[25]),
        .R(1'b0));
  FDRE \data_to_send_reg[26] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[26]_i_1_n_0 ),
        .Q(data_to_send[26]),
        .R(1'b0));
  FDRE \data_to_send_reg[27] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[27]_i_1_n_0 ),
        .Q(data_to_send[27]),
        .R(1'b0));
  FDRE \data_to_send_reg[28] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[28]_i_1_n_0 ),
        .Q(data_to_send[28]),
        .R(1'b0));
  FDRE \data_to_send_reg[29] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[29]_i_1_n_0 ),
        .Q(data_to_send[29]),
        .R(1'b0));
  FDRE \data_to_send_reg[2] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[2]_i_1_n_0 ),
        .Q(data_to_send[2]),
        .R(1'b0));
  FDRE \data_to_send_reg[30] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[30]_i_1_n_0 ),
        .Q(data_to_send[30]),
        .R(1'b0));
  FDRE \data_to_send_reg[31] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[31]_i_2_n_0 ),
        .Q(data_to_send[31]),
        .R(1'b0));
  FDRE \data_to_send_reg[3] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[3]_i_1_n_0 ),
        .Q(data_to_send[3]),
        .R(1'b0));
  FDRE \data_to_send_reg[4] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[4]_i_1_n_0 ),
        .Q(data_to_send[4]),
        .R(1'b0));
  FDRE \data_to_send_reg[5] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[5]_i_1_n_0 ),
        .Q(data_to_send[5]),
        .R(1'b0));
  FDRE \data_to_send_reg[6] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[6]_i_1_n_0 ),
        .Q(data_to_send[6]),
        .R(1'b0));
  FDRE \data_to_send_reg[7] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[7]_i_1_n_0 ),
        .Q(data_to_send[7]),
        .R(1'b0));
  FDRE \data_to_send_reg[8] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[8]_i_1_n_0 ),
        .Q(data_to_send[8]),
        .R(1'b0));
  FDRE \data_to_send_reg[9] 
       (.C(m_clk),
        .CE(\data_to_send[31]_i_1_n_0 ),
        .D(\data_to_send[9]_i_1_n_0 ),
        .Q(data_to_send[9]),
        .R(1'b0));
  CARRY4 m_awaddr0_carry
       (.CI(1'b0),
        .CO({m_awaddr0_carry_n_0,m_awaddr0_carry_n_1,m_awaddr0_carry_n_2,m_awaddr0_carry_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,m_awaddr[2],1'b0}),
        .O(in10[4:1]),
        .S({m_awaddr[4:3],m_awaddr0_carry_i_1_n_0,m_awaddr[1]}));
  CARRY4 m_awaddr0_carry__0
       (.CI(m_awaddr0_carry_n_0),
        .CO({m_awaddr0_carry__0_n_0,m_awaddr0_carry__0_n_1,m_awaddr0_carry__0_n_2,m_awaddr0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in10[8:5]),
        .S(m_awaddr[8:5]));
  CARRY4 m_awaddr0_carry__1
       (.CI(m_awaddr0_carry__0_n_0),
        .CO({m_awaddr0_carry__1_n_0,m_awaddr0_carry__1_n_1,m_awaddr0_carry__1_n_2,m_awaddr0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in10[12:9]),
        .S(m_awaddr[12:9]));
  CARRY4 m_awaddr0_carry__2
       (.CI(m_awaddr0_carry__1_n_0),
        .CO({m_awaddr0_carry__2_n_0,m_awaddr0_carry__2_n_1,m_awaddr0_carry__2_n_2,m_awaddr0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in10[16:13]),
        .S(m_awaddr[16:13]));
  CARRY4 m_awaddr0_carry__3
       (.CI(m_awaddr0_carry__2_n_0),
        .CO({m_awaddr0_carry__3_n_0,m_awaddr0_carry__3_n_1,m_awaddr0_carry__3_n_2,m_awaddr0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in10[20:17]),
        .S(m_awaddr[20:17]));
  CARRY4 m_awaddr0_carry__4
       (.CI(m_awaddr0_carry__3_n_0),
        .CO({m_awaddr0_carry__4_n_0,m_awaddr0_carry__4_n_1,m_awaddr0_carry__4_n_2,m_awaddr0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in10[24:21]),
        .S(m_awaddr[24:21]));
  CARRY4 m_awaddr0_carry__5
       (.CI(m_awaddr0_carry__4_n_0),
        .CO({m_awaddr0_carry__5_n_0,m_awaddr0_carry__5_n_1,m_awaddr0_carry__5_n_2,m_awaddr0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(in10[28:25]),
        .S(m_awaddr[28:25]));
  CARRY4 m_awaddr0_carry__6
       (.CI(m_awaddr0_carry__5_n_0),
        .CO({NLW_m_awaddr0_carry__6_CO_UNCONNECTED[3:2],m_awaddr0_carry__6_n_2,m_awaddr0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_m_awaddr0_carry__6_O_UNCONNECTED[3],in10[31:29]}),
        .S({1'b0,m_awaddr[31:29]}));
  LUT1 #(
    .INIT(2'h1)) 
    m_awaddr0_carry_i_1
       (.I0(m_awaddr[2]),
        .O(m_awaddr0_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[0]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(m_awaddr[0]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[0]),
        .O(p_1_in[0]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[10]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[10]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[10]),
        .O(p_1_in[10]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[11]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[11]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[11]),
        .O(p_1_in[11]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[12]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[12]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[12]),
        .O(p_1_in[12]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[13]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[13]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[13]),
        .O(p_1_in[13]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[14]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[14]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[14]),
        .O(p_1_in[14]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[15]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[15]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[15]),
        .O(p_1_in[15]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[16]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[16]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[16]),
        .O(p_1_in[16]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[17]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[17]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[17]),
        .O(p_1_in[17]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[18]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[18]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[18]),
        .O(p_1_in[18]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[19]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[19]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[19]),
        .O(p_1_in[19]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[1]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[1]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[1]),
        .O(p_1_in[1]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[20]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[20]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[20]),
        .O(p_1_in[20]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[21]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[21]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[21]),
        .O(p_1_in[21]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[22]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[22]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[22]),
        .O(p_1_in[22]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[23]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[23]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[23]),
        .O(p_1_in[23]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[24]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[24]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[24]),
        .O(p_1_in[24]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[25]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[25]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[25]),
        .O(p_1_in[25]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[26]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[26]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[26]),
        .O(p_1_in[26]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[27]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[27]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[27]),
        .O(p_1_in[27]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[28]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[28]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[28]),
        .O(p_1_in[28]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[29]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[29]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[29]),
        .O(p_1_in[29]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[2]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[2]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[2]),
        .O(p_1_in[2]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[30]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[30]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[30]),
        .O(p_1_in[30]));
  LUT6 #(
    .INIT(64'hFFFFFF4FFF4FFF4F)) 
    \m_awaddr[31]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(m_awready),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(ps_image_read),
        .O(\m_awaddr[31]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[31]_i_2 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[31]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[31]),
        .O(p_1_in[31]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[3]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[3]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[3]),
        .O(p_1_in[3]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[4]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[4]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[4]),
        .O(p_1_in[4]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[5]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[5]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[5]),
        .O(p_1_in[5]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[6]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[6]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[6]),
        .O(p_1_in[6]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[7]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[7]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[7]),
        .O(p_1_in[7]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[8]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[8]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[8]),
        .O(p_1_in[8]));
  LUT6 #(
    .INIT(64'hFFFFFF4F40404040)) 
    \m_awaddr[9]_i_1 
       (.I0(m_awvalid_i_2_n_0),
        .I1(in10[9]),
        .I2(m_resetn),
        .I3(\FSM_onehot_state_reg_n_0_[0] ),
        .I4(\FSM_onehot_state_reg_n_0_[6] ),
        .I5(BASE_ADDR_MM[9]),
        .O(p_1_in[9]));
  FDRE \m_awaddr_reg[0] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[0]),
        .Q(m_awaddr[0]),
        .R(1'b0));
  FDRE \m_awaddr_reg[10] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[10]),
        .Q(m_awaddr[10]),
        .R(1'b0));
  FDRE \m_awaddr_reg[11] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[11]),
        .Q(m_awaddr[11]),
        .R(1'b0));
  FDRE \m_awaddr_reg[12] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[12]),
        .Q(m_awaddr[12]),
        .R(1'b0));
  FDRE \m_awaddr_reg[13] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[13]),
        .Q(m_awaddr[13]),
        .R(1'b0));
  FDRE \m_awaddr_reg[14] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[14]),
        .Q(m_awaddr[14]),
        .R(1'b0));
  FDRE \m_awaddr_reg[15] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[15]),
        .Q(m_awaddr[15]),
        .R(1'b0));
  FDRE \m_awaddr_reg[16] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[16]),
        .Q(m_awaddr[16]),
        .R(1'b0));
  FDRE \m_awaddr_reg[17] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[17]),
        .Q(m_awaddr[17]),
        .R(1'b0));
  FDRE \m_awaddr_reg[18] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[18]),
        .Q(m_awaddr[18]),
        .R(1'b0));
  FDRE \m_awaddr_reg[19] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[19]),
        .Q(m_awaddr[19]),
        .R(1'b0));
  FDRE \m_awaddr_reg[1] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[1]),
        .Q(m_awaddr[1]),
        .R(1'b0));
  FDRE \m_awaddr_reg[20] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[20]),
        .Q(m_awaddr[20]),
        .R(1'b0));
  FDRE \m_awaddr_reg[21] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[21]),
        .Q(m_awaddr[21]),
        .R(1'b0));
  FDRE \m_awaddr_reg[22] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[22]),
        .Q(m_awaddr[22]),
        .R(1'b0));
  FDRE \m_awaddr_reg[23] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[23]),
        .Q(m_awaddr[23]),
        .R(1'b0));
  FDRE \m_awaddr_reg[24] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[24]),
        .Q(m_awaddr[24]),
        .R(1'b0));
  FDRE \m_awaddr_reg[25] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[25]),
        .Q(m_awaddr[25]),
        .R(1'b0));
  FDRE \m_awaddr_reg[26] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[26]),
        .Q(m_awaddr[26]),
        .R(1'b0));
  FDRE \m_awaddr_reg[27] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[27]),
        .Q(m_awaddr[27]),
        .R(1'b0));
  FDRE \m_awaddr_reg[28] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[28]),
        .Q(m_awaddr[28]),
        .R(1'b0));
  FDRE \m_awaddr_reg[29] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[29]),
        .Q(m_awaddr[29]),
        .R(1'b0));
  FDRE \m_awaddr_reg[2] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[2]),
        .Q(m_awaddr[2]),
        .R(1'b0));
  FDRE \m_awaddr_reg[30] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[30]),
        .Q(m_awaddr[30]),
        .R(1'b0));
  FDRE \m_awaddr_reg[31] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[31]),
        .Q(m_awaddr[31]),
        .R(1'b0));
  FDRE \m_awaddr_reg[3] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[3]),
        .Q(m_awaddr[3]),
        .R(1'b0));
  FDRE \m_awaddr_reg[4] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[4]),
        .Q(m_awaddr[4]),
        .R(1'b0));
  FDRE \m_awaddr_reg[5] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[5]),
        .Q(m_awaddr[5]),
        .R(1'b0));
  FDRE \m_awaddr_reg[6] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[6]),
        .Q(m_awaddr[6]),
        .R(1'b0));
  FDRE \m_awaddr_reg[7] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[7]),
        .Q(m_awaddr[7]),
        .R(1'b0));
  FDRE \m_awaddr_reg[8] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[8]),
        .Q(m_awaddr[8]),
        .R(1'b0));
  FDRE \m_awaddr_reg[9] 
       (.C(m_clk),
        .CE(\m_awaddr[31]_i_1_n_0 ),
        .D(p_1_in[9]),
        .Q(m_awaddr[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFA2A2A200000000)) 
    m_awvalid_i_1
       (.I0(m_awvalid),
        .I1(m_awready),
        .I2(m_awvalid_i_2_n_0),
        .I3(pio_valid_in),
        .I4(\FSM_onehot_state_reg_n_0_[1] ),
        .I5(m_resetn),
        .O(m_awvalid_i_1_n_0));
  LUT2 #(
    .INIT(4'h1)) 
    m_awvalid_i_2
       (.I0(\FSM_onehot_state_reg_n_0_[3] ),
        .I1(\FSM_onehot_state_reg_n_0_[2] ),
        .O(m_awvalid_i_2_n_0));
  FDRE m_awvalid_reg
       (.C(m_clk),
        .CE(1'b1),
        .D(m_awvalid_i_1_n_0),
        .Q(m_awvalid),
        .R(1'b0));
  LUT2 #(
    .INIT(4'h8)) 
    \m_wdata[31]_i_1 
       (.I0(m_resetn),
        .I1(\FSM_onehot_state_reg_n_0_[3] ),
        .O(\m_wdata[31]_i_1_n_0 ));
  FDRE \m_wdata_reg[0] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[0]),
        .Q(m_wdata[0]),
        .R(1'b0));
  FDRE \m_wdata_reg[10] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[10]),
        .Q(m_wdata[10]),
        .R(1'b0));
  FDRE \m_wdata_reg[11] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[11]),
        .Q(m_wdata[11]),
        .R(1'b0));
  FDRE \m_wdata_reg[12] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[12]),
        .Q(m_wdata[12]),
        .R(1'b0));
  FDRE \m_wdata_reg[13] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[13]),
        .Q(m_wdata[13]),
        .R(1'b0));
  FDRE \m_wdata_reg[14] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[14]),
        .Q(m_wdata[14]),
        .R(1'b0));
  FDRE \m_wdata_reg[15] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[15]),
        .Q(m_wdata[15]),
        .R(1'b0));
  FDRE \m_wdata_reg[16] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[16]),
        .Q(m_wdata[16]),
        .R(1'b0));
  FDRE \m_wdata_reg[17] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[17]),
        .Q(m_wdata[17]),
        .R(1'b0));
  FDRE \m_wdata_reg[18] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[18]),
        .Q(m_wdata[18]),
        .R(1'b0));
  FDRE \m_wdata_reg[19] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[19]),
        .Q(m_wdata[19]),
        .R(1'b0));
  FDRE \m_wdata_reg[1] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[1]),
        .Q(m_wdata[1]),
        .R(1'b0));
  FDRE \m_wdata_reg[20] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[20]),
        .Q(m_wdata[20]),
        .R(1'b0));
  FDRE \m_wdata_reg[21] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[21]),
        .Q(m_wdata[21]),
        .R(1'b0));
  FDRE \m_wdata_reg[22] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[22]),
        .Q(m_wdata[22]),
        .R(1'b0));
  FDRE \m_wdata_reg[23] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[23]),
        .Q(m_wdata[23]),
        .R(1'b0));
  FDRE \m_wdata_reg[24] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[24]),
        .Q(m_wdata[24]),
        .R(1'b0));
  FDRE \m_wdata_reg[25] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[25]),
        .Q(m_wdata[25]),
        .R(1'b0));
  FDRE \m_wdata_reg[26] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[26]),
        .Q(m_wdata[26]),
        .R(1'b0));
  FDRE \m_wdata_reg[27] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[27]),
        .Q(m_wdata[27]),
        .R(1'b0));
  FDRE \m_wdata_reg[28] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[28]),
        .Q(m_wdata[28]),
        .R(1'b0));
  FDRE \m_wdata_reg[29] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[29]),
        .Q(m_wdata[29]),
        .R(1'b0));
  FDRE \m_wdata_reg[2] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[2]),
        .Q(m_wdata[2]),
        .R(1'b0));
  FDRE \m_wdata_reg[30] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[30]),
        .Q(m_wdata[30]),
        .R(1'b0));
  FDRE \m_wdata_reg[31] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[31]),
        .Q(m_wdata[31]),
        .R(1'b0));
  FDRE \m_wdata_reg[3] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[3]),
        .Q(m_wdata[3]),
        .R(1'b0));
  FDRE \m_wdata_reg[4] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[4]),
        .Q(m_wdata[4]),
        .R(1'b0));
  FDRE \m_wdata_reg[5] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[5]),
        .Q(m_wdata[5]),
        .R(1'b0));
  FDRE \m_wdata_reg[6] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[6]),
        .Q(m_wdata[6]),
        .R(1'b0));
  FDRE \m_wdata_reg[7] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[7]),
        .Q(m_wdata[7]),
        .R(1'b0));
  FDRE \m_wdata_reg[8] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[8]),
        .Q(m_wdata[8]),
        .R(1'b0));
  FDRE \m_wdata_reg[9] 
       (.C(m_clk),
        .CE(\m_wdata[31]_i_1_n_0 ),
        .D(pio_data_in_r1[9]),
        .Q(m_wdata[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hFFFF222A00000000)) 
    m_wvalid_i_1
       (.I0(m_wlast),
        .I1(m_wready),
        .I2(\FSM_onehot_state_reg_n_0_[4] ),
        .I3(\FSM_onehot_state_reg_n_0_[5] ),
        .I4(\FSM_onehot_state_reg_n_0_[3] ),
        .I5(m_resetn),
        .O(m_wvalid_i_1_n_0));
  FDRE m_wvalid_reg
       (.C(m_clk),
        .CE(1'b1),
        .D(m_wvalid_i_1_n_0),
        .Q(m_wlast),
        .R(1'b0));
  LUT3 #(
    .INIT(8'h80)) 
    \pio_data_in_r1[31]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(pio_valid_in),
        .I2(m_resetn),
        .O(\pio_data_in_r1[31]_i_1_n_0 ));
  FDRE \pio_data_in_r1_reg[0] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[0]),
        .Q(pio_data_in_r1[0]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[10] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[10]),
        .Q(pio_data_in_r1[10]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[11] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[11]),
        .Q(pio_data_in_r1[11]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[12] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[12]),
        .Q(pio_data_in_r1[12]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[13] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[13]),
        .Q(pio_data_in_r1[13]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[14] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[14]),
        .Q(pio_data_in_r1[14]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[15] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[15]),
        .Q(pio_data_in_r1[15]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[16] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[16]),
        .Q(pio_data_in_r1[16]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[17] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[17]),
        .Q(pio_data_in_r1[17]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[18] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[18]),
        .Q(pio_data_in_r1[18]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[19] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[19]),
        .Q(pio_data_in_r1[19]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[1] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[1]),
        .Q(pio_data_in_r1[1]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[20] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[20]),
        .Q(pio_data_in_r1[20]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[21] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[21]),
        .Q(pio_data_in_r1[21]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[22] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[22]),
        .Q(pio_data_in_r1[22]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[23] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[23]),
        .Q(pio_data_in_r1[23]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[24] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[24]),
        .Q(pio_data_in_r1[24]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[25] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[25]),
        .Q(pio_data_in_r1[25]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[26] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[26]),
        .Q(pio_data_in_r1[26]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[27] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[27]),
        .Q(pio_data_in_r1[27]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[28] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[28]),
        .Q(pio_data_in_r1[28]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[29] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[29]),
        .Q(pio_data_in_r1[29]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[2] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[2]),
        .Q(pio_data_in_r1[2]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[30] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[30]),
        .Q(pio_data_in_r1[30]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[31] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[31]),
        .Q(pio_data_in_r1[31]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[3] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[3]),
        .Q(pio_data_in_r1[3]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[4] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[4]),
        .Q(pio_data_in_r1[4]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[5] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[5]),
        .Q(pio_data_in_r1[5]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[6] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[6]),
        .Q(pio_data_in_r1[6]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[7] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[7]),
        .Q(pio_data_in_r1[7]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[8] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[8]),
        .Q(pio_data_in_r1[8]),
        .R(1'b0));
  FDRE \pio_data_in_r1_reg[9] 
       (.C(m_clk),
        .CE(\pio_data_in_r1[31]_i_1_n_0 ),
        .D(pio_data_in[9]),
        .Q(pio_data_in_r1[9]),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hCCCCAAAE00000000)) 
    trnfr_cmpl_i_1
       (.I0(trnfr_cmpl),
        .I1(trnfr_cmpl_i_2_n_0),
        .I2(trnfr_cmpl_i_3_n_0),
        .I3(trnfr_cmpl_i_4_n_0),
        .I4(trnfr_cmpl_i_5_n_0),
        .I5(m_resetn),
        .O(trnfr_cmpl_i_1_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    trnfr_cmpl_i_10
       (.I0(data_to_send[27]),
        .I1(data_to_send[28]),
        .I2(data_to_send[29]),
        .I3(data_to_send[31]),
        .O(trnfr_cmpl_i_10_n_0));
  LUT3 #(
    .INIT(8'hFE)) 
    trnfr_cmpl_i_11
       (.I0(data_to_send[13]),
        .I1(data_to_send[5]),
        .I2(data_to_send[23]),
        .O(trnfr_cmpl_i_11_n_0));
  LUT4 #(
    .INIT(16'h0200)) 
    trnfr_cmpl_i_2
       (.I0(\FSM_onehot_state_reg_n_0_[5] ),
        .I1(m_bresp[1]),
        .I2(m_bresp[0]),
        .I3(m_bvalid),
        .O(trnfr_cmpl_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    trnfr_cmpl_i_3
       (.I0(\FSM_onehot_state[1]_i_4_n_0 ),
        .I1(trnfr_cmpl_i_6_n_0),
        .I2(trnfr_cmpl_i_7_n_0),
        .I3(trnfr_cmpl_i_8_n_0),
        .I4(trnfr_cmpl_i_9_n_0),
        .O(trnfr_cmpl_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFE0)) 
    trnfr_cmpl_i_4
       (.I0(data_to_send[0]),
        .I1(data_to_send[1]),
        .I2(data_to_send[2]),
        .I3(trnfr_cmpl_i_10_n_0),
        .I4(trnfr_cmpl_i_11_n_0),
        .O(trnfr_cmpl_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'h8)) 
    trnfr_cmpl_i_5
       (.I0(ps_image_read),
        .I1(\FSM_onehot_state_reg_n_0_[6] ),
        .O(trnfr_cmpl_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    trnfr_cmpl_i_6
       (.I0(data_to_send[9]),
        .I1(data_to_send[11]),
        .I2(data_to_send[8]),
        .I3(data_to_send[30]),
        .O(trnfr_cmpl_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    trnfr_cmpl_i_7
       (.I0(data_to_send[21]),
        .I1(data_to_send[26]),
        .I2(data_to_send[12]),
        .I3(data_to_send[22]),
        .O(trnfr_cmpl_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    trnfr_cmpl_i_8
       (.I0(data_to_send[4]),
        .I1(data_to_send[24]),
        .I2(data_to_send[3]),
        .I3(data_to_send[6]),
        .O(trnfr_cmpl_i_8_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    trnfr_cmpl_i_9
       (.I0(data_to_send[19]),
        .I1(data_to_send[20]),
        .I2(data_to_send[14]),
        .I3(data_to_send[18]),
        .O(trnfr_cmpl_i_9_n_0));
  FDRE trnfr_cmpl_reg
       (.C(m_clk),
        .CE(1'b1),
        .D(trnfr_cmpl_i_1_n_0),
        .Q(trnfr_cmpl),
        .R(1'b0));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
