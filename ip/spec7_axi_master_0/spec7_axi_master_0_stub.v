// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Sat Oct  3 15:01:19 2020
// Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
// Command     : write_verilog -force -mode synth_stub
//               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_axi_master_0/spec7_axi_master_0_stub.v
// Design      : spec7_axi_master_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z030fbg676-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "axi_master,Vivado 2019.2" *)
module spec7_axi_master_0(ps_image_read, m_clk, m_resetn, pio_valid_in, 
  pio_data_in, BASE_ADDR_MM, NUM_BYTES, m_awvalid, m_awready, m_awid, m_awaddr, m_awlen, m_awsize, 
  m_awburst, m_awprot, m_awcache, m_wvalid, m_wready, m_wid, m_wdata, m_wstrb, m_wlast, m_bvalid, 
  m_bready, m_bid, m_bresp, m_arvalid, m_arready, m_arid, m_araddr, m_arlen, m_arsize, m_arburst, 
  m_arprot, m_arcache, m_rvalid, m_rready, m_rid, m_rdata, m_rresp, m_rlast, trnfr_cmpl)
/* synthesis syn_black_box black_box_pad_pin="ps_image_read[0:0],m_clk,m_resetn,pio_valid_in,pio_data_in[31:0],BASE_ADDR_MM[31:0],NUM_BYTES[31:0],m_awvalid,m_awready,m_awid[3:0],m_awaddr[31:0],m_awlen[3:0],m_awsize[2:0],m_awburst[1:0],m_awprot[2:0],m_awcache[3:0],m_wvalid,m_wready,m_wid[3:0],m_wdata[31:0],m_wstrb[3:0],m_wlast,m_bvalid,m_bready,m_bid[3:0],m_bresp[1:0],m_arvalid,m_arready,m_arid[3:0],m_araddr[31:0],m_arlen[3:0],m_arsize[2:0],m_arburst[1:0],m_arprot[2:0],m_arcache[3:0],m_rvalid,m_rready,m_rid[3:0],m_rdata[31:0],m_rresp[1:0],m_rlast,trnfr_cmpl[0:0]" */;
  input [0:0]ps_image_read;
  input m_clk;
  input m_resetn;
  input pio_valid_in;
  input [31:0]pio_data_in;
  input [31:0]BASE_ADDR_MM;
  input [31:0]NUM_BYTES;
  output m_awvalid;
  input m_awready;
  output [3:0]m_awid;
  output [31:0]m_awaddr;
  output [3:0]m_awlen;
  output [2:0]m_awsize;
  output [1:0]m_awburst;
  output [2:0]m_awprot;
  output [3:0]m_awcache;
  output m_wvalid;
  input m_wready;
  output [3:0]m_wid;
  output [31:0]m_wdata;
  output [3:0]m_wstrb;
  output m_wlast;
  input m_bvalid;
  output m_bready;
  input [3:0]m_bid;
  input [1:0]m_bresp;
  output m_arvalid;
  input m_arready;
  output [3:0]m_arid;
  output [31:0]m_araddr;
  output [3:0]m_arlen;
  output [2:0]m_arsize;
  output [1:0]m_arburst;
  output [2:0]m_arprot;
  output [3:0]m_arcache;
  input m_rvalid;
  output m_rready;
  input [3:0]m_rid;
  input [31:0]m_rdata;
  input [1:0]m_rresp;
  input m_rlast;
  output [0:0]trnfr_cmpl;
endmodule
