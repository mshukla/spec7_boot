// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
// Date        : Sat Oct  3 15:01:20 2020
// Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
// Command     : write_verilog -force -mode funcsim
//               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_user_axilite_control_0_0/spec7_user_axilite_control_0_0_sim_netlist.v
// Design      : spec7_user_axilite_control_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z030fbg676-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "spec7_user_axilite_control_0_0,user_axilite_control,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "user_axilite_control,Vivado 2019.2" *) 
(* NotValidForBitStream *)
module spec7_user_axilite_control_0_0
   (s_axi_aclk,
    s_axi_areset_n,
    s_axi_awaddr,
    s_axi_awvalid,
    s_axi_awready,
    s_axi_wdata,
    s_axi_wstrb,
    s_axi_wvalid,
    s_axi_wready,
    s_axi_bresp,
    s_axi_bvalid,
    s_axi_bready,
    s_axi_araddr,
    s_axi_arvalid,
    s_axi_arready,
    s_axi_rdata,
    s_axi_rvalid,
    s_axi_rresp,
    s_axi_rready,
    spad_reg,
    spad_val);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 s_axi_signal_clock CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi_signal_clock, ASSOCIATED_BUSIF s_axi, FREQ_HZ 250000000, PHASE 0.000, CLK_DOMAIN spec7_ps7_0_FCLK_CLK0, INSERT_VIP 0" *) input s_axi_aclk;
  input s_axi_areset_n;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi AWADDR" *) input [31:0]s_axi_awaddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi AWVALID" *) input s_axi_awvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi AWREADY" *) output s_axi_awready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi WDATA" *) input [31:0]s_axi_wdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi WSTRB" *) input [3:0]s_axi_wstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi WVALID" *) input s_axi_wvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi WREADY" *) output s_axi_wready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi BRESP" *) output [1:0]s_axi_bresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi BVALID" *) output s_axi_bvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi BREADY" *) input s_axi_bready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi ARADDR" *) input [31:0]s_axi_araddr;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi ARVALID" *) input s_axi_arvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi ARREADY" *) output s_axi_arready;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi RDATA" *) output [31:0]s_axi_rdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi RVALID" *) output s_axi_rvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi RRESP" *) output [1:0]s_axi_rresp;
  (* X_INTERFACE_INFO = "xilinx.com:interface:aximm:1.0 s_axi RREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 250000000, ID_WIDTH 0, ADDR_WIDTH 32, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN spec7_ps7_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0" *) input s_axi_rready;
  output [31:0]spad_reg;
  output [31:0]spad_val;

  wire \<const0> ;
  wire s_axi_aclk;
  wire [31:0]s_axi_araddr;
  wire s_axi_areset_n;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [31:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire s_axi_wvalid;
  wire [31:0]spad_reg;
  wire [31:0]spad_val;

  assign s_axi_bresp[1] = \<const0> ;
  assign s_axi_bresp[0] = \<const0> ;
  assign s_axi_rresp[1] = \<const0> ;
  assign s_axi_rresp[0] = \<const0> ;
  GND GND
       (.G(\<const0> ));
  spec7_user_axilite_control_0_0_user_axilite_control inst
       (.s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr[11:0]),
        .s_axi_areset_n(s_axi_areset_n),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr[11:0]),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .s_axi_rready(s_axi_rready),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wdata(s_axi_wdata),
        .s_axi_wready(s_axi_wready),
        .s_axi_wvalid(s_axi_wvalid),
        .spad_reg(spad_reg),
        .spad_val(spad_val));
endmodule

(* ORIG_REF_NAME = "address_decoder" *) 
module spec7_user_axilite_control_0_0_address_decoder
   (\FSM_sequential_access_cs_reg[0] ,
    cs_ce_ld_enable_i,
    axi_avalid,
    E,
    \bus2ip_addr_reg_reg[2] ,
    \bus2ip_addr_reg_reg[3] ,
    \bus2ip_addr_reg_reg[0] ,
    \MEM_DECODE_GEN[0].cs_out_i_reg[0]_0 ,
    IP2Bus_Data10_out,
    s_axi_areset_n_0,
    bus2ip_rnw_reg_reg,
    s_axi_aclk,
    access_cs,
    data_timeout,
    IP2Bus_WrAck,
    s_axi_areset_n,
    s_axi_wvalid,
    axi_avalid_reg,
    s_axi_arvalid,
    IP2Bus_RdAck,
    Q,
    \scratchpad_reg_reg[31] ,
    Bus2IP_RNW,
    \scratchpad_reg_reg[31]_0 ,
    \spad_val_reg[31] ,
    \scaling_factor_reg[1] ,
    \scaling_factor_reg[1]_0 ,
    s_axi_awvalid);
  output \FSM_sequential_access_cs_reg[0] ;
  output cs_ce_ld_enable_i;
  output axi_avalid;
  output [0:0]E;
  output [0:0]\bus2ip_addr_reg_reg[2] ;
  output [0:0]\bus2ip_addr_reg_reg[3] ;
  output [0:0]\bus2ip_addr_reg_reg[0] ;
  output \MEM_DECODE_GEN[0].cs_out_i_reg[0]_0 ;
  output IP2Bus_Data10_out;
  output [0:0]s_axi_areset_n_0;
  output bus2ip_rnw_reg_reg;
  input s_axi_aclk;
  input [2:0]access_cs;
  input data_timeout;
  input IP2Bus_WrAck;
  input s_axi_areset_n;
  input s_axi_wvalid;
  input axi_avalid_reg;
  input s_axi_arvalid;
  input IP2Bus_RdAck;
  input [6:0]Q;
  input \scratchpad_reg_reg[31] ;
  input Bus2IP_RNW;
  input \scratchpad_reg_reg[31]_0 ;
  input \spad_val_reg[31] ;
  input \scaling_factor_reg[1] ;
  input \scaling_factor_reg[1]_0 ;
  input s_axi_awvalid;

  wire Bus2IP_CS;
  wire Bus2IP_RNW;
  wire [0:0]E;
  wire \FSM_sequential_access_cs_reg[0] ;
  wire IP2Bus_Data10_out;
  wire IP2Bus_RdAck;
  wire IP2Bus_WrAck;
  wire \MEM_DECODE_GEN[0].cs_out_i[0]_i_1_n_0 ;
  wire \MEM_DECODE_GEN[0].cs_out_i[0]_i_2_n_0 ;
  wire \MEM_DECODE_GEN[0].cs_out_i_reg[0]_0 ;
  wire [6:0]Q;
  wire [2:0]access_cs;
  wire axi_avalid;
  wire axi_avalid_reg;
  wire axi_avalid_reg_i_2_n_0;
  wire [0:0]\bus2ip_addr_reg_reg[0] ;
  wire [0:0]\bus2ip_addr_reg_reg[2] ;
  wire [0:0]\bus2ip_addr_reg_reg[3] ;
  wire bus2ip_rnw_reg_reg;
  wire cs_ce_ld_enable_i;
  wire data_timeout;
  wire s_axi_aclk;
  wire s_axi_areset_n;
  wire [0:0]s_axi_areset_n_0;
  wire s_axi_arvalid;
  wire s_axi_awvalid;
  wire s_axi_wvalid;
  wire \scaling_factor_reg[1] ;
  wire \scaling_factor_reg[1]_0 ;
  wire \scratchpad_reg[31]_i_2_n_0 ;
  wire \scratchpad_reg_reg[31] ;
  wire \scratchpad_reg_reg[31]_0 ;
  wire \spad_reg[31]_i_3_n_0 ;
  wire \spad_val_reg[31] ;

  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \IP2Bus_Data[31]_i_1 
       (.I0(s_axi_areset_n),
        .I1(Bus2IP_CS),
        .I2(Bus2IP_RNW),
        .O(s_axi_areset_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h80)) 
    IP2Bus_RdAck_i_1
       (.I0(Bus2IP_RNW),
        .I1(Bus2IP_CS),
        .I2(s_axi_areset_n),
        .O(bus2ip_rnw_reg_reg));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h2)) 
    IP2Bus_WrAck_i_1
       (.I0(Bus2IP_CS),
        .I1(Bus2IP_RNW),
        .O(IP2Bus_Data10_out));
  LUT6 #(
    .INIT(64'h0000000044400040)) 
    \MEM_DECODE_GEN[0].cs_out_i[0]_i_1 
       (.I0(\FSM_sequential_access_cs_reg[0] ),
        .I1(s_axi_areset_n),
        .I2(Bus2IP_CS),
        .I3(cs_ce_ld_enable_i),
        .I4(axi_avalid),
        .I5(\MEM_DECODE_GEN[0].cs_out_i[0]_i_2_n_0 ),
        .O(\MEM_DECODE_GEN[0].cs_out_i[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h000000E0)) 
    \MEM_DECODE_GEN[0].cs_out_i[0]_i_2 
       (.I0(data_timeout),
        .I1(IP2Bus_RdAck),
        .I2(access_cs[0]),
        .I3(access_cs[2]),
        .I4(access_cs[1]),
        .O(\MEM_DECODE_GEN[0].cs_out_i[0]_i_2_n_0 ));
  FDRE \MEM_DECODE_GEN[0].cs_out_i_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\MEM_DECODE_GEN[0].cs_out_i[0]_i_1_n_0 ),
        .Q(Bus2IP_CS),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h0C0CC3C30000C202)) 
    axi_avalid_reg_i_1
       (.I0(axi_avalid_reg_i_2_n_0),
        .I1(access_cs[0]),
        .I2(access_cs[1]),
        .I3(s_axi_wvalid),
        .I4(access_cs[2]),
        .I5(axi_avalid_reg),
        .O(axi_avalid));
  LUT2 #(
    .INIT(4'hE)) 
    axi_avalid_reg_i_2
       (.I0(s_axi_awvalid),
        .I1(s_axi_arvalid),
        .O(axi_avalid_reg_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0020)) 
    \clk_period_reg[31]_i_1 
       (.I0(\scratchpad_reg[31]_i_2_n_0 ),
        .I1(Q[0]),
        .I2(Q[2]),
        .I3(Q[3]),
        .O(E));
  LUT5 #(
    .INIT(32'h00C20002)) 
    \icount_out[6]_i_3 
       (.I0(s_axi_arvalid),
        .I1(access_cs[1]),
        .I2(access_cs[0]),
        .I3(access_cs[2]),
        .I4(s_axi_wvalid),
        .O(cs_ce_ld_enable_i));
  LUT5 #(
    .INIT(32'h00005400)) 
    s_axi_wready_reg_i_1
       (.I0(access_cs[0]),
        .I1(data_timeout),
        .I2(IP2Bus_WrAck),
        .I3(access_cs[2]),
        .I4(access_cs[1]),
        .O(\FSM_sequential_access_cs_reg[0] ));
  LUT6 #(
    .INIT(64'h0000000000000020)) 
    \scaling_factor[1]_i_2 
       (.I0(\scaling_factor_reg[1] ),
        .I1(\scaling_factor_reg[1]_0 ),
        .I2(Bus2IP_CS),
        .I3(Bus2IP_RNW),
        .I4(Q[6]),
        .I5(Q[5]),
        .O(\MEM_DECODE_GEN[0].cs_out_i_reg[0]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'h0002)) 
    \scratchpad_reg[31]_i_1 
       (.I0(\scratchpad_reg[31]_i_2_n_0 ),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(Q[0]),
        .O(\bus2ip_addr_reg_reg[2] ));
  LUT6 #(
    .INIT(64'h0000000000000800)) 
    \scratchpad_reg[31]_i_2 
       (.I0(\scratchpad_reg_reg[31] ),
        .I1(Bus2IP_CS),
        .I2(Bus2IP_RNW),
        .I3(Q[4]),
        .I4(Q[1]),
        .I5(\scratchpad_reg_reg[31]_0 ),
        .O(\scratchpad_reg[31]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000002)) 
    \spad_reg[31]_i_1 
       (.I0(\spad_val_reg[31] ),
        .I1(Q[0]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(\spad_reg[31]_i_3_n_0 ),
        .O(\bus2ip_addr_reg_reg[0] ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'hFFFD)) 
    \spad_reg[31]_i_3 
       (.I0(Bus2IP_CS),
        .I1(Bus2IP_RNW),
        .I2(Q[6]),
        .I3(Q[5]),
        .O(\spad_reg[31]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h00000020)) 
    \spad_val[31]_i_1 
       (.I0(\spad_val_reg[31] ),
        .I1(Q[3]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(\spad_reg[31]_i_3_n_0 ),
        .O(\bus2ip_addr_reg_reg[3] ));
endmodule

(* ORIG_REF_NAME = "axi_lite_ipif" *) 
module spec7_user_axilite_control_0_0_axi_lite_ipif
   (s_axi_awready,
    s_axi_wready,
    s_axi_rvalid,
    s_axi_bvalid,
    Q,
    E,
    \bus2ip_addr_reg_reg[2] ,
    \bus2ip_addr_reg_reg[3] ,
    \bus2ip_addr_reg_reg[0] ,
    \MEM_DECODE_GEN[0].cs_out_i_reg[0] ,
    IP2Bus_Data10_out,
    D,
    \bus2ip_addr_reg_reg[10] ,
    \bus2ip_addr_reg_reg[3]_0 ,
    s_axi_areset_n_0,
    bus2ip_rnw_reg_reg,
    s_axi_arready,
    s_axi_rdata,
    SR,
    s_axi_aclk,
    s_axi_arvalid,
    s_axi_awvalid,
    IP2Bus_WrAck,
    \IP2Bus_Data_reg[4] ,
    spad_reg,
    \IP2Bus_Data_reg[4]_0 ,
    spad_val,
    s_axi_areset_n,
    s_axi_araddr,
    s_axi_awaddr,
    s_axi_wvalid,
    s_axi_bready,
    IP2Bus_RdAck,
    s_axi_rready,
    \IP2Bus_Data_reg[30] ,
    \IP2Bus_Data_reg[2] ,
    \IP2Bus_Data_reg[3] ,
    \IP2Bus_Data_reg[5] ,
    \IP2Bus_Data_reg[6] ,
    \IP2Bus_Data_reg[7] ,
    \IP2Bus_Data_reg[8] ,
    \IP2Bus_Data_reg[9] ,
    \IP2Bus_Data_reg[10] ,
    \IP2Bus_Data_reg[11] ,
    \IP2Bus_Data_reg[12] ,
    \IP2Bus_Data_reg[13] ,
    \IP2Bus_Data_reg[14] ,
    \IP2Bus_Data_reg[15] ,
    \IP2Bus_Data_reg[16] ,
    \IP2Bus_Data_reg[17] ,
    \IP2Bus_Data_reg[18] ,
    \IP2Bus_Data_reg[19] ,
    \IP2Bus_Data_reg[20] ,
    \IP2Bus_Data_reg[21] ,
    \IP2Bus_Data_reg[22] ,
    \IP2Bus_Data_reg[23] ,
    \IP2Bus_Data_reg[1] ,
    \IP2Bus_Data_reg[0] ,
    \s_axi_rdata_reg_reg[31] );
  output s_axi_awready;
  output s_axi_wready;
  output s_axi_rvalid;
  output s_axi_bvalid;
  output [3:0]Q;
  output [0:0]E;
  output [0:0]\bus2ip_addr_reg_reg[2] ;
  output [0:0]\bus2ip_addr_reg_reg[3] ;
  output [0:0]\bus2ip_addr_reg_reg[0] ;
  output \MEM_DECODE_GEN[0].cs_out_i_reg[0] ;
  output IP2Bus_Data10_out;
  output [24:0]D;
  output \bus2ip_addr_reg_reg[10] ;
  output [0:0]\bus2ip_addr_reg_reg[3]_0 ;
  output [0:0]s_axi_areset_n_0;
  output bus2ip_rnw_reg_reg;
  output s_axi_arready;
  output [31:0]s_axi_rdata;
  input [0:0]SR;
  input s_axi_aclk;
  input s_axi_arvalid;
  input s_axi_awvalid;
  input IP2Bus_WrAck;
  input [1:0]\IP2Bus_Data_reg[4] ;
  input [1:0]spad_reg;
  input [2:0]\IP2Bus_Data_reg[4]_0 ;
  input [2:0]spad_val;
  input s_axi_areset_n;
  input [11:0]s_axi_araddr;
  input [11:0]s_axi_awaddr;
  input s_axi_wvalid;
  input s_axi_bready;
  input IP2Bus_RdAck;
  input s_axi_rready;
  input \IP2Bus_Data_reg[30] ;
  input \IP2Bus_Data_reg[2] ;
  input \IP2Bus_Data_reg[3] ;
  input \IP2Bus_Data_reg[5] ;
  input \IP2Bus_Data_reg[6] ;
  input \IP2Bus_Data_reg[7] ;
  input \IP2Bus_Data_reg[8] ;
  input \IP2Bus_Data_reg[9] ;
  input \IP2Bus_Data_reg[10] ;
  input \IP2Bus_Data_reg[11] ;
  input \IP2Bus_Data_reg[12] ;
  input \IP2Bus_Data_reg[13] ;
  input \IP2Bus_Data_reg[14] ;
  input \IP2Bus_Data_reg[15] ;
  input \IP2Bus_Data_reg[16] ;
  input \IP2Bus_Data_reg[17] ;
  input \IP2Bus_Data_reg[18] ;
  input \IP2Bus_Data_reg[19] ;
  input \IP2Bus_Data_reg[20] ;
  input \IP2Bus_Data_reg[21] ;
  input \IP2Bus_Data_reg[22] ;
  input \IP2Bus_Data_reg[23] ;
  input \IP2Bus_Data_reg[1] ;
  input \IP2Bus_Data_reg[0] ;
  input [31:0]\s_axi_rdata_reg_reg[31] ;

  wire [24:0]D;
  wire [0:0]E;
  wire IP2Bus_Data10_out;
  wire \IP2Bus_Data_reg[0] ;
  wire \IP2Bus_Data_reg[10] ;
  wire \IP2Bus_Data_reg[11] ;
  wire \IP2Bus_Data_reg[12] ;
  wire \IP2Bus_Data_reg[13] ;
  wire \IP2Bus_Data_reg[14] ;
  wire \IP2Bus_Data_reg[15] ;
  wire \IP2Bus_Data_reg[16] ;
  wire \IP2Bus_Data_reg[17] ;
  wire \IP2Bus_Data_reg[18] ;
  wire \IP2Bus_Data_reg[19] ;
  wire \IP2Bus_Data_reg[1] ;
  wire \IP2Bus_Data_reg[20] ;
  wire \IP2Bus_Data_reg[21] ;
  wire \IP2Bus_Data_reg[22] ;
  wire \IP2Bus_Data_reg[23] ;
  wire \IP2Bus_Data_reg[2] ;
  wire \IP2Bus_Data_reg[30] ;
  wire \IP2Bus_Data_reg[3] ;
  wire [1:0]\IP2Bus_Data_reg[4] ;
  wire [2:0]\IP2Bus_Data_reg[4]_0 ;
  wire \IP2Bus_Data_reg[5] ;
  wire \IP2Bus_Data_reg[6] ;
  wire \IP2Bus_Data_reg[7] ;
  wire \IP2Bus_Data_reg[8] ;
  wire \IP2Bus_Data_reg[9] ;
  wire IP2Bus_RdAck;
  wire IP2Bus_WrAck;
  wire \MEM_DECODE_GEN[0].cs_out_i_reg[0] ;
  wire [3:0]Q;
  wire [0:0]SR;
  wire [0:0]\bus2ip_addr_reg_reg[0] ;
  wire \bus2ip_addr_reg_reg[10] ;
  wire [0:0]\bus2ip_addr_reg_reg[2] ;
  wire [0:0]\bus2ip_addr_reg_reg[3] ;
  wire [0:0]\bus2ip_addr_reg_reg[3]_0 ;
  wire bus2ip_rnw_reg_reg;
  wire s_axi_aclk;
  wire [11:0]s_axi_araddr;
  wire s_axi_areset_n;
  wire [0:0]s_axi_areset_n_0;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [11:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire [31:0]\s_axi_rdata_reg_reg[31] ;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire s_axi_wready;
  wire s_axi_wvalid;
  wire [1:0]spad_reg;
  wire [2:0]spad_val;

  spec7_user_axilite_control_0_0_slave_attachment I_SLAVE_ATTACHMENT
       (.D(D),
        .E(E),
        .IP2Bus_Data10_out(IP2Bus_Data10_out),
        .\IP2Bus_Data_reg[0] (\IP2Bus_Data_reg[0] ),
        .\IP2Bus_Data_reg[10] (\IP2Bus_Data_reg[10] ),
        .\IP2Bus_Data_reg[11] (\IP2Bus_Data_reg[11] ),
        .\IP2Bus_Data_reg[12] (\IP2Bus_Data_reg[12] ),
        .\IP2Bus_Data_reg[13] (\IP2Bus_Data_reg[13] ),
        .\IP2Bus_Data_reg[14] (\IP2Bus_Data_reg[14] ),
        .\IP2Bus_Data_reg[15] (\IP2Bus_Data_reg[15] ),
        .\IP2Bus_Data_reg[16] (\IP2Bus_Data_reg[16] ),
        .\IP2Bus_Data_reg[17] (\IP2Bus_Data_reg[17] ),
        .\IP2Bus_Data_reg[18] (\IP2Bus_Data_reg[18] ),
        .\IP2Bus_Data_reg[19] (\IP2Bus_Data_reg[19] ),
        .\IP2Bus_Data_reg[1] (\IP2Bus_Data_reg[1] ),
        .\IP2Bus_Data_reg[20] (\IP2Bus_Data_reg[20] ),
        .\IP2Bus_Data_reg[21] (\IP2Bus_Data_reg[21] ),
        .\IP2Bus_Data_reg[22] (\IP2Bus_Data_reg[22] ),
        .\IP2Bus_Data_reg[23] (\IP2Bus_Data_reg[23] ),
        .\IP2Bus_Data_reg[2] (\IP2Bus_Data_reg[2] ),
        .\IP2Bus_Data_reg[30] (\IP2Bus_Data_reg[30] ),
        .\IP2Bus_Data_reg[3] (\IP2Bus_Data_reg[3] ),
        .\IP2Bus_Data_reg[4] (\IP2Bus_Data_reg[4] ),
        .\IP2Bus_Data_reg[4]_0 (\IP2Bus_Data_reg[4]_0 ),
        .\IP2Bus_Data_reg[5] (\IP2Bus_Data_reg[5] ),
        .\IP2Bus_Data_reg[6] (\IP2Bus_Data_reg[6] ),
        .\IP2Bus_Data_reg[7] (\IP2Bus_Data_reg[7] ),
        .\IP2Bus_Data_reg[8] (\IP2Bus_Data_reg[8] ),
        .\IP2Bus_Data_reg[9] (\IP2Bus_Data_reg[9] ),
        .IP2Bus_RdAck(IP2Bus_RdAck),
        .IP2Bus_WrAck(IP2Bus_WrAck),
        .\MEM_DECODE_GEN[0].cs_out_i_reg[0] (\MEM_DECODE_GEN[0].cs_out_i_reg[0] ),
        .Q(Q),
        .SR(SR),
        .\bus2ip_addr_reg_reg[0]_0 (\bus2ip_addr_reg_reg[0] ),
        .\bus2ip_addr_reg_reg[10]_0 (\bus2ip_addr_reg_reg[10] ),
        .\bus2ip_addr_reg_reg[2]_0 (\bus2ip_addr_reg_reg[2] ),
        .\bus2ip_addr_reg_reg[3]_0 (\bus2ip_addr_reg_reg[3] ),
        .\bus2ip_addr_reg_reg[3]_1 (\bus2ip_addr_reg_reg[3]_0 ),
        .bus2ip_rnw_reg_reg_0(bus2ip_rnw_reg_reg),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr),
        .s_axi_areset_n(s_axi_areset_n),
        .s_axi_areset_n_0(s_axi_areset_n_0),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .\s_axi_rdata_reg_reg[31]_0 (\s_axi_rdata_reg_reg[31] ),
        .s_axi_rready(s_axi_rready),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wready(s_axi_wready),
        .s_axi_wvalid(s_axi_wvalid),
        .spad_reg(spad_reg),
        .spad_val(spad_val));
endmodule

(* ORIG_REF_NAME = "counter_f" *) 
module spec7_user_axilite_control_0_0_counter_f
   (counter_en_reg_reg,
    \DATA_PHASE_WDT.timeout_i ,
    counter_en_reg,
    access_cs,
    s_axi_wvalid,
    s_axi_arvalid,
    s_axi_awvalid,
    IP2Bus_RdAck,
    IP2Bus_WrAck,
    data_timeout,
    cs_ce_ld_enable_i,
    s_axi_aclk);
  output counter_en_reg_reg;
  output \DATA_PHASE_WDT.timeout_i ;
  input counter_en_reg;
  input [2:0]access_cs;
  input s_axi_wvalid;
  input s_axi_arvalid;
  input s_axi_awvalid;
  input IP2Bus_RdAck;
  input IP2Bus_WrAck;
  input data_timeout;
  input cs_ce_ld_enable_i;
  input s_axi_aclk;

  wire \DATA_PHASE_WDT.timeout_i ;
  wire IP2Bus_RdAck;
  wire IP2Bus_WrAck;
  wire [2:0]access_cs;
  wire counter_en_reg;
  wire counter_en_reg_reg;
  wire cs_ce_ld_enable_i;
  wire data_timeout;
  wire \icount_out[0]_i_1_n_0 ;
  wire \icount_out[1]_i_1_n_0 ;
  wire \icount_out[2]_i_1_n_0 ;
  wire \icount_out[3]_i_1_n_0 ;
  wire \icount_out[4]_i_1_n_0 ;
  wire \icount_out[4]_i_3_n_0 ;
  wire \icount_out[4]_i_4_n_0 ;
  wire \icount_out[4]_i_5_n_0 ;
  wire \icount_out[4]_i_6_n_0 ;
  wire \icount_out[4]_i_7_n_0 ;
  wire \icount_out[5]_i_2_n_0 ;
  wire \icount_out[5]_i_3_n_0 ;
  wire \icount_out[5]_i_4_n_0 ;
  wire \icount_out[6]_i_1_n_0 ;
  wire \icount_out[6]_i_4_n_0 ;
  wire \icount_out[6]_i_5_n_0 ;
  wire \icount_out_reg[4]_i_2_n_0 ;
  wire \icount_out_reg[4]_i_2_n_1 ;
  wire \icount_out_reg[4]_i_2_n_2 ;
  wire \icount_out_reg[4]_i_2_n_3 ;
  wire \icount_out_reg[4]_i_2_n_4 ;
  wire \icount_out_reg[4]_i_2_n_5 ;
  wire \icount_out_reg[4]_i_2_n_6 ;
  wire \icount_out_reg[4]_i_2_n_7 ;
  wire \icount_out_reg[6]_i_2_n_3 ;
  wire \icount_out_reg[6]_i_2_n_6 ;
  wire \icount_out_reg[6]_i_2_n_7 ;
  wire \icount_out_reg_n_0_[0] ;
  wire \icount_out_reg_n_0_[1] ;
  wire \icount_out_reg_n_0_[2] ;
  wire \icount_out_reg_n_0_[3] ;
  wire \icount_out_reg_n_0_[4] ;
  wire \icount_out_reg_n_0_[5] ;
  wire s_axi_aclk;
  wire s_axi_arvalid;
  wire s_axi_awvalid;
  wire s_axi_wvalid;
  wire [3:1]\NLW_icount_out_reg[6]_i_2_CO_UNCONNECTED ;
  wire [3:2]\NLW_icount_out_reg[6]_i_2_O_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h20032000FFFFFFFF)) 
    \icount_out[0]_i_1 
       (.I0(s_axi_wvalid),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(s_axi_arvalid),
        .I5(\icount_out_reg_n_0_[0] ),
        .O(\icount_out[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAFAEAAAAAA)) 
    \icount_out[1]_i_1 
       (.I0(\icount_out_reg[4]_i_2_n_7 ),
        .I1(s_axi_wvalid),
        .I2(access_cs[2]),
        .I3(access_cs[0]),
        .I4(access_cs[1]),
        .I5(s_axi_arvalid),
        .O(\icount_out[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAFAEAAAAAA)) 
    \icount_out[2]_i_1 
       (.I0(\icount_out_reg[4]_i_2_n_6 ),
        .I1(s_axi_wvalid),
        .I2(access_cs[2]),
        .I3(access_cs[0]),
        .I4(access_cs[1]),
        .I5(s_axi_arvalid),
        .O(\icount_out[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAFAEAAAAAA)) 
    \icount_out[3]_i_1 
       (.I0(\icount_out_reg[4]_i_2_n_5 ),
        .I1(s_axi_wvalid),
        .I2(access_cs[2]),
        .I3(access_cs[0]),
        .I4(access_cs[1]),
        .I5(s_axi_arvalid),
        .O(\icount_out[3]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAEAAAAAFAEAAAAAA)) 
    \icount_out[4]_i_1 
       (.I0(\icount_out_reg[4]_i_2_n_4 ),
        .I1(s_axi_wvalid),
        .I2(access_cs[2]),
        .I3(access_cs[0]),
        .I4(access_cs[1]),
        .I5(s_axi_arvalid),
        .O(\icount_out[4]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \icount_out[4]_i_3 
       (.I0(\icount_out_reg_n_0_[1] ),
        .O(\icount_out[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \icount_out[4]_i_4 
       (.I0(\icount_out_reg_n_0_[3] ),
        .I1(\icount_out_reg_n_0_[4] ),
        .O(\icount_out[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \icount_out[4]_i_5 
       (.I0(\icount_out_reg_n_0_[2] ),
        .I1(\icount_out_reg_n_0_[3] ),
        .O(\icount_out[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \icount_out[4]_i_6 
       (.I0(\icount_out_reg_n_0_[1] ),
        .I1(\icount_out_reg_n_0_[2] ),
        .O(\icount_out[4]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \icount_out[4]_i_7 
       (.I0(\icount_out_reg_n_0_[1] ),
        .I1(counter_en_reg_reg),
        .O(\icount_out[4]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'h444F)) 
    \icount_out[5]_i_1 
       (.I0(\icount_out[5]_i_3_n_0 ),
        .I1(counter_en_reg),
        .I2(\icount_out[5]_i_4_n_0 ),
        .I3(access_cs[2]),
        .O(counter_en_reg_reg));
  LUT6 #(
    .INIT(64'hA2AAAAA0A2AAAAAA)) 
    \icount_out[5]_i_2 
       (.I0(\icount_out_reg[6]_i_2_n_7 ),
        .I1(s_axi_wvalid),
        .I2(access_cs[2]),
        .I3(access_cs[0]),
        .I4(access_cs[1]),
        .I5(s_axi_arvalid),
        .O(\icount_out[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hCFCFCECEFFFFFFCF)) 
    \icount_out[5]_i_3 
       (.I0(IP2Bus_RdAck),
        .I1(access_cs[1]),
        .I2(access_cs[2]),
        .I3(IP2Bus_WrAck),
        .I4(data_timeout),
        .I5(access_cs[0]),
        .O(\icount_out[5]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h1111FFFFFFFF0F03)) 
    \icount_out[5]_i_4 
       (.I0(s_axi_wvalid),
        .I1(counter_en_reg),
        .I2(s_axi_arvalid),
        .I3(s_axi_awvalid),
        .I4(access_cs[0]),
        .I5(access_cs[1]),
        .O(\icount_out[5]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h00E2)) 
    \icount_out[6]_i_1 
       (.I0(\DATA_PHASE_WDT.timeout_i ),
        .I1(counter_en_reg_reg),
        .I2(\icount_out_reg[6]_i_2_n_6 ),
        .I3(cs_ce_ld_enable_i),
        .O(\icount_out[6]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \icount_out[6]_i_4 
       (.I0(\icount_out_reg_n_0_[5] ),
        .O(\icount_out[6]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h9)) 
    \icount_out[6]_i_5 
       (.I0(\icount_out_reg_n_0_[4] ),
        .I1(\icount_out_reg_n_0_[5] ),
        .O(\icount_out[6]_i_5_n_0 ));
  FDRE \icount_out_reg[0] 
       (.C(s_axi_aclk),
        .CE(counter_en_reg_reg),
        .D(\icount_out[0]_i_1_n_0 ),
        .Q(\icount_out_reg_n_0_[0] ),
        .R(1'b0));
  FDRE \icount_out_reg[1] 
       (.C(s_axi_aclk),
        .CE(counter_en_reg_reg),
        .D(\icount_out[1]_i_1_n_0 ),
        .Q(\icount_out_reg_n_0_[1] ),
        .R(1'b0));
  FDRE \icount_out_reg[2] 
       (.C(s_axi_aclk),
        .CE(counter_en_reg_reg),
        .D(\icount_out[2]_i_1_n_0 ),
        .Q(\icount_out_reg_n_0_[2] ),
        .R(1'b0));
  FDRE \icount_out_reg[3] 
       (.C(s_axi_aclk),
        .CE(counter_en_reg_reg),
        .D(\icount_out[3]_i_1_n_0 ),
        .Q(\icount_out_reg_n_0_[3] ),
        .R(1'b0));
  FDRE \icount_out_reg[4] 
       (.C(s_axi_aclk),
        .CE(counter_en_reg_reg),
        .D(\icount_out[4]_i_1_n_0 ),
        .Q(\icount_out_reg_n_0_[4] ),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \icount_out_reg[4]_i_2 
       (.CI(1'b0),
        .CO({\icount_out_reg[4]_i_2_n_0 ,\icount_out_reg[4]_i_2_n_1 ,\icount_out_reg[4]_i_2_n_2 ,\icount_out_reg[4]_i_2_n_3 }),
        .CYINIT(\icount_out_reg_n_0_[0] ),
        .DI({\icount_out_reg_n_0_[3] ,\icount_out_reg_n_0_[2] ,\icount_out_reg_n_0_[1] ,\icount_out[4]_i_3_n_0 }),
        .O({\icount_out_reg[4]_i_2_n_4 ,\icount_out_reg[4]_i_2_n_5 ,\icount_out_reg[4]_i_2_n_6 ,\icount_out_reg[4]_i_2_n_7 }),
        .S({\icount_out[4]_i_4_n_0 ,\icount_out[4]_i_5_n_0 ,\icount_out[4]_i_6_n_0 ,\icount_out[4]_i_7_n_0 }));
  FDRE \icount_out_reg[5] 
       (.C(s_axi_aclk),
        .CE(counter_en_reg_reg),
        .D(\icount_out[5]_i_2_n_0 ),
        .Q(\icount_out_reg_n_0_[5] ),
        .R(1'b0));
  FDRE \icount_out_reg[6] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\icount_out[6]_i_1_n_0 ),
        .Q(\DATA_PHASE_WDT.timeout_i ),
        .R(1'b0));
  (* METHODOLOGY_DRC_VIOS = "{SYNTH-8 {cell *THIS*}}" *) 
  CARRY4 \icount_out_reg[6]_i_2 
       (.CI(\icount_out_reg[4]_i_2_n_0 ),
        .CO({\NLW_icount_out_reg[6]_i_2_CO_UNCONNECTED [3:1],\icount_out_reg[6]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\icount_out_reg_n_0_[4] }),
        .O({\NLW_icount_out_reg[6]_i_2_O_UNCONNECTED [3:2],\icount_out_reg[6]_i_2_n_6 ,\icount_out_reg[6]_i_2_n_7 }),
        .S({1'b0,1'b0,\icount_out[6]_i_4_n_0 ,\icount_out[6]_i_5_n_0 }));
endmodule

(* ORIG_REF_NAME = "registers_common" *) 
module spec7_user_axilite_control_0_0_registers_common
   (IP2Bus_WrAck,
    SR,
    IP2Bus_RdAck,
    Q,
    spad_val,
    \scratchpad_reg_reg[4]_0 ,
    spad_reg,
    \clk_period_reg_reg[30]_0 ,
    \clk_period_reg_reg[23]_0 ,
    \clk_period_reg_reg[22]_0 ,
    \clk_period_reg_reg[21]_0 ,
    \clk_period_reg_reg[20]_0 ,
    \clk_period_reg_reg[19]_0 ,
    \clk_period_reg_reg[18]_0 ,
    \clk_period_reg_reg[17]_0 ,
    \clk_period_reg_reg[16]_0 ,
    \clk_period_reg_reg[15]_0 ,
    \clk_period_reg_reg[14]_0 ,
    \clk_period_reg_reg[13]_0 ,
    \clk_period_reg_reg[12]_0 ,
    \clk_period_reg_reg[11]_0 ,
    \clk_period_reg_reg[10]_0 ,
    \clk_period_reg_reg[9]_0 ,
    \clk_period_reg_reg[8]_0 ,
    \clk_period_reg_reg[7]_0 ,
    \clk_period_reg_reg[6]_0 ,
    \clk_period_reg_reg[5]_0 ,
    \clk_period_reg_reg[3]_0 ,
    \clk_period_reg_reg[2]_0 ,
    \spad_reg_reg[1]_0 ,
    \scratchpad_reg_reg[0]_0 ,
    \IP2Bus_Data_reg[31]_0 ,
    IP2Bus_Data10_out,
    s_axi_aclk,
    IP2Bus_RdAck_reg_0,
    \IP2Bus_Data_reg[24]_0 ,
    s_axi_areset_n,
    \IP2Bus_Data_reg[1]_0 ,
    s_axi_wdata,
    \scaling_factor_reg[1]_0 ,
    E,
    \clk_period_reg_reg[31]_0 ,
    \spad_reg_reg[31]_0 ,
    \spad_val_reg[31]_0 ,
    \IP2Bus_Data_reg[31]_1 ,
    \IP2Bus_Data_reg[31]_2 ,
    D);
  output IP2Bus_WrAck;
  output [0:0]SR;
  output IP2Bus_RdAck;
  output [2:0]Q;
  output [31:0]spad_val;
  output [1:0]\scratchpad_reg_reg[4]_0 ;
  output [31:0]spad_reg;
  output \clk_period_reg_reg[30]_0 ;
  output \clk_period_reg_reg[23]_0 ;
  output \clk_period_reg_reg[22]_0 ;
  output \clk_period_reg_reg[21]_0 ;
  output \clk_period_reg_reg[20]_0 ;
  output \clk_period_reg_reg[19]_0 ;
  output \clk_period_reg_reg[18]_0 ;
  output \clk_period_reg_reg[17]_0 ;
  output \clk_period_reg_reg[16]_0 ;
  output \clk_period_reg_reg[15]_0 ;
  output \clk_period_reg_reg[14]_0 ;
  output \clk_period_reg_reg[13]_0 ;
  output \clk_period_reg_reg[12]_0 ;
  output \clk_period_reg_reg[11]_0 ;
  output \clk_period_reg_reg[10]_0 ;
  output \clk_period_reg_reg[9]_0 ;
  output \clk_period_reg_reg[8]_0 ;
  output \clk_period_reg_reg[7]_0 ;
  output \clk_period_reg_reg[6]_0 ;
  output \clk_period_reg_reg[5]_0 ;
  output \clk_period_reg_reg[3]_0 ;
  output \clk_period_reg_reg[2]_0 ;
  output \spad_reg_reg[1]_0 ;
  output \scratchpad_reg_reg[0]_0 ;
  output [31:0]\IP2Bus_Data_reg[31]_0 ;
  input IP2Bus_Data10_out;
  input s_axi_aclk;
  input IP2Bus_RdAck_reg_0;
  input \IP2Bus_Data_reg[24]_0 ;
  input s_axi_areset_n;
  input [3:0]\IP2Bus_Data_reg[1]_0 ;
  input [31:0]s_axi_wdata;
  input \scaling_factor_reg[1]_0 ;
  input [0:0]E;
  input [0:0]\clk_period_reg_reg[31]_0 ;
  input [0:0]\spad_reg_reg[31]_0 ;
  input [0:0]\spad_val_reg[31]_0 ;
  input [0:0]\IP2Bus_Data_reg[31]_1 ;
  input [0:0]\IP2Bus_Data_reg[31]_2 ;
  input [24:0]D;

  wire [24:0]D;
  wire [0:0]E;
  wire IP2Bus_Data10_out;
  wire \IP2Bus_Data[24]_i_1_n_0 ;
  wire \IP2Bus_Data[24]_i_2_n_0 ;
  wire \IP2Bus_Data[25]_i_1_n_0 ;
  wire \IP2Bus_Data[25]_i_2_n_0 ;
  wire \IP2Bus_Data[26]_i_1_n_0 ;
  wire \IP2Bus_Data[26]_i_2_n_0 ;
  wire \IP2Bus_Data[27]_i_1_n_0 ;
  wire \IP2Bus_Data[27]_i_2_n_0 ;
  wire \IP2Bus_Data[28]_i_1_n_0 ;
  wire \IP2Bus_Data[28]_i_2_n_0 ;
  wire \IP2Bus_Data[29]_i_1_n_0 ;
  wire \IP2Bus_Data[29]_i_2_n_0 ;
  wire \IP2Bus_Data[31]_i_3_n_0 ;
  wire \IP2Bus_Data[31]_i_6_n_0 ;
  wire [3:0]\IP2Bus_Data_reg[1]_0 ;
  wire \IP2Bus_Data_reg[24]_0 ;
  wire [31:0]\IP2Bus_Data_reg[31]_0 ;
  wire [0:0]\IP2Bus_Data_reg[31]_1 ;
  wire [0:0]\IP2Bus_Data_reg[31]_2 ;
  wire IP2Bus_RdAck;
  wire IP2Bus_RdAck_reg_0;
  wire IP2Bus_WrAck;
  wire [2:0]Q;
  wire [0:0]SR;
  wire [31:2]clk_period_reg;
  wire \clk_period_reg_reg[10]_0 ;
  wire \clk_period_reg_reg[11]_0 ;
  wire \clk_period_reg_reg[12]_0 ;
  wire \clk_period_reg_reg[13]_0 ;
  wire \clk_period_reg_reg[14]_0 ;
  wire \clk_period_reg_reg[15]_0 ;
  wire \clk_period_reg_reg[16]_0 ;
  wire \clk_period_reg_reg[17]_0 ;
  wire \clk_period_reg_reg[18]_0 ;
  wire \clk_period_reg_reg[19]_0 ;
  wire \clk_period_reg_reg[20]_0 ;
  wire \clk_period_reg_reg[21]_0 ;
  wire \clk_period_reg_reg[22]_0 ;
  wire \clk_period_reg_reg[23]_0 ;
  wire \clk_period_reg_reg[2]_0 ;
  wire \clk_period_reg_reg[30]_0 ;
  wire [0:0]\clk_period_reg_reg[31]_0 ;
  wire \clk_period_reg_reg[3]_0 ;
  wire \clk_period_reg_reg[5]_0 ;
  wire \clk_period_reg_reg[6]_0 ;
  wire \clk_period_reg_reg[7]_0 ;
  wire \clk_period_reg_reg[8]_0 ;
  wire \clk_period_reg_reg[9]_0 ;
  wire s_axi_aclk;
  wire s_axi_areset_n;
  wire [31:0]s_axi_wdata;
  wire [1:0]scaling_factor;
  wire \scaling_factor[0]_i_1_n_0 ;
  wire \scaling_factor[1]_i_1_n_0 ;
  wire \scaling_factor_reg[1]_0 ;
  wire [31:0]scratchpad_reg;
  wire \scratchpad_reg_reg[0]_0 ;
  wire [1:0]\scratchpad_reg_reg[4]_0 ;
  wire [31:0]spad_reg;
  wire \spad_reg_reg[1]_0 ;
  wire [0:0]\spad_reg_reg[31]_0 ;
  wire [31:0]spad_val;
  wire [0:0]\spad_val_reg[31]_0 ;

  LUT4 #(
    .INIT(16'hF888)) 
    \IP2Bus_Data[0]_i_2 
       (.I0(scratchpad_reg[0]),
        .I1(\IP2Bus_Data_reg[1]_0 [2]),
        .I2(scaling_factor[0]),
        .I3(\IP2Bus_Data_reg[1]_0 [1]),
        .O(\scratchpad_reg_reg[0]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[10]_i_2 
       (.I0(clk_period_reg[10]),
        .I1(spad_val[10]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[10]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[10]),
        .O(\clk_period_reg_reg[10]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[11]_i_2 
       (.I0(clk_period_reg[11]),
        .I1(spad_val[11]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[11]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[11]),
        .O(\clk_period_reg_reg[11]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[12]_i_2 
       (.I0(clk_period_reg[12]),
        .I1(spad_val[12]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[12]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[12]),
        .O(\clk_period_reg_reg[12]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[13]_i_2 
       (.I0(clk_period_reg[13]),
        .I1(spad_val[13]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[13]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[13]),
        .O(\clk_period_reg_reg[13]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[14]_i_2 
       (.I0(clk_period_reg[14]),
        .I1(spad_val[14]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[14]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[14]),
        .O(\clk_period_reg_reg[14]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[15]_i_2 
       (.I0(clk_period_reg[15]),
        .I1(spad_val[15]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[15]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[15]),
        .O(\clk_period_reg_reg[15]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[16]_i_2 
       (.I0(clk_period_reg[16]),
        .I1(spad_val[16]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[16]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[16]),
        .O(\clk_period_reg_reg[16]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[17]_i_2 
       (.I0(clk_period_reg[17]),
        .I1(spad_val[17]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[17]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[17]),
        .O(\clk_period_reg_reg[17]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[18]_i_2 
       (.I0(clk_period_reg[18]),
        .I1(spad_val[18]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[18]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[18]),
        .O(\clk_period_reg_reg[18]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[19]_i_2 
       (.I0(clk_period_reg[19]),
        .I1(spad_val[19]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[19]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[19]),
        .O(\clk_period_reg_reg[19]_0 ));
  LUT4 #(
    .INIT(16'hF888)) 
    \IP2Bus_Data[1]_i_2 
       (.I0(spad_reg[1]),
        .I1(\IP2Bus_Data_reg[1]_0 [3]),
        .I2(scaling_factor[1]),
        .I3(\IP2Bus_Data_reg[1]_0 [1]),
        .O(\spad_reg_reg[1]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[20]_i_2 
       (.I0(clk_period_reg[20]),
        .I1(spad_val[20]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[20]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[20]),
        .O(\clk_period_reg_reg[20]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[21]_i_2 
       (.I0(clk_period_reg[21]),
        .I1(spad_val[21]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[21]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[21]),
        .O(\clk_period_reg_reg[21]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[22]_i_2 
       (.I0(clk_period_reg[22]),
        .I1(spad_val[22]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[22]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[22]),
        .O(\clk_period_reg_reg[22]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[23]_i_2 
       (.I0(clk_period_reg[23]),
        .I1(spad_val[23]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[23]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[23]),
        .O(\clk_period_reg_reg[23]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \IP2Bus_Data[24]_i_1 
       (.I0(\IP2Bus_Data[24]_i_2_n_0 ),
        .I1(\IP2Bus_Data_reg[24]_0 ),
        .O(\IP2Bus_Data[24]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[24]_i_2 
       (.I0(clk_period_reg[24]),
        .I1(spad_val[24]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[24]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[24]),
        .O(\IP2Bus_Data[24]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \IP2Bus_Data[25]_i_1 
       (.I0(\IP2Bus_Data[25]_i_2_n_0 ),
        .I1(\IP2Bus_Data_reg[24]_0 ),
        .O(\IP2Bus_Data[25]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[25]_i_2 
       (.I0(clk_period_reg[25]),
        .I1(spad_val[25]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[25]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[25]),
        .O(\IP2Bus_Data[25]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \IP2Bus_Data[26]_i_1 
       (.I0(\IP2Bus_Data[26]_i_2_n_0 ),
        .I1(\IP2Bus_Data_reg[24]_0 ),
        .O(\IP2Bus_Data[26]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[26]_i_2 
       (.I0(clk_period_reg[26]),
        .I1(spad_val[26]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[26]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[26]),
        .O(\IP2Bus_Data[26]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \IP2Bus_Data[27]_i_1 
       (.I0(\IP2Bus_Data[27]_i_2_n_0 ),
        .I1(\IP2Bus_Data_reg[24]_0 ),
        .O(\IP2Bus_Data[27]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[27]_i_2 
       (.I0(clk_period_reg[27]),
        .I1(spad_val[27]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[27]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[27]),
        .O(\IP2Bus_Data[27]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \IP2Bus_Data[28]_i_1 
       (.I0(\IP2Bus_Data[28]_i_2_n_0 ),
        .I1(\IP2Bus_Data_reg[24]_0 ),
        .O(\IP2Bus_Data[28]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[28]_i_2 
       (.I0(clk_period_reg[28]),
        .I1(spad_val[28]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[28]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[28]),
        .O(\IP2Bus_Data[28]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \IP2Bus_Data[29]_i_1 
       (.I0(\IP2Bus_Data[29]_i_2_n_0 ),
        .I1(\IP2Bus_Data_reg[24]_0 ),
        .O(\IP2Bus_Data[29]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[29]_i_2 
       (.I0(clk_period_reg[29]),
        .I1(spad_val[29]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[29]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[29]),
        .O(\IP2Bus_Data[29]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[2]_i_2 
       (.I0(clk_period_reg[2]),
        .I1(spad_val[2]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[2]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[2]),
        .O(\clk_period_reg_reg[2]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[30]_i_3 
       (.I0(clk_period_reg[30]),
        .I1(spad_val[30]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[30]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[30]),
        .O(\clk_period_reg_reg[30]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \IP2Bus_Data[31]_i_3 
       (.I0(\IP2Bus_Data[31]_i_6_n_0 ),
        .I1(\IP2Bus_Data_reg[24]_0 ),
        .O(\IP2Bus_Data[31]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[31]_i_6 
       (.I0(clk_period_reg[31]),
        .I1(spad_val[31]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[31]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[31]),
        .O(\IP2Bus_Data[31]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[3]_i_2 
       (.I0(clk_period_reg[3]),
        .I1(spad_val[3]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[3]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[3]),
        .O(\clk_period_reg_reg[3]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[5]_i_2 
       (.I0(clk_period_reg[5]),
        .I1(spad_val[5]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[5]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[5]),
        .O(\clk_period_reg_reg[5]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[6]_i_2 
       (.I0(clk_period_reg[6]),
        .I1(spad_val[6]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[6]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[6]),
        .O(\clk_period_reg_reg[6]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[7]_i_2 
       (.I0(clk_period_reg[7]),
        .I1(spad_val[7]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[7]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[7]),
        .O(\clk_period_reg_reg[7]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[8]_i_2 
       (.I0(clk_period_reg[8]),
        .I1(spad_val[8]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[8]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[8]),
        .O(\clk_period_reg_reg[8]_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \IP2Bus_Data[9]_i_2 
       (.I0(clk_period_reg[9]),
        .I1(spad_val[9]),
        .I2(\IP2Bus_Data_reg[1]_0 [0]),
        .I3(scratchpad_reg[9]),
        .I4(\IP2Bus_Data_reg[1]_0 [2]),
        .I5(spad_reg[9]),
        .O(\clk_period_reg_reg[9]_0 ));
  FDRE \IP2Bus_Data_reg[0] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[0]),
        .Q(\IP2Bus_Data_reg[31]_0 [0]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[10] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[10]),
        .Q(\IP2Bus_Data_reg[31]_0 [10]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[11] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[11]),
        .Q(\IP2Bus_Data_reg[31]_0 [11]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[12] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[12]),
        .Q(\IP2Bus_Data_reg[31]_0 [12]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[13] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[13]),
        .Q(\IP2Bus_Data_reg[31]_0 [13]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[14] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[14]),
        .Q(\IP2Bus_Data_reg[31]_0 [14]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[15] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[15]),
        .Q(\IP2Bus_Data_reg[31]_0 [15]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[16] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[16]),
        .Q(\IP2Bus_Data_reg[31]_0 [16]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[17] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[17]),
        .Q(\IP2Bus_Data_reg[31]_0 [17]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[18] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[18]),
        .Q(\IP2Bus_Data_reg[31]_0 [18]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[19] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[19]),
        .Q(\IP2Bus_Data_reg[31]_0 [19]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[1] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[1]),
        .Q(\IP2Bus_Data_reg[31]_0 [1]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[20] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[20]),
        .Q(\IP2Bus_Data_reg[31]_0 [20]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[21] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[21]),
        .Q(\IP2Bus_Data_reg[31]_0 [21]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[22] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[22]),
        .Q(\IP2Bus_Data_reg[31]_0 [22]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[23] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[23]),
        .Q(\IP2Bus_Data_reg[31]_0 [23]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[24] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(\IP2Bus_Data[24]_i_1_n_0 ),
        .Q(\IP2Bus_Data_reg[31]_0 [24]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[25] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(\IP2Bus_Data[25]_i_1_n_0 ),
        .Q(\IP2Bus_Data_reg[31]_0 [25]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[26] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(\IP2Bus_Data[26]_i_1_n_0 ),
        .Q(\IP2Bus_Data_reg[31]_0 [26]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[27] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(\IP2Bus_Data[27]_i_1_n_0 ),
        .Q(\IP2Bus_Data_reg[31]_0 [27]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[28] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(\IP2Bus_Data[28]_i_1_n_0 ),
        .Q(\IP2Bus_Data_reg[31]_0 [28]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[29] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(\IP2Bus_Data[29]_i_1_n_0 ),
        .Q(\IP2Bus_Data_reg[31]_0 [29]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[2] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[2]),
        .Q(\IP2Bus_Data_reg[31]_0 [2]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[30] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[24]),
        .Q(\IP2Bus_Data_reg[31]_0 [30]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[31] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(\IP2Bus_Data[31]_i_3_n_0 ),
        .Q(\IP2Bus_Data_reg[31]_0 [31]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[3] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[3]),
        .Q(\IP2Bus_Data_reg[31]_0 [3]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[4] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[4]),
        .Q(\IP2Bus_Data_reg[31]_0 [4]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[5] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[5]),
        .Q(\IP2Bus_Data_reg[31]_0 [5]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[6] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[6]),
        .Q(\IP2Bus_Data_reg[31]_0 [6]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[7] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[7]),
        .Q(\IP2Bus_Data_reg[31]_0 [7]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[8] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[8]),
        .Q(\IP2Bus_Data_reg[31]_0 [8]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE \IP2Bus_Data_reg[9] 
       (.C(s_axi_aclk),
        .CE(\IP2Bus_Data_reg[31]_2 ),
        .D(D[9]),
        .Q(\IP2Bus_Data_reg[31]_0 [9]),
        .R(\IP2Bus_Data_reg[31]_1 ));
  FDRE IP2Bus_RdAck_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(IP2Bus_RdAck_reg_0),
        .Q(IP2Bus_RdAck),
        .R(1'b0));
  FDRE IP2Bus_WrAck_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(IP2Bus_Data10_out),
        .Q(IP2Bus_WrAck),
        .R(SR));
  FDRE \clk_period_reg_reg[0] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \clk_period_reg_reg[10] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[10]),
        .Q(clk_period_reg[10]),
        .R(SR));
  FDRE \clk_period_reg_reg[11] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[11]),
        .Q(clk_period_reg[11]),
        .R(SR));
  FDSE \clk_period_reg_reg[12] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[12]),
        .Q(clk_period_reg[12]),
        .S(SR));
  FDSE \clk_period_reg_reg[13] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[13]),
        .Q(clk_period_reg[13]),
        .S(SR));
  FDRE \clk_period_reg_reg[14] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[14]),
        .Q(clk_period_reg[14]),
        .R(SR));
  FDSE \clk_period_reg_reg[15] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[15]),
        .Q(clk_period_reg[15]),
        .S(SR));
  FDRE \clk_period_reg_reg[16] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[16]),
        .Q(clk_period_reg[16]),
        .R(SR));
  FDSE \clk_period_reg_reg[17] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[17]),
        .Q(clk_period_reg[17]),
        .S(SR));
  FDSE \clk_period_reg_reg[18] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[18]),
        .Q(clk_period_reg[18]),
        .S(SR));
  FDRE \clk_period_reg_reg[19] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[19]),
        .Q(clk_period_reg[19]),
        .R(SR));
  FDRE \clk_period_reg_reg[1] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \clk_period_reg_reg[20] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[20]),
        .Q(clk_period_reg[20]),
        .R(SR));
  FDSE \clk_period_reg_reg[21] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[21]),
        .Q(clk_period_reg[21]),
        .S(SR));
  FDSE \clk_period_reg_reg[22] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[22]),
        .Q(clk_period_reg[22]),
        .S(SR));
  FDSE \clk_period_reg_reg[23] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[23]),
        .Q(clk_period_reg[23]),
        .S(SR));
  FDRE \clk_period_reg_reg[24] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[24]),
        .Q(clk_period_reg[24]),
        .R(SR));
  FDSE \clk_period_reg_reg[25] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[25]),
        .Q(clk_period_reg[25]),
        .S(SR));
  FDSE \clk_period_reg_reg[26] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[26]),
        .Q(clk_period_reg[26]),
        .S(SR));
  FDSE \clk_period_reg_reg[27] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[27]),
        .Q(clk_period_reg[27]),
        .S(SR));
  FDRE \clk_period_reg_reg[28] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[28]),
        .Q(clk_period_reg[28]),
        .R(SR));
  FDRE \clk_period_reg_reg[29] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[29]),
        .Q(clk_period_reg[29]),
        .R(SR));
  FDRE \clk_period_reg_reg[2] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[2]),
        .Q(clk_period_reg[2]),
        .R(SR));
  FDRE \clk_period_reg_reg[30] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[30]),
        .Q(clk_period_reg[30]),
        .R(SR));
  FDRE \clk_period_reg_reg[31] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[31]),
        .Q(clk_period_reg[31]),
        .R(SR));
  FDRE \clk_period_reg_reg[3] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[3]),
        .Q(clk_period_reg[3]),
        .R(SR));
  FDRE \clk_period_reg_reg[4] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[4]),
        .Q(Q[2]),
        .R(SR));
  FDRE \clk_period_reg_reg[5] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[5]),
        .Q(clk_period_reg[5]),
        .R(SR));
  FDRE \clk_period_reg_reg[6] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[6]),
        .Q(clk_period_reg[6]),
        .R(SR));
  FDSE \clk_period_reg_reg[7] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[7]),
        .Q(clk_period_reg[7]),
        .S(SR));
  FDRE \clk_period_reg_reg[8] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[8]),
        .Q(clk_period_reg[8]),
        .R(SR));
  FDSE \clk_period_reg_reg[9] 
       (.C(s_axi_aclk),
        .CE(\clk_period_reg_reg[31]_0 ),
        .D(s_axi_wdata[9]),
        .Q(clk_period_reg[9]),
        .S(SR));
  LUT1 #(
    .INIT(2'h1)) 
    s_axi_awready_reg_i_1
       (.I0(s_axi_areset_n),
        .O(SR));
  LUT3 #(
    .INIT(8'hB8)) 
    \scaling_factor[0]_i_1 
       (.I0(s_axi_wdata[0]),
        .I1(\scaling_factor_reg[1]_0 ),
        .I2(scaling_factor[0]),
        .O(\scaling_factor[0]_i_1_n_0 ));
  LUT3 #(
    .INIT(8'hB8)) 
    \scaling_factor[1]_i_1 
       (.I0(s_axi_wdata[1]),
        .I1(\scaling_factor_reg[1]_0 ),
        .I2(scaling_factor[1]),
        .O(\scaling_factor[1]_i_1_n_0 ));
  FDRE \scaling_factor_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\scaling_factor[0]_i_1_n_0 ),
        .Q(scaling_factor[0]),
        .R(SR));
  FDSE \scaling_factor_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\scaling_factor[1]_i_1_n_0 ),
        .Q(scaling_factor[1]),
        .S(SR));
  FDSE \scratchpad_reg_reg[0] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[0]),
        .Q(scratchpad_reg[0]),
        .S(SR));
  FDSE \scratchpad_reg_reg[10] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[10]),
        .Q(scratchpad_reg[10]),
        .S(SR));
  FDSE \scratchpad_reg_reg[11] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[11]),
        .Q(scratchpad_reg[11]),
        .S(SR));
  FDSE \scratchpad_reg_reg[12] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[12]),
        .Q(scratchpad_reg[12]),
        .S(SR));
  FDRE \scratchpad_reg_reg[13] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[13]),
        .Q(scratchpad_reg[13]),
        .R(SR));
  FDSE \scratchpad_reg_reg[14] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[14]),
        .Q(scratchpad_reg[14]),
        .S(SR));
  FDSE \scratchpad_reg_reg[15] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[15]),
        .Q(scratchpad_reg[15]),
        .S(SR));
  FDSE \scratchpad_reg_reg[16] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[16]),
        .Q(scratchpad_reg[16]),
        .S(SR));
  FDRE \scratchpad_reg_reg[17] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[17]),
        .Q(scratchpad_reg[17]),
        .R(SR));
  FDSE \scratchpad_reg_reg[18] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[18]),
        .Q(scratchpad_reg[18]),
        .S(SR));
  FDSE \scratchpad_reg_reg[19] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[19]),
        .Q(scratchpad_reg[19]),
        .S(SR));
  FDSE \scratchpad_reg_reg[1] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[1]),
        .Q(\scratchpad_reg_reg[4]_0 [0]),
        .S(SR));
  FDRE \scratchpad_reg_reg[20] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[20]),
        .Q(scratchpad_reg[20]),
        .R(SR));
  FDSE \scratchpad_reg_reg[21] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[21]),
        .Q(scratchpad_reg[21]),
        .S(SR));
  FDRE \scratchpad_reg_reg[22] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[22]),
        .Q(scratchpad_reg[22]),
        .R(SR));
  FDSE \scratchpad_reg_reg[23] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[23]),
        .Q(scratchpad_reg[23]),
        .S(SR));
  FDRE \scratchpad_reg_reg[24] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[24]),
        .Q(scratchpad_reg[24]),
        .R(SR));
  FDSE \scratchpad_reg_reg[25] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[25]),
        .Q(scratchpad_reg[25]),
        .S(SR));
  FDSE \scratchpad_reg_reg[26] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[26]),
        .Q(scratchpad_reg[26]),
        .S(SR));
  FDSE \scratchpad_reg_reg[27] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[27]),
        .Q(scratchpad_reg[27]),
        .S(SR));
  FDSE \scratchpad_reg_reg[28] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[28]),
        .Q(scratchpad_reg[28]),
        .S(SR));
  FDRE \scratchpad_reg_reg[29] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[29]),
        .Q(scratchpad_reg[29]),
        .R(SR));
  FDSE \scratchpad_reg_reg[2] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[2]),
        .Q(scratchpad_reg[2]),
        .S(SR));
  FDSE \scratchpad_reg_reg[30] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[30]),
        .Q(scratchpad_reg[30]),
        .S(SR));
  FDSE \scratchpad_reg_reg[31] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[31]),
        .Q(scratchpad_reg[31]),
        .S(SR));
  FDSE \scratchpad_reg_reg[3] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[3]),
        .Q(scratchpad_reg[3]),
        .S(SR));
  FDRE \scratchpad_reg_reg[4] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[4]),
        .Q(\scratchpad_reg_reg[4]_0 [1]),
        .R(SR));
  FDSE \scratchpad_reg_reg[5] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[5]),
        .Q(scratchpad_reg[5]),
        .S(SR));
  FDRE \scratchpad_reg_reg[6] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[6]),
        .Q(scratchpad_reg[6]),
        .R(SR));
  FDSE \scratchpad_reg_reg[7] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[7]),
        .Q(scratchpad_reg[7]),
        .S(SR));
  FDRE \scratchpad_reg_reg[8] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[8]),
        .Q(scratchpad_reg[8]),
        .R(SR));
  FDSE \scratchpad_reg_reg[9] 
       (.C(s_axi_aclk),
        .CE(E),
        .D(s_axi_wdata[9]),
        .Q(scratchpad_reg[9]),
        .S(SR));
  FDRE \spad_reg_reg[0] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[0]),
        .Q(spad_reg[0]),
        .R(SR));
  FDRE \spad_reg_reg[10] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[10]),
        .Q(spad_reg[10]),
        .R(SR));
  FDRE \spad_reg_reg[11] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[11]),
        .Q(spad_reg[11]),
        .R(SR));
  FDRE \spad_reg_reg[12] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[12]),
        .Q(spad_reg[12]),
        .R(SR));
  FDRE \spad_reg_reg[13] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[13]),
        .Q(spad_reg[13]),
        .R(SR));
  FDRE \spad_reg_reg[14] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[14]),
        .Q(spad_reg[14]),
        .R(SR));
  FDRE \spad_reg_reg[15] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[15]),
        .Q(spad_reg[15]),
        .R(SR));
  FDRE \spad_reg_reg[16] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[16]),
        .Q(spad_reg[16]),
        .R(SR));
  FDRE \spad_reg_reg[17] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[17]),
        .Q(spad_reg[17]),
        .R(SR));
  FDRE \spad_reg_reg[18] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[18]),
        .Q(spad_reg[18]),
        .R(SR));
  FDRE \spad_reg_reg[19] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[19]),
        .Q(spad_reg[19]),
        .R(SR));
  FDRE \spad_reg_reg[1] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[1]),
        .Q(spad_reg[1]),
        .R(SR));
  FDRE \spad_reg_reg[20] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[20]),
        .Q(spad_reg[20]),
        .R(SR));
  FDRE \spad_reg_reg[21] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[21]),
        .Q(spad_reg[21]),
        .R(SR));
  FDRE \spad_reg_reg[22] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[22]),
        .Q(spad_reg[22]),
        .R(SR));
  FDRE \spad_reg_reg[23] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[23]),
        .Q(spad_reg[23]),
        .R(SR));
  FDRE \spad_reg_reg[24] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[24]),
        .Q(spad_reg[24]),
        .R(SR));
  FDRE \spad_reg_reg[25] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[25]),
        .Q(spad_reg[25]),
        .R(SR));
  FDRE \spad_reg_reg[26] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[26]),
        .Q(spad_reg[26]),
        .R(SR));
  FDRE \spad_reg_reg[27] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[27]),
        .Q(spad_reg[27]),
        .R(SR));
  FDRE \spad_reg_reg[28] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[28]),
        .Q(spad_reg[28]),
        .R(SR));
  FDRE \spad_reg_reg[29] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[29]),
        .Q(spad_reg[29]),
        .R(SR));
  FDRE \spad_reg_reg[2] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[2]),
        .Q(spad_reg[2]),
        .R(SR));
  FDRE \spad_reg_reg[30] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[30]),
        .Q(spad_reg[30]),
        .R(SR));
  FDRE \spad_reg_reg[31] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[31]),
        .Q(spad_reg[31]),
        .R(SR));
  FDRE \spad_reg_reg[3] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[3]),
        .Q(spad_reg[3]),
        .R(SR));
  FDRE \spad_reg_reg[4] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[4]),
        .Q(spad_reg[4]),
        .R(SR));
  FDRE \spad_reg_reg[5] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[5]),
        .Q(spad_reg[5]),
        .R(SR));
  FDRE \spad_reg_reg[6] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[6]),
        .Q(spad_reg[6]),
        .R(SR));
  FDRE \spad_reg_reg[7] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[7]),
        .Q(spad_reg[7]),
        .R(SR));
  FDRE \spad_reg_reg[8] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[8]),
        .Q(spad_reg[8]),
        .R(SR));
  FDRE \spad_reg_reg[9] 
       (.C(s_axi_aclk),
        .CE(\spad_reg_reg[31]_0 ),
        .D(s_axi_wdata[9]),
        .Q(spad_reg[9]),
        .R(SR));
  FDSE \spad_val_reg[0] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[0]),
        .Q(spad_val[0]),
        .S(SR));
  FDRE \spad_val_reg[10] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[10]),
        .Q(spad_val[10]),
        .R(SR));
  FDRE \spad_val_reg[11] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[11]),
        .Q(spad_val[11]),
        .R(SR));
  FDRE \spad_val_reg[12] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[12]),
        .Q(spad_val[12]),
        .R(SR));
  FDRE \spad_val_reg[13] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[13]),
        .Q(spad_val[13]),
        .R(SR));
  FDRE \spad_val_reg[14] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[14]),
        .Q(spad_val[14]),
        .R(SR));
  FDRE \spad_val_reg[15] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[15]),
        .Q(spad_val[15]),
        .R(SR));
  FDRE \spad_val_reg[16] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[16]),
        .Q(spad_val[16]),
        .R(SR));
  FDRE \spad_val_reg[17] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[17]),
        .Q(spad_val[17]),
        .R(SR));
  FDRE \spad_val_reg[18] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[18]),
        .Q(spad_val[18]),
        .R(SR));
  FDRE \spad_val_reg[19] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[19]),
        .Q(spad_val[19]),
        .R(SR));
  FDSE \spad_val_reg[1] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[1]),
        .Q(spad_val[1]),
        .S(SR));
  FDRE \spad_val_reg[20] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[20]),
        .Q(spad_val[20]),
        .R(SR));
  FDRE \spad_val_reg[21] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[21]),
        .Q(spad_val[21]),
        .R(SR));
  FDRE \spad_val_reg[22] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[22]),
        .Q(spad_val[22]),
        .R(SR));
  FDRE \spad_val_reg[23] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[23]),
        .Q(spad_val[23]),
        .R(SR));
  FDRE \spad_val_reg[24] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[24]),
        .Q(spad_val[24]),
        .R(SR));
  FDRE \spad_val_reg[25] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[25]),
        .Q(spad_val[25]),
        .R(SR));
  FDRE \spad_val_reg[26] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[26]),
        .Q(spad_val[26]),
        .R(SR));
  FDRE \spad_val_reg[27] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[27]),
        .Q(spad_val[27]),
        .R(SR));
  FDRE \spad_val_reg[28] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[28]),
        .Q(spad_val[28]),
        .R(SR));
  FDRE \spad_val_reg[29] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[29]),
        .Q(spad_val[29]),
        .R(SR));
  FDSE \spad_val_reg[2] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[2]),
        .Q(spad_val[2]),
        .S(SR));
  FDRE \spad_val_reg[30] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[30]),
        .Q(spad_val[30]),
        .R(SR));
  FDRE \spad_val_reg[31] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[31]),
        .Q(spad_val[31]),
        .R(SR));
  FDSE \spad_val_reg[3] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[3]),
        .Q(spad_val[3]),
        .S(SR));
  FDSE \spad_val_reg[4] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[4]),
        .Q(spad_val[4]),
        .S(SR));
  FDSE \spad_val_reg[5] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[5]),
        .Q(spad_val[5]),
        .S(SR));
  FDRE \spad_val_reg[6] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[6]),
        .Q(spad_val[6]),
        .R(SR));
  FDRE \spad_val_reg[7] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[7]),
        .Q(spad_val[7]),
        .R(SR));
  FDSE \spad_val_reg[8] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[8]),
        .Q(spad_val[8]),
        .S(SR));
  FDRE \spad_val_reg[9] 
       (.C(s_axi_aclk),
        .CE(\spad_val_reg[31]_0 ),
        .D(s_axi_wdata[9]),
        .Q(spad_val[9]),
        .R(SR));
endmodule

(* ORIG_REF_NAME = "slave_attachment" *) 
module spec7_user_axilite_control_0_0_slave_attachment
   (s_axi_awready,
    s_axi_wready,
    s_axi_rvalid,
    s_axi_bvalid,
    Q,
    E,
    \bus2ip_addr_reg_reg[2]_0 ,
    \bus2ip_addr_reg_reg[3]_0 ,
    \bus2ip_addr_reg_reg[0]_0 ,
    \MEM_DECODE_GEN[0].cs_out_i_reg[0] ,
    IP2Bus_Data10_out,
    D,
    \bus2ip_addr_reg_reg[10]_0 ,
    \bus2ip_addr_reg_reg[3]_1 ,
    s_axi_areset_n_0,
    bus2ip_rnw_reg_reg_0,
    s_axi_arready,
    s_axi_rdata,
    SR,
    s_axi_aclk,
    s_axi_arvalid,
    s_axi_awvalid,
    IP2Bus_WrAck,
    \IP2Bus_Data_reg[4] ,
    spad_reg,
    \IP2Bus_Data_reg[4]_0 ,
    spad_val,
    s_axi_areset_n,
    s_axi_araddr,
    s_axi_awaddr,
    s_axi_wvalid,
    s_axi_bready,
    IP2Bus_RdAck,
    s_axi_rready,
    \IP2Bus_Data_reg[30] ,
    \IP2Bus_Data_reg[2] ,
    \IP2Bus_Data_reg[3] ,
    \IP2Bus_Data_reg[5] ,
    \IP2Bus_Data_reg[6] ,
    \IP2Bus_Data_reg[7] ,
    \IP2Bus_Data_reg[8] ,
    \IP2Bus_Data_reg[9] ,
    \IP2Bus_Data_reg[10] ,
    \IP2Bus_Data_reg[11] ,
    \IP2Bus_Data_reg[12] ,
    \IP2Bus_Data_reg[13] ,
    \IP2Bus_Data_reg[14] ,
    \IP2Bus_Data_reg[15] ,
    \IP2Bus_Data_reg[16] ,
    \IP2Bus_Data_reg[17] ,
    \IP2Bus_Data_reg[18] ,
    \IP2Bus_Data_reg[19] ,
    \IP2Bus_Data_reg[20] ,
    \IP2Bus_Data_reg[21] ,
    \IP2Bus_Data_reg[22] ,
    \IP2Bus_Data_reg[23] ,
    \IP2Bus_Data_reg[1] ,
    \IP2Bus_Data_reg[0] ,
    \s_axi_rdata_reg_reg[31]_0 );
  output s_axi_awready;
  output s_axi_wready;
  output s_axi_rvalid;
  output s_axi_bvalid;
  output [3:0]Q;
  output [0:0]E;
  output [0:0]\bus2ip_addr_reg_reg[2]_0 ;
  output [0:0]\bus2ip_addr_reg_reg[3]_0 ;
  output [0:0]\bus2ip_addr_reg_reg[0]_0 ;
  output \MEM_DECODE_GEN[0].cs_out_i_reg[0] ;
  output IP2Bus_Data10_out;
  output [24:0]D;
  output \bus2ip_addr_reg_reg[10]_0 ;
  output [0:0]\bus2ip_addr_reg_reg[3]_1 ;
  output [0:0]s_axi_areset_n_0;
  output bus2ip_rnw_reg_reg_0;
  output s_axi_arready;
  output [31:0]s_axi_rdata;
  input [0:0]SR;
  input s_axi_aclk;
  input s_axi_arvalid;
  input s_axi_awvalid;
  input IP2Bus_WrAck;
  input [1:0]\IP2Bus_Data_reg[4] ;
  input [1:0]spad_reg;
  input [2:0]\IP2Bus_Data_reg[4]_0 ;
  input [2:0]spad_val;
  input s_axi_areset_n;
  input [11:0]s_axi_araddr;
  input [11:0]s_axi_awaddr;
  input s_axi_wvalid;
  input s_axi_bready;
  input IP2Bus_RdAck;
  input s_axi_rready;
  input \IP2Bus_Data_reg[30] ;
  input \IP2Bus_Data_reg[2] ;
  input \IP2Bus_Data_reg[3] ;
  input \IP2Bus_Data_reg[5] ;
  input \IP2Bus_Data_reg[6] ;
  input \IP2Bus_Data_reg[7] ;
  input \IP2Bus_Data_reg[8] ;
  input \IP2Bus_Data_reg[9] ;
  input \IP2Bus_Data_reg[10] ;
  input \IP2Bus_Data_reg[11] ;
  input \IP2Bus_Data_reg[12] ;
  input \IP2Bus_Data_reg[13] ;
  input \IP2Bus_Data_reg[14] ;
  input \IP2Bus_Data_reg[15] ;
  input \IP2Bus_Data_reg[16] ;
  input \IP2Bus_Data_reg[17] ;
  input \IP2Bus_Data_reg[18] ;
  input \IP2Bus_Data_reg[19] ;
  input \IP2Bus_Data_reg[20] ;
  input \IP2Bus_Data_reg[21] ;
  input \IP2Bus_Data_reg[22] ;
  input \IP2Bus_Data_reg[23] ;
  input \IP2Bus_Data_reg[1] ;
  input \IP2Bus_Data_reg[0] ;
  input [31:0]\s_axi_rdata_reg_reg[31]_0 ;

  wire [11:0]Bus2IP_Addr;
  wire Bus2IP_RNW;
  wire [24:0]D;
  wire \DATA_PHASE_WDT.I_DPTO_COUNTER_n_0 ;
  wire \DATA_PHASE_WDT.timeout_i ;
  wire [0:0]E;
  wire \FSM_sequential_access_cs[0]_i_1_n_0 ;
  wire \FSM_sequential_access_cs[1]_i_1_n_0 ;
  wire \FSM_sequential_access_cs[2]_i_1_n_0 ;
  wire \FSM_sequential_access_cs[2]_i_2_n_0 ;
  wire \FSM_sequential_access_cs[2]_i_3_n_0 ;
  wire \FSM_sequential_access_cs[2]_i_4_n_0 ;
  wire IP2Bus_Data10_out;
  wire \IP2Bus_Data[0]_i_3_n_0 ;
  wire \IP2Bus_Data[0]_i_4_n_0 ;
  wire \IP2Bus_Data[1]_i_3_n_0 ;
  wire \IP2Bus_Data[1]_i_4_n_0 ;
  wire \IP2Bus_Data[30]_i_2_n_0 ;
  wire \IP2Bus_Data[31]_i_4_n_0 ;
  wire \IP2Bus_Data[31]_i_5_n_0 ;
  wire \IP2Bus_Data[31]_i_8_n_0 ;
  wire \IP2Bus_Data[31]_i_9_n_0 ;
  wire \IP2Bus_Data[4]_i_2_n_0 ;
  wire \IP2Bus_Data[4]_i_3_n_0 ;
  wire \IP2Bus_Data_reg[0] ;
  wire \IP2Bus_Data_reg[10] ;
  wire \IP2Bus_Data_reg[11] ;
  wire \IP2Bus_Data_reg[12] ;
  wire \IP2Bus_Data_reg[13] ;
  wire \IP2Bus_Data_reg[14] ;
  wire \IP2Bus_Data_reg[15] ;
  wire \IP2Bus_Data_reg[16] ;
  wire \IP2Bus_Data_reg[17] ;
  wire \IP2Bus_Data_reg[18] ;
  wire \IP2Bus_Data_reg[19] ;
  wire \IP2Bus_Data_reg[1] ;
  wire \IP2Bus_Data_reg[20] ;
  wire \IP2Bus_Data_reg[21] ;
  wire \IP2Bus_Data_reg[22] ;
  wire \IP2Bus_Data_reg[23] ;
  wire \IP2Bus_Data_reg[2] ;
  wire \IP2Bus_Data_reg[30] ;
  wire \IP2Bus_Data_reg[3] ;
  wire [1:0]\IP2Bus_Data_reg[4] ;
  wire [2:0]\IP2Bus_Data_reg[4]_0 ;
  wire \IP2Bus_Data_reg[5] ;
  wire \IP2Bus_Data_reg[6] ;
  wire \IP2Bus_Data_reg[7] ;
  wire \IP2Bus_Data_reg[8] ;
  wire \IP2Bus_Data_reg[9] ;
  wire IP2Bus_RdAck;
  wire IP2Bus_WrAck;
  wire I_DECODER_n_0;
  wire \MEM_DECODE_GEN[0].cs_out_i_reg[0] ;
  wire [3:0]Q;
  wire [0:0]SR;
  wire [2:0]access_cs;
  wire axi_avalid;
  wire axi_avalid_reg;
  wire [11:0]bus2ip_addr_i;
  wire \bus2ip_addr_reg[0]_i_2_n_0 ;
  wire \bus2ip_addr_reg[10]_i_2_n_0 ;
  wire \bus2ip_addr_reg[11]_i_2_n_0 ;
  wire \bus2ip_addr_reg[11]_i_3_n_0 ;
  wire \bus2ip_addr_reg[1]_i_2_n_0 ;
  wire \bus2ip_addr_reg[2]_i_2_n_0 ;
  wire \bus2ip_addr_reg[3]_i_2_n_0 ;
  wire \bus2ip_addr_reg[4]_i_2_n_0 ;
  wire \bus2ip_addr_reg[5]_i_2_n_0 ;
  wire \bus2ip_addr_reg[6]_i_2_n_0 ;
  wire \bus2ip_addr_reg[7]_i_2_n_0 ;
  wire \bus2ip_addr_reg[8]_i_2_n_0 ;
  wire \bus2ip_addr_reg[9]_i_2_n_0 ;
  wire [0:0]\bus2ip_addr_reg_reg[0]_0 ;
  wire \bus2ip_addr_reg_reg[10]_0 ;
  wire [0:0]\bus2ip_addr_reg_reg[2]_0 ;
  wire [0:0]\bus2ip_addr_reg_reg[3]_0 ;
  wire [0:0]\bus2ip_addr_reg_reg[3]_1 ;
  wire bus2ip_rnw_i;
  wire bus2ip_rnw_reg_reg_0;
  wire counter_en_reg;
  wire cs_ce_ld_enable_i;
  wire data_timeout;
  wire lite_arready_out;
  wire s_axi_aclk;
  wire [11:0]s_axi_araddr;
  wire s_axi_areset_n;
  wire [0:0]s_axi_areset_n_0;
  wire s_axi_arready;
  wire s_axi_arready_reg_i_1_n_0;
  wire s_axi_arvalid;
  wire [11:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awready_reg_i_2_n_0;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire s_axi_bvalid_reg_i_1_n_0;
  wire [31:0]s_axi_rdata;
  wire [31:0]s_axi_rdata_i1_in;
  wire [31:0]\s_axi_rdata_reg_reg[31]_0 ;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire s_axi_rvalid_i;
  wire s_axi_rvalid_reg_i_1_n_0;
  wire s_axi_wready;
  wire s_axi_wvalid;
  wire \scaling_factor[1]_i_3_n_0 ;
  wire \scaling_factor[1]_i_4_n_0 ;
  wire \scratchpad_reg[31]_i_3_n_0 ;
  wire [1:0]spad_reg;
  wire \spad_reg[31]_i_2_n_0 ;
  wire [2:0]spad_val;

  spec7_user_axilite_control_0_0_counter_f \DATA_PHASE_WDT.I_DPTO_COUNTER 
       (.\DATA_PHASE_WDT.timeout_i (\DATA_PHASE_WDT.timeout_i ),
        .IP2Bus_RdAck(IP2Bus_RdAck),
        .IP2Bus_WrAck(IP2Bus_WrAck),
        .access_cs(access_cs),
        .counter_en_reg(counter_en_reg),
        .counter_en_reg_reg(\DATA_PHASE_WDT.I_DPTO_COUNTER_n_0 ),
        .cs_ce_ld_enable_i(cs_ce_ld_enable_i),
        .data_timeout(data_timeout),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_wvalid(s_axi_wvalid));
  FDRE \DATA_PHASE_WDT.data_timeout_reg 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\DATA_PHASE_WDT.timeout_i ),
        .Q(data_timeout),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000FFFF55540000)) 
    \FSM_sequential_access_cs[0]_i_1 
       (.I0(access_cs[1]),
        .I1(s_axi_awvalid),
        .I2(s_axi_arvalid),
        .I3(access_cs[2]),
        .I4(\FSM_sequential_access_cs[2]_i_2_n_0 ),
        .I5(access_cs[0]),
        .O(\FSM_sequential_access_cs[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h0000FFFFCDCC0000)) 
    \FSM_sequential_access_cs[1]_i_1 
       (.I0(access_cs[2]),
        .I1(access_cs[0]),
        .I2(s_axi_arvalid),
        .I3(s_axi_awvalid),
        .I4(\FSM_sequential_access_cs[2]_i_2_n_0 ),
        .I5(access_cs[1]),
        .O(\FSM_sequential_access_cs[1]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h3F80)) 
    \FSM_sequential_access_cs[2]_i_1 
       (.I0(access_cs[0]),
        .I1(access_cs[1]),
        .I2(\FSM_sequential_access_cs[2]_i_2_n_0 ),
        .I3(access_cs[2]),
        .O(\FSM_sequential_access_cs[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFCCFBC83F0C3B08)) 
    \FSM_sequential_access_cs[2]_i_2 
       (.I0(\FSM_sequential_access_cs[2]_i_3_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[1]),
        .I3(\FSM_sequential_access_cs[2]_i_4_n_0 ),
        .I4(access_cs[0]),
        .I5(s_axi_bready),
        .O(\FSM_sequential_access_cs[2]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_access_cs[2]_i_3 
       (.I0(data_timeout),
        .I1(IP2Bus_WrAck),
        .O(\FSM_sequential_access_cs[2]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAFAFAFA0CFCFCFCF)) 
    \FSM_sequential_access_cs[2]_i_4 
       (.I0(s_axi_wvalid),
        .I1(s_axi_rready),
        .I2(access_cs[1]),
        .I3(data_timeout),
        .I4(IP2Bus_RdAck),
        .I5(access_cs[0]),
        .O(\FSM_sequential_access_cs[2]_i_4_n_0 ));
  (* FSM_ENCODED_STATES = "READ_WAIT:010,WRITING:100,B_VALID:101,BRESP_WAIT:110,READING:001,IDLE:000,WRITE_WAIT:011" *) 
  FDRE \FSM_sequential_access_cs_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\FSM_sequential_access_cs[0]_i_1_n_0 ),
        .Q(access_cs[0]),
        .R(SR));
  (* FSM_ENCODED_STATES = "READ_WAIT:010,WRITING:100,B_VALID:101,BRESP_WAIT:110,READING:001,IDLE:000,WRITE_WAIT:011" *) 
  FDRE \FSM_sequential_access_cs_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\FSM_sequential_access_cs[1]_i_1_n_0 ),
        .Q(access_cs[1]),
        .R(SR));
  (* FSM_ENCODED_STATES = "READ_WAIT:010,WRITING:100,B_VALID:101,BRESP_WAIT:110,READING:001,IDLE:000,WRITE_WAIT:011" *) 
  FDRE \FSM_sequential_access_cs_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\FSM_sequential_access_cs[2]_i_1_n_0 ),
        .Q(access_cs[2]),
        .R(SR));
  LUT6 #(
    .INIT(64'hA3A0A3A3A0A0A0A0)) 
    \IP2Bus_Data[0]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[8]),
        .I2(\IP2Bus_Data[30]_i_2_n_0 ),
        .I3(\IP2Bus_Data_reg[0] ),
        .I4(\IP2Bus_Data[0]_i_3_n_0 ),
        .I5(\IP2Bus_Data[0]_i_4_n_0 ),
        .O(D[0]));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT3 #(
    .INIT(8'h15)) 
    \IP2Bus_Data[0]_i_3 
       (.I0(Q[0]),
        .I1(spad_reg[0]),
        .I2(Q[3]),
        .O(\IP2Bus_Data[0]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hF808FFFF)) 
    \IP2Bus_Data[0]_i_4 
       (.I0(\IP2Bus_Data_reg[4]_0 [0]),
        .I1(Q[2]),
        .I2(Q[3]),
        .I3(spad_val[0]),
        .I4(Q[0]),
        .O(\IP2Bus_Data[0]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[10]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[10] ),
        .O(D[10]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[11]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[11] ),
        .O(D[11]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[12]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[12] ),
        .O(D[12]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[13]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[13] ),
        .O(D[13]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[14]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[14] ),
        .O(D[14]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[15]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[15] ),
        .O(D[15]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[16]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[16] ),
        .O(D[16]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[17]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[17] ),
        .O(D[17]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[18]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[18] ),
        .O(D[18]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[19]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[19] ),
        .O(D[19]));
  LUT6 #(
    .INIT(64'hFFEAFFEAFFEA0000)) 
    \IP2Bus_Data[1]_i_1 
       (.I0(Q[0]),
        .I1(\IP2Bus_Data_reg[4] [0]),
        .I2(Q[2]),
        .I3(\IP2Bus_Data_reg[1] ),
        .I4(\IP2Bus_Data[1]_i_3_n_0 ),
        .I5(\IP2Bus_Data[1]_i_4_n_0 ),
        .O(D[1]));
  LUT6 #(
    .INIT(64'hAA808080AAAAAAAA)) 
    \IP2Bus_Data[1]_i_3 
       (.I0(\scaling_factor[1]_i_3_n_0 ),
        .I1(spad_val[1]),
        .I2(Q[3]),
        .I3(\IP2Bus_Data_reg[4]_0 [1]),
        .I4(Q[2]),
        .I5(Q[0]),
        .O(\IP2Bus_Data[1]_i_3_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'hFE00)) 
    \IP2Bus_Data[1]_i_4 
       (.I0(Bus2IP_Addr[10]),
        .I1(Bus2IP_Addr[11]),
        .I2(Bus2IP_Addr[9]),
        .I3(Q[0]),
        .O(\IP2Bus_Data[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[20]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[20] ),
        .O(D[20]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[21]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[21] ),
        .O(D[21]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[22]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[22] ),
        .O(D[22]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[23]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[23] ),
        .O(D[23]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[2]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[2] ),
        .O(D[2]));
  LUT5 #(
    .INIT(32'hBBBAAAAA)) 
    \IP2Bus_Data[30]_i_1 
       (.I0(\IP2Bus_Data[30]_i_2_n_0 ),
        .I1(Bus2IP_Addr[8]),
        .I2(Q[2]),
        .I3(Q[3]),
        .I4(\IP2Bus_Data_reg[30] ),
        .O(D[24]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \IP2Bus_Data[30]_i_2 
       (.I0(Bus2IP_Addr[9]),
        .I1(Bus2IP_Addr[11]),
        .I2(Bus2IP_Addr[10]),
        .O(\IP2Bus_Data[30]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h00000000DDDD555D)) 
    \IP2Bus_Data[31]_i_2 
       (.I0(Q[1]),
        .I1(\IP2Bus_Data[31]_i_4_n_0 ),
        .I2(Q[0]),
        .I3(Q[2]),
        .I4(Bus2IP_Addr[8]),
        .I5(\IP2Bus_Data[31]_i_5_n_0 ),
        .O(\bus2ip_addr_reg_reg[3]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \IP2Bus_Data[31]_i_4 
       (.I0(Q[3]),
        .I1(Bus2IP_Addr[9]),
        .O(\IP2Bus_Data[31]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFF3FFF3FFB3)) 
    \IP2Bus_Data[31]_i_5 
       (.I0(Bus2IP_Addr[8]),
        .I1(\IP2Bus_Data[31]_i_8_n_0 ),
        .I2(Bus2IP_Addr[9]),
        .I3(\IP2Bus_Data[31]_i_9_n_0 ),
        .I4(Q[2]),
        .I5(Q[3]),
        .O(\IP2Bus_Data[31]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFEFEFEFF)) 
    \IP2Bus_Data[31]_i_7 
       (.I0(Bus2IP_Addr[10]),
        .I1(Bus2IP_Addr[11]),
        .I2(Bus2IP_Addr[9]),
        .I3(Q[3]),
        .I4(Q[2]),
        .I5(Bus2IP_Addr[8]),
        .O(\bus2ip_addr_reg_reg[10]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \IP2Bus_Data[31]_i_8 
       (.I0(Bus2IP_Addr[6]),
        .I1(Bus2IP_Addr[7]),
        .O(\IP2Bus_Data[31]_i_8_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    \IP2Bus_Data[31]_i_9 
       (.I0(Bus2IP_Addr[11]),
        .I1(Bus2IP_Addr[10]),
        .I2(Bus2IP_Addr[1]),
        .I3(Bus2IP_Addr[0]),
        .O(\IP2Bus_Data[31]_i_9_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[3]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[3] ),
        .O(D[3]));
  LUT5 #(
    .INIT(32'hA0A3A0A0)) 
    \IP2Bus_Data[4]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[8]),
        .I2(\IP2Bus_Data[30]_i_2_n_0 ),
        .I3(\IP2Bus_Data[4]_i_2_n_0 ),
        .I4(\IP2Bus_Data[4]_i_3_n_0 ),
        .O(D[4]));
  LUT5 #(
    .INIT(32'h04550404)) 
    \IP2Bus_Data[4]_i_2 
       (.I0(Q[0]),
        .I1(Q[2]),
        .I2(\IP2Bus_Data_reg[4] [1]),
        .I3(spad_reg[1]),
        .I4(Q[3]),
        .O(\IP2Bus_Data[4]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h5555405540554055)) 
    \IP2Bus_Data[4]_i_3 
       (.I0(Q[1]),
        .I1(\IP2Bus_Data_reg[4]_0 [2]),
        .I2(Q[2]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(spad_val[2]),
        .O(\IP2Bus_Data[4]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[5]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[5] ),
        .O(D[5]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[6]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[6] ),
        .O(D[6]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[7]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[7] ),
        .O(D[7]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[8]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[8] ),
        .O(D[8]));
  LUT6 #(
    .INIT(64'hAAA8FFFFAAA8AAA8)) 
    \IP2Bus_Data[9]_i_1 
       (.I0(Q[0]),
        .I1(Bus2IP_Addr[9]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[10]),
        .I4(\bus2ip_addr_reg_reg[10]_0 ),
        .I5(\IP2Bus_Data_reg[9] ),
        .O(D[9]));
  spec7_user_axilite_control_0_0_address_decoder I_DECODER
       (.Bus2IP_RNW(Bus2IP_RNW),
        .E(E),
        .\FSM_sequential_access_cs_reg[0] (I_DECODER_n_0),
        .IP2Bus_Data10_out(IP2Bus_Data10_out),
        .IP2Bus_RdAck(IP2Bus_RdAck),
        .IP2Bus_WrAck(IP2Bus_WrAck),
        .\MEM_DECODE_GEN[0].cs_out_i_reg[0]_0 (\MEM_DECODE_GEN[0].cs_out_i_reg[0] ),
        .Q({Bus2IP_Addr[7:6],Q[2:0],Bus2IP_Addr[1:0]}),
        .access_cs(access_cs),
        .axi_avalid(axi_avalid),
        .axi_avalid_reg(axi_avalid_reg),
        .\bus2ip_addr_reg_reg[0] (\bus2ip_addr_reg_reg[0]_0 ),
        .\bus2ip_addr_reg_reg[2] (\bus2ip_addr_reg_reg[2]_0 ),
        .\bus2ip_addr_reg_reg[3] (\bus2ip_addr_reg_reg[3]_0 ),
        .bus2ip_rnw_reg_reg(bus2ip_rnw_reg_reg_0),
        .cs_ce_ld_enable_i(cs_ce_ld_enable_i),
        .data_timeout(data_timeout),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_areset_n(s_axi_areset_n),
        .s_axi_areset_n_0(s_axi_areset_n_0),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_wvalid(s_axi_wvalid),
        .\scaling_factor_reg[1] (\scaling_factor[1]_i_3_n_0 ),
        .\scaling_factor_reg[1]_0 (\scaling_factor[1]_i_4_n_0 ),
        .\scratchpad_reg_reg[31] (\scratchpad_reg[31]_i_3_n_0 ),
        .\scratchpad_reg_reg[31]_0 (\IP2Bus_Data[30]_i_2_n_0 ),
        .\spad_val_reg[31] (\spad_reg[31]_i_2_n_0 ));
  FDRE axi_avalid_reg_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(axi_avalid),
        .Q(axi_avalid_reg),
        .R(SR));
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[0]_i_1 
       (.I0(\bus2ip_addr_reg[0]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Bus2IP_Addr[0]),
        .O(bus2ip_addr_i[0]));
  LUT6 #(
    .INIT(64'h88CC88C0880088C0)) 
    \bus2ip_addr_reg[0]_i_2 
       (.I0(s_axi_araddr[0]),
        .I1(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .I2(Bus2IP_Addr[0]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(s_axi_awaddr[0]),
        .O(\bus2ip_addr_reg[0]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[10]_i_1 
       (.I0(\bus2ip_addr_reg[10]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Bus2IP_Addr[10]),
        .O(bus2ip_addr_i[10]));
  LUT6 #(
    .INIT(64'hAACCAAF000000000)) 
    \bus2ip_addr_reg[10]_i_2 
       (.I0(s_axi_araddr[10]),
        .I1(s_axi_awaddr[10]),
        .I2(Bus2IP_Addr[10]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .O(\bus2ip_addr_reg[10]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[11]_i_1 
       (.I0(\bus2ip_addr_reg[11]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Bus2IP_Addr[11]),
        .O(bus2ip_addr_i[11]));
  LUT6 #(
    .INIT(64'hCCAACCF000000000)) 
    \bus2ip_addr_reg[11]_i_2 
       (.I0(s_axi_awaddr[11]),
        .I1(s_axi_araddr[11]),
        .I2(Bus2IP_Addr[11]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .O(\bus2ip_addr_reg[11]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \bus2ip_addr_reg[11]_i_3 
       (.I0(access_cs[0]),
        .I1(access_cs[1]),
        .I2(access_cs[2]),
        .O(\bus2ip_addr_reg[11]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[1]_i_1 
       (.I0(\bus2ip_addr_reg[1]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Bus2IP_Addr[1]),
        .O(bus2ip_addr_i[1]));
  LUT6 #(
    .INIT(64'hAACCAAF000000000)) 
    \bus2ip_addr_reg[1]_i_2 
       (.I0(s_axi_araddr[1]),
        .I1(s_axi_awaddr[1]),
        .I2(Bus2IP_Addr[1]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .O(\bus2ip_addr_reg[1]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[2]_i_1 
       (.I0(\bus2ip_addr_reg[2]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Q[0]),
        .O(bus2ip_addr_i[2]));
  LUT6 #(
    .INIT(64'hCCAACCF000000000)) 
    \bus2ip_addr_reg[2]_i_2 
       (.I0(s_axi_awaddr[2]),
        .I1(s_axi_araddr[2]),
        .I2(Q[0]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .O(\bus2ip_addr_reg[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[3]_i_1 
       (.I0(\bus2ip_addr_reg[3]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Q[1]),
        .O(bus2ip_addr_i[3]));
  LUT6 #(
    .INIT(64'h88CC88C0880088C0)) 
    \bus2ip_addr_reg[3]_i_2 
       (.I0(s_axi_araddr[3]),
        .I1(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .I2(Q[1]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(s_axi_awaddr[3]),
        .O(\bus2ip_addr_reg[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[4]_i_1 
       (.I0(\bus2ip_addr_reg[4]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Q[2]),
        .O(bus2ip_addr_i[4]));
  LUT6 #(
    .INIT(64'h88CC88C0880088C0)) 
    \bus2ip_addr_reg[4]_i_2 
       (.I0(s_axi_araddr[4]),
        .I1(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .I2(Q[2]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(s_axi_awaddr[4]),
        .O(\bus2ip_addr_reg[4]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[5]_i_1 
       (.I0(\bus2ip_addr_reg[5]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Q[3]),
        .O(bus2ip_addr_i[5]));
  LUT6 #(
    .INIT(64'h88CC88C0880088C0)) 
    \bus2ip_addr_reg[5]_i_2 
       (.I0(s_axi_araddr[5]),
        .I1(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .I2(Q[3]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(s_axi_awaddr[5]),
        .O(\bus2ip_addr_reg[5]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[6]_i_1 
       (.I0(\bus2ip_addr_reg[6]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Bus2IP_Addr[6]),
        .O(bus2ip_addr_i[6]));
  LUT6 #(
    .INIT(64'hAACCAAF000000000)) 
    \bus2ip_addr_reg[6]_i_2 
       (.I0(s_axi_araddr[6]),
        .I1(s_axi_awaddr[6]),
        .I2(Bus2IP_Addr[6]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .O(\bus2ip_addr_reg[6]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[7]_i_1 
       (.I0(\bus2ip_addr_reg[7]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Bus2IP_Addr[7]),
        .O(bus2ip_addr_i[7]));
  LUT6 #(
    .INIT(64'h88CC88C0880088C0)) 
    \bus2ip_addr_reg[7]_i_2 
       (.I0(s_axi_araddr[7]),
        .I1(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .I2(Bus2IP_Addr[7]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(s_axi_awaddr[7]),
        .O(\bus2ip_addr_reg[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[8]_i_1 
       (.I0(\bus2ip_addr_reg[8]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Bus2IP_Addr[8]),
        .O(bus2ip_addr_i[8]));
  LUT6 #(
    .INIT(64'h88CC88C0880088C0)) 
    \bus2ip_addr_reg[8]_i_2 
       (.I0(s_axi_araddr[8]),
        .I1(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .I2(Bus2IP_Addr[8]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(s_axi_awaddr[8]),
        .O(\bus2ip_addr_reg[8]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hBFFEAAAA)) 
    \bus2ip_addr_reg[9]_i_1 
       (.I0(\bus2ip_addr_reg[9]_i_2_n_0 ),
        .I1(access_cs[2]),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(Bus2IP_Addr[9]),
        .O(bus2ip_addr_i[9]));
  LUT6 #(
    .INIT(64'hAACCAAF000000000)) 
    \bus2ip_addr_reg[9]_i_2 
       (.I0(s_axi_araddr[9]),
        .I1(s_axi_awaddr[9]),
        .I2(Bus2IP_Addr[9]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .I5(\bus2ip_addr_reg[11]_i_3_n_0 ),
        .O(\bus2ip_addr_reg[9]_i_2_n_0 ));
  FDRE \bus2ip_addr_reg_reg[0] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[0]),
        .Q(Bus2IP_Addr[0]),
        .R(SR));
  FDRE \bus2ip_addr_reg_reg[10] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[10]),
        .Q(Bus2IP_Addr[10]),
        .R(SR));
  FDRE \bus2ip_addr_reg_reg[11] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[11]),
        .Q(Bus2IP_Addr[11]),
        .R(SR));
  FDRE \bus2ip_addr_reg_reg[1] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[1]),
        .Q(Bus2IP_Addr[1]),
        .R(SR));
  FDRE \bus2ip_addr_reg_reg[2] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[2]),
        .Q(Q[0]),
        .R(SR));
  FDRE \bus2ip_addr_reg_reg[3] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[3]),
        .Q(Q[1]),
        .R(SR));
  FDRE \bus2ip_addr_reg_reg[4] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[4]),
        .Q(Q[2]),
        .R(SR));
  FDRE \bus2ip_addr_reg_reg[5] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[5]),
        .Q(Q[3]),
        .R(SR));
  FDRE \bus2ip_addr_reg_reg[6] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[6]),
        .Q(Bus2IP_Addr[6]),
        .R(SR));
  FDRE \bus2ip_addr_reg_reg[7] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[7]),
        .Q(Bus2IP_Addr[7]),
        .R(SR));
  FDRE \bus2ip_addr_reg_reg[8] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[8]),
        .Q(Bus2IP_Addr[8]),
        .R(SR));
  FDRE \bus2ip_addr_reg_reg[9] 
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_addr_i[9]),
        .Q(Bus2IP_Addr[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h0FFFFFFB0000000A)) 
    bus2ip_rnw_reg_i_1
       (.I0(s_axi_arvalid),
        .I1(s_axi_awvalid),
        .I2(access_cs[0]),
        .I3(access_cs[1]),
        .I4(access_cs[2]),
        .I5(Bus2IP_RNW),
        .O(bus2ip_rnw_i));
  FDRE bus2ip_rnw_reg_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(bus2ip_rnw_i),
        .Q(Bus2IP_RNW),
        .R(SR));
  FDRE counter_en_reg_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(\DATA_PHASE_WDT.I_DPTO_COUNTER_n_0 ),
        .Q(counter_en_reg),
        .R(SR));
  LUT2 #(
    .INIT(4'h2)) 
    s_axi_arready_INST_0
       (.I0(lite_arready_out),
        .I1(s_axi_rvalid),
        .O(s_axi_arready));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h55550001)) 
    s_axi_arready_reg_i_1
       (.I0(access_cs[0]),
        .I1(access_cs[2]),
        .I2(s_axi_arvalid),
        .I3(s_axi_awvalid),
        .I4(access_cs[1]),
        .O(s_axi_arready_reg_i_1_n_0));
  FDRE s_axi_arready_reg_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(s_axi_arready_reg_i_1_n_0),
        .Q(lite_arready_out),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h00010000)) 
    s_axi_awready_reg_i_2
       (.I0(access_cs[2]),
        .I1(access_cs[1]),
        .I2(access_cs[0]),
        .I3(s_axi_arvalid),
        .I4(s_axi_awvalid),
        .O(s_axi_awready_reg_i_2_n_0));
  FDRE s_axi_awready_reg_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(s_axi_awready_reg_i_2_n_0),
        .Q(s_axi_awready),
        .R(SR));
  LUT5 #(
    .INIT(32'hFF7F2200)) 
    s_axi_bvalid_reg_i_1
       (.I0(access_cs[2]),
        .I1(access_cs[1]),
        .I2(s_axi_bready),
        .I3(access_cs[0]),
        .I4(s_axi_bvalid),
        .O(s_axi_bvalid_reg_i_1_n_0));
  FDRE s_axi_bvalid_reg_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(s_axi_bvalid_reg_i_1_n_0),
        .Q(s_axi_bvalid),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[0]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [0]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[10]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [10]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[10]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[11]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [11]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[11]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[12]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [12]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[12]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[13]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [13]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[13]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[14]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [14]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[14]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[15]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [15]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[15]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[16]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [16]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[16]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[17]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [17]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[17]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[18]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [18]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[18]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[19]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [19]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[19]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[1]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [1]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[20]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [20]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[20]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[21]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [21]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[21]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[22]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [22]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[22]));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[23]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [23]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[23]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[24]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [24]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[24]));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[25]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [25]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[25]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[26]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [26]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[26]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[27]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [27]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[27]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[28]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [28]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[28]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[29]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [29]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[29]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[2]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [2]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[30]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [30]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[30]));
  LUT6 #(
    .INIT(64'h00000E0E0000F000)) 
    \s_axi_rdata_reg[31]_i_1 
       (.I0(IP2Bus_RdAck),
        .I1(data_timeout),
        .I2(access_cs[1]),
        .I3(s_axi_rready),
        .I4(access_cs[2]),
        .I5(access_cs[0]),
        .O(s_axi_rvalid_i));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[31]_i_2 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [31]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[31]));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[3]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [3]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[4]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [4]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[4]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[5]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [5]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[6]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [6]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[7]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [7]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[7]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[8]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [8]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[8]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \s_axi_rdata_reg[9]_i_1 
       (.I0(\s_axi_rdata_reg_reg[31]_0 [9]),
        .I1(access_cs[1]),
        .I2(data_timeout),
        .O(s_axi_rdata_i1_in[9]));
  FDRE \s_axi_rdata_reg_reg[0] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[0]),
        .Q(s_axi_rdata[0]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[10] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[10]),
        .Q(s_axi_rdata[10]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[11] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[11]),
        .Q(s_axi_rdata[11]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[12] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[12]),
        .Q(s_axi_rdata[12]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[13] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[13]),
        .Q(s_axi_rdata[13]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[14] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[14]),
        .Q(s_axi_rdata[14]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[15] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[15]),
        .Q(s_axi_rdata[15]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[16] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[16]),
        .Q(s_axi_rdata[16]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[17] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[17]),
        .Q(s_axi_rdata[17]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[18] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[18]),
        .Q(s_axi_rdata[18]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[19] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[19]),
        .Q(s_axi_rdata[19]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[1] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[1]),
        .Q(s_axi_rdata[1]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[20] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[20]),
        .Q(s_axi_rdata[20]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[21] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[21]),
        .Q(s_axi_rdata[21]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[22] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[22]),
        .Q(s_axi_rdata[22]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[23] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[23]),
        .Q(s_axi_rdata[23]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[24] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[24]),
        .Q(s_axi_rdata[24]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[25] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[25]),
        .Q(s_axi_rdata[25]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[26] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[26]),
        .Q(s_axi_rdata[26]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[27] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[27]),
        .Q(s_axi_rdata[27]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[28] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[28]),
        .Q(s_axi_rdata[28]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[29] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[29]),
        .Q(s_axi_rdata[29]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[2] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[2]),
        .Q(s_axi_rdata[2]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[30] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[30]),
        .Q(s_axi_rdata[30]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[31] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[31]),
        .Q(s_axi_rdata[31]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[3] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[3]),
        .Q(s_axi_rdata[3]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[4] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[4]),
        .Q(s_axi_rdata[4]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[5] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[5]),
        .Q(s_axi_rdata[5]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[6] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[6]),
        .Q(s_axi_rdata[6]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[7] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[7]),
        .Q(s_axi_rdata[7]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[8] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[8]),
        .Q(s_axi_rdata[8]),
        .R(SR));
  FDRE \s_axi_rdata_reg_reg[9] 
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rdata_i1_in[9]),
        .Q(s_axi_rdata[9]),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h0E)) 
    s_axi_rvalid_reg_i_1
       (.I0(data_timeout),
        .I1(IP2Bus_RdAck),
        .I2(access_cs[1]),
        .O(s_axi_rvalid_reg_i_1_n_0));
  FDRE s_axi_rvalid_reg_reg
       (.C(s_axi_aclk),
        .CE(s_axi_rvalid_i),
        .D(s_axi_rvalid_reg_i_1_n_0),
        .Q(s_axi_rvalid),
        .R(SR));
  FDRE s_axi_wready_reg_reg
       (.C(s_axi_aclk),
        .CE(1'b1),
        .D(I_DECODER_n_0),
        .Q(s_axi_wready),
        .R(SR));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \scaling_factor[1]_i_3 
       (.I0(Bus2IP_Addr[8]),
        .I1(Bus2IP_Addr[10]),
        .I2(Bus2IP_Addr[11]),
        .I3(Bus2IP_Addr[9]),
        .O(\scaling_factor[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFEF)) 
    \scaling_factor[1]_i_4 
       (.I0(Bus2IP_Addr[1]),
        .I1(Bus2IP_Addr[0]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(Q[3]),
        .I5(Q[2]),
        .O(\scaling_factor[1]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \scratchpad_reg[31]_i_3 
       (.I0(Bus2IP_Addr[7]),
        .I1(Bus2IP_Addr[6]),
        .I2(Bus2IP_Addr[8]),
        .I3(Q[3]),
        .O(\scratchpad_reg[31]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'h00000010)) 
    \spad_reg[31]_i_2 
       (.I0(\IP2Bus_Data[30]_i_2_n_0 ),
        .I1(Bus2IP_Addr[8]),
        .I2(Q[3]),
        .I3(Q[2]),
        .I4(Bus2IP_Addr[1]),
        .O(\spad_reg[31]_i_2_n_0 ));
endmodule

(* ORIG_REF_NAME = "user_axilite_control" *) 
module spec7_user_axilite_control_0_0_user_axilite_control
   (spad_reg,
    spad_val,
    s_axi_awready,
    s_axi_wready,
    s_axi_rdata,
    s_axi_rvalid,
    s_axi_arready,
    s_axi_bvalid,
    s_axi_arvalid,
    s_axi_awvalid,
    s_axi_aclk,
    s_axi_wdata,
    s_axi_areset_n,
    s_axi_araddr,
    s_axi_awaddr,
    s_axi_wvalid,
    s_axi_bready,
    s_axi_rready);
  output [31:0]spad_reg;
  output [31:0]spad_val;
  output s_axi_awready;
  output s_axi_wready;
  output [31:0]s_axi_rdata;
  output s_axi_rvalid;
  output s_axi_arready;
  output s_axi_bvalid;
  input s_axi_arvalid;
  input s_axi_awvalid;
  input s_axi_aclk;
  input [31:0]s_axi_wdata;
  input s_axi_areset_n;
  input [11:0]s_axi_araddr;
  input [11:0]s_axi_awaddr;
  input s_axi_wvalid;
  input s_axi_bready;
  input s_axi_rready;

  wire [5:2]Bus2IP_Addr;
  wire [31:0]IP2Bus_Data;
  wire IP2Bus_Data10_out;
  wire IP2Bus_RdAck;
  wire IP2Bus_WrAck;
  wire axi_lite_ipif_inst_n_10;
  wire axi_lite_ipif_inst_n_11;
  wire axi_lite_ipif_inst_n_12;
  wire axi_lite_ipif_inst_n_14;
  wire axi_lite_ipif_inst_n_15;
  wire axi_lite_ipif_inst_n_16;
  wire axi_lite_ipif_inst_n_17;
  wire axi_lite_ipif_inst_n_18;
  wire axi_lite_ipif_inst_n_19;
  wire axi_lite_ipif_inst_n_20;
  wire axi_lite_ipif_inst_n_21;
  wire axi_lite_ipif_inst_n_22;
  wire axi_lite_ipif_inst_n_23;
  wire axi_lite_ipif_inst_n_24;
  wire axi_lite_ipif_inst_n_25;
  wire axi_lite_ipif_inst_n_26;
  wire axi_lite_ipif_inst_n_27;
  wire axi_lite_ipif_inst_n_28;
  wire axi_lite_ipif_inst_n_29;
  wire axi_lite_ipif_inst_n_30;
  wire axi_lite_ipif_inst_n_31;
  wire axi_lite_ipif_inst_n_32;
  wire axi_lite_ipif_inst_n_33;
  wire axi_lite_ipif_inst_n_34;
  wire axi_lite_ipif_inst_n_35;
  wire axi_lite_ipif_inst_n_36;
  wire axi_lite_ipif_inst_n_37;
  wire axi_lite_ipif_inst_n_38;
  wire axi_lite_ipif_inst_n_39;
  wire axi_lite_ipif_inst_n_40;
  wire axi_lite_ipif_inst_n_41;
  wire axi_lite_ipif_inst_n_42;
  wire axi_lite_ipif_inst_n_8;
  wire [4:0]clk_period_reg;
  wire reg_common_i_n_1;
  wire reg_common_i_n_72;
  wire reg_common_i_n_73;
  wire reg_common_i_n_74;
  wire reg_common_i_n_75;
  wire reg_common_i_n_76;
  wire reg_common_i_n_77;
  wire reg_common_i_n_78;
  wire reg_common_i_n_79;
  wire reg_common_i_n_80;
  wire reg_common_i_n_81;
  wire reg_common_i_n_82;
  wire reg_common_i_n_83;
  wire reg_common_i_n_84;
  wire reg_common_i_n_85;
  wire reg_common_i_n_86;
  wire reg_common_i_n_87;
  wire reg_common_i_n_88;
  wire reg_common_i_n_89;
  wire reg_common_i_n_90;
  wire reg_common_i_n_91;
  wire reg_common_i_n_92;
  wire reg_common_i_n_93;
  wire reg_common_i_n_94;
  wire reg_common_i_n_95;
  wire s_axi_aclk;
  wire [11:0]s_axi_araddr;
  wire s_axi_areset_n;
  wire s_axi_arready;
  wire s_axi_arvalid;
  wire [11:0]s_axi_awaddr;
  wire s_axi_awready;
  wire s_axi_awvalid;
  wire s_axi_bready;
  wire s_axi_bvalid;
  wire [31:0]s_axi_rdata;
  wire s_axi_rready;
  wire s_axi_rvalid;
  wire [31:0]s_axi_wdata;
  wire s_axi_wready;
  wire s_axi_wvalid;
  wire [4:1]scratchpad_reg;
  wire scratchpad_reg_0;
  wire [31:0]spad_reg;
  wire [31:0]spad_val;

  spec7_user_axilite_control_0_0_axi_lite_ipif axi_lite_ipif_inst
       (.D({axi_lite_ipif_inst_n_14,axi_lite_ipif_inst_n_15,axi_lite_ipif_inst_n_16,axi_lite_ipif_inst_n_17,axi_lite_ipif_inst_n_18,axi_lite_ipif_inst_n_19,axi_lite_ipif_inst_n_20,axi_lite_ipif_inst_n_21,axi_lite_ipif_inst_n_22,axi_lite_ipif_inst_n_23,axi_lite_ipif_inst_n_24,axi_lite_ipif_inst_n_25,axi_lite_ipif_inst_n_26,axi_lite_ipif_inst_n_27,axi_lite_ipif_inst_n_28,axi_lite_ipif_inst_n_29,axi_lite_ipif_inst_n_30,axi_lite_ipif_inst_n_31,axi_lite_ipif_inst_n_32,axi_lite_ipif_inst_n_33,axi_lite_ipif_inst_n_34,axi_lite_ipif_inst_n_35,axi_lite_ipif_inst_n_36,axi_lite_ipif_inst_n_37,axi_lite_ipif_inst_n_38}),
        .E(axi_lite_ipif_inst_n_8),
        .IP2Bus_Data10_out(IP2Bus_Data10_out),
        .\IP2Bus_Data_reg[0] (reg_common_i_n_95),
        .\IP2Bus_Data_reg[10] (reg_common_i_n_86),
        .\IP2Bus_Data_reg[11] (reg_common_i_n_85),
        .\IP2Bus_Data_reg[12] (reg_common_i_n_84),
        .\IP2Bus_Data_reg[13] (reg_common_i_n_83),
        .\IP2Bus_Data_reg[14] (reg_common_i_n_82),
        .\IP2Bus_Data_reg[15] (reg_common_i_n_81),
        .\IP2Bus_Data_reg[16] (reg_common_i_n_80),
        .\IP2Bus_Data_reg[17] (reg_common_i_n_79),
        .\IP2Bus_Data_reg[18] (reg_common_i_n_78),
        .\IP2Bus_Data_reg[19] (reg_common_i_n_77),
        .\IP2Bus_Data_reg[1] (reg_common_i_n_94),
        .\IP2Bus_Data_reg[20] (reg_common_i_n_76),
        .\IP2Bus_Data_reg[21] (reg_common_i_n_75),
        .\IP2Bus_Data_reg[22] (reg_common_i_n_74),
        .\IP2Bus_Data_reg[23] (reg_common_i_n_73),
        .\IP2Bus_Data_reg[2] (reg_common_i_n_93),
        .\IP2Bus_Data_reg[30] (reg_common_i_n_72),
        .\IP2Bus_Data_reg[3] (reg_common_i_n_92),
        .\IP2Bus_Data_reg[4] ({scratchpad_reg[4],scratchpad_reg[1]}),
        .\IP2Bus_Data_reg[4]_0 ({clk_period_reg[4],clk_period_reg[1:0]}),
        .\IP2Bus_Data_reg[5] (reg_common_i_n_91),
        .\IP2Bus_Data_reg[6] (reg_common_i_n_90),
        .\IP2Bus_Data_reg[7] (reg_common_i_n_89),
        .\IP2Bus_Data_reg[8] (reg_common_i_n_88),
        .\IP2Bus_Data_reg[9] (reg_common_i_n_87),
        .IP2Bus_RdAck(IP2Bus_RdAck),
        .IP2Bus_WrAck(IP2Bus_WrAck),
        .\MEM_DECODE_GEN[0].cs_out_i_reg[0] (axi_lite_ipif_inst_n_12),
        .Q(Bus2IP_Addr),
        .SR(reg_common_i_n_1),
        .\bus2ip_addr_reg_reg[0] (axi_lite_ipif_inst_n_11),
        .\bus2ip_addr_reg_reg[10] (axi_lite_ipif_inst_n_39),
        .\bus2ip_addr_reg_reg[2] (scratchpad_reg_0),
        .\bus2ip_addr_reg_reg[3] (axi_lite_ipif_inst_n_10),
        .\bus2ip_addr_reg_reg[3]_0 (axi_lite_ipif_inst_n_40),
        .bus2ip_rnw_reg_reg(axi_lite_ipif_inst_n_42),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_araddr(s_axi_araddr),
        .s_axi_areset_n(s_axi_areset_n),
        .s_axi_areset_n_0(axi_lite_ipif_inst_n_41),
        .s_axi_arready(s_axi_arready),
        .s_axi_arvalid(s_axi_arvalid),
        .s_axi_awaddr(s_axi_awaddr),
        .s_axi_awready(s_axi_awready),
        .s_axi_awvalid(s_axi_awvalid),
        .s_axi_bready(s_axi_bready),
        .s_axi_bvalid(s_axi_bvalid),
        .s_axi_rdata(s_axi_rdata),
        .\s_axi_rdata_reg_reg[31] (IP2Bus_Data),
        .s_axi_rready(s_axi_rready),
        .s_axi_rvalid(s_axi_rvalid),
        .s_axi_wready(s_axi_wready),
        .s_axi_wvalid(s_axi_wvalid),
        .spad_reg({spad_reg[4],spad_reg[0]}),
        .spad_val({spad_val[4],spad_val[1:0]}));
  spec7_user_axilite_control_0_0_registers_common reg_common_i
       (.D({axi_lite_ipif_inst_n_14,axi_lite_ipif_inst_n_15,axi_lite_ipif_inst_n_16,axi_lite_ipif_inst_n_17,axi_lite_ipif_inst_n_18,axi_lite_ipif_inst_n_19,axi_lite_ipif_inst_n_20,axi_lite_ipif_inst_n_21,axi_lite_ipif_inst_n_22,axi_lite_ipif_inst_n_23,axi_lite_ipif_inst_n_24,axi_lite_ipif_inst_n_25,axi_lite_ipif_inst_n_26,axi_lite_ipif_inst_n_27,axi_lite_ipif_inst_n_28,axi_lite_ipif_inst_n_29,axi_lite_ipif_inst_n_30,axi_lite_ipif_inst_n_31,axi_lite_ipif_inst_n_32,axi_lite_ipif_inst_n_33,axi_lite_ipif_inst_n_34,axi_lite_ipif_inst_n_35,axi_lite_ipif_inst_n_36,axi_lite_ipif_inst_n_37,axi_lite_ipif_inst_n_38}),
        .E(scratchpad_reg_0),
        .IP2Bus_Data10_out(IP2Bus_Data10_out),
        .\IP2Bus_Data_reg[1]_0 (Bus2IP_Addr),
        .\IP2Bus_Data_reg[24]_0 (axi_lite_ipif_inst_n_39),
        .\IP2Bus_Data_reg[31]_0 (IP2Bus_Data),
        .\IP2Bus_Data_reg[31]_1 (axi_lite_ipif_inst_n_41),
        .\IP2Bus_Data_reg[31]_2 (axi_lite_ipif_inst_n_40),
        .IP2Bus_RdAck(IP2Bus_RdAck),
        .IP2Bus_RdAck_reg_0(axi_lite_ipif_inst_n_42),
        .IP2Bus_WrAck(IP2Bus_WrAck),
        .Q({clk_period_reg[4],clk_period_reg[1:0]}),
        .SR(reg_common_i_n_1),
        .\clk_period_reg_reg[10]_0 (reg_common_i_n_86),
        .\clk_period_reg_reg[11]_0 (reg_common_i_n_85),
        .\clk_period_reg_reg[12]_0 (reg_common_i_n_84),
        .\clk_period_reg_reg[13]_0 (reg_common_i_n_83),
        .\clk_period_reg_reg[14]_0 (reg_common_i_n_82),
        .\clk_period_reg_reg[15]_0 (reg_common_i_n_81),
        .\clk_period_reg_reg[16]_0 (reg_common_i_n_80),
        .\clk_period_reg_reg[17]_0 (reg_common_i_n_79),
        .\clk_period_reg_reg[18]_0 (reg_common_i_n_78),
        .\clk_period_reg_reg[19]_0 (reg_common_i_n_77),
        .\clk_period_reg_reg[20]_0 (reg_common_i_n_76),
        .\clk_period_reg_reg[21]_0 (reg_common_i_n_75),
        .\clk_period_reg_reg[22]_0 (reg_common_i_n_74),
        .\clk_period_reg_reg[23]_0 (reg_common_i_n_73),
        .\clk_period_reg_reg[2]_0 (reg_common_i_n_93),
        .\clk_period_reg_reg[30]_0 (reg_common_i_n_72),
        .\clk_period_reg_reg[31]_0 (axi_lite_ipif_inst_n_8),
        .\clk_period_reg_reg[3]_0 (reg_common_i_n_92),
        .\clk_period_reg_reg[5]_0 (reg_common_i_n_91),
        .\clk_period_reg_reg[6]_0 (reg_common_i_n_90),
        .\clk_period_reg_reg[7]_0 (reg_common_i_n_89),
        .\clk_period_reg_reg[8]_0 (reg_common_i_n_88),
        .\clk_period_reg_reg[9]_0 (reg_common_i_n_87),
        .s_axi_aclk(s_axi_aclk),
        .s_axi_areset_n(s_axi_areset_n),
        .s_axi_wdata(s_axi_wdata),
        .\scaling_factor_reg[1]_0 (axi_lite_ipif_inst_n_12),
        .\scratchpad_reg_reg[0]_0 (reg_common_i_n_95),
        .\scratchpad_reg_reg[4]_0 ({scratchpad_reg[4],scratchpad_reg[1]}),
        .spad_reg(spad_reg),
        .\spad_reg_reg[1]_0 (reg_common_i_n_94),
        .\spad_reg_reg[31]_0 (axi_lite_ipif_inst_n_11),
        .spad_val(spad_val),
        .\spad_val_reg[31]_0 (axi_lite_ipif_inst_n_10));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
