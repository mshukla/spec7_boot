-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.2 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
-- Date        : Sat Oct  3 15:01:20 2020
-- Host        : pcbe16482.cern.ch running 64-bit CentOS Linux release 7.8.2003 (Core)
-- Command     : write_vhdl -force -mode funcsim
--               /home/spec7_trial/spec7_trial.srcs/sources_1/bd/spec7/ip/spec7_user_axilite_control_0_0/spec7_user_axilite_control_0_0_sim_netlist.vhdl
-- Design      : spec7_user_axilite_control_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z030fbg676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_user_axilite_control_0_0_address_decoder is
  port (
    \FSM_sequential_access_cs_reg[0]\ : out STD_LOGIC;
    cs_ce_ld_enable_i : out STD_LOGIC;
    axi_avalid : out STD_LOGIC;
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \bus2ip_addr_reg_reg[2]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \bus2ip_addr_reg_reg[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \bus2ip_addr_reg_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \MEM_DECODE_GEN[0].cs_out_i_reg[0]_0\ : out STD_LOGIC;
    IP2Bus_Data10_out : out STD_LOGIC;
    s_axi_areset_n_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    bus2ip_rnw_reg_reg : out STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    access_cs : in STD_LOGIC_VECTOR ( 2 downto 0 );
    data_timeout : in STD_LOGIC;
    IP2Bus_WrAck : in STD_LOGIC;
    s_axi_areset_n : in STD_LOGIC;
    s_axi_wvalid : in STD_LOGIC;
    axi_avalid_reg : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    IP2Bus_RdAck : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 6 downto 0 );
    \scratchpad_reg_reg[31]\ : in STD_LOGIC;
    Bus2IP_RNW : in STD_LOGIC;
    \scratchpad_reg_reg[31]_0\ : in STD_LOGIC;
    \spad_val_reg[31]\ : in STD_LOGIC;
    \scaling_factor_reg[1]\ : in STD_LOGIC;
    \scaling_factor_reg[1]_0\ : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_user_axilite_control_0_0_address_decoder : entity is "address_decoder";
end spec7_user_axilite_control_0_0_address_decoder;

architecture STRUCTURE of spec7_user_axilite_control_0_0_address_decoder is
  signal Bus2IP_CS : STD_LOGIC;
  signal \^fsm_sequential_access_cs_reg[0]\ : STD_LOGIC;
  signal \MEM_DECODE_GEN[0].cs_out_i[0]_i_1_n_0\ : STD_LOGIC;
  signal \MEM_DECODE_GEN[0].cs_out_i[0]_i_2_n_0\ : STD_LOGIC;
  signal \^axi_avalid\ : STD_LOGIC;
  signal axi_avalid_reg_i_2_n_0 : STD_LOGIC;
  signal \^cs_ce_ld_enable_i\ : STD_LOGIC;
  signal \scratchpad_reg[31]_i_2_n_0\ : STD_LOGIC;
  signal \spad_reg[31]_i_3_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \IP2Bus_Data[31]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of IP2Bus_RdAck_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of IP2Bus_WrAck_i_1 : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \clk_period_reg[31]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \scratchpad_reg[31]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \spad_reg[31]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \spad_reg[31]_i_3\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \spad_val[31]_i_1\ : label is "soft_lutpair0";
begin
  \FSM_sequential_access_cs_reg[0]\ <= \^fsm_sequential_access_cs_reg[0]\;
  axi_avalid <= \^axi_avalid\;
  cs_ce_ld_enable_i <= \^cs_ce_ld_enable_i\;
\IP2Bus_Data[31]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => s_axi_areset_n,
      I1 => Bus2IP_CS,
      I2 => Bus2IP_RNW,
      O => s_axi_areset_n_0(0)
    );
IP2Bus_RdAck_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => Bus2IP_RNW,
      I1 => Bus2IP_CS,
      I2 => s_axi_areset_n,
      O => bus2ip_rnw_reg_reg
    );
IP2Bus_WrAck_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => Bus2IP_CS,
      I1 => Bus2IP_RNW,
      O => IP2Bus_Data10_out
    );
\MEM_DECODE_GEN[0].cs_out_i[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000044400040"
    )
        port map (
      I0 => \^fsm_sequential_access_cs_reg[0]\,
      I1 => s_axi_areset_n,
      I2 => Bus2IP_CS,
      I3 => \^cs_ce_ld_enable_i\,
      I4 => \^axi_avalid\,
      I5 => \MEM_DECODE_GEN[0].cs_out_i[0]_i_2_n_0\,
      O => \MEM_DECODE_GEN[0].cs_out_i[0]_i_1_n_0\
    );
\MEM_DECODE_GEN[0].cs_out_i[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"000000E0"
    )
        port map (
      I0 => data_timeout,
      I1 => IP2Bus_RdAck,
      I2 => access_cs(0),
      I3 => access_cs(2),
      I4 => access_cs(1),
      O => \MEM_DECODE_GEN[0].cs_out_i[0]_i_2_n_0\
    );
\MEM_DECODE_GEN[0].cs_out_i_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \MEM_DECODE_GEN[0].cs_out_i[0]_i_1_n_0\,
      Q => Bus2IP_CS,
      R => '0'
    );
axi_avalid_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0C0CC3C30000C202"
    )
        port map (
      I0 => axi_avalid_reg_i_2_n_0,
      I1 => access_cs(0),
      I2 => access_cs(1),
      I3 => s_axi_wvalid,
      I4 => access_cs(2),
      I5 => axi_avalid_reg,
      O => \^axi_avalid\
    );
axi_avalid_reg_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => s_axi_awvalid,
      I1 => s_axi_arvalid,
      O => axi_avalid_reg_i_2_n_0
    );
\clk_period_reg[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => \scratchpad_reg[31]_i_2_n_0\,
      I1 => Q(0),
      I2 => Q(2),
      I3 => Q(3),
      O => E(0)
    );
\icount_out[6]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00C20002"
    )
        port map (
      I0 => s_axi_arvalid,
      I1 => access_cs(1),
      I2 => access_cs(0),
      I3 => access_cs(2),
      I4 => s_axi_wvalid,
      O => \^cs_ce_ld_enable_i\
    );
s_axi_wready_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00005400"
    )
        port map (
      I0 => access_cs(0),
      I1 => data_timeout,
      I2 => IP2Bus_WrAck,
      I3 => access_cs(2),
      I4 => access_cs(1),
      O => \^fsm_sequential_access_cs_reg[0]\
    );
\scaling_factor[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000020"
    )
        port map (
      I0 => \scaling_factor_reg[1]\,
      I1 => \scaling_factor_reg[1]_0\,
      I2 => Bus2IP_CS,
      I3 => Bus2IP_RNW,
      I4 => Q(6),
      I5 => Q(5),
      O => \MEM_DECODE_GEN[0].cs_out_i_reg[0]_0\
    );
\scratchpad_reg[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0002"
    )
        port map (
      I0 => \scratchpad_reg[31]_i_2_n_0\,
      I1 => Q(2),
      I2 => Q(3),
      I3 => Q(0),
      O => \bus2ip_addr_reg_reg[2]\(0)
    );
\scratchpad_reg[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000800"
    )
        port map (
      I0 => \scratchpad_reg_reg[31]\,
      I1 => Bus2IP_CS,
      I2 => Bus2IP_RNW,
      I3 => Q(4),
      I4 => Q(1),
      I5 => \scratchpad_reg_reg[31]_0\,
      O => \scratchpad_reg[31]_i_2_n_0\
    );
\spad_reg[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000002"
    )
        port map (
      I0 => \spad_val_reg[31]\,
      I1 => Q(0),
      I2 => Q(3),
      I3 => Q(2),
      I4 => \spad_reg[31]_i_3_n_0\,
      O => \bus2ip_addr_reg_reg[0]\(0)
    );
\spad_reg[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => Bus2IP_CS,
      I1 => Bus2IP_RNW,
      I2 => Q(6),
      I3 => Q(5),
      O => \spad_reg[31]_i_3_n_0\
    );
\spad_val[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000020"
    )
        port map (
      I0 => \spad_val_reg[31]\,
      I1 => Q(3),
      I2 => Q(2),
      I3 => Q(0),
      I4 => \spad_reg[31]_i_3_n_0\,
      O => \bus2ip_addr_reg_reg[3]\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_user_axilite_control_0_0_counter_f is
  port (
    counter_en_reg_reg : out STD_LOGIC;
    \DATA_PHASE_WDT.timeout_i\ : out STD_LOGIC;
    counter_en_reg : in STD_LOGIC;
    access_cs : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    IP2Bus_RdAck : in STD_LOGIC;
    IP2Bus_WrAck : in STD_LOGIC;
    data_timeout : in STD_LOGIC;
    cs_ce_ld_enable_i : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_user_axilite_control_0_0_counter_f : entity is "counter_f";
end spec7_user_axilite_control_0_0_counter_f;

architecture STRUCTURE of spec7_user_axilite_control_0_0_counter_f is
  signal \^data_phase_wdt.timeout_i\ : STD_LOGIC;
  signal \^counter_en_reg_reg\ : STD_LOGIC;
  signal \icount_out[0]_i_1_n_0\ : STD_LOGIC;
  signal \icount_out[1]_i_1_n_0\ : STD_LOGIC;
  signal \icount_out[2]_i_1_n_0\ : STD_LOGIC;
  signal \icount_out[3]_i_1_n_0\ : STD_LOGIC;
  signal \icount_out[4]_i_1_n_0\ : STD_LOGIC;
  signal \icount_out[4]_i_3_n_0\ : STD_LOGIC;
  signal \icount_out[4]_i_4_n_0\ : STD_LOGIC;
  signal \icount_out[4]_i_5_n_0\ : STD_LOGIC;
  signal \icount_out[4]_i_6_n_0\ : STD_LOGIC;
  signal \icount_out[4]_i_7_n_0\ : STD_LOGIC;
  signal \icount_out[5]_i_2_n_0\ : STD_LOGIC;
  signal \icount_out[5]_i_3_n_0\ : STD_LOGIC;
  signal \icount_out[5]_i_4_n_0\ : STD_LOGIC;
  signal \icount_out[6]_i_1_n_0\ : STD_LOGIC;
  signal \icount_out[6]_i_4_n_0\ : STD_LOGIC;
  signal \icount_out[6]_i_5_n_0\ : STD_LOGIC;
  signal \icount_out_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \icount_out_reg[4]_i_2_n_1\ : STD_LOGIC;
  signal \icount_out_reg[4]_i_2_n_2\ : STD_LOGIC;
  signal \icount_out_reg[4]_i_2_n_3\ : STD_LOGIC;
  signal \icount_out_reg[4]_i_2_n_4\ : STD_LOGIC;
  signal \icount_out_reg[4]_i_2_n_5\ : STD_LOGIC;
  signal \icount_out_reg[4]_i_2_n_6\ : STD_LOGIC;
  signal \icount_out_reg[4]_i_2_n_7\ : STD_LOGIC;
  signal \icount_out_reg[6]_i_2_n_3\ : STD_LOGIC;
  signal \icount_out_reg[6]_i_2_n_6\ : STD_LOGIC;
  signal \icount_out_reg[6]_i_2_n_7\ : STD_LOGIC;
  signal \icount_out_reg_n_0_[0]\ : STD_LOGIC;
  signal \icount_out_reg_n_0_[1]\ : STD_LOGIC;
  signal \icount_out_reg_n_0_[2]\ : STD_LOGIC;
  signal \icount_out_reg_n_0_[3]\ : STD_LOGIC;
  signal \icount_out_reg_n_0_[4]\ : STD_LOGIC;
  signal \icount_out_reg_n_0_[5]\ : STD_LOGIC;
  signal \NLW_icount_out_reg[6]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 1 );
  signal \NLW_icount_out_reg[6]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  attribute METHODOLOGY_DRC_VIOS : string;
  attribute METHODOLOGY_DRC_VIOS of \icount_out_reg[4]_i_2\ : label is "{SYNTH-8 {cell *THIS*}}";
  attribute METHODOLOGY_DRC_VIOS of \icount_out_reg[6]_i_2\ : label is "{SYNTH-8 {cell *THIS*}}";
begin
  \DATA_PHASE_WDT.timeout_i\ <= \^data_phase_wdt.timeout_i\;
  counter_en_reg_reg <= \^counter_en_reg_reg\;
\icount_out[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"20032000FFFFFFFF"
    )
        port map (
      I0 => s_axi_wvalid,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => s_axi_arvalid,
      I5 => \icount_out_reg_n_0_[0]\,
      O => \icount_out[0]_i_1_n_0\
    );
\icount_out[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAFAEAAAAAA"
    )
        port map (
      I0 => \icount_out_reg[4]_i_2_n_7\,
      I1 => s_axi_wvalid,
      I2 => access_cs(2),
      I3 => access_cs(0),
      I4 => access_cs(1),
      I5 => s_axi_arvalid,
      O => \icount_out[1]_i_1_n_0\
    );
\icount_out[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAFAEAAAAAA"
    )
        port map (
      I0 => \icount_out_reg[4]_i_2_n_6\,
      I1 => s_axi_wvalid,
      I2 => access_cs(2),
      I3 => access_cs(0),
      I4 => access_cs(1),
      I5 => s_axi_arvalid,
      O => \icount_out[2]_i_1_n_0\
    );
\icount_out[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAFAEAAAAAA"
    )
        port map (
      I0 => \icount_out_reg[4]_i_2_n_5\,
      I1 => s_axi_wvalid,
      I2 => access_cs(2),
      I3 => access_cs(0),
      I4 => access_cs(1),
      I5 => s_axi_arvalid,
      O => \icount_out[3]_i_1_n_0\
    );
\icount_out[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AEAAAAAFAEAAAAAA"
    )
        port map (
      I0 => \icount_out_reg[4]_i_2_n_4\,
      I1 => s_axi_wvalid,
      I2 => access_cs(2),
      I3 => access_cs(0),
      I4 => access_cs(1),
      I5 => s_axi_arvalid,
      O => \icount_out[4]_i_1_n_0\
    );
\icount_out[4]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \icount_out_reg_n_0_[1]\,
      O => \icount_out[4]_i_3_n_0\
    );
\icount_out[4]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \icount_out_reg_n_0_[3]\,
      I1 => \icount_out_reg_n_0_[4]\,
      O => \icount_out[4]_i_4_n_0\
    );
\icount_out[4]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \icount_out_reg_n_0_[2]\,
      I1 => \icount_out_reg_n_0_[3]\,
      O => \icount_out[4]_i_5_n_0\
    );
\icount_out[4]_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \icount_out_reg_n_0_[1]\,
      I1 => \icount_out_reg_n_0_[2]\,
      O => \icount_out[4]_i_6_n_0\
    );
\icount_out[4]_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \icount_out_reg_n_0_[1]\,
      I1 => \^counter_en_reg_reg\,
      O => \icount_out[4]_i_7_n_0\
    );
\icount_out[5]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"444F"
    )
        port map (
      I0 => \icount_out[5]_i_3_n_0\,
      I1 => counter_en_reg,
      I2 => \icount_out[5]_i_4_n_0\,
      I3 => access_cs(2),
      O => \^counter_en_reg_reg\
    );
\icount_out[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A2AAAAA0A2AAAAAA"
    )
        port map (
      I0 => \icount_out_reg[6]_i_2_n_7\,
      I1 => s_axi_wvalid,
      I2 => access_cs(2),
      I3 => access_cs(0),
      I4 => access_cs(1),
      I5 => s_axi_arvalid,
      O => \icount_out[5]_i_2_n_0\
    );
\icount_out[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CFCFCECEFFFFFFCF"
    )
        port map (
      I0 => IP2Bus_RdAck,
      I1 => access_cs(1),
      I2 => access_cs(2),
      I3 => IP2Bus_WrAck,
      I4 => data_timeout,
      I5 => access_cs(0),
      O => \icount_out[5]_i_3_n_0\
    );
\icount_out[5]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1111FFFFFFFF0F03"
    )
        port map (
      I0 => s_axi_wvalid,
      I1 => counter_en_reg,
      I2 => s_axi_arvalid,
      I3 => s_axi_awvalid,
      I4 => access_cs(0),
      I5 => access_cs(1),
      O => \icount_out[5]_i_4_n_0\
    );
\icount_out[6]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00E2"
    )
        port map (
      I0 => \^data_phase_wdt.timeout_i\,
      I1 => \^counter_en_reg_reg\,
      I2 => \icount_out_reg[6]_i_2_n_6\,
      I3 => cs_ce_ld_enable_i,
      O => \icount_out[6]_i_1_n_0\
    );
\icount_out[6]_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \icount_out_reg_n_0_[5]\,
      O => \icount_out[6]_i_4_n_0\
    );
\icount_out[6]_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"9"
    )
        port map (
      I0 => \icount_out_reg_n_0_[4]\,
      I1 => \icount_out_reg_n_0_[5]\,
      O => \icount_out[6]_i_5_n_0\
    );
\icount_out_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \^counter_en_reg_reg\,
      D => \icount_out[0]_i_1_n_0\,
      Q => \icount_out_reg_n_0_[0]\,
      R => '0'
    );
\icount_out_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \^counter_en_reg_reg\,
      D => \icount_out[1]_i_1_n_0\,
      Q => \icount_out_reg_n_0_[1]\,
      R => '0'
    );
\icount_out_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \^counter_en_reg_reg\,
      D => \icount_out[2]_i_1_n_0\,
      Q => \icount_out_reg_n_0_[2]\,
      R => '0'
    );
\icount_out_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \^counter_en_reg_reg\,
      D => \icount_out[3]_i_1_n_0\,
      Q => \icount_out_reg_n_0_[3]\,
      R => '0'
    );
\icount_out_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \^counter_en_reg_reg\,
      D => \icount_out[4]_i_1_n_0\,
      Q => \icount_out_reg_n_0_[4]\,
      R => '0'
    );
\icount_out_reg[4]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \icount_out_reg[4]_i_2_n_0\,
      CO(2) => \icount_out_reg[4]_i_2_n_1\,
      CO(1) => \icount_out_reg[4]_i_2_n_2\,
      CO(0) => \icount_out_reg[4]_i_2_n_3\,
      CYINIT => \icount_out_reg_n_0_[0]\,
      DI(3) => \icount_out_reg_n_0_[3]\,
      DI(2) => \icount_out_reg_n_0_[2]\,
      DI(1) => \icount_out_reg_n_0_[1]\,
      DI(0) => \icount_out[4]_i_3_n_0\,
      O(3) => \icount_out_reg[4]_i_2_n_4\,
      O(2) => \icount_out_reg[4]_i_2_n_5\,
      O(1) => \icount_out_reg[4]_i_2_n_6\,
      O(0) => \icount_out_reg[4]_i_2_n_7\,
      S(3) => \icount_out[4]_i_4_n_0\,
      S(2) => \icount_out[4]_i_5_n_0\,
      S(1) => \icount_out[4]_i_6_n_0\,
      S(0) => \icount_out[4]_i_7_n_0\
    );
\icount_out_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \^counter_en_reg_reg\,
      D => \icount_out[5]_i_2_n_0\,
      Q => \icount_out_reg_n_0_[5]\,
      R => '0'
    );
\icount_out_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \icount_out[6]_i_1_n_0\,
      Q => \^data_phase_wdt.timeout_i\,
      R => '0'
    );
\icount_out_reg[6]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \icount_out_reg[4]_i_2_n_0\,
      CO(3 downto 1) => \NLW_icount_out_reg[6]_i_2_CO_UNCONNECTED\(3 downto 1),
      CO(0) => \icount_out_reg[6]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \icount_out_reg_n_0_[4]\,
      O(3 downto 2) => \NLW_icount_out_reg[6]_i_2_O_UNCONNECTED\(3 downto 2),
      O(1) => \icount_out_reg[6]_i_2_n_6\,
      O(0) => \icount_out_reg[6]_i_2_n_7\,
      S(3 downto 2) => B"00",
      S(1) => \icount_out[6]_i_4_n_0\,
      S(0) => \icount_out[6]_i_5_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_user_axilite_control_0_0_registers_common is
  port (
    IP2Bus_WrAck : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    IP2Bus_RdAck : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    spad_val : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \scratchpad_reg_reg[4]_0\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    spad_reg : out STD_LOGIC_VECTOR ( 31 downto 0 );
    \clk_period_reg_reg[30]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[23]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[22]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[21]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[20]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[19]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[18]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[17]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[16]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[15]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[14]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[13]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[12]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[11]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[10]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[9]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[8]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[7]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[6]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[5]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[3]_0\ : out STD_LOGIC;
    \clk_period_reg_reg[2]_0\ : out STD_LOGIC;
    \spad_reg_reg[1]_0\ : out STD_LOGIC;
    \scratchpad_reg_reg[0]_0\ : out STD_LOGIC;
    \IP2Bus_Data_reg[31]_0\ : out STD_LOGIC_VECTOR ( 31 downto 0 );
    IP2Bus_Data10_out : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    IP2Bus_RdAck_reg_0 : in STD_LOGIC;
    \IP2Bus_Data_reg[24]_0\ : in STD_LOGIC;
    s_axi_areset_n : in STD_LOGIC;
    \IP2Bus_Data_reg[1]_0\ : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    \scaling_factor_reg[1]_0\ : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \clk_period_reg_reg[31]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \spad_reg_reg[31]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \spad_val_reg[31]_0\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \IP2Bus_Data_reg[31]_1\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \IP2Bus_Data_reg[31]_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    D : in STD_LOGIC_VECTOR ( 24 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_user_axilite_control_0_0_registers_common : entity is "registers_common";
end spec7_user_axilite_control_0_0_registers_common;

architecture STRUCTURE of spec7_user_axilite_control_0_0_registers_common is
  signal \IP2Bus_Data[24]_i_1_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[24]_i_2_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[25]_i_1_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[25]_i_2_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[26]_i_1_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[26]_i_2_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[27]_i_1_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[27]_i_2_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[28]_i_1_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[28]_i_2_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[29]_i_1_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[29]_i_2_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[31]_i_3_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[31]_i_6_n_0\ : STD_LOGIC;
  signal \^sr\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal clk_period_reg : STD_LOGIC_VECTOR ( 31 downto 2 );
  signal scaling_factor : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \scaling_factor[0]_i_1_n_0\ : STD_LOGIC;
  signal \scaling_factor[1]_i_1_n_0\ : STD_LOGIC;
  signal scratchpad_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^spad_reg\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^spad_val\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \IP2Bus_Data[24]_i_1\ : label is "soft_lutpair27";
  attribute SOFT_HLUTNM of \IP2Bus_Data[25]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \IP2Bus_Data[26]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \IP2Bus_Data[28]_i_1\ : label is "soft_lutpair29";
  attribute SOFT_HLUTNM of \IP2Bus_Data[29]_i_1\ : label is "soft_lutpair28";
  attribute SOFT_HLUTNM of \IP2Bus_Data[31]_i_3\ : label is "soft_lutpair27";
begin
  SR(0) <= \^sr\(0);
  spad_reg(31 downto 0) <= \^spad_reg\(31 downto 0);
  spad_val(31 downto 0) <= \^spad_val\(31 downto 0);
\IP2Bus_Data[0]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => scratchpad_reg(0),
      I1 => \IP2Bus_Data_reg[1]_0\(2),
      I2 => scaling_factor(0),
      I3 => \IP2Bus_Data_reg[1]_0\(1),
      O => \scratchpad_reg_reg[0]_0\
    );
\IP2Bus_Data[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(10),
      I1 => \^spad_val\(10),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(10),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(10),
      O => \clk_period_reg_reg[10]_0\
    );
\IP2Bus_Data[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(11),
      I1 => \^spad_val\(11),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(11),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(11),
      O => \clk_period_reg_reg[11]_0\
    );
\IP2Bus_Data[12]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(12),
      I1 => \^spad_val\(12),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(12),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(12),
      O => \clk_period_reg_reg[12]_0\
    );
\IP2Bus_Data[13]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(13),
      I1 => \^spad_val\(13),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(13),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(13),
      O => \clk_period_reg_reg[13]_0\
    );
\IP2Bus_Data[14]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(14),
      I1 => \^spad_val\(14),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(14),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(14),
      O => \clk_period_reg_reg[14]_0\
    );
\IP2Bus_Data[15]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(15),
      I1 => \^spad_val\(15),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(15),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(15),
      O => \clk_period_reg_reg[15]_0\
    );
\IP2Bus_Data[16]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(16),
      I1 => \^spad_val\(16),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(16),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(16),
      O => \clk_period_reg_reg[16]_0\
    );
\IP2Bus_Data[17]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(17),
      I1 => \^spad_val\(17),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(17),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(17),
      O => \clk_period_reg_reg[17]_0\
    );
\IP2Bus_Data[18]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(18),
      I1 => \^spad_val\(18),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(18),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(18),
      O => \clk_period_reg_reg[18]_0\
    );
\IP2Bus_Data[19]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(19),
      I1 => \^spad_val\(19),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(19),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(19),
      O => \clk_period_reg_reg[19]_0\
    );
\IP2Bus_Data[1]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"F888"
    )
        port map (
      I0 => \^spad_reg\(1),
      I1 => \IP2Bus_Data_reg[1]_0\(3),
      I2 => scaling_factor(1),
      I3 => \IP2Bus_Data_reg[1]_0\(1),
      O => \spad_reg_reg[1]_0\
    );
\IP2Bus_Data[20]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(20),
      I1 => \^spad_val\(20),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(20),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(20),
      O => \clk_period_reg_reg[20]_0\
    );
\IP2Bus_Data[21]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(21),
      I1 => \^spad_val\(21),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(21),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(21),
      O => \clk_period_reg_reg[21]_0\
    );
\IP2Bus_Data[22]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(22),
      I1 => \^spad_val\(22),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(22),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(22),
      O => \clk_period_reg_reg[22]_0\
    );
\IP2Bus_Data[23]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(23),
      I1 => \^spad_val\(23),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(23),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(23),
      O => \clk_period_reg_reg[23]_0\
    );
\IP2Bus_Data[24]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \IP2Bus_Data[24]_i_2_n_0\,
      I1 => \IP2Bus_Data_reg[24]_0\,
      O => \IP2Bus_Data[24]_i_1_n_0\
    );
\IP2Bus_Data[24]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(24),
      I1 => \^spad_val\(24),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(24),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(24),
      O => \IP2Bus_Data[24]_i_2_n_0\
    );
\IP2Bus_Data[25]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \IP2Bus_Data[25]_i_2_n_0\,
      I1 => \IP2Bus_Data_reg[24]_0\,
      O => \IP2Bus_Data[25]_i_1_n_0\
    );
\IP2Bus_Data[25]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(25),
      I1 => \^spad_val\(25),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(25),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(25),
      O => \IP2Bus_Data[25]_i_2_n_0\
    );
\IP2Bus_Data[26]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \IP2Bus_Data[26]_i_2_n_0\,
      I1 => \IP2Bus_Data_reg[24]_0\,
      O => \IP2Bus_Data[26]_i_1_n_0\
    );
\IP2Bus_Data[26]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(26),
      I1 => \^spad_val\(26),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(26),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(26),
      O => \IP2Bus_Data[26]_i_2_n_0\
    );
\IP2Bus_Data[27]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \IP2Bus_Data[27]_i_2_n_0\,
      I1 => \IP2Bus_Data_reg[24]_0\,
      O => \IP2Bus_Data[27]_i_1_n_0\
    );
\IP2Bus_Data[27]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(27),
      I1 => \^spad_val\(27),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(27),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(27),
      O => \IP2Bus_Data[27]_i_2_n_0\
    );
\IP2Bus_Data[28]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \IP2Bus_Data[28]_i_2_n_0\,
      I1 => \IP2Bus_Data_reg[24]_0\,
      O => \IP2Bus_Data[28]_i_1_n_0\
    );
\IP2Bus_Data[28]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(28),
      I1 => \^spad_val\(28),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(28),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(28),
      O => \IP2Bus_Data[28]_i_2_n_0\
    );
\IP2Bus_Data[29]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \IP2Bus_Data[29]_i_2_n_0\,
      I1 => \IP2Bus_Data_reg[24]_0\,
      O => \IP2Bus_Data[29]_i_1_n_0\
    );
\IP2Bus_Data[29]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(29),
      I1 => \^spad_val\(29),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(29),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(29),
      O => \IP2Bus_Data[29]_i_2_n_0\
    );
\IP2Bus_Data[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(2),
      I1 => \^spad_val\(2),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(2),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(2),
      O => \clk_period_reg_reg[2]_0\
    );
\IP2Bus_Data[30]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(30),
      I1 => \^spad_val\(30),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(30),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(30),
      O => \clk_period_reg_reg[30]_0\
    );
\IP2Bus_Data[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \IP2Bus_Data[31]_i_6_n_0\,
      I1 => \IP2Bus_Data_reg[24]_0\,
      O => \IP2Bus_Data[31]_i_3_n_0\
    );
\IP2Bus_Data[31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(31),
      I1 => \^spad_val\(31),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(31),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(31),
      O => \IP2Bus_Data[31]_i_6_n_0\
    );
\IP2Bus_Data[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(3),
      I1 => \^spad_val\(3),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(3),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(3),
      O => \clk_period_reg_reg[3]_0\
    );
\IP2Bus_Data[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(5),
      I1 => \^spad_val\(5),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(5),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(5),
      O => \clk_period_reg_reg[5]_0\
    );
\IP2Bus_Data[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(6),
      I1 => \^spad_val\(6),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(6),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(6),
      O => \clk_period_reg_reg[6]_0\
    );
\IP2Bus_Data[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(7),
      I1 => \^spad_val\(7),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(7),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(7),
      O => \clk_period_reg_reg[7]_0\
    );
\IP2Bus_Data[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(8),
      I1 => \^spad_val\(8),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(8),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(8),
      O => \clk_period_reg_reg[8]_0\
    );
\IP2Bus_Data[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFA0CFCFAFA0C0C0"
    )
        port map (
      I0 => clk_period_reg(9),
      I1 => \^spad_val\(9),
      I2 => \IP2Bus_Data_reg[1]_0\(0),
      I3 => scratchpad_reg(9),
      I4 => \IP2Bus_Data_reg[1]_0\(2),
      I5 => \^spad_reg\(9),
      O => \clk_period_reg_reg[9]_0\
    );
\IP2Bus_Data_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(0),
      Q => \IP2Bus_Data_reg[31]_0\(0),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(10),
      Q => \IP2Bus_Data_reg[31]_0\(10),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(11),
      Q => \IP2Bus_Data_reg[31]_0\(11),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(12),
      Q => \IP2Bus_Data_reg[31]_0\(12),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(13),
      Q => \IP2Bus_Data_reg[31]_0\(13),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(14),
      Q => \IP2Bus_Data_reg[31]_0\(14),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(15),
      Q => \IP2Bus_Data_reg[31]_0\(15),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(16),
      Q => \IP2Bus_Data_reg[31]_0\(16),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(17),
      Q => \IP2Bus_Data_reg[31]_0\(17),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(18),
      Q => \IP2Bus_Data_reg[31]_0\(18),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(19),
      Q => \IP2Bus_Data_reg[31]_0\(19),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(1),
      Q => \IP2Bus_Data_reg[31]_0\(1),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(20),
      Q => \IP2Bus_Data_reg[31]_0\(20),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(21),
      Q => \IP2Bus_Data_reg[31]_0\(21),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(22),
      Q => \IP2Bus_Data_reg[31]_0\(22),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(23),
      Q => \IP2Bus_Data_reg[31]_0\(23),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => \IP2Bus_Data[24]_i_1_n_0\,
      Q => \IP2Bus_Data_reg[31]_0\(24),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => \IP2Bus_Data[25]_i_1_n_0\,
      Q => \IP2Bus_Data_reg[31]_0\(25),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => \IP2Bus_Data[26]_i_1_n_0\,
      Q => \IP2Bus_Data_reg[31]_0\(26),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => \IP2Bus_Data[27]_i_1_n_0\,
      Q => \IP2Bus_Data_reg[31]_0\(27),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => \IP2Bus_Data[28]_i_1_n_0\,
      Q => \IP2Bus_Data_reg[31]_0\(28),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => \IP2Bus_Data[29]_i_1_n_0\,
      Q => \IP2Bus_Data_reg[31]_0\(29),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(2),
      Q => \IP2Bus_Data_reg[31]_0\(2),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(24),
      Q => \IP2Bus_Data_reg[31]_0\(30),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => \IP2Bus_Data[31]_i_3_n_0\,
      Q => \IP2Bus_Data_reg[31]_0\(31),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(3),
      Q => \IP2Bus_Data_reg[31]_0\(3),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(4),
      Q => \IP2Bus_Data_reg[31]_0\(4),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(5),
      Q => \IP2Bus_Data_reg[31]_0\(5),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(6),
      Q => \IP2Bus_Data_reg[31]_0\(6),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(7),
      Q => \IP2Bus_Data_reg[31]_0\(7),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(8),
      Q => \IP2Bus_Data_reg[31]_0\(8),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
\IP2Bus_Data_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \IP2Bus_Data_reg[31]_2\(0),
      D => D(9),
      Q => \IP2Bus_Data_reg[31]_0\(9),
      R => \IP2Bus_Data_reg[31]_1\(0)
    );
IP2Bus_RdAck_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => IP2Bus_RdAck_reg_0,
      Q => IP2Bus_RdAck,
      R => '0'
    );
IP2Bus_WrAck_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => IP2Bus_Data10_out,
      Q => IP2Bus_WrAck,
      R => \^sr\(0)
    );
\clk_period_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(0),
      Q => Q(0),
      R => \^sr\(0)
    );
\clk_period_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(10),
      Q => clk_period_reg(10),
      R => \^sr\(0)
    );
\clk_period_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(11),
      Q => clk_period_reg(11),
      R => \^sr\(0)
    );
\clk_period_reg_reg[12]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(12),
      Q => clk_period_reg(12),
      S => \^sr\(0)
    );
\clk_period_reg_reg[13]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(13),
      Q => clk_period_reg(13),
      S => \^sr\(0)
    );
\clk_period_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(14),
      Q => clk_period_reg(14),
      R => \^sr\(0)
    );
\clk_period_reg_reg[15]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(15),
      Q => clk_period_reg(15),
      S => \^sr\(0)
    );
\clk_period_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(16),
      Q => clk_period_reg(16),
      R => \^sr\(0)
    );
\clk_period_reg_reg[17]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(17),
      Q => clk_period_reg(17),
      S => \^sr\(0)
    );
\clk_period_reg_reg[18]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(18),
      Q => clk_period_reg(18),
      S => \^sr\(0)
    );
\clk_period_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(19),
      Q => clk_period_reg(19),
      R => \^sr\(0)
    );
\clk_period_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(1),
      Q => Q(1),
      R => \^sr\(0)
    );
\clk_period_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(20),
      Q => clk_period_reg(20),
      R => \^sr\(0)
    );
\clk_period_reg_reg[21]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(21),
      Q => clk_period_reg(21),
      S => \^sr\(0)
    );
\clk_period_reg_reg[22]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(22),
      Q => clk_period_reg(22),
      S => \^sr\(0)
    );
\clk_period_reg_reg[23]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(23),
      Q => clk_period_reg(23),
      S => \^sr\(0)
    );
\clk_period_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(24),
      Q => clk_period_reg(24),
      R => \^sr\(0)
    );
\clk_period_reg_reg[25]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(25),
      Q => clk_period_reg(25),
      S => \^sr\(0)
    );
\clk_period_reg_reg[26]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(26),
      Q => clk_period_reg(26),
      S => \^sr\(0)
    );
\clk_period_reg_reg[27]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(27),
      Q => clk_period_reg(27),
      S => \^sr\(0)
    );
\clk_period_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(28),
      Q => clk_period_reg(28),
      R => \^sr\(0)
    );
\clk_period_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(29),
      Q => clk_period_reg(29),
      R => \^sr\(0)
    );
\clk_period_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(2),
      Q => clk_period_reg(2),
      R => \^sr\(0)
    );
\clk_period_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(30),
      Q => clk_period_reg(30),
      R => \^sr\(0)
    );
\clk_period_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(31),
      Q => clk_period_reg(31),
      R => \^sr\(0)
    );
\clk_period_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(3),
      Q => clk_period_reg(3),
      R => \^sr\(0)
    );
\clk_period_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(4),
      Q => Q(2),
      R => \^sr\(0)
    );
\clk_period_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(5),
      Q => clk_period_reg(5),
      R => \^sr\(0)
    );
\clk_period_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(6),
      Q => clk_period_reg(6),
      R => \^sr\(0)
    );
\clk_period_reg_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(7),
      Q => clk_period_reg(7),
      S => \^sr\(0)
    );
\clk_period_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(8),
      Q => clk_period_reg(8),
      R => \^sr\(0)
    );
\clk_period_reg_reg[9]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \clk_period_reg_reg[31]_0\(0),
      D => s_axi_wdata(9),
      Q => clk_period_reg(9),
      S => \^sr\(0)
    );
s_axi_awready_reg_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => s_axi_areset_n,
      O => \^sr\(0)
    );
\scaling_factor[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_wdata(0),
      I1 => \scaling_factor_reg[1]_0\,
      I2 => scaling_factor(0),
      O => \scaling_factor[0]_i_1_n_0\
    );
\scaling_factor[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => s_axi_wdata(1),
      I1 => \scaling_factor_reg[1]_0\,
      I2 => scaling_factor(1),
      O => \scaling_factor[1]_i_1_n_0\
    );
\scaling_factor_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \scaling_factor[0]_i_1_n_0\,
      Q => scaling_factor(0),
      R => \^sr\(0)
    );
\scaling_factor_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \scaling_factor[1]_i_1_n_0\,
      Q => scaling_factor(1),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(0),
      Q => scratchpad_reg(0),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[10]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(10),
      Q => scratchpad_reg(10),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[11]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(11),
      Q => scratchpad_reg(11),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[12]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(12),
      Q => scratchpad_reg(12),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(13),
      Q => scratchpad_reg(13),
      R => \^sr\(0)
    );
\scratchpad_reg_reg[14]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(14),
      Q => scratchpad_reg(14),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[15]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(15),
      Q => scratchpad_reg(15),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[16]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(16),
      Q => scratchpad_reg(16),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(17),
      Q => scratchpad_reg(17),
      R => \^sr\(0)
    );
\scratchpad_reg_reg[18]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(18),
      Q => scratchpad_reg(18),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[19]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(19),
      Q => scratchpad_reg(19),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(1),
      Q => \scratchpad_reg_reg[4]_0\(0),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(20),
      Q => scratchpad_reg(20),
      R => \^sr\(0)
    );
\scratchpad_reg_reg[21]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(21),
      Q => scratchpad_reg(21),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(22),
      Q => scratchpad_reg(22),
      R => \^sr\(0)
    );
\scratchpad_reg_reg[23]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(23),
      Q => scratchpad_reg(23),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(24),
      Q => scratchpad_reg(24),
      R => \^sr\(0)
    );
\scratchpad_reg_reg[25]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(25),
      Q => scratchpad_reg(25),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[26]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(26),
      Q => scratchpad_reg(26),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[27]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(27),
      Q => scratchpad_reg(27),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[28]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(28),
      Q => scratchpad_reg(28),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(29),
      Q => scratchpad_reg(29),
      R => \^sr\(0)
    );
\scratchpad_reg_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(2),
      Q => scratchpad_reg(2),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[30]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(30),
      Q => scratchpad_reg(30),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[31]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(31),
      Q => scratchpad_reg(31),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(3),
      Q => scratchpad_reg(3),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(4),
      Q => \scratchpad_reg_reg[4]_0\(1),
      R => \^sr\(0)
    );
\scratchpad_reg_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(5),
      Q => scratchpad_reg(5),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(6),
      Q => scratchpad_reg(6),
      R => \^sr\(0)
    );
\scratchpad_reg_reg[7]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(7),
      Q => scratchpad_reg(7),
      S => \^sr\(0)
    );
\scratchpad_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(8),
      Q => scratchpad_reg(8),
      R => \^sr\(0)
    );
\scratchpad_reg_reg[9]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => E(0),
      D => s_axi_wdata(9),
      Q => scratchpad_reg(9),
      S => \^sr\(0)
    );
\spad_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(0),
      Q => \^spad_reg\(0),
      R => \^sr\(0)
    );
\spad_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(10),
      Q => \^spad_reg\(10),
      R => \^sr\(0)
    );
\spad_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(11),
      Q => \^spad_reg\(11),
      R => \^sr\(0)
    );
\spad_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(12),
      Q => \^spad_reg\(12),
      R => \^sr\(0)
    );
\spad_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(13),
      Q => \^spad_reg\(13),
      R => \^sr\(0)
    );
\spad_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(14),
      Q => \^spad_reg\(14),
      R => \^sr\(0)
    );
\spad_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(15),
      Q => \^spad_reg\(15),
      R => \^sr\(0)
    );
\spad_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(16),
      Q => \^spad_reg\(16),
      R => \^sr\(0)
    );
\spad_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(17),
      Q => \^spad_reg\(17),
      R => \^sr\(0)
    );
\spad_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(18),
      Q => \^spad_reg\(18),
      R => \^sr\(0)
    );
\spad_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(19),
      Q => \^spad_reg\(19),
      R => \^sr\(0)
    );
\spad_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(1),
      Q => \^spad_reg\(1),
      R => \^sr\(0)
    );
\spad_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(20),
      Q => \^spad_reg\(20),
      R => \^sr\(0)
    );
\spad_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(21),
      Q => \^spad_reg\(21),
      R => \^sr\(0)
    );
\spad_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(22),
      Q => \^spad_reg\(22),
      R => \^sr\(0)
    );
\spad_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(23),
      Q => \^spad_reg\(23),
      R => \^sr\(0)
    );
\spad_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(24),
      Q => \^spad_reg\(24),
      R => \^sr\(0)
    );
\spad_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(25),
      Q => \^spad_reg\(25),
      R => \^sr\(0)
    );
\spad_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(26),
      Q => \^spad_reg\(26),
      R => \^sr\(0)
    );
\spad_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(27),
      Q => \^spad_reg\(27),
      R => \^sr\(0)
    );
\spad_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(28),
      Q => \^spad_reg\(28),
      R => \^sr\(0)
    );
\spad_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(29),
      Q => \^spad_reg\(29),
      R => \^sr\(0)
    );
\spad_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(2),
      Q => \^spad_reg\(2),
      R => \^sr\(0)
    );
\spad_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(30),
      Q => \^spad_reg\(30),
      R => \^sr\(0)
    );
\spad_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(31),
      Q => \^spad_reg\(31),
      R => \^sr\(0)
    );
\spad_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(3),
      Q => \^spad_reg\(3),
      R => \^sr\(0)
    );
\spad_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(4),
      Q => \^spad_reg\(4),
      R => \^sr\(0)
    );
\spad_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(5),
      Q => \^spad_reg\(5),
      R => \^sr\(0)
    );
\spad_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(6),
      Q => \^spad_reg\(6),
      R => \^sr\(0)
    );
\spad_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(7),
      Q => \^spad_reg\(7),
      R => \^sr\(0)
    );
\spad_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(8),
      Q => \^spad_reg\(8),
      R => \^sr\(0)
    );
\spad_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_reg_reg[31]_0\(0),
      D => s_axi_wdata(9),
      Q => \^spad_reg\(9),
      R => \^sr\(0)
    );
\spad_val_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(0),
      Q => \^spad_val\(0),
      S => \^sr\(0)
    );
\spad_val_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(10),
      Q => \^spad_val\(10),
      R => \^sr\(0)
    );
\spad_val_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(11),
      Q => \^spad_val\(11),
      R => \^sr\(0)
    );
\spad_val_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(12),
      Q => \^spad_val\(12),
      R => \^sr\(0)
    );
\spad_val_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(13),
      Q => \^spad_val\(13),
      R => \^sr\(0)
    );
\spad_val_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(14),
      Q => \^spad_val\(14),
      R => \^sr\(0)
    );
\spad_val_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(15),
      Q => \^spad_val\(15),
      R => \^sr\(0)
    );
\spad_val_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(16),
      Q => \^spad_val\(16),
      R => \^sr\(0)
    );
\spad_val_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(17),
      Q => \^spad_val\(17),
      R => \^sr\(0)
    );
\spad_val_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(18),
      Q => \^spad_val\(18),
      R => \^sr\(0)
    );
\spad_val_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(19),
      Q => \^spad_val\(19),
      R => \^sr\(0)
    );
\spad_val_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(1),
      Q => \^spad_val\(1),
      S => \^sr\(0)
    );
\spad_val_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(20),
      Q => \^spad_val\(20),
      R => \^sr\(0)
    );
\spad_val_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(21),
      Q => \^spad_val\(21),
      R => \^sr\(0)
    );
\spad_val_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(22),
      Q => \^spad_val\(22),
      R => \^sr\(0)
    );
\spad_val_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(23),
      Q => \^spad_val\(23),
      R => \^sr\(0)
    );
\spad_val_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(24),
      Q => \^spad_val\(24),
      R => \^sr\(0)
    );
\spad_val_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(25),
      Q => \^spad_val\(25),
      R => \^sr\(0)
    );
\spad_val_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(26),
      Q => \^spad_val\(26),
      R => \^sr\(0)
    );
\spad_val_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(27),
      Q => \^spad_val\(27),
      R => \^sr\(0)
    );
\spad_val_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(28),
      Q => \^spad_val\(28),
      R => \^sr\(0)
    );
\spad_val_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(29),
      Q => \^spad_val\(29),
      R => \^sr\(0)
    );
\spad_val_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(2),
      Q => \^spad_val\(2),
      S => \^sr\(0)
    );
\spad_val_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(30),
      Q => \^spad_val\(30),
      R => \^sr\(0)
    );
\spad_val_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(31),
      Q => \^spad_val\(31),
      R => \^sr\(0)
    );
\spad_val_reg[3]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(3),
      Q => \^spad_val\(3),
      S => \^sr\(0)
    );
\spad_val_reg[4]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(4),
      Q => \^spad_val\(4),
      S => \^sr\(0)
    );
\spad_val_reg[5]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(5),
      Q => \^spad_val\(5),
      S => \^sr\(0)
    );
\spad_val_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(6),
      Q => \^spad_val\(6),
      R => \^sr\(0)
    );
\spad_val_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(7),
      Q => \^spad_val\(7),
      R => \^sr\(0)
    );
\spad_val_reg[8]\: unisim.vcomponents.FDSE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(8),
      Q => \^spad_val\(8),
      S => \^sr\(0)
    );
\spad_val_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => \spad_val_reg[31]_0\(0),
      D => s_axi_wdata(9),
      Q => \^spad_val\(9),
      R => \^sr\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_user_axilite_control_0_0_slave_attachment is
  port (
    s_axi_awready : out STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 3 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \bus2ip_addr_reg_reg[2]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \bus2ip_addr_reg_reg[3]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \bus2ip_addr_reg_reg[0]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \MEM_DECODE_GEN[0].cs_out_i_reg[0]\ : out STD_LOGIC;
    IP2Bus_Data10_out : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 24 downto 0 );
    \bus2ip_addr_reg_reg[10]_0\ : out STD_LOGIC;
    \bus2ip_addr_reg_reg[3]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_areset_n_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    bus2ip_rnw_reg_reg_0 : out STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_aclk : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    IP2Bus_WrAck : in STD_LOGIC;
    \IP2Bus_Data_reg[4]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    spad_reg : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \IP2Bus_Data_reg[4]_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    spad_val : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_areset_n : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    IP2Bus_RdAck : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    \IP2Bus_Data_reg[30]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[2]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[3]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[5]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[6]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[7]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[8]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[9]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[10]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[11]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[12]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[13]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[14]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[15]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[16]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[17]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[18]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[19]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[20]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[21]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[22]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[23]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[1]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[0]\ : in STD_LOGIC;
    \s_axi_rdata_reg_reg[31]_0\ : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_user_axilite_control_0_0_slave_attachment : entity is "slave_attachment";
end spec7_user_axilite_control_0_0_slave_attachment;

architecture STRUCTURE of spec7_user_axilite_control_0_0_slave_attachment is
  signal Bus2IP_Addr : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal Bus2IP_RNW : STD_LOGIC;
  signal \DATA_PHASE_WDT.I_DPTO_COUNTER_n_0\ : STD_LOGIC;
  signal \DATA_PHASE_WDT.timeout_i\ : STD_LOGIC;
  signal \FSM_sequential_access_cs[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_access_cs[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_access_cs[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_sequential_access_cs[2]_i_2_n_0\ : STD_LOGIC;
  signal \FSM_sequential_access_cs[2]_i_3_n_0\ : STD_LOGIC;
  signal \FSM_sequential_access_cs[2]_i_4_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[0]_i_3_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[0]_i_4_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[1]_i_3_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[1]_i_4_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[30]_i_2_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[31]_i_4_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[31]_i_5_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[31]_i_8_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[31]_i_9_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[4]_i_2_n_0\ : STD_LOGIC;
  signal \IP2Bus_Data[4]_i_3_n_0\ : STD_LOGIC;
  signal I_DECODER_n_0 : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal access_cs : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal axi_avalid : STD_LOGIC;
  signal axi_avalid_reg : STD_LOGIC;
  signal bus2ip_addr_i : STD_LOGIC_VECTOR ( 11 downto 0 );
  signal \bus2ip_addr_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[10]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[11]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[11]_i_3_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[1]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[2]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[3]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[5]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[6]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[7]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \bus2ip_addr_reg[9]_i_2_n_0\ : STD_LOGIC;
  signal \^bus2ip_addr_reg_reg[10]_0\ : STD_LOGIC;
  signal bus2ip_rnw_i : STD_LOGIC;
  signal counter_en_reg : STD_LOGIC;
  signal cs_ce_ld_enable_i : STD_LOGIC;
  signal data_timeout : STD_LOGIC;
  signal lite_arready_out : STD_LOGIC;
  signal s_axi_arready_reg_i_1_n_0 : STD_LOGIC;
  signal s_axi_awready_reg_i_2_n_0 : STD_LOGIC;
  signal \^s_axi_bvalid\ : STD_LOGIC;
  signal s_axi_bvalid_reg_i_1_n_0 : STD_LOGIC;
  signal s_axi_rdata_i1_in : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^s_axi_rvalid\ : STD_LOGIC;
  signal s_axi_rvalid_i : STD_LOGIC;
  signal s_axi_rvalid_reg_i_1_n_0 : STD_LOGIC;
  signal \scaling_factor[1]_i_3_n_0\ : STD_LOGIC;
  signal \scaling_factor[1]_i_4_n_0\ : STD_LOGIC;
  signal \scratchpad_reg[31]_i_3_n_0\ : STD_LOGIC;
  signal \spad_reg[31]_i_2_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_sequential_access_cs[2]_i_3\ : label is "soft_lutpair25";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_sequential_access_cs_reg[0]\ : label is "READ_WAIT:010,WRITING:100,B_VALID:101,BRESP_WAIT:110,READING:001,IDLE:000,WRITE_WAIT:011";
  attribute FSM_ENCODED_STATES of \FSM_sequential_access_cs_reg[1]\ : label is "READ_WAIT:010,WRITING:100,B_VALID:101,BRESP_WAIT:110,READING:001,IDLE:000,WRITE_WAIT:011";
  attribute FSM_ENCODED_STATES of \FSM_sequential_access_cs_reg[2]\ : label is "READ_WAIT:010,WRITING:100,B_VALID:101,BRESP_WAIT:110,READING:001,IDLE:000,WRITE_WAIT:011";
  attribute SOFT_HLUTNM of \IP2Bus_Data[0]_i_3\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \IP2Bus_Data[1]_i_4\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \IP2Bus_Data[30]_i_2\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \IP2Bus_Data[31]_i_4\ : label is "soft_lutpair26";
  attribute SOFT_HLUTNM of \IP2Bus_Data[31]_i_8\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \IP2Bus_Data[31]_i_9\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \bus2ip_addr_reg[10]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \bus2ip_addr_reg[11]_i_3\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of s_axi_arready_reg_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of s_axi_awready_reg_i_2 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[0]_i_1\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[10]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[11]_i_1\ : label is "soft_lutpair15";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[12]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[13]_i_1\ : label is "soft_lutpair16";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[14]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[15]_i_1\ : label is "soft_lutpair17";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[16]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[17]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[18]_i_1\ : label is "soft_lutpair18";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[19]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[1]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[20]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[21]_i_1\ : label is "soft_lutpair20";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[22]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[23]_i_1\ : label is "soft_lutpair21";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[24]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[25]_i_1\ : label is "soft_lutpair22";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[26]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[27]_i_1\ : label is "soft_lutpair19";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[28]_i_1\ : label is "soft_lutpair23";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[29]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[2]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[30]_i_1\ : label is "soft_lutpair24";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[31]_i_2\ : label is "soft_lutpair25";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[3]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[4]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[5]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[6]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[7]_i_1\ : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[8]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \s_axi_rdata_reg[9]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of s_axi_rvalid_reg_i_1 : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \scaling_factor[1]_i_3\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \scratchpad_reg[31]_i_3\ : label is "soft_lutpair7";
begin
  Q(3 downto 0) <= \^q\(3 downto 0);
  \bus2ip_addr_reg_reg[10]_0\ <= \^bus2ip_addr_reg_reg[10]_0\;
  s_axi_bvalid <= \^s_axi_bvalid\;
  s_axi_rvalid <= \^s_axi_rvalid\;
\DATA_PHASE_WDT.I_DPTO_COUNTER\: entity work.spec7_user_axilite_control_0_0_counter_f
     port map (
      \DATA_PHASE_WDT.timeout_i\ => \DATA_PHASE_WDT.timeout_i\,
      IP2Bus_RdAck => IP2Bus_RdAck,
      IP2Bus_WrAck => IP2Bus_WrAck,
      access_cs(2 downto 0) => access_cs(2 downto 0),
      counter_en_reg => counter_en_reg,
      counter_en_reg_reg => \DATA_PHASE_WDT.I_DPTO_COUNTER_n_0\,
      cs_ce_ld_enable_i => cs_ce_ld_enable_i,
      data_timeout => data_timeout,
      s_axi_aclk => s_axi_aclk,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_wvalid => s_axi_wvalid
    );
\DATA_PHASE_WDT.data_timeout_reg\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \DATA_PHASE_WDT.timeout_i\,
      Q => data_timeout,
      R => SR(0)
    );
\FSM_sequential_access_cs[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFF55540000"
    )
        port map (
      I0 => access_cs(1),
      I1 => s_axi_awvalid,
      I2 => s_axi_arvalid,
      I3 => access_cs(2),
      I4 => \FSM_sequential_access_cs[2]_i_2_n_0\,
      I5 => access_cs(0),
      O => \FSM_sequential_access_cs[0]_i_1_n_0\
    );
\FSM_sequential_access_cs[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000FFFFCDCC0000"
    )
        port map (
      I0 => access_cs(2),
      I1 => access_cs(0),
      I2 => s_axi_arvalid,
      I3 => s_axi_awvalid,
      I4 => \FSM_sequential_access_cs[2]_i_2_n_0\,
      I5 => access_cs(1),
      O => \FSM_sequential_access_cs[1]_i_1_n_0\
    );
\FSM_sequential_access_cs[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"3F80"
    )
        port map (
      I0 => access_cs(0),
      I1 => access_cs(1),
      I2 => \FSM_sequential_access_cs[2]_i_2_n_0\,
      I3 => access_cs(2),
      O => \FSM_sequential_access_cs[2]_i_1_n_0\
    );
\FSM_sequential_access_cs[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFCCFBC83F0C3B08"
    )
        port map (
      I0 => \FSM_sequential_access_cs[2]_i_3_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(1),
      I3 => \FSM_sequential_access_cs[2]_i_4_n_0\,
      I4 => access_cs(0),
      I5 => s_axi_bready,
      O => \FSM_sequential_access_cs[2]_i_2_n_0\
    );
\FSM_sequential_access_cs[2]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => data_timeout,
      I1 => IP2Bus_WrAck,
      O => \FSM_sequential_access_cs[2]_i_3_n_0\
    );
\FSM_sequential_access_cs[2]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AFAFAFA0CFCFCFCF"
    )
        port map (
      I0 => s_axi_wvalid,
      I1 => s_axi_rready,
      I2 => access_cs(1),
      I3 => data_timeout,
      I4 => IP2Bus_RdAck,
      I5 => access_cs(0),
      O => \FSM_sequential_access_cs[2]_i_4_n_0\
    );
\FSM_sequential_access_cs_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \FSM_sequential_access_cs[0]_i_1_n_0\,
      Q => access_cs(0),
      R => SR(0)
    );
\FSM_sequential_access_cs_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \FSM_sequential_access_cs[1]_i_1_n_0\,
      Q => access_cs(1),
      R => SR(0)
    );
\FSM_sequential_access_cs_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \FSM_sequential_access_cs[2]_i_1_n_0\,
      Q => access_cs(2),
      R => SR(0)
    );
\IP2Bus_Data[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"A3A0A3A3A0A0A0A0"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(8),
      I2 => \IP2Bus_Data[30]_i_2_n_0\,
      I3 => \IP2Bus_Data_reg[0]\,
      I4 => \IP2Bus_Data[0]_i_3_n_0\,
      I5 => \IP2Bus_Data[0]_i_4_n_0\,
      O => D(0)
    );
\IP2Bus_Data[0]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"15"
    )
        port map (
      I0 => \^q\(0),
      I1 => spad_reg(0),
      I2 => \^q\(3),
      O => \IP2Bus_Data[0]_i_3_n_0\
    );
\IP2Bus_Data[0]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"F808FFFF"
    )
        port map (
      I0 => \IP2Bus_Data_reg[4]_0\(0),
      I1 => \^q\(2),
      I2 => \^q\(3),
      I3 => spad_val(0),
      I4 => \^q\(0),
      O => \IP2Bus_Data[0]_i_4_n_0\
    );
\IP2Bus_Data[10]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[10]\,
      O => D(10)
    );
\IP2Bus_Data[11]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[11]\,
      O => D(11)
    );
\IP2Bus_Data[12]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[12]\,
      O => D(12)
    );
\IP2Bus_Data[13]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[13]\,
      O => D(13)
    );
\IP2Bus_Data[14]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[14]\,
      O => D(14)
    );
\IP2Bus_Data[15]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[15]\,
      O => D(15)
    );
\IP2Bus_Data[16]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[16]\,
      O => D(16)
    );
\IP2Bus_Data[17]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[17]\,
      O => D(17)
    );
\IP2Bus_Data[18]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[18]\,
      O => D(18)
    );
\IP2Bus_Data[19]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[19]\,
      O => D(19)
    );
\IP2Bus_Data[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFEAFFEAFFEA0000"
    )
        port map (
      I0 => \^q\(0),
      I1 => \IP2Bus_Data_reg[4]\(0),
      I2 => \^q\(2),
      I3 => \IP2Bus_Data_reg[1]\,
      I4 => \IP2Bus_Data[1]_i_3_n_0\,
      I5 => \IP2Bus_Data[1]_i_4_n_0\,
      O => D(1)
    );
\IP2Bus_Data[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA808080AAAAAAAA"
    )
        port map (
      I0 => \scaling_factor[1]_i_3_n_0\,
      I1 => spad_val(1),
      I2 => \^q\(3),
      I3 => \IP2Bus_Data_reg[4]_0\(1),
      I4 => \^q\(2),
      I5 => \^q\(0),
      O => \IP2Bus_Data[1]_i_3_n_0\
    );
\IP2Bus_Data[1]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FE00"
    )
        port map (
      I0 => Bus2IP_Addr(10),
      I1 => Bus2IP_Addr(11),
      I2 => Bus2IP_Addr(9),
      I3 => \^q\(0),
      O => \IP2Bus_Data[1]_i_4_n_0\
    );
\IP2Bus_Data[20]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[20]\,
      O => D(20)
    );
\IP2Bus_Data[21]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[21]\,
      O => D(21)
    );
\IP2Bus_Data[22]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[22]\,
      O => D(22)
    );
\IP2Bus_Data[23]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[23]\,
      O => D(23)
    );
\IP2Bus_Data[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[2]\,
      O => D(2)
    );
\IP2Bus_Data[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BBBAAAAA"
    )
        port map (
      I0 => \IP2Bus_Data[30]_i_2_n_0\,
      I1 => Bus2IP_Addr(8),
      I2 => \^q\(2),
      I3 => \^q\(3),
      I4 => \IP2Bus_Data_reg[30]\,
      O => D(24)
    );
\IP2Bus_Data[30]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => Bus2IP_Addr(9),
      I1 => Bus2IP_Addr(11),
      I2 => Bus2IP_Addr(10),
      O => \IP2Bus_Data[30]_i_2_n_0\
    );
\IP2Bus_Data[31]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000000DDDD555D"
    )
        port map (
      I0 => \^q\(1),
      I1 => \IP2Bus_Data[31]_i_4_n_0\,
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => Bus2IP_Addr(8),
      I5 => \IP2Bus_Data[31]_i_5_n_0\,
      O => \bus2ip_addr_reg_reg[3]_1\(0)
    );
\IP2Bus_Data[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(3),
      I1 => Bus2IP_Addr(9),
      O => \IP2Bus_Data[31]_i_4_n_0\
    );
\IP2Bus_Data[31]_i_5\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFF3FFF3FFB3"
    )
        port map (
      I0 => Bus2IP_Addr(8),
      I1 => \IP2Bus_Data[31]_i_8_n_0\,
      I2 => Bus2IP_Addr(9),
      I3 => \IP2Bus_Data[31]_i_9_n_0\,
      I4 => \^q\(2),
      I5 => \^q\(3),
      O => \IP2Bus_Data[31]_i_5_n_0\
    );
\IP2Bus_Data[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFEFEFF"
    )
        port map (
      I0 => Bus2IP_Addr(10),
      I1 => Bus2IP_Addr(11),
      I2 => Bus2IP_Addr(9),
      I3 => \^q\(3),
      I4 => \^q\(2),
      I5 => Bus2IP_Addr(8),
      O => \^bus2ip_addr_reg_reg[10]_0\
    );
\IP2Bus_Data[31]_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => Bus2IP_Addr(6),
      I1 => Bus2IP_Addr(7),
      O => \IP2Bus_Data[31]_i_8_n_0\
    );
\IP2Bus_Data[31]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => Bus2IP_Addr(11),
      I1 => Bus2IP_Addr(10),
      I2 => Bus2IP_Addr(1),
      I3 => Bus2IP_Addr(0),
      O => \IP2Bus_Data[31]_i_9_n_0\
    );
\IP2Bus_Data[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[3]\,
      O => D(3)
    );
\IP2Bus_Data[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A3A0A0"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(8),
      I2 => \IP2Bus_Data[30]_i_2_n_0\,
      I3 => \IP2Bus_Data[4]_i_2_n_0\,
      I4 => \IP2Bus_Data[4]_i_3_n_0\,
      O => D(4)
    );
\IP2Bus_Data[4]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"04550404"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(2),
      I2 => \IP2Bus_Data_reg[4]\(1),
      I3 => spad_reg(1),
      I4 => \^q\(3),
      O => \IP2Bus_Data[4]_i_2_n_0\
    );
\IP2Bus_Data[4]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"5555405540554055"
    )
        port map (
      I0 => \^q\(1),
      I1 => \IP2Bus_Data_reg[4]_0\(2),
      I2 => \^q\(2),
      I3 => \^q\(0),
      I4 => \^q\(3),
      I5 => spad_val(2),
      O => \IP2Bus_Data[4]_i_3_n_0\
    );
\IP2Bus_Data[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[5]\,
      O => D(5)
    );
\IP2Bus_Data[6]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[6]\,
      O => D(6)
    );
\IP2Bus_Data[7]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[7]\,
      O => D(7)
    );
\IP2Bus_Data[8]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[8]\,
      O => D(8)
    );
\IP2Bus_Data[9]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAA8FFFFAAA8AAA8"
    )
        port map (
      I0 => \^q\(0),
      I1 => Bus2IP_Addr(9),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(10),
      I4 => \^bus2ip_addr_reg_reg[10]_0\,
      I5 => \IP2Bus_Data_reg[9]\,
      O => D(9)
    );
I_DECODER: entity work.spec7_user_axilite_control_0_0_address_decoder
     port map (
      Bus2IP_RNW => Bus2IP_RNW,
      E(0) => E(0),
      \FSM_sequential_access_cs_reg[0]\ => I_DECODER_n_0,
      IP2Bus_Data10_out => IP2Bus_Data10_out,
      IP2Bus_RdAck => IP2Bus_RdAck,
      IP2Bus_WrAck => IP2Bus_WrAck,
      \MEM_DECODE_GEN[0].cs_out_i_reg[0]_0\ => \MEM_DECODE_GEN[0].cs_out_i_reg[0]\,
      Q(6 downto 5) => Bus2IP_Addr(7 downto 6),
      Q(4 downto 2) => \^q\(2 downto 0),
      Q(1 downto 0) => Bus2IP_Addr(1 downto 0),
      access_cs(2 downto 0) => access_cs(2 downto 0),
      axi_avalid => axi_avalid,
      axi_avalid_reg => axi_avalid_reg,
      \bus2ip_addr_reg_reg[0]\(0) => \bus2ip_addr_reg_reg[0]_0\(0),
      \bus2ip_addr_reg_reg[2]\(0) => \bus2ip_addr_reg_reg[2]_0\(0),
      \bus2ip_addr_reg_reg[3]\(0) => \bus2ip_addr_reg_reg[3]_0\(0),
      bus2ip_rnw_reg_reg => bus2ip_rnw_reg_reg_0,
      cs_ce_ld_enable_i => cs_ce_ld_enable_i,
      data_timeout => data_timeout,
      s_axi_aclk => s_axi_aclk,
      s_axi_areset_n => s_axi_areset_n,
      s_axi_areset_n_0(0) => s_axi_areset_n_0(0),
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_wvalid => s_axi_wvalid,
      \scaling_factor_reg[1]\ => \scaling_factor[1]_i_3_n_0\,
      \scaling_factor_reg[1]_0\ => \scaling_factor[1]_i_4_n_0\,
      \scratchpad_reg_reg[31]\ => \scratchpad_reg[31]_i_3_n_0\,
      \scratchpad_reg_reg[31]_0\ => \IP2Bus_Data[30]_i_2_n_0\,
      \spad_val_reg[31]\ => \spad_reg[31]_i_2_n_0\
    );
axi_avalid_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => axi_avalid,
      Q => axi_avalid_reg,
      R => SR(0)
    );
\bus2ip_addr_reg[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[0]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => Bus2IP_Addr(0),
      O => bus2ip_addr_i(0)
    );
\bus2ip_addr_reg[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88CC88C0880088C0"
    )
        port map (
      I0 => s_axi_araddr(0),
      I1 => \bus2ip_addr_reg[11]_i_3_n_0\,
      I2 => Bus2IP_Addr(0),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => s_axi_awaddr(0),
      O => \bus2ip_addr_reg[0]_i_2_n_0\
    );
\bus2ip_addr_reg[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[10]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => Bus2IP_Addr(10),
      O => bus2ip_addr_i(10)
    );
\bus2ip_addr_reg[10]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AACCAAF000000000"
    )
        port map (
      I0 => s_axi_araddr(10),
      I1 => s_axi_awaddr(10),
      I2 => Bus2IP_Addr(10),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => \bus2ip_addr_reg[11]_i_3_n_0\,
      O => \bus2ip_addr_reg[10]_i_2_n_0\
    );
\bus2ip_addr_reg[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[11]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => Bus2IP_Addr(11),
      O => bus2ip_addr_i(11)
    );
\bus2ip_addr_reg[11]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCAACCF000000000"
    )
        port map (
      I0 => s_axi_awaddr(11),
      I1 => s_axi_araddr(11),
      I2 => Bus2IP_Addr(11),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => \bus2ip_addr_reg[11]_i_3_n_0\,
      O => \bus2ip_addr_reg[11]_i_2_n_0\
    );
\bus2ip_addr_reg[11]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => access_cs(0),
      I1 => access_cs(1),
      I2 => access_cs(2),
      O => \bus2ip_addr_reg[11]_i_3_n_0\
    );
\bus2ip_addr_reg[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[1]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => Bus2IP_Addr(1),
      O => bus2ip_addr_i(1)
    );
\bus2ip_addr_reg[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AACCAAF000000000"
    )
        port map (
      I0 => s_axi_araddr(1),
      I1 => s_axi_awaddr(1),
      I2 => Bus2IP_Addr(1),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => \bus2ip_addr_reg[11]_i_3_n_0\,
      O => \bus2ip_addr_reg[1]_i_2_n_0\
    );
\bus2ip_addr_reg[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[2]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => \^q\(0),
      O => bus2ip_addr_i(2)
    );
\bus2ip_addr_reg[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"CCAACCF000000000"
    )
        port map (
      I0 => s_axi_awaddr(2),
      I1 => s_axi_araddr(2),
      I2 => \^q\(0),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => \bus2ip_addr_reg[11]_i_3_n_0\,
      O => \bus2ip_addr_reg[2]_i_2_n_0\
    );
\bus2ip_addr_reg[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[3]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => \^q\(1),
      O => bus2ip_addr_i(3)
    );
\bus2ip_addr_reg[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88CC88C0880088C0"
    )
        port map (
      I0 => s_axi_araddr(3),
      I1 => \bus2ip_addr_reg[11]_i_3_n_0\,
      I2 => \^q\(1),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => s_axi_awaddr(3),
      O => \bus2ip_addr_reg[3]_i_2_n_0\
    );
\bus2ip_addr_reg[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[4]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => \^q\(2),
      O => bus2ip_addr_i(4)
    );
\bus2ip_addr_reg[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88CC88C0880088C0"
    )
        port map (
      I0 => s_axi_araddr(4),
      I1 => \bus2ip_addr_reg[11]_i_3_n_0\,
      I2 => \^q\(2),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => s_axi_awaddr(4),
      O => \bus2ip_addr_reg[4]_i_2_n_0\
    );
\bus2ip_addr_reg[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[5]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => \^q\(3),
      O => bus2ip_addr_i(5)
    );
\bus2ip_addr_reg[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88CC88C0880088C0"
    )
        port map (
      I0 => s_axi_araddr(5),
      I1 => \bus2ip_addr_reg[11]_i_3_n_0\,
      I2 => \^q\(3),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => s_axi_awaddr(5),
      O => \bus2ip_addr_reg[5]_i_2_n_0\
    );
\bus2ip_addr_reg[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[6]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => Bus2IP_Addr(6),
      O => bus2ip_addr_i(6)
    );
\bus2ip_addr_reg[6]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AACCAAF000000000"
    )
        port map (
      I0 => s_axi_araddr(6),
      I1 => s_axi_awaddr(6),
      I2 => Bus2IP_Addr(6),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => \bus2ip_addr_reg[11]_i_3_n_0\,
      O => \bus2ip_addr_reg[6]_i_2_n_0\
    );
\bus2ip_addr_reg[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[7]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => Bus2IP_Addr(7),
      O => bus2ip_addr_i(7)
    );
\bus2ip_addr_reg[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88CC88C0880088C0"
    )
        port map (
      I0 => s_axi_araddr(7),
      I1 => \bus2ip_addr_reg[11]_i_3_n_0\,
      I2 => Bus2IP_Addr(7),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => s_axi_awaddr(7),
      O => \bus2ip_addr_reg[7]_i_2_n_0\
    );
\bus2ip_addr_reg[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[8]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => Bus2IP_Addr(8),
      O => bus2ip_addr_i(8)
    );
\bus2ip_addr_reg[8]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"88CC88C0880088C0"
    )
        port map (
      I0 => s_axi_araddr(8),
      I1 => \bus2ip_addr_reg[11]_i_3_n_0\,
      I2 => Bus2IP_Addr(8),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => s_axi_awaddr(8),
      O => \bus2ip_addr_reg[8]_i_2_n_0\
    );
\bus2ip_addr_reg[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BFFEAAAA"
    )
        port map (
      I0 => \bus2ip_addr_reg[9]_i_2_n_0\,
      I1 => access_cs(2),
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => Bus2IP_Addr(9),
      O => bus2ip_addr_i(9)
    );
\bus2ip_addr_reg[9]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AACCAAF000000000"
    )
        port map (
      I0 => s_axi_araddr(9),
      I1 => s_axi_awaddr(9),
      I2 => Bus2IP_Addr(9),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      I5 => \bus2ip_addr_reg[11]_i_3_n_0\,
      O => \bus2ip_addr_reg[9]_i_2_n_0\
    );
\bus2ip_addr_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(0),
      Q => Bus2IP_Addr(0),
      R => SR(0)
    );
\bus2ip_addr_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(10),
      Q => Bus2IP_Addr(10),
      R => SR(0)
    );
\bus2ip_addr_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(11),
      Q => Bus2IP_Addr(11),
      R => SR(0)
    );
\bus2ip_addr_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(1),
      Q => Bus2IP_Addr(1),
      R => SR(0)
    );
\bus2ip_addr_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(2),
      Q => \^q\(0),
      R => SR(0)
    );
\bus2ip_addr_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(3),
      Q => \^q\(1),
      R => SR(0)
    );
\bus2ip_addr_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(4),
      Q => \^q\(2),
      R => SR(0)
    );
\bus2ip_addr_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(5),
      Q => \^q\(3),
      R => SR(0)
    );
\bus2ip_addr_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(6),
      Q => Bus2IP_Addr(6),
      R => SR(0)
    );
\bus2ip_addr_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(7),
      Q => Bus2IP_Addr(7),
      R => SR(0)
    );
\bus2ip_addr_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(8),
      Q => Bus2IP_Addr(8),
      R => SR(0)
    );
\bus2ip_addr_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_addr_i(9),
      Q => Bus2IP_Addr(9),
      R => SR(0)
    );
bus2ip_rnw_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0FFFFFFB0000000A"
    )
        port map (
      I0 => s_axi_arvalid,
      I1 => s_axi_awvalid,
      I2 => access_cs(0),
      I3 => access_cs(1),
      I4 => access_cs(2),
      I5 => Bus2IP_RNW,
      O => bus2ip_rnw_i
    );
bus2ip_rnw_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => bus2ip_rnw_i,
      Q => Bus2IP_RNW,
      R => SR(0)
    );
counter_en_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => \DATA_PHASE_WDT.I_DPTO_COUNTER_n_0\,
      Q => counter_en_reg,
      R => SR(0)
    );
s_axi_arready_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => lite_arready_out,
      I1 => \^s_axi_rvalid\,
      O => s_axi_arready
    );
s_axi_arready_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"55550001"
    )
        port map (
      I0 => access_cs(0),
      I1 => access_cs(2),
      I2 => s_axi_arvalid,
      I3 => s_axi_awvalid,
      I4 => access_cs(1),
      O => s_axi_arready_reg_i_1_n_0
    );
s_axi_arready_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => s_axi_arready_reg_i_1_n_0,
      Q => lite_arready_out,
      R => SR(0)
    );
s_axi_awready_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => access_cs(2),
      I1 => access_cs(1),
      I2 => access_cs(0),
      I3 => s_axi_arvalid,
      I4 => s_axi_awvalid,
      O => s_axi_awready_reg_i_2_n_0
    );
s_axi_awready_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => s_axi_awready_reg_i_2_n_0,
      Q => s_axi_awready,
      R => SR(0)
    );
s_axi_bvalid_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FF7F2200"
    )
        port map (
      I0 => access_cs(2),
      I1 => access_cs(1),
      I2 => s_axi_bready,
      I3 => access_cs(0),
      I4 => \^s_axi_bvalid\,
      O => s_axi_bvalid_reg_i_1_n_0
    );
s_axi_bvalid_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => s_axi_bvalid_reg_i_1_n_0,
      Q => \^s_axi_bvalid\,
      R => SR(0)
    );
\s_axi_rdata_reg[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(0),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(0)
    );
\s_axi_rdata_reg[10]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(10),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(10)
    );
\s_axi_rdata_reg[11]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(11),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(11)
    );
\s_axi_rdata_reg[12]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(12),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(12)
    );
\s_axi_rdata_reg[13]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(13),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(13)
    );
\s_axi_rdata_reg[14]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(14),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(14)
    );
\s_axi_rdata_reg[15]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(15),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(15)
    );
\s_axi_rdata_reg[16]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(16),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(16)
    );
\s_axi_rdata_reg[17]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(17),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(17)
    );
\s_axi_rdata_reg[18]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(18),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(18)
    );
\s_axi_rdata_reg[19]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(19),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(19)
    );
\s_axi_rdata_reg[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(1),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(1)
    );
\s_axi_rdata_reg[20]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(20),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(20)
    );
\s_axi_rdata_reg[21]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(21),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(21)
    );
\s_axi_rdata_reg[22]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(22),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(22)
    );
\s_axi_rdata_reg[23]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(23),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(23)
    );
\s_axi_rdata_reg[24]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(24),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(24)
    );
\s_axi_rdata_reg[25]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(25),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(25)
    );
\s_axi_rdata_reg[26]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(26),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(26)
    );
\s_axi_rdata_reg[27]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(27),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(27)
    );
\s_axi_rdata_reg[28]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(28),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(28)
    );
\s_axi_rdata_reg[29]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(29),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(29)
    );
\s_axi_rdata_reg[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(2),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(2)
    );
\s_axi_rdata_reg[30]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(30),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(30)
    );
\s_axi_rdata_reg[31]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"00000E0E0000F000"
    )
        port map (
      I0 => IP2Bus_RdAck,
      I1 => data_timeout,
      I2 => access_cs(1),
      I3 => s_axi_rready,
      I4 => access_cs(2),
      I5 => access_cs(0),
      O => s_axi_rvalid_i
    );
\s_axi_rdata_reg[31]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(31),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(31)
    );
\s_axi_rdata_reg[3]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(3),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(3)
    );
\s_axi_rdata_reg[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(4),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(4)
    );
\s_axi_rdata_reg[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(5),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(5)
    );
\s_axi_rdata_reg[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(6),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(6)
    );
\s_axi_rdata_reg[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(7),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(7)
    );
\s_axi_rdata_reg[8]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(8),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(8)
    );
\s_axi_rdata_reg[9]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"02"
    )
        port map (
      I0 => \s_axi_rdata_reg_reg[31]_0\(9),
      I1 => access_cs(1),
      I2 => data_timeout,
      O => s_axi_rdata_i1_in(9)
    );
\s_axi_rdata_reg_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(0),
      Q => s_axi_rdata(0),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(10),
      Q => s_axi_rdata(10),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(11),
      Q => s_axi_rdata(11),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(12),
      Q => s_axi_rdata(12),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(13),
      Q => s_axi_rdata(13),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(14),
      Q => s_axi_rdata(14),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(15),
      Q => s_axi_rdata(15),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(16),
      Q => s_axi_rdata(16),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(17),
      Q => s_axi_rdata(17),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(18),
      Q => s_axi_rdata(18),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(19),
      Q => s_axi_rdata(19),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(1),
      Q => s_axi_rdata(1),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(20),
      Q => s_axi_rdata(20),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(21),
      Q => s_axi_rdata(21),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(22),
      Q => s_axi_rdata(22),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(23),
      Q => s_axi_rdata(23),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(24),
      Q => s_axi_rdata(24),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(25),
      Q => s_axi_rdata(25),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(26),
      Q => s_axi_rdata(26),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(27),
      Q => s_axi_rdata(27),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(28),
      Q => s_axi_rdata(28),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(29),
      Q => s_axi_rdata(29),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(2),
      Q => s_axi_rdata(2),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(30),
      Q => s_axi_rdata(30),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(31),
      Q => s_axi_rdata(31),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(3),
      Q => s_axi_rdata(3),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(4),
      Q => s_axi_rdata(4),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(5),
      Q => s_axi_rdata(5),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(6),
      Q => s_axi_rdata(6),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(7),
      Q => s_axi_rdata(7),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(8),
      Q => s_axi_rdata(8),
      R => SR(0)
    );
\s_axi_rdata_reg_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rdata_i1_in(9),
      Q => s_axi_rdata(9),
      R => SR(0)
    );
s_axi_rvalid_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"0E"
    )
        port map (
      I0 => data_timeout,
      I1 => IP2Bus_RdAck,
      I2 => access_cs(1),
      O => s_axi_rvalid_reg_i_1_n_0
    );
s_axi_rvalid_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => s_axi_rvalid_i,
      D => s_axi_rvalid_reg_i_1_n_0,
      Q => \^s_axi_rvalid\,
      R => SR(0)
    );
s_axi_wready_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => s_axi_aclk,
      CE => '1',
      D => I_DECODER_n_0,
      Q => s_axi_wready,
      R => SR(0)
    );
\scaling_factor[1]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => Bus2IP_Addr(8),
      I1 => Bus2IP_Addr(10),
      I2 => Bus2IP_Addr(11),
      I3 => Bus2IP_Addr(9),
      O => \scaling_factor[1]_i_3_n_0\
    );
\scaling_factor[1]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFEF"
    )
        port map (
      I0 => Bus2IP_Addr(1),
      I1 => Bus2IP_Addr(0),
      I2 => \^q\(1),
      I3 => \^q\(0),
      I4 => \^q\(3),
      I5 => \^q\(2),
      O => \scaling_factor[1]_i_4_n_0\
    );
\scratchpad_reg[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => Bus2IP_Addr(7),
      I1 => Bus2IP_Addr(6),
      I2 => Bus2IP_Addr(8),
      I3 => \^q\(3),
      O => \scratchpad_reg[31]_i_3_n_0\
    );
\spad_reg[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000010"
    )
        port map (
      I0 => \IP2Bus_Data[30]_i_2_n_0\,
      I1 => Bus2IP_Addr(8),
      I2 => \^q\(3),
      I3 => \^q\(2),
      I4 => Bus2IP_Addr(1),
      O => \spad_reg[31]_i_2_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_user_axilite_control_0_0_axi_lite_ipif is
  port (
    s_axi_awready : out STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_rvalid : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 3 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    \bus2ip_addr_reg_reg[2]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \bus2ip_addr_reg_reg[3]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \bus2ip_addr_reg_reg[0]\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    \MEM_DECODE_GEN[0].cs_out_i_reg[0]\ : out STD_LOGIC;
    IP2Bus_Data10_out : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 24 downto 0 );
    \bus2ip_addr_reg_reg[10]\ : out STD_LOGIC;
    \bus2ip_addr_reg_reg[3]_0\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_areset_n_0 : out STD_LOGIC_VECTOR ( 0 to 0 );
    bus2ip_rnw_reg_reg : out STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    s_axi_aclk : in STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    IP2Bus_WrAck : in STD_LOGIC;
    \IP2Bus_Data_reg[4]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    spad_reg : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \IP2Bus_Data_reg[4]_0\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    spad_val : in STD_LOGIC_VECTOR ( 2 downto 0 );
    s_axi_areset_n : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    IP2Bus_RdAck : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC;
    \IP2Bus_Data_reg[30]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[2]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[3]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[5]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[6]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[7]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[8]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[9]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[10]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[11]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[12]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[13]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[14]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[15]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[16]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[17]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[18]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[19]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[20]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[21]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[22]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[23]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[1]\ : in STD_LOGIC;
    \IP2Bus_Data_reg[0]\ : in STD_LOGIC;
    \s_axi_rdata_reg_reg[31]\ : in STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_user_axilite_control_0_0_axi_lite_ipif : entity is "axi_lite_ipif";
end spec7_user_axilite_control_0_0_axi_lite_ipif;

architecture STRUCTURE of spec7_user_axilite_control_0_0_axi_lite_ipif is
begin
I_SLAVE_ATTACHMENT: entity work.spec7_user_axilite_control_0_0_slave_attachment
     port map (
      D(24 downto 0) => D(24 downto 0),
      E(0) => E(0),
      IP2Bus_Data10_out => IP2Bus_Data10_out,
      \IP2Bus_Data_reg[0]\ => \IP2Bus_Data_reg[0]\,
      \IP2Bus_Data_reg[10]\ => \IP2Bus_Data_reg[10]\,
      \IP2Bus_Data_reg[11]\ => \IP2Bus_Data_reg[11]\,
      \IP2Bus_Data_reg[12]\ => \IP2Bus_Data_reg[12]\,
      \IP2Bus_Data_reg[13]\ => \IP2Bus_Data_reg[13]\,
      \IP2Bus_Data_reg[14]\ => \IP2Bus_Data_reg[14]\,
      \IP2Bus_Data_reg[15]\ => \IP2Bus_Data_reg[15]\,
      \IP2Bus_Data_reg[16]\ => \IP2Bus_Data_reg[16]\,
      \IP2Bus_Data_reg[17]\ => \IP2Bus_Data_reg[17]\,
      \IP2Bus_Data_reg[18]\ => \IP2Bus_Data_reg[18]\,
      \IP2Bus_Data_reg[19]\ => \IP2Bus_Data_reg[19]\,
      \IP2Bus_Data_reg[1]\ => \IP2Bus_Data_reg[1]\,
      \IP2Bus_Data_reg[20]\ => \IP2Bus_Data_reg[20]\,
      \IP2Bus_Data_reg[21]\ => \IP2Bus_Data_reg[21]\,
      \IP2Bus_Data_reg[22]\ => \IP2Bus_Data_reg[22]\,
      \IP2Bus_Data_reg[23]\ => \IP2Bus_Data_reg[23]\,
      \IP2Bus_Data_reg[2]\ => \IP2Bus_Data_reg[2]\,
      \IP2Bus_Data_reg[30]\ => \IP2Bus_Data_reg[30]\,
      \IP2Bus_Data_reg[3]\ => \IP2Bus_Data_reg[3]\,
      \IP2Bus_Data_reg[4]\(1 downto 0) => \IP2Bus_Data_reg[4]\(1 downto 0),
      \IP2Bus_Data_reg[4]_0\(2 downto 0) => \IP2Bus_Data_reg[4]_0\(2 downto 0),
      \IP2Bus_Data_reg[5]\ => \IP2Bus_Data_reg[5]\,
      \IP2Bus_Data_reg[6]\ => \IP2Bus_Data_reg[6]\,
      \IP2Bus_Data_reg[7]\ => \IP2Bus_Data_reg[7]\,
      \IP2Bus_Data_reg[8]\ => \IP2Bus_Data_reg[8]\,
      \IP2Bus_Data_reg[9]\ => \IP2Bus_Data_reg[9]\,
      IP2Bus_RdAck => IP2Bus_RdAck,
      IP2Bus_WrAck => IP2Bus_WrAck,
      \MEM_DECODE_GEN[0].cs_out_i_reg[0]\ => \MEM_DECODE_GEN[0].cs_out_i_reg[0]\,
      Q(3 downto 0) => Q(3 downto 0),
      SR(0) => SR(0),
      \bus2ip_addr_reg_reg[0]_0\(0) => \bus2ip_addr_reg_reg[0]\(0),
      \bus2ip_addr_reg_reg[10]_0\ => \bus2ip_addr_reg_reg[10]\,
      \bus2ip_addr_reg_reg[2]_0\(0) => \bus2ip_addr_reg_reg[2]\(0),
      \bus2ip_addr_reg_reg[3]_0\(0) => \bus2ip_addr_reg_reg[3]\(0),
      \bus2ip_addr_reg_reg[3]_1\(0) => \bus2ip_addr_reg_reg[3]_0\(0),
      bus2ip_rnw_reg_reg_0 => bus2ip_rnw_reg_reg,
      s_axi_aclk => s_axi_aclk,
      s_axi_araddr(11 downto 0) => s_axi_araddr(11 downto 0),
      s_axi_areset_n => s_axi_areset_n,
      s_axi_areset_n_0(0) => s_axi_areset_n_0(0),
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(11 downto 0) => s_axi_awaddr(11 downto 0),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(31 downto 0) => s_axi_rdata(31 downto 0),
      \s_axi_rdata_reg_reg[31]_0\(31 downto 0) => \s_axi_rdata_reg_reg[31]\(31 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wready => s_axi_wready,
      s_axi_wvalid => s_axi_wvalid,
      spad_reg(1 downto 0) => spad_reg(1 downto 0),
      spad_val(2 downto 0) => spad_val(2 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_user_axilite_control_0_0_user_axilite_control is
  port (
    spad_reg : out STD_LOGIC_VECTOR ( 31 downto 0 );
    spad_val : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awready : out STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_bvalid : out STD_LOGIC;
    s_axi_arvalid : in STD_LOGIC;
    s_axi_awvalid : in STD_LOGIC;
    s_axi_aclk : in STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_areset_n : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 11 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_rready : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of spec7_user_axilite_control_0_0_user_axilite_control : entity is "user_axilite_control";
end spec7_user_axilite_control_0_0_user_axilite_control;

architecture STRUCTURE of spec7_user_axilite_control_0_0_user_axilite_control is
  signal Bus2IP_Addr : STD_LOGIC_VECTOR ( 5 downto 2 );
  signal IP2Bus_Data : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal IP2Bus_Data10_out : STD_LOGIC;
  signal IP2Bus_RdAck : STD_LOGIC;
  signal IP2Bus_WrAck : STD_LOGIC;
  signal axi_lite_ipif_inst_n_10 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_11 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_12 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_14 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_15 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_16 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_17 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_18 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_19 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_20 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_21 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_22 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_23 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_24 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_25 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_26 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_27 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_28 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_29 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_30 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_31 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_32 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_33 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_34 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_35 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_36 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_37 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_38 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_39 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_40 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_41 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_42 : STD_LOGIC;
  signal axi_lite_ipif_inst_n_8 : STD_LOGIC;
  signal clk_period_reg : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal reg_common_i_n_1 : STD_LOGIC;
  signal reg_common_i_n_72 : STD_LOGIC;
  signal reg_common_i_n_73 : STD_LOGIC;
  signal reg_common_i_n_74 : STD_LOGIC;
  signal reg_common_i_n_75 : STD_LOGIC;
  signal reg_common_i_n_76 : STD_LOGIC;
  signal reg_common_i_n_77 : STD_LOGIC;
  signal reg_common_i_n_78 : STD_LOGIC;
  signal reg_common_i_n_79 : STD_LOGIC;
  signal reg_common_i_n_80 : STD_LOGIC;
  signal reg_common_i_n_81 : STD_LOGIC;
  signal reg_common_i_n_82 : STD_LOGIC;
  signal reg_common_i_n_83 : STD_LOGIC;
  signal reg_common_i_n_84 : STD_LOGIC;
  signal reg_common_i_n_85 : STD_LOGIC;
  signal reg_common_i_n_86 : STD_LOGIC;
  signal reg_common_i_n_87 : STD_LOGIC;
  signal reg_common_i_n_88 : STD_LOGIC;
  signal reg_common_i_n_89 : STD_LOGIC;
  signal reg_common_i_n_90 : STD_LOGIC;
  signal reg_common_i_n_91 : STD_LOGIC;
  signal reg_common_i_n_92 : STD_LOGIC;
  signal reg_common_i_n_93 : STD_LOGIC;
  signal reg_common_i_n_94 : STD_LOGIC;
  signal reg_common_i_n_95 : STD_LOGIC;
  signal scratchpad_reg : STD_LOGIC_VECTOR ( 4 downto 1 );
  signal scratchpad_reg_0 : STD_LOGIC;
  signal \^spad_reg\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \^spad_val\ : STD_LOGIC_VECTOR ( 31 downto 0 );
begin
  spad_reg(31 downto 0) <= \^spad_reg\(31 downto 0);
  spad_val(31 downto 0) <= \^spad_val\(31 downto 0);
axi_lite_ipif_inst: entity work.spec7_user_axilite_control_0_0_axi_lite_ipif
     port map (
      D(24) => axi_lite_ipif_inst_n_14,
      D(23) => axi_lite_ipif_inst_n_15,
      D(22) => axi_lite_ipif_inst_n_16,
      D(21) => axi_lite_ipif_inst_n_17,
      D(20) => axi_lite_ipif_inst_n_18,
      D(19) => axi_lite_ipif_inst_n_19,
      D(18) => axi_lite_ipif_inst_n_20,
      D(17) => axi_lite_ipif_inst_n_21,
      D(16) => axi_lite_ipif_inst_n_22,
      D(15) => axi_lite_ipif_inst_n_23,
      D(14) => axi_lite_ipif_inst_n_24,
      D(13) => axi_lite_ipif_inst_n_25,
      D(12) => axi_lite_ipif_inst_n_26,
      D(11) => axi_lite_ipif_inst_n_27,
      D(10) => axi_lite_ipif_inst_n_28,
      D(9) => axi_lite_ipif_inst_n_29,
      D(8) => axi_lite_ipif_inst_n_30,
      D(7) => axi_lite_ipif_inst_n_31,
      D(6) => axi_lite_ipif_inst_n_32,
      D(5) => axi_lite_ipif_inst_n_33,
      D(4) => axi_lite_ipif_inst_n_34,
      D(3) => axi_lite_ipif_inst_n_35,
      D(2) => axi_lite_ipif_inst_n_36,
      D(1) => axi_lite_ipif_inst_n_37,
      D(0) => axi_lite_ipif_inst_n_38,
      E(0) => axi_lite_ipif_inst_n_8,
      IP2Bus_Data10_out => IP2Bus_Data10_out,
      \IP2Bus_Data_reg[0]\ => reg_common_i_n_95,
      \IP2Bus_Data_reg[10]\ => reg_common_i_n_86,
      \IP2Bus_Data_reg[11]\ => reg_common_i_n_85,
      \IP2Bus_Data_reg[12]\ => reg_common_i_n_84,
      \IP2Bus_Data_reg[13]\ => reg_common_i_n_83,
      \IP2Bus_Data_reg[14]\ => reg_common_i_n_82,
      \IP2Bus_Data_reg[15]\ => reg_common_i_n_81,
      \IP2Bus_Data_reg[16]\ => reg_common_i_n_80,
      \IP2Bus_Data_reg[17]\ => reg_common_i_n_79,
      \IP2Bus_Data_reg[18]\ => reg_common_i_n_78,
      \IP2Bus_Data_reg[19]\ => reg_common_i_n_77,
      \IP2Bus_Data_reg[1]\ => reg_common_i_n_94,
      \IP2Bus_Data_reg[20]\ => reg_common_i_n_76,
      \IP2Bus_Data_reg[21]\ => reg_common_i_n_75,
      \IP2Bus_Data_reg[22]\ => reg_common_i_n_74,
      \IP2Bus_Data_reg[23]\ => reg_common_i_n_73,
      \IP2Bus_Data_reg[2]\ => reg_common_i_n_93,
      \IP2Bus_Data_reg[30]\ => reg_common_i_n_72,
      \IP2Bus_Data_reg[3]\ => reg_common_i_n_92,
      \IP2Bus_Data_reg[4]\(1) => scratchpad_reg(4),
      \IP2Bus_Data_reg[4]\(0) => scratchpad_reg(1),
      \IP2Bus_Data_reg[4]_0\(2) => clk_period_reg(4),
      \IP2Bus_Data_reg[4]_0\(1 downto 0) => clk_period_reg(1 downto 0),
      \IP2Bus_Data_reg[5]\ => reg_common_i_n_91,
      \IP2Bus_Data_reg[6]\ => reg_common_i_n_90,
      \IP2Bus_Data_reg[7]\ => reg_common_i_n_89,
      \IP2Bus_Data_reg[8]\ => reg_common_i_n_88,
      \IP2Bus_Data_reg[9]\ => reg_common_i_n_87,
      IP2Bus_RdAck => IP2Bus_RdAck,
      IP2Bus_WrAck => IP2Bus_WrAck,
      \MEM_DECODE_GEN[0].cs_out_i_reg[0]\ => axi_lite_ipif_inst_n_12,
      Q(3 downto 0) => Bus2IP_Addr(5 downto 2),
      SR(0) => reg_common_i_n_1,
      \bus2ip_addr_reg_reg[0]\(0) => axi_lite_ipif_inst_n_11,
      \bus2ip_addr_reg_reg[10]\ => axi_lite_ipif_inst_n_39,
      \bus2ip_addr_reg_reg[2]\(0) => scratchpad_reg_0,
      \bus2ip_addr_reg_reg[3]\(0) => axi_lite_ipif_inst_n_10,
      \bus2ip_addr_reg_reg[3]_0\(0) => axi_lite_ipif_inst_n_40,
      bus2ip_rnw_reg_reg => axi_lite_ipif_inst_n_42,
      s_axi_aclk => s_axi_aclk,
      s_axi_araddr(11 downto 0) => s_axi_araddr(11 downto 0),
      s_axi_areset_n => s_axi_areset_n,
      s_axi_areset_n_0(0) => axi_lite_ipif_inst_n_41,
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(11 downto 0) => s_axi_awaddr(11 downto 0),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(31 downto 0) => s_axi_rdata(31 downto 0),
      \s_axi_rdata_reg_reg[31]\(31 downto 0) => IP2Bus_Data(31 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wready => s_axi_wready,
      s_axi_wvalid => s_axi_wvalid,
      spad_reg(1) => \^spad_reg\(4),
      spad_reg(0) => \^spad_reg\(0),
      spad_val(2) => \^spad_val\(4),
      spad_val(1 downto 0) => \^spad_val\(1 downto 0)
    );
reg_common_i: entity work.spec7_user_axilite_control_0_0_registers_common
     port map (
      D(24) => axi_lite_ipif_inst_n_14,
      D(23) => axi_lite_ipif_inst_n_15,
      D(22) => axi_lite_ipif_inst_n_16,
      D(21) => axi_lite_ipif_inst_n_17,
      D(20) => axi_lite_ipif_inst_n_18,
      D(19) => axi_lite_ipif_inst_n_19,
      D(18) => axi_lite_ipif_inst_n_20,
      D(17) => axi_lite_ipif_inst_n_21,
      D(16) => axi_lite_ipif_inst_n_22,
      D(15) => axi_lite_ipif_inst_n_23,
      D(14) => axi_lite_ipif_inst_n_24,
      D(13) => axi_lite_ipif_inst_n_25,
      D(12) => axi_lite_ipif_inst_n_26,
      D(11) => axi_lite_ipif_inst_n_27,
      D(10) => axi_lite_ipif_inst_n_28,
      D(9) => axi_lite_ipif_inst_n_29,
      D(8) => axi_lite_ipif_inst_n_30,
      D(7) => axi_lite_ipif_inst_n_31,
      D(6) => axi_lite_ipif_inst_n_32,
      D(5) => axi_lite_ipif_inst_n_33,
      D(4) => axi_lite_ipif_inst_n_34,
      D(3) => axi_lite_ipif_inst_n_35,
      D(2) => axi_lite_ipif_inst_n_36,
      D(1) => axi_lite_ipif_inst_n_37,
      D(0) => axi_lite_ipif_inst_n_38,
      E(0) => scratchpad_reg_0,
      IP2Bus_Data10_out => IP2Bus_Data10_out,
      \IP2Bus_Data_reg[1]_0\(3 downto 0) => Bus2IP_Addr(5 downto 2),
      \IP2Bus_Data_reg[24]_0\ => axi_lite_ipif_inst_n_39,
      \IP2Bus_Data_reg[31]_0\(31 downto 0) => IP2Bus_Data(31 downto 0),
      \IP2Bus_Data_reg[31]_1\(0) => axi_lite_ipif_inst_n_41,
      \IP2Bus_Data_reg[31]_2\(0) => axi_lite_ipif_inst_n_40,
      IP2Bus_RdAck => IP2Bus_RdAck,
      IP2Bus_RdAck_reg_0 => axi_lite_ipif_inst_n_42,
      IP2Bus_WrAck => IP2Bus_WrAck,
      Q(2) => clk_period_reg(4),
      Q(1 downto 0) => clk_period_reg(1 downto 0),
      SR(0) => reg_common_i_n_1,
      \clk_period_reg_reg[10]_0\ => reg_common_i_n_86,
      \clk_period_reg_reg[11]_0\ => reg_common_i_n_85,
      \clk_period_reg_reg[12]_0\ => reg_common_i_n_84,
      \clk_period_reg_reg[13]_0\ => reg_common_i_n_83,
      \clk_period_reg_reg[14]_0\ => reg_common_i_n_82,
      \clk_period_reg_reg[15]_0\ => reg_common_i_n_81,
      \clk_period_reg_reg[16]_0\ => reg_common_i_n_80,
      \clk_period_reg_reg[17]_0\ => reg_common_i_n_79,
      \clk_period_reg_reg[18]_0\ => reg_common_i_n_78,
      \clk_period_reg_reg[19]_0\ => reg_common_i_n_77,
      \clk_period_reg_reg[20]_0\ => reg_common_i_n_76,
      \clk_period_reg_reg[21]_0\ => reg_common_i_n_75,
      \clk_period_reg_reg[22]_0\ => reg_common_i_n_74,
      \clk_period_reg_reg[23]_0\ => reg_common_i_n_73,
      \clk_period_reg_reg[2]_0\ => reg_common_i_n_93,
      \clk_period_reg_reg[30]_0\ => reg_common_i_n_72,
      \clk_period_reg_reg[31]_0\(0) => axi_lite_ipif_inst_n_8,
      \clk_period_reg_reg[3]_0\ => reg_common_i_n_92,
      \clk_period_reg_reg[5]_0\ => reg_common_i_n_91,
      \clk_period_reg_reg[6]_0\ => reg_common_i_n_90,
      \clk_period_reg_reg[7]_0\ => reg_common_i_n_89,
      \clk_period_reg_reg[8]_0\ => reg_common_i_n_88,
      \clk_period_reg_reg[9]_0\ => reg_common_i_n_87,
      s_axi_aclk => s_axi_aclk,
      s_axi_areset_n => s_axi_areset_n,
      s_axi_wdata(31 downto 0) => s_axi_wdata(31 downto 0),
      \scaling_factor_reg[1]_0\ => axi_lite_ipif_inst_n_12,
      \scratchpad_reg_reg[0]_0\ => reg_common_i_n_95,
      \scratchpad_reg_reg[4]_0\(1) => scratchpad_reg(4),
      \scratchpad_reg_reg[4]_0\(0) => scratchpad_reg(1),
      spad_reg(31 downto 0) => \^spad_reg\(31 downto 0),
      \spad_reg_reg[1]_0\ => reg_common_i_n_94,
      \spad_reg_reg[31]_0\(0) => axi_lite_ipif_inst_n_11,
      spad_val(31 downto 0) => \^spad_val\(31 downto 0),
      \spad_val_reg[31]_0\(0) => axi_lite_ipif_inst_n_10
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity spec7_user_axilite_control_0_0 is
  port (
    s_axi_aclk : in STD_LOGIC;
    s_axi_areset_n : in STD_LOGIC;
    s_axi_awaddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_awvalid : in STD_LOGIC;
    s_axi_awready : out STD_LOGIC;
    s_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    s_axi_wvalid : in STD_LOGIC;
    s_axi_wready : out STD_LOGIC;
    s_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_bvalid : out STD_LOGIC;
    s_axi_bready : in STD_LOGIC;
    s_axi_araddr : in STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_arvalid : in STD_LOGIC;
    s_axi_arready : out STD_LOGIC;
    s_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    s_axi_rvalid : out STD_LOGIC;
    s_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    s_axi_rready : in STD_LOGIC;
    spad_reg : out STD_LOGIC_VECTOR ( 31 downto 0 );
    spad_val : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of spec7_user_axilite_control_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of spec7_user_axilite_control_0_0 : entity is "spec7_user_axilite_control_0_0,user_axilite_control,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of spec7_user_axilite_control_0_0 : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of spec7_user_axilite_control_0_0 : entity is "user_axilite_control,Vivado 2019.2";
end spec7_user_axilite_control_0_0;

architecture STRUCTURE of spec7_user_axilite_control_0_0 is
  signal \<const0>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s_axi_aclk : signal is "xilinx.com:signal:clock:1.0 s_axi_signal_clock CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s_axi_aclk : signal is "XIL_INTERFACENAME s_axi_signal_clock, ASSOCIATED_BUSIF s_axi, FREQ_HZ 250000000, PHASE 0.000, CLK_DOMAIN spec7_ps7_0_FCLK_CLK0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s_axi_arready : signal is "xilinx.com:interface:aximm:1.0 s_axi ARREADY";
  attribute X_INTERFACE_INFO of s_axi_arvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi ARVALID";
  attribute X_INTERFACE_INFO of s_axi_awready : signal is "xilinx.com:interface:aximm:1.0 s_axi AWREADY";
  attribute X_INTERFACE_INFO of s_axi_awvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi AWVALID";
  attribute X_INTERFACE_INFO of s_axi_bready : signal is "xilinx.com:interface:aximm:1.0 s_axi BREADY";
  attribute X_INTERFACE_INFO of s_axi_bvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi BVALID";
  attribute X_INTERFACE_INFO of s_axi_rready : signal is "xilinx.com:interface:aximm:1.0 s_axi RREADY";
  attribute X_INTERFACE_PARAMETER of s_axi_rready : signal is "XIL_INTERFACENAME s_axi, DATA_WIDTH 32, PROTOCOL AXI4LITE, FREQ_HZ 250000000, ID_WIDTH 0, ADDR_WIDTH 32, AWUSER_WIDTH 0, ARUSER_WIDTH 0, WUSER_WIDTH 0, RUSER_WIDTH 0, BUSER_WIDTH 0, READ_WRITE_MODE READ_WRITE, HAS_BURST 0, HAS_LOCK 0, HAS_PROT 0, HAS_CACHE 0, HAS_QOS 0, HAS_REGION 0, HAS_WSTRB 1, HAS_BRESP 1, HAS_RRESP 1, SUPPORTS_NARROW_BURST 0, NUM_READ_OUTSTANDING 1, NUM_WRITE_OUTSTANDING 1, MAX_BURST_LENGTH 1, PHASE 0.000, CLK_DOMAIN spec7_ps7_0_FCLK_CLK0, NUM_READ_THREADS 1, NUM_WRITE_THREADS 1, RUSER_BITS_PER_BYTE 0, WUSER_BITS_PER_BYTE 0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of s_axi_rvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi RVALID";
  attribute X_INTERFACE_INFO of s_axi_wready : signal is "xilinx.com:interface:aximm:1.0 s_axi WREADY";
  attribute X_INTERFACE_INFO of s_axi_wvalid : signal is "xilinx.com:interface:aximm:1.0 s_axi WVALID";
  attribute X_INTERFACE_INFO of s_axi_araddr : signal is "xilinx.com:interface:aximm:1.0 s_axi ARADDR";
  attribute X_INTERFACE_INFO of s_axi_awaddr : signal is "xilinx.com:interface:aximm:1.0 s_axi AWADDR";
  attribute X_INTERFACE_INFO of s_axi_bresp : signal is "xilinx.com:interface:aximm:1.0 s_axi BRESP";
  attribute X_INTERFACE_INFO of s_axi_rdata : signal is "xilinx.com:interface:aximm:1.0 s_axi RDATA";
  attribute X_INTERFACE_INFO of s_axi_rresp : signal is "xilinx.com:interface:aximm:1.0 s_axi RRESP";
  attribute X_INTERFACE_INFO of s_axi_wdata : signal is "xilinx.com:interface:aximm:1.0 s_axi WDATA";
  attribute X_INTERFACE_INFO of s_axi_wstrb : signal is "xilinx.com:interface:aximm:1.0 s_axi WSTRB";
begin
  s_axi_bresp(1) <= \<const0>\;
  s_axi_bresp(0) <= \<const0>\;
  s_axi_rresp(1) <= \<const0>\;
  s_axi_rresp(0) <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.spec7_user_axilite_control_0_0_user_axilite_control
     port map (
      s_axi_aclk => s_axi_aclk,
      s_axi_araddr(11 downto 0) => s_axi_araddr(11 downto 0),
      s_axi_areset_n => s_axi_areset_n,
      s_axi_arready => s_axi_arready,
      s_axi_arvalid => s_axi_arvalid,
      s_axi_awaddr(11 downto 0) => s_axi_awaddr(11 downto 0),
      s_axi_awready => s_axi_awready,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_bready => s_axi_bready,
      s_axi_bvalid => s_axi_bvalid,
      s_axi_rdata(31 downto 0) => s_axi_rdata(31 downto 0),
      s_axi_rready => s_axi_rready,
      s_axi_rvalid => s_axi_rvalid,
      s_axi_wdata(31 downto 0) => s_axi_wdata(31 downto 0),
      s_axi_wready => s_axi_wready,
      s_axi_wvalid => s_axi_wvalid,
      spad_reg(31 downto 0) => spad_reg(31 downto 0),
      spad_val(31 downto 0) => spad_val(31 downto 0)
    );
end STRUCTURE;
