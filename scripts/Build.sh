#!/bin/bash

SW_DIR=../sw

cd $SW_DIR

for dir in $(ls  $SW_DIR)
do
	make -C  $dir
done

OUTPUT_DIR=../output
cd $OUTPUT_DIR

echo "the_ROM_image:" > $OUTPUT_DIR/spec7.bif
echo "{" >> $OUTPUT_DIR/spec7.bif
echo "[bootloader] zynq_fsbl.elf" >> $OUTPUT_DIR/spec7.bif
echo "spec7_wrapper.bit" >> $OUTPUT_DIR/spec7.bif
echo "u-boot.elf" >> $OUTPUT_DIR/spec7.bif
echo "}" >> $OUTPUT_DIR/spec7.bif

bootgen -image spec7.bif -arch zynq -o BOOT.bin -w
